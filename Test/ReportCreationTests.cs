﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Domain;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class ReportCreationTests
    {
        public ReportCreationTests()
        {
            Now = TimeZoner.Now;

            Tenant = Creator.CreateDomain(Now);
            Business = Tenant.Businesses.First();
            Settlement = Business.Settlements.First();
            Zone = Settlement.Zones.First();
            Supervisor = Zone.Supervisors.First();
            Lot = Zone.Lots.First();
        }

        Tennant Tenant;
        Settlement Settlement;
        Business Business;
        Zone Zone;
        Lot Lot;
        Supervisor Supervisor;
        DateTime Now;

        [TestMethod]
        public void Playground()
        {
            var report1 = CreateReport(0, 10);
            var report2 = CreateReport(1, 50);
            var report3 = CreateReport(2, 10);

            Settlement.Resync(Now);

            Assert.AreEqual(10, Lot.Zone.ZonePeriods[0].EndingPercentage);
            Assert.AreEqual(60, Lot.Zone.ZonePeriods[1].EndingPercentage);
            Assert.AreEqual(70, Lot.Zone.ZonePeriods[2].EndingPercentage);
        }

        Report CreateReport(int periodIndex, double increment)
        {
            var periods = Tenant.Periods
                .OrderBy(x => x.StartDate)
                .ToList();

            var zonePeriods = Lot.Zone.ZonePeriods
                .OrderBy(x => x.Period.StartDate)
                .ToList();

            var buildingConcepts = Lot.Model.BuildingConcepts
                .OrderBy(x => x.Order)
                .ToList();

            var period = periods[periodIndex];

            var lastReport = Lot.Reports
                .OrderBy(x => x.Folio)
                .LastOrDefault();

            Report report = null;
            if (lastReport == null)
            {
                report = new Report(
                    Lot, Lot.Settlement, Supervisor.User,
                    period, 1, "first-report.png");

                foreach (var buildingConcept in buildingConcepts)
                    new ReportMembership(report, buildingConcept, 0, increment);
            }
            else
            {
                report = new Report(
                    Lot, Lot.Settlement, Supervisor.User,
                    period, lastReport.Folio + 1, "first-report.png");

                foreach (var buildingConcept in buildingConcepts)
                {
                    var pastReportMembership = lastReport.ReportMemberships
                        .SingleOrDefault(x => x.BuildingConcept == buildingConcept);

                    new ReportMembership(report, buildingConcept,
                        pastReportMembership.Percentage,
                        pastReportMembership.Percentage + increment);
                }
            }

            return report;
        }
    }
}