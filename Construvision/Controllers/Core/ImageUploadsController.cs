﻿using Domain;
using Domain.ImageUploadContext;
using System;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class ImageUploadsController : BaseApiController
    {
        public ImageUploadsController()
        {
        }

        public string TempFolder = "~/content/images/uploads/temp/";
        public string ImagesFolder = "~/content/images/uploads/";

        [AuthorizeRoles(Roles = "Resident Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/images/upload/temp")]
        public HttpResponseMessage PostImageUploadTemp()
        {
            try
            {
                var request = HttpContext.Current.Request;

                if (request.Files.Count == 0)
                    return Error("Imagen es requerida.");

                if (request.Files.Count > 1)
                    return Error("Solo puedes enviar una imagen a la vez.");

                var file = request.Files[0];

                return UploadImage(file, TempFolder);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogTypes.ImageUpload);
                return Error(string.Empty, "Error al subir el archivo.");
            }
        }

        [AuthorizeRoles(Roles = "Resident Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/images/upload")]
        public HttpResponseMessage PostImageCrop(PostCropInput dto)
        {
            try
            {
                if (string.IsNullOrEmpty(dto.FileName))
                    return Error("Imagen no encontrada.");

                var tempBasePath = HttpContext.Current.Server.MapPath("~/Content/images/uploads/temp");

                var tempFilePath = Path.Combine(tempBasePath, dto.FileName);

                if (!File.Exists(tempFilePath))
                    return Error("Imagen no encontrada.");

                var contentType = MimeMapping.GetMimeMapping(tempFilePath);

                var X = Convert.ToInt32(dto.X);
                var Y = Convert.ToInt32(dto.Y);
                var W = Convert.ToInt32(dto.Width);
                var H = Convert.ToInt32(dto.Height);

                using (var image = new Bitmap(tempFilePath))
                using (var bitMap = new Bitmap(W, H))
                using (var graphic = Graphics.FromImage(bitMap))
                {
                    graphic.DrawImage(image, new Rectangle(0, 0, W, H), X, Y, W, H, GraphicsUnit.Pixel);

                    var guid = Guid.NewGuid().ToString("N");
                    var fileExtension = GetImageData(contentType);

                    if (!ModelState.IsValid) return null;

                    var fileName = guid + fileExtension;
                    var basePath = HttpContext.Current.Server.MapPath("~/Content/images/uploads/");
                    var filePath = Path.Combine(basePath, fileName);
                    bitMap.Save(filePath);

                    var output = new Output
                    {
                        FileName = fileName,
                        Width = bitMap.Width,
                        Height = bitMap.Height,
                    };

                    return OK(output);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return Error();
            }
        }

        [AuthorizeRoles(Roles = "Resident Supervisor Admin SalesAdmin Seller Manager")]
        [Route("~/api/images/mobile-upload")]
        public HttpResponseMessage PostImageUpload()
        {
            try
            {
                var request = HttpContext.Current.Request;

                if (request.Files.Count == 0)
                    return Error("Imagen es requerida.");

                if (request.Files.Count > 1)
                    return Error("Solo puedes enviar una imagen a la vez.");

                var file = request.Files[0];

                return UploadImage(file, ImagesFolder);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogTypes.ImageUpload);
                return Error("Error al subir el archivo.");
            }
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin")]
        [Route("~/api/files/upload")]
        public HttpResponseMessage PostFile()
        {
            try
            {
                var request = HttpContext.Current.Request;

                if (request.Files.Count == 0)
                    return Error("Archivo requerido");

                if (request.Files.Count > 1)
                    return Error("Solo puedes subir un archivo a la vez");

                var file = request.Files[0];

                if (file.ContentType != "application/pdf")
                    return Error("Solo PDF");

                var output = UploadFile(file.InputStream);

                if (output == null)
                    return Error();

                return OK(new { FileName = output });
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return Error("Error al subir el archivo");
            }
        }

        string GetImageData(string contentType)
        {
            switch (contentType)
            {
                case "image/jpeg": return ".jpg";
                case "image/png": return ".png";
                case "image/bmp": return ".bmp";
                case "image/gif": return ".gif";
            }

            ModelState.AddModelError(string.Empty, "Solo formatos jpg, png, bmp o gif.");

            return string.Empty;
        }
        HttpResponseMessage UploadImage(HttpPostedFile file, string folder)
        {
            try
            {
                var image = Image.FromStream(file.InputStream);

                var bitMap = new Bitmap(image);

                var guid = Guid.NewGuid().ToString("N");
                var fileExtension = GetImageData(file.ContentType);

                var fileName = guid + fileExtension;
                var basePath = HttpContext.Current.Server.MapPath(folder);
                var filePath = Path.Combine(basePath, fileName);

                bitMap.Save(filePath);

                var output = new Output
                {
                    FileName = fileName,
                    Height = bitMap.Height,
                    Width = bitMap.Width,
                };

                return OK(output);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return Error();
            }
        }
        string UploadFile(Stream stream)
        {
            try
            {
                var guid = Guid.NewGuid().ToString("N");

                var path = "~/content/files";

                var fileName = guid + ".pdf";

                var basePath = HostingEnvironment.MapPath(path);

                var filePath = Path.Combine(basePath, fileName);

                using (var file = File.Create(filePath))
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    stream.CopyTo(file);
                }

                return fileName;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return null;
            }
        }
    }
}