﻿using Domain;
using Domain.SessionContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class SessionsController : BaseApiController
    {
        public SessionsController()
        {
        }

        [AuthorizeRoles(Roles = "None Resident Supervisor Admin SalesAdmin Seller Manager")]
        [Route("~/api/sessions")]
        public async Task<HttpResponseMessage> GetSessions()
        {
            var output = await GetUserSessions(Principal.User);
            return OK(output);
        }

        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/sessions/user")]
        public async Task<HttpResponseMessage> PostUserSessions(PostSessionInput dto)
        {
            var manager = await Context.Managers
                .SingleOrDefaultAsync(x => x.UserId == Principal.User.UserId);

            var user = await Context.Users
                .SingleOrDefaultAsync(x => x.BusinessId == manager.BusinessId
                    && x.Email.Equals(dto.Email, StringComparison.OrdinalIgnoreCase));

            if (user == null)
                return Error("Email", "Usuario no encontrado");

            var output = new UserSessionsOutput
            {
                User = user.ToOutput(),
                Sessions = await GetUserSessions(user),
            };

            return OK(output);
        }




        [AllowAnonymous]
        [Route("~/api/login")]
        public HttpResponseMessage Post(PostLoginInput dto)
        {
            var ip = HttpContext.Current.Request.UserHostAddress;
            var userAgent = HttpContext.Current.Request.UserAgent;

            var context = new EFContext();

            var user = context.Users
                .SingleOrDefault(x => x.Email.Equals(dto.Email, StringComparison.OrdinalIgnoreCase));

            if (user == null || !user.IsThisMyPassword(dto.Password))
                return Error("Email", "El email o la contraseña son incorrectos.");

            var token = Guid.NewGuid().ToString("n") + "424ugg" + Guid.NewGuid().ToString("n");

            var session = new Session(user, token, ip, userAgent, TimeZoner.Now.AddMinutes(1 * 60 * 24 * 60));

            GetValidationErrors(session.Validate());

            if (!ModelState.IsValid)
                return Error();

            if (!user.LastLogin.HasValue)
            {

            }

            user.UpdateLastLoginDate();

            context.Sessions.Add(session);
            context.SaveChanges();

            var output = new LoginOutput
            {
                Token = token,
                Email = user.Email,
                Name = user.Name,
                FirstLastName = user.FirstLastName,
                SecondLastName = user.SecondLastName,
            };

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/user/sessions")]
        public async Task<HttpResponseMessage> PostSession(PostSessionInput dto)
        {
            if (dto == null)
                return Error();

            if (!Enum.IsDefined(typeof(SessionTypes), dto.Type))
                return Error("Type", "Tipo de sesion no soportado");

            if (dto.Type == SessionTypes.None)
                return Error("Type", "Tipo de sesion no soportado");

            var manager = await Context.Managers
                .Include(x => x.Business)
                .Include(x => x.User)
                .SingleOrDefaultAsync(x => x.UserId == Principal.User.UserId);

            var user = await Context.Users
                .SingleOrDefaultAsync(x => x.BusinessId == manager.BusinessId
                    && x.Email.Equals(dto.Email, StringComparison.OrdinalIgnoreCase));

            if (user == null)
                return Error("Email", "Usuario no encontrado");

            Settlement settlement = null;
            Zone zone = null;

            if (dto.SettlementId > 0)
            {
                settlement = await Context.Settlements
                    .SingleOrDefaultAsync(x => x.BusinessId == manager.BusinessId
                        && x.SettlementId == dto.SettlementId);
            }

            if (dto.ZoneId > 0)
            {
                zone = await Context.Zones
                    .SingleOrDefaultAsync(x => x.Settlement.BusinessId == manager.BusinessId
                        && x.ZoneId == dto.ZoneId);
            }

            if (dto.Type == SessionTypes.SalesAdmin || dto.Type == SessionTypes.SalesAdmin)
                if (settlement == null)
                    return Error("Residencial no encontrado.");

            if (dto.Type == SessionTypes.Seller || dto.Type == SessionTypes.Supervisor)
                if (zone == null)
                    return Error("Cerrada no encontrada.");


            if (zone != null && settlement != null && zone.SettlementId != settlement.SettlementId)
                return Error($"{zone.Name} no pertenece a {settlement.Name}");

            switch (dto.Type)
            {
                default: return Error();

                case SessionTypes.Admin:

                    var admin = await Context.Admins
                        .SingleOrDefaultAsync(x => x.SettlementId == settlement.SettlementId
                            && x.UserId == user.UserId);

                    if (admin != null)
                        return Error("Email", $"{user.FullName} ya es admin de {settlement.Name}");

                    new Admin(user, settlement);

                    break;

                case SessionTypes.SalesAdmin:

                    var saleAdmin = await Context.SalesAdmins
                        .SingleOrDefaultAsync(x => x.SettlementId == settlement.SettlementId
                            && x.UserId == user.UserId);

                    if (saleAdmin != null)
                        return Error("Email", $"{user.FullName} ya es admin de ventas de {settlement.Name}");

                    new SalesAdmin(user, settlement);

                    break;

                case SessionTypes.Supervisor:

                    var supervisor = await Context.Supervisors
                        .SingleOrDefaultAsync(x => x.ZoneId == zone.ZoneId
                            && x.UserId == user.UserId);

                    if (supervisor != null)
                        return Error("Email", $"{user.FullName} ya es supervisor de {zone.Name}");

                    new Supervisor(user, zone);

                    break;

                case SessionTypes.Seller:

                    var seller = await Context.Sellers
                        .SingleOrDefaultAsync(x => x.ZoneId == zone.ZoneId
                            && x.UserId == user.UserId);

                    if (seller != null)
                        return Error("Email", $"{user.FullName} ya es supervisor de {zone.Name}");

                    new Seller(user, zone);

                    break;

                case SessionTypes.Manager:

                    var newManager = await Context.Managers
                    .SingleOrDefaultAsync(x => x.BusinessId == manager.BusinessId
                        && x.UserId == user.UserId);

                    if (newManager != null)
                        return Error("Email", $"{user.FullName} ya es manager de {manager.Business.Name}");

                    newManager = new Manager(user, manager.Business);
                    break;

            }

            Context.SaveChanges();

            return OK(user.ToOutput());
        }

        [AllowAnonymous]
        [Route("~/api/logout")]
        public HttpResponseMessage PostLogout()
        {
            try
            {
                var authorizationHeader = Request.Headers.Authorization;
                if (authorizationHeader == null)
                    return OK(true);

                if (string.IsNullOrWhiteSpace(authorizationHeader.Parameter))
                    return OK(true);

                var context = new EFContext();

                var session = context.Sessions
                    .SingleOrDefault(x => x.Token == authorizationHeader.Parameter);

                if (session == null)
                    return Error();

                session.Expire();

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }

            return OK(true);
        }

        [Route("~/api/password-reset")]
        public HttpResponseMessage PostReset(Domain.SessionContext.Password.PostResetInput dto)
        {
            var smartLink = Context.SmartLinks
                 .Include(x => x.User)
                 .SingleOrDefault(x =>
                     !x.ActivationDate.HasValue &&
                     (x.SmartLinkType == SmartLinkTypes.PasswordReset ||
                     x.SmartLinkType == SmartLinkTypes.PasswordChange) &&
                     x.ExpirationDate > TimeZoner.Now &&
                     x.Code == dto.Token);

            if (smartLink == null)
                return Error(string.Empty, "Enlace de restauración de contraseña inválido.");

            var user = smartLink.User;

            user.UpdatePassword(dto.NewPassword);
            GetValidationErrors(user.Validate());
            GetValidationErrors(user.ValidatePassword());

            if (!ModelState.IsValid)
                return Error();

            user.HashPassword();
            smartLink.Activate();

            Context.SaveChanges();

            return OK(user.Email);
        }

        [AuthorizeRoles(Roles = "Resident Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/password-change")]
        public HttpResponseMessage PostChange(Domain.SessionContext.Password.PostChangeInput dto)
        {
            var user = Principal.User;

            if (user.Password != Cryptography.BCrypt(dto.OldPassword, user.Salt))
                return Error("OldPassword", "Contraseña incorrecta.");


            user.UpdatePassword(dto.NewPassword);
            GetValidationErrors(user.Validate());
            GetValidationErrors(user.ValidatePassword());

            if (!ModelState.IsValid)
                return Error();

            user.HashPassword();

            Context.SaveChanges();

            return OK(true);
        }


        async Task<List<Output>> GetUserSessions(User user)
        {
            var output = new List<Output>();

            output.AddRange(await Context.Admins
                .Where(x => x.UserId == user.UserId)
                .Select(x => new Output
                {
                    SessionId = x.AdminId,
                    SessionType = SessionTypes.Admin,
                    Description = x.Settlement.Name,
                    ImageCode = x.Settlement.ImageCode,
                    SettlementId = x.SettlementId,
                })
                .ToListAsync());

            output.AddRange(await Context.SalesAdmins
                .Where(x => x.UserId == user.UserId)
                .Select(x => new Output
                {
                    SessionId = x.SalesAdminId,
                    SessionType = SessionTypes.SalesAdmin,
                    Description = x.Settlement.Name,
                    ImageCode = x.Settlement.ImageCode,
                    SettlementId = x.SettlementId,
                })
                .ToListAsync());

            output.AddRange(await Context.Supervisors
                .Where(x => x.UserId == user.UserId)
                .Select(x => new Output
                {
                    SessionId = x.SupervisorId,
                    SessionType = SessionTypes.Supervisor,
                    Description = x.Zone.Name,
                    ImageCode = x.Zone.ImageCode,
                    Alias = x.Zone.Alias,
                    MapId = x.Zone.MapId ?? 0,
                    SettlementId = x.Zone.SettlementId,
                    ZoneId = x.Zone.ZoneId,
                })
                .ToListAsync());

            output.AddRange(await Context.Sellers
                .Where(x => x.UserId == user.UserId)
                .Select(x => new Output
                {
                    SessionId = x.SellerId,
                    SessionType = SessionTypes.Seller,
                    Description = x.Zone.Name,
                    ImageCode = x.Zone.ImageCode,
                    Alias = x.Zone.Alias,
                    MapId = x.Zone.MapId ?? 0,
                    SettlementId = x.Zone.SettlementId,
                    ZoneId = x.Zone.ZoneId,
                })
                .ToListAsync());

            output.AddRange(await Context.Managers
                .Where(x => x.UserId == user.UserId)
                .Select(x => new Output
                {
                    SessionId = x.ManagerId,
                    SessionType = SessionTypes.Manager,
                    Description = "Manager de negocio",
                    Alias = x.Business.Name,
                    ImageCode = x.Business.ImageCode,
                })
                .ToListAsync());

            return output;
        }
    }
}