﻿using Backend.Common.SparkPost.Models;
using Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers.Core
{
    public class EmailWebhookController : BaseApiController
    {
        public EmailWebhookController()
        {

        }

        [AllowAnonymous]
        [Route("~/api/webhooks/email")]
        public async Task<HttpResponseMessage> Post(List<WebhookWrapper> dto)
        {
            try
            {
                if (dto == null)
                {
                    App.Instance.EventLog.Push($"Webhook_email | null payload");
                    return OK(false);
                }

                if (dto.Count == 0)
                {
                    App.Instance.EventLog.Push($"Webhook_email | zero payload items");
                    return OK(false);
                }

                Context = new EFContext();

                foreach (var item in dto)
                {
                    if (item.msys.message_event == null)
                    {
                        App.Instance.EventLog.Push($"Webhook_email | null msys.message_event");
                        continue;
                    }

                    if (item.msys.message_event.rcpt_meta == null)
                    {
                        App.Instance.EventLog.Push($"Webhook_email | null msys.message_event.rcpt_meta");
                        continue;
                    }

                    if (string.IsNullOrWhiteSpace(item.msys.message_event.rcpt_meta.id)
                        || !int.TryParse(item.msys.message_event.rcpt_meta.id, out int emailId))
                    {
                        App.Instance.EventLog.Push($"Webhook_email | bad metadata");
                        return OK(false);
                    }

                    var email = await Context.Emails
                        .Include(x => x.User)
                        .SingleOrDefaultAsync(x => x.EmailId == emailId);

                    if (email == null)
                    {
                        App.Instance.EventLog.Push($"Webhook_email | not found | {emailId}");
                        return OK(false);
                    }

                    //HANDLE ONLY VALID MESSAGES -------------------------------------------------------

                    var type = item.msys.message_event.type.ToLower();

                    if (type == "bounce")
                    {
                        await OnBounce(item, email);
                    }
                    else if (type == "delivery")
                    {
                        await OnDelivery(email);
                    }
                    else
                    {
                        App.Instance.EventLog.Push($"Webhook_email | bad event type | {item.msys.message_event.type}");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogTypes.Webhook_Email);
            }

            return OK(true);
        }

        async Task<bool> OnBounce(WebhookWrapper item, Email email)
        {
            if (item.msys.message_event.bounce_class != "10")
                return false;

            if (email.UserId.HasValue && !email.User.IsBadEmail)
                email.User.SetBadEmail(true);

            await Context.SaveChangesAsync();

            App.Instance.EventLog.Push($"Webhook_email | bounce | {email.AuxEmail}");

            return true;
        }
        async Task<bool> OnDelivery(Email email)
        {
            if (email.UserId.HasValue && email.User.IsBadEmail)
                email.User.SetBadEmail(false);

            Context.Emails.Remove(email);

            await Context.SaveChangesAsync();

            App.Instance.EventLog.Push($"Webhook_email | OK | {email.AuxEmail}");

            return true;
        }
    }
}