﻿using Domain.PeriodContext;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class PeriodsController : BaseApiController
    {
        public PeriodsController()
        {
        }


        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/periods")]
        public HttpResponseMessage GetAll()
        {
            var currentPeriod = GetCurrentPeriod();

            var output = Context.Periods
                .Where(x => x.EndDate <= currentPeriod.EndDate)
                .OrderByDescending(x => x.StartDate)
                .Select(x => new Output
                {
                    PeriodId = x.PeriodId,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate
                })
                .ToList();

            return OK(output);
        }
    }
}
