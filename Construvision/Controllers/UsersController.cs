﻿using Domain;
using Domain.SessionContext;
using Domain.UserContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class UsersController : BaseApiController
    {
        public UsersController()
        {
            Message404 = "Usuario no encontrado";
        }


        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/settlements/{id:int:min(1)}/users")]
        public async Task<HttpResponseMessage> GetAll(int id)
        {
            var settlement = await Context.Settlements
                .OrderByDescending(x => x.CreationDate)
                .SingleOrDefaultAsync(x => x.BusinessId == Principal.Business.BusinessId
                    && x.SettlementId == id);

            if (settlement == null)
                return Error("Residencial no encontrado");

            var output = new UsersOutput();

            output.Admins = (await Context.Admins
                .Where(x => x.SettlementId == settlement.SettlementId)
                .Select(x => x.User)
                .OrderBy(x => x.Name)
                .ToListAsync())
                .Select(x => x.ToOutput())
                .Distinct()
                .ToList();

            output.SalesAdmins = (await Context.SalesAdmins
                .Where(x => x.SettlementId == settlement.SettlementId)
                .Select(x => x.User)
                .OrderBy(x => x.Name)
                .Distinct()
                .ToListAsync())
                .Select(x => x.ToOutput())
                .ToList();

            output.Sellers = (await Context.Sellers
               .Where(x => x.Zone.SettlementId == settlement.SettlementId)
               .Select(x => x.User)
               .OrderBy(x => x.Name)
               .Distinct()
               .ToListAsync())
               .Select(x => x.ToOutput())
               .ToList();

            output.Supervisors = (await Context.Supervisors
               .Where(x => x.Zone.SettlementId == settlement.SettlementId)
               .Select(x => x.User)
               .OrderBy(x => x.Name)
               .Distinct()
               .ToListAsync())
               .Select(x => x.ToOutput())
               .ToList();

            output.Managers = (await Context.Managers
               .Where(x => x.BusinessId == settlement.BusinessId)
               .Select(x => x.User)
               .OrderBy(x => x.Name)
               .ToListAsync())
               .Distinct()
               .Select(x => x.ToOutput())
               .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/settlements/{id:int:min(1)}/zones")]
        public async Task<HttpResponseMessage> GetSettlementZones(int id)
        {
            var settlement = await Context.Settlements
                .SingleOrDefaultAsync(x => x.BusinessId == Principal.Business.BusinessId
                    && x.SettlementId == id);

            var zones = await Context.Zones
                .Where(x => x.SettlementId == settlement.SettlementId)
                .ToListAsync();

            var output = zones
                .Select(x => new Domain.ZoneContext.Output
                {
                    ZoneId = x.ZoneId,
                    Name = x.Name,
                    Alias = x.Alias,

                    Percentage = x.GetPercentage(),
                    StartingPercentage = x.GetStartingPercentage(),

                    MapId = x.MapId,
                    SettlementId = x.SettlementId,
                })
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/users/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> GetOne(int id)
        {
            var user = await Context.Users
                .SingleOrDefaultAsync(x => x.BusinessId == Principal.Manager.BusinessId
                    && x.UserId == id);

            var output = user.ToOutput();

            return OK(output);
        }


        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/settlements/{id:int:min(1)}/user")]
        public async Task<HttpResponseMessage> Post(int id, PostInput dto)
        {
            var settlement = await Context.Settlements
                .Include(x => x.Business.Tennant)
                .SingleOrDefaultAsync(x => x.BusinessId == Principal.Business.BusinessId
                    && x.SettlementId == id);

            Zone zone = null;
            if (dto.Type == SessionTypes.Supervisor || dto.Type == SessionTypes.Seller)
            {
                if (!dto.ZoneId.HasValue)
                    return Error("Cerrada es requerida");

                zone = await Context.Zones
                    .SingleOrDefaultAsync(x => x.SettlementId == settlement.SettlementId
                        && x.ZoneId == dto.ZoneId);

                if (zone == null)
                    return Error("Cerrada no encontrada");
            }

            var isEmailInUse = await Context.Users
                .AnyAsync(x => x.BusinessId == settlement.BusinessId
                    && x.Email.Equals(dto.Email, StringComparison.OrdinalIgnoreCase));

            if (isEmailInUse)
                return Error("Email", "Email en uso");

            var user = new User(
                settlement.Business,
                dto.Name,
                dto.FirstLastName,
                dto.SecondLastName,
                dto.Email,
                dto.MobilePhone,
                dto.LocalPhone,
                null);

            if (dto.Type == SessionTypes.Supervisor)
                new Supervisor(user, zone);

            if (dto.Type == SessionTypes.Seller)
                new Seller(user, zone);

            if (dto.Type == SessionTypes.Admin)
                new Admin(user, settlement);

            if (dto.Type == SessionTypes.SalesAdmin)
                new SalesAdmin(user, settlement);

            if (dto.Type == SessionTypes.Manager)
                new Manager(user, settlement.Business);

            await Context.SaveChangesAsync();

            var output = user.ToOutput();

            return OK(output);
        }


        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/users/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> Put(int id, PutInput dto)
        {
            var user = await Context.Users
                .SingleOrDefaultAsync(x => x.BusinessId == Principal.Manager.BusinessId
                    && x.UserId == id);

            if (user == null)
                return Error404();

            user.Update(dto);

            GetValidationErrors(user.Validate());

            if (!ModelState.IsValid)
                return Error();

            await Context.SaveChangesAsync();

            return OK(true);
        }

        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/users/{id:int:min(1)}/password")]
        public async Task<HttpResponseMessage> PutPassword(int id, PutPasswordInput dto)
        {
            var user = await Context.Users
                .SingleOrDefaultAsync(x => x.BusinessId == Principal.Manager.BusinessId
                    && x.UserId == id);

            if (user == null)
                return Error404();

            if (dto.Password != dto.RePassword)
                return Error("Las contraseñas no coinciden.");

            user.UpdatePassword(dto.Password);
            GetValidationErrors(user.ValidatePassword());

            if (!ModelState.IsValid)
                return Error();

            user.HashPassword();
            await Context.SaveChangesAsync();

            return OK(true);
        }
    }
}