﻿using Construvision.ChronJobs;
using Construvision.ScriptServices;
using Domain;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Construvision.Controllers
{
    public class HomeController : Controller
    {
        [Route("~/")]
        public ActionResult Index()
        {
            ViewBag.BaseUrl = WebConfigData.BaseUrl;
            return View();
        }

        [HttpGet]
        [Route("~/system/resync")]
        public async Task<ActionResult> Resync(string code)
        {
            if (string.IsNullOrWhiteSpace(code) || code != "123123")
                return FailMessage();

            var service = new SyncService();
            var output = await service.Run();
            return Content("<h1>" + (output ? "OK" : "KO") + "</h1>", "text/html");
        }

        [HttpGet]
        [Route("~/system/chronjobs")]
        public ActionResult ChronJobs(string code)
        {
            if (string.IsNullOrWhiteSpace(code) || code != "123123")
                return FailMessage();

            var now = TimeZoner.Now;

            var html = string.Empty;
            var title = "Reloading";

            var heartBeatCache = HttpRuntime.Cache[Construvision.ChronJobs.Context.CacheHeartBeatKey];
            if (heartBeatCache != null)
            {
                title = "next: " + (now - (DateTime)heartBeatCache).Seconds;
            }

            var buffer = string.Empty;

            foreach (var chronJob in App.Instance.Chronjobs.Jobs)
            {
                buffer += GetChronjobRow(chronJob);
            }

            html += $@"
            <!DOCTYPE html> 
            <html> 
                <head> 
                    <title>Construvisión - ChronJobs</title> 
                    <meta charset='UTF-8'> 
                    <meta http-equiv='refresh' content='3'> 
                    <link rel='shortcut icon' href='/content/images/favicon.ico' type='image/x-icon'> 
                    <link rel='icon' href='/content/images/favicon.ico' type='image/x-icon'> 
                    <style> 
                        h1, h2 {{ border: 1px solid #000; text-align: center; }}
                        table {{ width: 100%; }}
                        table td {{ border: 1px solid #000; padding: 3px 5px; text-align: center; }}
                        #container {{ margin: auto; width: 70%; }}
                    </style> 
                </head> 
                <body> 
                    <div id='container'> 
                        <h1>{title}</h1> 
                        <h1>{now.AddSeconds(1).ToString("HH:mm:ss")}</h1> 
                        <table><tbody>{buffer}</tbody></table>
                    </div> 
                </body> 
            </html>";

            return Content(html, "text/html");
        }

        [Route("~/system/log")]
        public ActionResult Log(string code)
        {
            if (string.IsNullOrWhiteSpace(code) || code != "123123")
                return FailMessage();

            var html = string.Empty;
            if (App.Instance.EventLog.Count == 0)
                html = GetRowHtml("The activity log is empty. Try again later.");
            else
            {
                var eventLog = App.Instance.EventLog.ToArray();

                foreach (var item in eventLog)
                    html += GetRowHtml(item);
            }

            html = "" +
            "<!DOCTYPE html>" +
            "<html>" +
                "<head>" +
                    "<title>Construvision - Activity Log</title>" +
                    "<meta charset='UTF-8'>" +
                    "<meta http-equiv='refresh' content='30'>" +
                    "<link rel='shortcut icon' href='/content/images/favicon.ico' type='image/x-icon'>" +
                    "<link rel='icon' href='/content/images/favicon.ico' type='image/x-icon'>" +
                    "<style>" +
                        "h1, h2 { border: 1px solid #000; text-align: center; } " +
                        "table { width: 100%; } " +
                        "table td { border: 1px solid #000; padding: 3px 5px; text-align: center; } " +
                        "#container { margin: auto; width: 70%; }" +
                    "</style>" +
                "</head>" +
                "<body>" +
                    "<div id='container'>" +
                        "<table><tbody>" + html + "</tbody></table>" +
                    "</div>" +
                "</body>" +
            "</html>";

            return Content(html, "text/html");
        }

        ContentResult FailMessage()
        {
            return new ContentResult()
            {
                Content = "<h1>Good Bye!</h1>",
                ContentType = "text/html",
                ContentEncoding = UTF8Encoding.UTF8,
            };
        }
        string GetRowHtml(string message)
        {
            return "<tr><td>" + message + "</td></tr>";
        }
        string GetChronjobRow(BaseChronJob chronjob)
        {
            return GetRowHtml("<h3>" +
            chronjob.Counter.ToString("0000") +
            " | " + chronjob.Timer + " / " + chronjob.Wait +
            (chronjob.Working ? " | Processing" : string.Empty) +
            " | " + chronjob.Name +
            "</h3>");
        }
    }
}

