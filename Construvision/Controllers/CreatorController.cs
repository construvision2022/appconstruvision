﻿using Domain;
using Domain.CreatorContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class CreatorController : BaseApiController
    {
        public CreatorController()
        {
            Message404 = "No encontrado";
        }

        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/creator")]
        public async Task<HttpResponseMessage> Post(PostInput dto)
        {
            if (dto == null)
                return Error("Error en el envío de datos");

            if (dto.Settlements?.Count == 0)
                return Error("Columna residenciales es inválida");

            if (dto.Zones?.Count == 0)
                return Error("Columna cerradas es inválida");

            if (dto?.Blocks?.Count == 0)
                return Error("Columna manzanas es inválida");

            if (dto.Models?.Count == 0)
                return Error("Columna modelos es inválida");

            if (dto.Streets?.Count == 0)
                return Error("Columna calles es inválida");

            if (dto.LotNumbers?.Count == 0)
                return Error("Columna números de lote es inválida");

            if (dto.OfficialNumbers?.Count == 0)
                return Error("Columna números oficiales es inválida");

            if (dto.RUVPackages?.Count == 0)
                return Error("Columna paquetes RUV es inválida");

            if (dto.BankPackages?.Count == 0)
                return Error("Columna paquetes bancarios es inválida");

            var elements = dto.Settlements.Count;
            if (dto.Zones.Count != elements
                || dto.Blocks.Count != elements
                || dto.Models.Count != elements
                || dto.Streets.Count != elements
                || dto.LotNumbers.Count != elements
                || dto.OfficialNumbers.Count != elements
                || dto.RUVPackages.Count != elements
                || dto.BankPackages.Count != elements)
                return Error("Número de elementos de columnas es diferente");

            var now = TimeZoner.Now;

            var manager = await Context.Managers
                .Include(x => x.Business.Tennant)
                .SingleAsync(x => x.ManagerId == Principal.Id);

            var business = manager.Business;

            await Context.Settlements
                .Where(x => x.BusinessId == manager.BusinessId)
                .LoadAsync();

            await Context.Zones
                .Include(x => x.Streets)
                .Include(x => x.Blocks)
                .Include(x => x.Models)
                .Include(x => x.Lots)
                .Where(x => x.Settlement.BusinessId == manager.BusinessId)
                .LoadAsync();

            await Context.Institutions
                .Include(x => x.Packages)
                .Where(x => x.Zone.Settlement.BusinessId == manager.BusinessId)
                .LoadAsync();

            var output = manager.Business.Create(dto, manager.User, now);

            if (output.OK)
            {
                await Context.SaveChangesAsync();

                return OK(output);
            }
            else
            {
                return Error(output.Error);
            }
        }
    }
}