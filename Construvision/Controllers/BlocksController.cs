﻿using Domain;
using Domain.BlockContext;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class BlocksController : BaseApiController
    {
        public BlocksController()
        {
            Message404 = "Manzana no encontrada";
        }


        [AuthorizeRoles(Roles = "Admin Supervisor SalesAdmin Seller")]
        [Route("~/api/blocks")]
        public async Task<HttpResponseMessage> GetAll()
        {
            var blocks = await Context.Blocks
                .Include(x => x.Zone)
                .Where(Principal.Blocks())
                .ToListAsync();

            var output = blocks
                .Select(x => x.ToOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin Supervisor SalesAdmin Seller")]
        [Route("~/api/blocks/{id:int:min(1)}/lots")]
        public HttpResponseMessage GetLots(int id)
        {
            var block = Context.Blocks
                .Include(x => x.Zone)
                .Where(Principal.Blocks())
                .SingleOrDefault(x => x.BlockId == id);

            if (block == null)
                return Error404();

            var output = Context.Lots
                .Where(Principal.Lots())
                .Where(x => x.BlockId == id)
                .ToList()
                .OrderBy(x => x.LotNumber.PadLeft(4, '0'))
                .Select(x => x.ToOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin Supervisor SalesAdmin Seller")]
        [Route("~/api/blocks/{id:int:min(1)}/unassignedlots")]
        public HttpResponseMessage GetUnassignedLots(int id)
        {
            var output = Context.Lots
                .Include(x => x.MapImages)
                .Where(Principal.Lots())
                .Where(x =>
                     x.BlockId == id &&
                     x.MapImages.Count == 0)
                .AsEnumerable()
                .OrderBy(x => x.LotNumber.PadLeft(4, '0'))
                .Take(5)
                .Select(x => new
                {
                    x.LotId,
                    x.LotNumber,
                })
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin Supervisor SalesAdmin Seller")]
        [Route("~/api/blocks/{id:int:min(1)}")]
        public HttpResponseMessage GetOne(int id)
        {
            var block = Context.Blocks
                .Where(Principal.Blocks())
                .SingleOrDefault(x => x.BlockId == id);

            if (block == null)
                return Error404();

            var output = block.ToOutput();

            return OK(output);
        }



        [AuthorizeRoles(Roles = "Admin")]
        [Route("~/api/blocks")]
        public async Task<HttpResponseMessage> Post(PostInput dto)
        {
            var settlement = Principal.Settlement;

            var zone = await Context.Zones
                .SingleOrDefaultAsync(x =>
                    x.SettlementId == settlement.SettlementId &&
                    x.ZoneId == dto.ZoneId);

            if (zone == null)
                return Error(string.Empty, "Cerrada no encontrada");

            if (!ModelState.IsValid)
                return Error();

            var nameAlreadyExists = await Context.Blocks
                .AnyAsync(x => x.ZoneId == zone.ZoneId
                    && x.Name.Equals(dto.Name, StringComparison.OrdinalIgnoreCase));

            if (nameAlreadyExists)
                return Error("Name", "Ya existe una manzana con ese nombre.");

            var block = new Block(settlement, zone, dto.Name, dto.Color);

            GetValidationErrors(block.Validate());

            if (!ModelState.IsValid)
                return Error();

            await Context.SaveChangesAsync();

            return OK(block.ToOutput());
        }


        [AuthorizeRoles(Roles = "Admin")]
        [Route("~/api/blocks/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> Put(int id, PutInput dto)
        {
            var settlement = Principal.Settlement;

            var block = await Context.Blocks
                .SingleOrDefaultAsync(x => x.SettlementId == settlement.SettlementId
                    && x.BlockId == id);

            if (block == null)
                return Error404();

            var nameAlreadyExists = Context.Blocks
                .Any(x => x.ZoneId == block.ZoneId
                    && x.BlockId != block.BlockId
                    && x.Name.Equals(dto.Name, StringComparison.OrdinalIgnoreCase));

            if (nameAlreadyExists)
                return Error("Name", "Ya existe una manzana con ese nombre.");

            block.Update(dto.Name, dto.Color);

            GetValidationErrors(block.Validate());

            if (!ModelState.IsValid)
                return Error();

            Context.SaveChanges();

            return OK(true);
        }


        [AuthorizeRoles(Roles = "Admin")]
        [Route("~/api/blocks/{id:int:min(1)}")]
        public HttpResponseMessage Delete(int id)
        {
            var settlement = Principal.Settlement;

            var block = Context.Blocks
                .SingleOrDefault(x =>
                    x.SettlementId == settlement.SettlementId &&
                    x.BlockId == id);

            if (block == null)
                return Error404();

            var blockHasLots = Context.Lots
                .Any(x => x.BlockId == block.BlockId);

            if (blockHasLots)
                return Error("No se puede eliminar manzanas con lotes.");

            Context.Blocks.Remove(block);

            Context.SaveChanges();

            return OK(true);
        }
    }
}