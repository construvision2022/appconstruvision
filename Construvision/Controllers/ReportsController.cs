﻿using Domain;
using Domain.ReportContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class ReportsController : BaseApiController
    {
        public ReportsController()
        {
            Message404 = "Reporte no encontrado";
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/reports/{reportId:int:min(1)}")]
        public HttpResponseMessage GetReport(int reportId)
        {
            var report = Context.Reports
                .Include(x => x.Lot)
                .Where(Principal.Reports())
                .SingleOrDefault(x => x.ReportId == reportId);

            if (report == null)
                return Error404();

            Context.ReportMemberships
                .Include(x => x.ReportMembershipImages)
                .Where(x => x.ReportId == report.ReportId)
                .Load();

            var previousReport = Context.Reports
                .Include(x => x.ReportMemberships)
                .SingleOrDefault(x => x.LotId == report.LotId &&
                    x.Folio == report.Folio - 1);

            //if (!report.Lot.ModelId.HasValue)
            //{
            //    _validationDictionary.Error("Lote sin modelo.");
            //    return null;
            //}

            var buildingConcepts = Context.BuildingConcepts
                .Where(x => x.ModelId == report.Lot.ModelId)
                .ToList();

            //do not return (skip) building concepts with no progress in the current report
            var buildingConceptsToSkip = new List<int>();

            foreach (var buildingConcept in buildingConcepts)
            {
                var currentReportMembership = report.ReportMemberships
                    .Single(x => x.BuildingConceptId == buildingConcept.BuildingConceptId);

                if (currentReportMembership.Percentage == 0)
                {
                    buildingConceptsToSkip.Add(buildingConcept.BuildingConceptId);
                }
            }

            var output = new DetailOutput
            {
                ReportId = report.ReportId,

                LotId = report.Lot.LotId,
                Percentage = report.GetPercentage(),
                ImageCode = report.ImageCode,
                CreationDate = report.CreationDate,
                LastUpdate = report.LastUpdate,
                PreviousReportPercentage = report.PreviousReportPercentage,
                BuildingConcepts = new List<Domain.ReportContext.Membership.Output>(),
            };

            foreach (var buildingConcept in buildingConcepts)
            {
                if (buildingConceptsToSkip.Any(x => x == buildingConcept.BuildingConceptId)) continue;

                var reportMembership = report.ReportMemberships
                    .Single(x => x.BuildingConceptId == buildingConcept.BuildingConceptId);

                var startingPercentage = 0.0;
                if (previousReport != null)
                {
                    var previousReportMembership = previousReport.ReportMemberships
                        .Single(x => x.BuildingConceptId == buildingConcept.BuildingConceptId);

                    if (previousReportMembership.Percentage == reportMembership.Percentage)
                        continue;

                    startingPercentage = previousReportMembership.Percentage;
                }

                output.BuildingConcepts.Add(new Domain.ReportContext.Membership.Output
                {
                    BuildingConceptId = buildingConcept.BuildingConceptId,
                    Name = buildingConcept.Name,
                    Percentage = reportMembership.GetPercentage(),
                    LastPeriodPercentage = startingPercentage,
                    CriticalRoute = buildingConcept.CriticalRoute,
                });
            }

            return OK(output);
        }


        [AuthorizeRoles(Roles = "Supervisor Admin")]
        [Route("~/api/reports")]
        public HttpResponseMessage Post(PostInput dto)
        {
            if (dto == null || dto.BuildingConcepts.Count == 0)
                return Error("No se encontraron conceptos de construccion.");

            if (!ImageFileExists(dto.FileName))
            {
                if (dto.FileName == "1")
                {
                    dto.FileName = "first-report.png";
                }
                else
                {
                    return Error("Imagen de fachada no encontrada.");
                }
            }

            using (var _transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    var lot = Context.Lots
                        .Include(x => x.PackageMemberships)
                        .Include(x => x.Zone.Settlement)
                        .Include(x => x.Street)
                        .Include(x => x.Model)
                        .Include(x => x.Report.ReportMemberships)
                        .Include(x => x.LastReport.ReportMemberships)
                        .Where(Principal.Lots())
                        .SingleOrDefault(x => x.LotId == dto.LotId);

                    if (lot == null)
                        return Error("Lote no encontrado.");

                    if (lot.CurrentPeriodInitialPercentage >= 100)
                        return Error("Lote terminado");

                    var period = GetCurrentPeriod();

                    Report previousReport = null;

                    var folio = 1;

                    if (lot.ReportId.HasValue)
                    {
                        folio = lot.Report.Folio + 1;

                        if (lot.Report.PeriodId != period.PeriodId)
                        {
                            previousReport = lot.Report;
                        }
                        else if (lot.LastReportId.HasValue)
                        {
                            previousReport = lot.LastReport;
                        }
                    }

                    var settlementLots = Context.Lots
                        .Where(x => x.SettlementId == lot.SettlementId)
                        .ToList();

                    var buildingConcepts = Context.BuildingConcepts
                        .Where(x => x.ModelId == lot.ModelId)
                        .ToList();

                    var packages = Context.Packages
                        .Include(x => x.PackageMemberships)
                        .Where(x => x.ZoneId == lot.ZoneId)
                        .ToList();

                    var institutions = Context.Institutions
                        .Where(x => x.ZoneId == lot.ZoneId)
                        .ToList();


                    var zoneLots = settlementLots
                        .Where(x => x.ZoneId == lot.ZoneId)
                        .ToList();

                    //Is every buildingConcept listed in the dto?
                    foreach (var item in buildingConcepts)
                    {
                        if (!dto.BuildingConcepts.Any(x => x.BuildingConceptId == item.BuildingConceptId))
                            return Error("El concepto de construcción " + item.Name + " es requerido.");
                    }

                    if (!ModelState.IsValid) return null;


                    //is the building concept in the dto equal or higher than the previous value?
                    foreach (var item in dto.BuildingConcepts)
                    {
                        var buildingConcept = buildingConcepts
                            .Single(x => x.BuildingConceptId == item.BuildingConceptId);

                        if (item.Percentage < 0 || item.Percentage > 100)
                            return Error("El porcentaje de " + buildingConcept.Name + " es inválido.");

                        if (previousReport != null)
                        {
                            var previousMembership = previousReport.ReportMemberships
                                .SingleOrDefault(x => x.BuildingConceptId == item.BuildingConceptId);

                            if (previousMembership != null && item.Percentage < previousMembership.Percentage)
                            {
                                return Error($"El porcentaje de {buildingConcept.Name} no puede ser menor a {previousMembership.Percentage}%");
                            }
                        }
                    }

                    Report report;

                    if (lot.Report != null && lot.Report.PeriodId == period.PeriodId)
                    {
                        report = lot.Report;

                        var reportMembershipImages = Context.ReportMembershipImages
                            .Where(x => x.ReportMembership.ReportId == report.ReportId)
                            .ToList();

                        var imagesToDelete = reportMembershipImages
                            .ToList();

                        foreach (var item in imagesToDelete)
                        {
                            Context.Entry(item).State = EntityState.Deleted;
                        }

                        var membershipsToDelete = report.ReportMemberships.ToList();

                        foreach (var item in membershipsToDelete)
                        {
                            Context.Entry(item).State = EntityState.Deleted;
                        }

                        report.SetImage(dto.FileName);
                        report.UpdatePreviousReportPercentage();
                        Context.SaveChanges();
                    }
                    else
                    {
                        report = new Report(lot, Principal.Settlement, Principal.User, period, folio, dto.FileName);
                    }

                    //create or update the report and add the memberships
                    foreach (var item in dto.BuildingConcepts)
                    {
                        var buildingConcept = buildingConcepts
                            .Single(x => x.BuildingConceptId == item.BuildingConceptId);

                        var previousPercentage = 0.0;

                        var membership = new ReportMembership(
                            report,
                            buildingConcept,
                            previousPercentage,
                            item.Percentage,
                            item.Comment);

                        if (item.Images != null && item.Images.Count > 0)
                        {
                            foreach (var image in item.Images)
                            {
                                if (ImageFileExists(image.FileName))
                                {
                                    new ReportMembershipImage(membership, image.FileName);
                                }
                                else
                                {
                                    Logger.Log("ReportMembershipImage.Filename -> no such file!");
                                }
                            }
                        }

                        //report.ReportMemberships.Add(membership);
                    }

                    GetValidationErrors(report.Validate());

                    if (!ModelState.IsValid) return null;

                    var lotPreviousPercentage = lot.Percentage;

                    lot.UpdatePercentageWithReport(report);

                    if (lot.Percentage == lotPreviousPercentage)
                        return Error("No hay cambios en el porcentaje de avance.");

                    /* BEGIN UPDATE ZONE PERCENTAGE --------------------------------------------- */

                    var previousZonePercentage = zoneLots.Count > 0 ? lotPreviousPercentage / zoneLots.Count : 0;
                    var newZonePercentage = zoneLots.Count > 0 ? lot.Percentage / zoneLots.Count : 0;

                    lot.Zone.UpdatePercentage(newZonePercentage - previousZonePercentage);
                    /* END UPDATE ZONE PERCENTAGE */

                    /* BEGIN UPDATE PACKAGE PERCENTAGES --------------------------------------------- */
                    foreach (var package in packages)
                    {
                        var packageMembership = package.PackageMemberships
                            .SingleOrDefault(x => x.LotId == lot.LotId);

                        if (packageMembership == null)
                            continue;

                        var previousPackagePercentage = lotPreviousPercentage / package.PackageMemberships.Count;
                        var newPackagePercentage = lot.Percentage / package.PackageMemberships.Count;

                        //_track(package);

                        package.UpdatePercentage(newPackagePercentage - previousPackagePercentage);
                    }
                    /* END UPDATE PACKAGE PERCENTAGES */

                    /* BEGIN UPDATE INSTITUTION PERCENTAGE --------------------------------------------- */
                    foreach (var institution in institutions)
                    {
                        var institutionLots = new List<Lot>();

                        var institutionPackages = packages
                            .Where(x => x.InstitutionId == institution.InstitutionId)
                            .ToList();

                        foreach (var institutionPackage in institutionPackages)
                        {
                            foreach (var packageMembership in institutionPackage.PackageMemberships)
                            {
                                var institutionLot = zoneLots
                                    .SingleOrDefault(x => x.LotId == packageMembership.LotId);

                                institutionLots.Add(institutionLot);
                            }
                        }

                        var previousInstitutionPercentage = institutionLots.Count > 0 ?
                            lotPreviousPercentage / institutionLots.Count : 0;

                        var newInstitutionPercentage = institutionLots.Count > 0 ?
                            lot.Percentage / institutionLots.Count : 0;

                        institution.UpdatePercentage(newInstitutionPercentage - previousInstitutionPercentage);
                    }
                    /* END UPDATE INSTITUTION PERCENTAGES */

                    /* BEGIN UPDATE SETTLEMENT PERCENTAGE --------------------------------------------- */

                    var previousSettlementPercentage = settlementLots.Count > 0 ? lotPreviousPercentage / settlementLots.Count : 0;
                    var newSettlementPercentage = settlementLots.Count > 0 ? lot.Percentage / settlementLots.Count : 0;

                    lot.Settlement.UpdatePercentage(newSettlementPercentage - previousSettlementPercentage);
                    /* END UPDATE SETTLEMENT PERCENTAGE */



                    Context.SaveChanges();
                    _transaction.Commit();


                    var output = new ReportOutput
                    {
                        ReportId = report.ReportId,

                        CreationDate = report.CreationDate,
                        LastUpdate = report.LastUpdate,

                        LotId = lot.LotId,
                        LotPercentage = lot.GetPercentage(),
                        LotStatus = lot.Status,

                        ZoneId = lot.Zone.ZoneId,
                        ZonePercentage = lot.Zone.GetPercentage(),

                        SettlementId = lot.Zone.Settlement.SettlementId,
                        SettlementPercentage = lot.Zone.Settlement.GetPercentage(),

                        Packages = new List<Domain.PackageContext.Output>(),
                        Institutions = new List<Domain.InstitutionContext.Output>(),
                    };

                    foreach (var packageMembership in lot.PackageMemberships)
                    {
                        var package = packages
                            .Single(x => x.PackageId == packageMembership.PackageId);

                        output.Packages.Add(new Domain.PackageContext.Output
                        {
                            PackageId = packageMembership.PackageId,
                            InstitutionId = package.InstitutionId,

                            Name = package.Name,
                            Percentage = package.GetPercentage(),
                        });
                    }

                    foreach (var institution in institutions)
                    {
                        output.Institutions.Add(new Domain.InstitutionContext.Output
                        {
                            InstitutionId = institution.InstitutionId,
                            Name = institution.Name,
                            Percentage = institution.GetPercentage(),
                        });
                    }

                    App.Instance.EventLog.Push($"Report " +
                        $" | {lot.Percentage}%" +
                        $" | {lot.Zone.Name}" +
                        $" | {lot.LotId }");

                    //var notification = new Notification(
                    //    lot.Zone.Settlement,
                    //    "Nuevo reporte en " + lot.Zone.Name,
                    //    lot.LotId,
                    //    NotificationContext.NotificationTypes.None);

                    //var admins = _context.Admins
                    //    .Include(x => x.User)
                    //    .Where(x => x.SettlementId == lot.SettlementId)
                    //    .ToList();

                    //if (admins.Count > 0)
                    //    notification.AddAdmins(admins);

                    //var supervisors = _context.Supervisors
                    //    .Include(x => x.User)
                    //    .Where(x => x.ZoneId == lot.ZoneId)
                    //    .ToList();

                    //if (supervisors.Count > 0)
                    //    notification.AddSupervisors(supervisors);

                    //var residents = _context.Residents
                    //    .Include(x => x.User)
                    //    .Where(x => x.LotId == lot.LotId)
                    //    .ToList();

                    //if (residents.Count > 0)
                    //    notification.AddResidents(residents);

                    //_context.Entry(notification).State = EntityState.Added;

                    //_context.SaveChanges();

                    //_pushNotificationService.Send(notification);



                    return OK(output);
                }
                catch (Exception ex)
                {
                    Logger.Log(ex);
                    return Error();
                }
            }
        }
    }
}