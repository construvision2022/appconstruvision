﻿using Domain.InstitutionContext;
using Domain.PeriodContext;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class InstitutionsController : BaseApiController
    {
        public InstitutionsController()
        {
            Message404 = "Institución no encontrada.";
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/institutions/{id:int:min(1)}/history")]
        public async Task<HttpResponseMessage> GetHistory(int id)
        {
            var institution = await Context.Institutions
                .Where(Principal.Institutions())
                .SingleOrDefaultAsync(x => x.InstitutionId == id);

            if (institution == null)
                return Error404();

            var institutionPeriods = await Context.InstitutionPeriods
                .Include(x => x.Period)
                .Where(x => x.InstitutionId == institution.InstitutionId)
                .OrderByDescending(x => x.PeriodId)
                .ToListAsync();

            var output = institutionPeriods
                .Select(x => x.ToHistoryOutput())
                .ToList();

            return OK(output);
        }
    }
}