﻿using Domain;
using Domain.PackageContext;
using Domain.PeriodContext;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class PackagesController : BaseApiController
    {
        public PackagesController()
        {
            Message404 = "Paquete no encontrado.";
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/packages/{id:int:min(1)}/history")]
        public async Task<HttpResponseMessage> GetHistory(int id)
        {
            var package = await Context.Packages
                .Where(Principal.Packages())
                .SingleOrDefaultAsync(x => x.PackageId == id);

            if (package == null)
                return Error404();

            var packagePeriods = await Context.PackagePeriods
                .Include(x => x.Period)
                .Where(x => x.PackageId == package.PackageId)
                .OrderByDescending(x => x.Period.StartDate)
                .ToListAsync();

            var output = packagePeriods
                .Select(x => x.ToHistoryOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin")]
        [Route("~/api/zone/{id:int:min(1)}/packages/select")]
        public HttpResponseMessage GetSelectPackages(int id)
        {
            var zone = Context.Zones
                .Where(Principal.Zones())
                .SingleOrDefault(x => x.ZoneId == id);

            if (zone == null)
                return Error("Cerrada no encontrada");

            var packages = Context.Packages
                .Include(x => x.Institution)
                .Where(x => x.ZoneId == zone.ZoneId)
                .OrderBy(x => x.Name)
                .ToList();

            var output = new SelectOutput();

            foreach (var package in packages)
            {
                var item = new SelectItem
                {
                    PackageId = package.PackageId,
                    PackageName = package.Name,
                    InstitutionName = package.Institution.Name,
                    InstitutionId = package.InstitutionId,
                };

                if (package.Institution.Name == "RUV")
                    output.RUV.Add(item);
                else if (package.Institution.Name == "Bancario")
                    output.Bank.Add(item);
            }

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/packages/{id:int:min(1)}")]
        public HttpResponseMessage Get(int id)
        {
            var package = Context.Packages
                .Where(Principal.Packages())
                .SingleOrDefault(x => x.PackageId == id);

            if (package == null)
                return Error404();

            var output = package.ToOutput();
            //output.LastPeriods = new System.Collections.Generic.List<HistoryOutput>();
            //output.Memberships = new System.Collections.Generic.List<int>();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/zone/{id:int:min(1)}/packages")]
        public async Task<HttpResponseMessage> Post(int id, PostInput dto)
        {
            if (dto == null)
                return Error();

            var zone = await Context.Zones
                .Where(Principal.Zones())
                .SingleOrDefaultAsync(x => x.ZoneId == id);

            if (zone == null)
                return Error("Cerrada no encontrada");

            var institutionName = dto.Institution == 1 ?
                "RUV" : "Bancario";

            var isNameInUse = await Context.Packages
                .AnyAsync(x => x.ZoneId == zone.ZoneId
                    && x.Institution.Name == institutionName
                    && x.Name.Equals(dto.Name, StringComparison.OrdinalIgnoreCase));

            if (isNameInUse)
                return Error("Name", "Nombre en uso");


            var institution = await Context.Institutions
                .SingleOrDefaultAsync(x => x.ZoneId == zone.ZoneId
                    && x.Name == institutionName);

            if (institution == null)
                institution = new Institution(zone, institutionName, 0);

            var package = new Package(zone, institution, dto.Name);

            GetValidationErrors(package.Validate());

            if (!ModelState.IsValid)
                return Error();

            await Context.SaveChangesAsync();

            var output = package.ToOutput();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/packages/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> Put(int id, PutInput dto)
        {
            if (dto == null)
                return Error();

            var package = await Context.Packages
                .Where(Principal.Packages())
                .SingleOrDefaultAsync(x => x.PackageId == id);

            if (package == null)
                return Error("Paquete no encontrado");

            var isNameInUse = await Context.Packages
                .AnyAsync(x => x.ZoneId == package.ZoneId
                    && x.InstitutionId == package.InstitutionId
                    && x.PackageId != package.PackageId
                    && x.Name.Equals(dto.Name, StringComparison.OrdinalIgnoreCase));

            if (isNameInUse)
                return Error("Name", "Nombre en uso");

            package.Update(dto.Name);

            GetValidationErrors(package.Validate());

            if (!ModelState.IsValid)
                return Error();

            await Context.SaveChangesAsync();

            return OK(true);
        }
    }
}
