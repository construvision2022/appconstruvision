﻿using Domain;
using Domain.SettlementContext;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class SettlementsController : BaseApiController
    {
        public SettlementsController()
        {
            Message404 = "Residencial no encontrado";
        }


        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller Manager")]
        [Route("~/api/settlements")]
        public HttpResponseMessage GetAll()
        {
            var output = Context.Settlements
                .OrderByDescending(x => x.CreationDate)
                .Where(Principal.Settlements())
                .AsEnumerable()
                .Select(x => new Output
                {
                    SettlementId = x.SettlementId,

                    Description = x.Description,
                    Name = x.Name,
                    Alias = x.Alias,
                    RFC = x.RFC,
                    ZipCode = x.ZipCode,
                    ImageCode = x.ImageCode,

                    Percentage = x.GetPercentage(),
                    StartingPercentage = x.GetStartingPercentage(),

                }).ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller Manager")]
        [Route("~/api/settlements/{id:int:min(1)}")]
        public HttpResponseMessage GetOne(int id)
        {
            var settlement = Context.Settlements
                .Where(Principal.Settlements())
                .SingleOrDefault(x => x.SettlementId == id);

            if (settlement == null)
                return Error404();

            var lastPeriods = Context.SettlementPeriods
                .Include(x => x.Period)
                .Where(x => x.SettlementId == settlement.SettlementId)
                .OrderByDescending(x => x.PeriodId)
                .AsEnumerable()
                .Take(3)
                .Select(x => new Domain.PeriodContext.HistoryOutput
                {
                    PeriodId = x.PeriodId,
                    StartingPercentage = x.GetStartingPercentage(),
                    EndingPercentage = x.GetEndingPercentage(),
                    StartDate = x.Period.StartDate,
                    EndDate = x.Period.EndDate,
                })
                .ToList();

            var output = new Output
            {
                SettlementId = settlement.SettlementId,

                Description = settlement.Description,
                Name = settlement.Name,
                Alias = settlement.Alias,
                RFC = settlement.RFC,
                ZipCode = settlement.ZipCode,
                ImageCode = settlement.ImageCode,

                Percentage = settlement.GetPercentage(),
                StartingPercentage = settlement.GetStartingPercentage(),
                LastPeriods = lastPeriods
            };

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller Manager")]
        [Route("~/api/settlements/{id:int:min(1)}/history")]
        public async Task<HttpResponseMessage> GetHistory(int id)
        {
            var settlement = await Context.Settlements
                .Where(Principal.Settlements())
                .SingleOrDefaultAsync(x => x.SettlementId == id);

            if (settlement == null)
                return Error404();

            var settlementPeriods = await Context.SettlementPeriods
                .Include(x => x.Period)
                .Where(x => x.SettlementId == settlement.SettlementId)
                .OrderByDescending(x => x.PeriodId)
                .ToListAsync();

            var output = settlementPeriods
                .Select(x => x.ToHistoryOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/settlements")]
        public async Task<HttpResponseMessage> Post(PostInput dto)
        {
            try
            {
                var business = await Context.Businesses
                    .Include(x => x.Tennant)
                    .SingleOrDefaultAsync(x => x.BusinessId == Principal.Business.BusinessId);

                var user = await Context.Users
                    .SingleOrDefaultAsync(x => x.UserId == Principal.User.UserId);

                var isNameInUse = await Context.Settlements
                    .Where(Principal.Settlements())
                    .AnyAsync(x => x.Name.Equals(dto.Name, StringComparison.OrdinalIgnoreCase));

                if (isNameInUse)
                    return Error("Name", "Ya existe un residencial con ese nombre en " + business.Name);

                var settlement = new Settlement(
                    business,
                    user,
                    dto.Name,
                    dto.Name,
                    "Descripción",
                    "RFC",
                    "83100");

                GetValidationErrors(business.Validate());

                if (!ModelState.IsValid)
                    return Error();

                var admin = new Admin(user, settlement);

                await Context.SaveChangesAsync();

                var output = business.ToOutput();

                return OK(output);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
                return Error();
            }
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin")]
        [Route("~/api/settlements/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> Put(int id, PutInput dto)
        {
            try
            {
                var settlement = await Context.Settlements
                    .Include(x => x.Business.Tennant)
                    .Where(Principal.Settlements())
                    .SingleOrDefaultAsync(x => x.SettlementId == id);

                settlement.Update(dto.Name);
                GetValidationErrors(settlement.Validate());

                if (!ModelState.IsValid)
                    return Error();

                await Context.SaveChangesAsync();

                return OK(true);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
                return Error();
            }
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin Manager")]
        [Route("~/api/settlements/{id:int:min(1)}/image")]
        public async Task<HttpResponseMessage> PutImage(int id, PutImageInput dto)
        {
            try
            {
                var settlement = await Context.Settlements
                    .Include(x => x.Business.Tennant)
                    .Where(Principal.Settlements())
                    .SingleOrDefaultAsync(x => x.SettlementId == id);

                settlement.UpdateImage(dto.ImageCode);
                GetValidationErrors(settlement.Validate());

                if (!ModelState.IsValid)
                    return Error();

                await Context.SaveChangesAsync();

                return OK(true);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
                return Error();
            }
        }
    }
}