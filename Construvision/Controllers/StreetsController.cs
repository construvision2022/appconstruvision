﻿using Domain;
using Domain.StreetContext;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class StreetsController : BaseApiController
    {
        public StreetsController()
        {
            Message404 = "Calle no encontrada.";
        }


        [AuthorizeRoles(Roles = "Admin Supervisor SalesAdmin Seller")]
        [Route("~/api/streets")]
        public HttpResponseMessage GetAll()
        {
            var output = Context.Streets
                .Where(Principal.Streets())
                .OrderByDescending(x => x.StreetId)
                .Select(x => new Output
                {
                    StreetId = x.StreetId,

                    Name = x.Name,
                    ZoneName = x.Zone.Name,

                    SettlementId = x.SettlementId,
                    ZoneId = x.ZoneId,
                }).ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin Supervisor SalesAdmin Seller")]
        [Route("~/api/streets/{id:int:min(1)}/lots")]
        public HttpResponseMessage GetLots(int id)
        {
            var street = Context.Streets
                .Where(Principal.Streets())
                .SingleOrDefault(x => x.StreetId == id);

            if (street == null)
                return Error404();

            var output = Context.Lots
                .Where(Principal.Lots())
                .Where(x => x.StreetId == street.StreetId)
                .Select(x => new Domain.LotContext.Raw
                {
                    LotId = x.LotId,
                    LotNumber = x.LotNumber,
                })
                .ToList()
                .OrderBy(x => x.LotNumber.PadLeft(4, '0'))
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin Supervisor SalesAdmin Seller")]
        [Route("~/api/streets/{id:int:min(1)}")]
        public HttpResponseMessage GetOne(int id)
        {
            var street = Context.Streets
                .Include(x => x.Zone)
                .Where(Principal.Streets())
                .SingleOrDefault(x => x.StreetId == id);

            if (street == null)
                return Error404();

            var output = street.ToOutput();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin")]
        [Route("~/api/streets")]
        public async Task<HttpResponseMessage> Post(PostInput dto)
        {
            var settlement = Principal.Settlement;

            var zone = await Context.Zones
                .SingleOrDefaultAsync(x =>
                    x.SettlementId == settlement.SettlementId &&
                    x.ZoneId == dto.ZoneId);

            if (zone == null)
                return Error(string.Empty, "Cerrada no encontrada");

            if (!ModelState.IsValid)
                return Error();

            var nameAlreadyExists = await Context.Streets
                .AnyAsync(x => x.ZoneId == zone.ZoneId
                    && x.Name.Equals(dto.Name, StringComparison.OrdinalIgnoreCase));

            if (nameAlreadyExists)
                return Error("Name", "Ya existe una calle con ese nombre.");

            var street = new Street(settlement, zone, dto.Name);

            GetValidationErrors(street.Validate());

            if (!ModelState.IsValid)
                return Error();

            await Context.SaveChangesAsync();

            return OK(street.ToOutput());
        }

        [AuthorizeRoles(Roles = "Admin")]
        [Route("~/api/streets/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> Put(int id, PutInput dto)
        {
            var settlement = Principal.Settlement;

            var street = await Context.Streets
                .SingleOrDefaultAsync(x => x.SettlementId == settlement.SettlementId
                    && x.StreetId == id);

            if (street == null)
                return Error404();

            var nameAlreadyExists = Context.Streets
                .Any(x => x.ZoneId == street.ZoneId
                    && x.StreetId != street.StreetId
                    && x.Name.Equals(dto.Name, StringComparison.OrdinalIgnoreCase));

            if (nameAlreadyExists)
                return Error("Name", "Ya existe una calle con ese nombre.");

            street.Update(dto.Name);

            GetValidationErrors(street.Validate());

            if (!ModelState.IsValid)
                return Error();

            Context.SaveChanges();

            return OK(true);
        }
    }
}