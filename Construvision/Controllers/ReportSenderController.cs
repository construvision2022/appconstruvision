﻿using Domain;
using Domain.CreatorContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class ReportSenderController : BaseApiController
    {
        public ReportSenderController()
        {
            Message404 = "No encontrado";
        }

        [AuthorizeRoles(Roles = "Manager")]
        [Route("~/api/send-report")]
        public async Task<HttpResponseMessage> Post()
        {
            var manager = await Context.Managers
                .Include(x => x.User)
                .Include(x => x.Business.Tennant)
                .SingleAsync(x => x.ManagerId == Principal.Id);

            var currentPeriod = await GetCurrentPeriodAsync();

            new ExcelExport(manager.Business, currentPeriod.StartDate, currentPeriod.EndDate);

            await Context.SaveChangesAsync();

            _ = new ScriptServices.GeneratePeriodExcel().Run();

            return OK(true);
        }
    }
}