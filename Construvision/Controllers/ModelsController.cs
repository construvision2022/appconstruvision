﻿
using Domain;
using Domain.ModelContext;
using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class ModelController : BaseApiController
    {
        public ModelController()
        {
            Message404 = "Modelo no encontrado.";
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/models")]
        public HttpResponseMessage GetAll()
        {
            var output = Context.Models
                .Include(x => x.Zone)
                .Where(Principal.Models())
                .OrderBy(x => x.Zone.Name)
                .ThenBy(x => x.Name)
                .AsEnumerable()
                .Select(x => x.ToOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/models/{id:int:min(1)}")]
        public HttpResponseMessage GetOne(int id)
        {
            var model = Context.Models
                .Include(x => x.Zone)
                .Where(Principal.Models())
                .SingleOrDefault(x => x.ModelId == id);

            if (model == null)
                return Error404();

            var output = model.ToOutput();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/models/")]
        public async Task<HttpResponseMessage> Post(PostInput dto)
        {
            if (dto == null)
                return Error();

            if (dto.BuildingConcepts.Count == 0)
                return Error("Debes crear conceptos de construcción");

            var percentage = dto.BuildingConcepts
                .Sum(x => x.Percentage);

            if (percentage != 1)
                return Error("El porcentaje no suma 1");

            var zone = await Context.Zones
                .Include(x => x.Settlement)
                .SingleOrDefaultAsync(x =>
                    x.SettlementId == Principal.Settlement.SettlementId &&
                    x.ZoneId == dto.ZoneId);

            if (zone == null)
                return Error("Zona no encontrada.");

            var nameAlreadyExists = await Context.Models
                .AnyAsync(x => x.SettlementId == Principal.Settlement.SettlementId
                    && x.ZoneId == zone.ZoneId
                    && x.Name.Equals(dto.Name, StringComparison.OrdinalIgnoreCase));

            if (nameAlreadyExists)
                return Error("Name", "Ya existe un modelo con ese nombre.");

            var model = new Model(
                zone,
                dto.Name,
                dto.Color,
                dto.Area,
                dto.GroundArea,
                dto.Floors);

            GetValidationErrors(model.Validate());

            if (!ModelState.IsValid)
                return Error();

            foreach (var item in dto.BuildingConcepts)
            {
                var buildingConcept = new BuildingConcept(
                    model,
                    item.Name.Trim(),
                    (double)item.Percentage,
                    item.Order,
                    item.CriticalRoute);

                var error = buildingConcept.Validate();

                if (!string.IsNullOrWhiteSpace(error))
                    return Error($"{buildingConcept.Name} - {error}");
            }

            await Context.SaveChangesAsync();

            var output = model.ToOutput();

            return OK(output);
        }


        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/models/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> Put(int id, PutInput dto)
        {
            var model = await Context.Models
                .Include(x => x.Zone)
                .SingleOrDefaultAsync(x => x.SettlementId == Principal.Settlement.SettlementId
                    && x.ModelId == id);

            if (model == null)
                return Error404();

            var nameAlreadyExists = await Context.Models
                .AnyAsync(x => x.SettlementId == Principal.Settlement.SettlementId
                    && x.ZoneId == model.ZoneId
                    && x.ModelId != model.ModelId
                    && x.Name.Equals(dto.Name, StringComparison.OrdinalIgnoreCase));

            if (nameAlreadyExists)
                return Error("Name", "Ya existe un modelo con ese nombre.");

            var updatedAreas = dto.Area != model.Area || dto.GroundArea != model.GroundArea;

            model.Update(dto);

            GetValidationErrors(model.Validate());

            if (!ModelState.IsValid)
                return Error();

            if (updatedAreas)
            {
                await Context.Lots
                    .Where(x => x.ModelId == model.ModelId
                        && x.HasModelArea)
                    .LoadAsync();

                foreach (var lot in model.Zone.Lots)
                {
                    if (!lot.HasModelArea)
                        continue;

                    lot.Update(
                        lot.OfficialNumber,
                        lot.LotNumber,
                        model.GroundArea,
                        model.Area);
                }
            }

            if (model.DefaultData)
            {
                if (dto.BuildingConcepts.Count == 0)
                    return Error("Debes crear conceptos de construcción");

                var percentage = dto.BuildingConcepts
                    .Sum(x => x.Percentage);

                if (percentage != 1)
                    return Error("El porcentaje no suma 1");

                foreach (var item in dto.BuildingConcepts)
                {
                    var buildingConcept = new BuildingConcept(
                        model,
                        item.Name.Trim(),
                        (double)item.Percentage,
                        item.Order,
                        item.CriticalRoute);

                    var error = buildingConcept.Validate();

                    if (!string.IsNullOrWhiteSpace(error))
                        return Error($"{buildingConcept.Name} - {error}");
                }

                model.UpdateDefaultData(false);
            }

            await Context.SaveChangesAsync();

            return OK(true);
        }

        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/models/{id:int:min(1)}/pdf")]
        public HttpResponseMessage PutPDF(int id, PutFileInput dto)
        {
            try
            {
                var basePath = HttpContext.Current.Server.MapPath("~/Content/files");
                var filePath = Path.Combine(basePath, dto.FileName);

                if (!File.Exists(filePath))
                    return Error("PDF no encontrado");

                var model = Context.Models
                    .SingleOrDefault(x => x.SettlementId == Principal.Settlement.SettlementId
                        && x.ModelId == id);

                if (model == null)
                    return Error404();

                model.SetFile(dto.FileName);

                Context.SaveChanges();

                return OK(true);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return Error();
            }
        }

        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/models/{id:int:min(1)}/image")]
        public HttpResponseMessage PutImage(int id, PutFileInput dto)
        {
            try
            {
                var basePath = HttpContext.Current.Server.MapPath("~/Content/images/uploads");
                var filePath = Path.Combine(basePath, dto.FileName);

                if (!File.Exists(filePath))
                    return Error("Imagen no encontrada");

                var model = Context.Models
                    .SingleOrDefault(x => x.SettlementId == Principal.Settlement.SettlementId
                        && x.ModelId == id);

                if (model == null)
                    return Error404();

                model.SetImage(dto.FileName);

                Context.SaveChanges();

                return OK(true);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return Error();
            }
        }

        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/models/{id:int:min(1)}/reset")]
        public async Task<HttpResponseMessage> PutReset(int id, PutResetInput dto)
        {
            if (dto == null || !dto.Reset)
                return Error("OK");

            var model = await Context.Models
                .SingleOrDefaultAsync(x => x.SettlementId == Principal.Settlement.SettlementId
                    && x.ModelId == id);

            if (model == null)
                return Error404();

            var anyLotModelHasReports = await Context.Reports
                .AnyAsync(x => x.Lot.ModelId == model.ModelId);

            if (anyLotModelHasReports)
                return Error("No puedes reiniciar un modelo con lotes con reportes.");

            await Context.BuildingConcepts
                .Where(x => x.ModelId == model.ModelId)
                .LoadAsync();

            model.UpdateDefaultData(true);

            Context.BuildingConcepts.RemoveRange(model.BuildingConcepts);

            await Context.SaveChangesAsync();

            return OK(true);
        }

        [AuthorizeRoles(Roles = "Admin")]
        [Route("~/api/models/{id:int:min(1)}")]
        public HttpResponseMessage Delete(int id)
        {
            var model = Context.Models
                .Include(x => x.BuildingConcepts)
                .SingleOrDefault(x => x.SettlementId == Principal.Settlement.SettlementId
                    && x.ModelId == id);

            if (model == null)
                return Error404();

            var modelHasLots = Context.Lots
                .Any(x => x.ModelId == model.ModelId);

            if (modelHasLots)
                return Error("No puedes eliminar modelos con lotes.");

            Context.BuildingConcepts.RemoveRange(model.BuildingConcepts);
            Context.Models.Remove(model);

            Context.SaveChanges();

            return OK(true);
        }
    }
}