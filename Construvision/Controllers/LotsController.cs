﻿using Domain;
using Domain.LotContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class LotsController : BaseApiController
    {
        public LotsController()
        {
            Message404 = "Lote no encontrado";
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin Supervisor Seller")]
        [Route("~/api/lots")]
        public HttpResponseMessage Get()
        {
            var output = Context.Lots
                .Include(x => x.LastReport)
                .Include(x => x.Report)
                .Include(x => x.Zone)
                .Include(x => x.Model)
                .OrderBy(x => x.ZoneId)
                .ThenBy(x => x.BlockId)
                .ThenBy(x => x.LotNumber)
                .Where(Principal.Lots())
                .AsEnumerable()
                .Select(x => x.ToOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin")]
        [Route("~/api/admin/lots")]
        public async Task<HttpResponseMessage> GetAdmin()
        {
            var lots = await Context.Lots
                .Include(x => x.LastReport)
                .Include(x => x.Report)
                .Include(x => x.Zone)
                .Include(x => x.Street)
                .Include(x => x.Model)
                .Include(x => x.Block)
                .Include(x => x.PackageMemberships)
                .OrderBy(x => x.ZoneId)
                .ThenBy(x => x.BlockId)
                .ThenBy(x => x.LotNumber)
                .Where(x => x.SettlementId == Principal.Settlement.SettlementId)
                .ToListAsync();

            await Context.Packages
                .Include(x => x.Institution)
                .Where(x => x.Zone.SettlementId == Principal.Settlement.SettlementId)
                .LoadAsync();

            var output = lots
                .OrderBy(x => x.LotNumber.PadLeft(4, '0'))
                .Select(x => x.ToOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin")]
        [Route("~/api/admin/zones/{id:int:min(1)}/lots")]
        public async Task<HttpResponseMessage> GetZoneLots(int id)
        {
            var zone = await Context.Zones
                .Where(Principal.Zones())
                .SingleOrDefaultAsync(x => x.ZoneId == id);

            if (zone == null)
                return Error("Cerrada no encontrada.");

            var lots = await Context.Lots
                .Include(x => x.LastReport)
                .Include(x => x.Report)
                .Include(x => x.Street)
                .Include(x => x.Model)
                .Include(x => x.Block)
                .Include(x => x.PackageMemberships)
                .Where(x => x.ZoneId == zone.ZoneId)
                .ToListAsync();

            await Context.Packages
                .Include(x => x.Institution)
                .Where(x => x.ZoneId == zone.ZoneId)
                .LoadAsync();

            var output = lots
                .OrderBy(x => x.LotNumber.PadLeft(4, '0'))
                .Select(x => x.ToOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/lots/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> Get(int id, bool packages = false)
        {
            var lot = await Context.Lots
                .Include(x => x.LastReport)
                .Include(x => x.Report)
                .Include(x => x.Zone)
                .Include(x => x.Street)
                .Include(x => x.Model)
                .Include(x => x.Block)
                .Where(Principal.Lots())
                .Where(x => x.LotId == id)
                .SingleOrDefaultAsync();

            if (packages)
            {
                await Context.PackageMemberships
                    .Include(x => x.Package.Institution)
                    .Where(x => x.LotId == lot.LotId)
                    .LoadAsync();
            }

            var output = lot.ToOutput();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/lots/{id:int:min(1)}/status")]
        public HttpResponseMessage GetStatus(int id)
        {
            var lot = Context.Lots
               .Include(x => x.Model.BuildingConcepts)
               .Include(x => x.Street)
               .Include(x => x.Report.ReportMemberships)
               .Include(x => x.LastReport.ReportMemberships)
               .Where(Principal.Lots())
               .SingleOrDefault(x => x.LotId == id);

            if (lot == null)
                return Error404();

            var report = lot.Report;
            var previousPeriodReport = lot.LastReport;

            var period = GetCurrentPeriod();

            var buildingConcepts = lot.Model.BuildingConcepts
                .OrderBy(x => x.Order)
                .ToList();

            var output = new StatusOutput
            {
                CompletePercentage = report != null ? report.GetPercentage() : lot.GetPercentage(),
                Concepts = new List<Domain.ReportContext.Membership.Output>()
            };

            foreach (var buildingConcept in buildingConcepts)
            {
                var percentage = 0.0;
                var lastPeriodPercentage = 0.0;

                if (report != null)
                {
                    var reportMembership = report.ReportMemberships
                        .SingleOrDefault(x => x.BuildingConceptId == buildingConcept.BuildingConceptId);

                    percentage = reportMembership != null ? reportMembership.Percentage : 0;

                    if (report.PeriodId != period.PeriodId)
                    {
                        lastPeriodPercentage = reportMembership != null ? reportMembership.Percentage : 0;
                    }
                    else
                    {
                        if (previousPeriodReport != null)
                        {
                            var previousReportMembership = previousPeriodReport.ReportMemberships
                                .SingleOrDefault(x => x.BuildingConceptId == buildingConcept.BuildingConceptId);

                            lastPeriodPercentage = previousReportMembership != null ? previousReportMembership.Percentage : 0;
                        }
                    }
                }




                output.Concepts.Add(new Domain.ReportContext.Membership.Output
                {
                    BuildingConceptId = buildingConcept.BuildingConceptId,
                    Name = buildingConcept.Name,
                    Percentage = percentage,
                    LastPeriodPercentage = lastPeriodPercentage,
                    CriticalRoute = buildingConcept.CriticalRoute,
                });
            }

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/lots/{id:int:min(1)}/reports")]
        public HttpResponseMessage GetReports(int id)
        {
            var lot = Context.Lots
                .SingleOrDefault(x =>
                    x.LotId == id &&
                    x.SettlementId == Principal.Settlement.SettlementId);

            if (lot == null)
                return Error404();

            var output = Context.Reports
                .Where(x => x.LotId == lot.LotId)
                .OrderByDescending(x => x.Folio)
                .AsEnumerable()
                .Select(x => new Domain.ReportContext.Output
                {
                    ReportId = x.ReportId,
                    LotId = x.LotId,

                    ImageCode = x.ImageCode,
                    Percentage = x.GetPercentage(),
                    LastUpdate = x.LastUpdate,
                    CreationDate = x.CreationDate,
                    PreviousReportPercentage = x.PreviousReportPercentage,
                })
                .ToList();

            return OK(output);
        }




        [AuthorizeRoles(Roles = "Admin SalesAdmin")]
        [Route("~/api/lots")]
        public async Task<HttpResponseMessage> Post(PostInput dto)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    var zone = await Context.Zones
                        .Where(Principal.Zones())
                        .SingleOrDefaultAsync(x => x.ZoneId == dto.ZoneId);

                    if (zone == null)
                        return Error("Cerrada no encontrada.");

                    var street = await Context.Streets
                        .Where(Principal.Streets())
                        .SingleOrDefaultAsync(x => x.ZoneId == zone.ZoneId
                            && x.StreetId == dto.StreetId);

                    if (street == null)
                        return Error("Calle no encontrada.");

                    var block = await Context.Blocks
                        .Where(Principal.Blocks())
                        .SingleOrDefaultAsync(x => x.ZoneId == zone.ZoneId
                            && x.BlockId == dto.BlockId);

                    if (block == null)
                        return Error("Manzana no encontrada.");

                    var model = await Context.Models
                        .Where(Principal.Models())
                        .SingleOrDefaultAsync(x => x.ZoneId == zone.ZoneId
                            && x.ModelId == dto.ModelId);

                    if (model == null)
                        return Error("Modelo no encontrado.");

                    var bankPackage = await Context.Packages
                        .Where(Principal.Packages())
                        .SingleOrDefaultAsync(x => x.ZoneId == zone.ZoneId
                            && x.PackageId == dto.Bank);

                    if (bankPackage == null)
                        return Error("Paquete bancario no encontrado.");

                    var ruvPackage = await Context.Packages
                        .Where(Principal.Packages())
                        .SingleOrDefaultAsync(x => x.ZoneId == zone.ZoneId
                            && x.PackageId == dto.RUV);

                    if (ruvPackage == null)
                        return Error("Paquete RUV no encontrado.");

                    //var isLotNumberInUse = await Context.Lots
                    //    .Where(Principal.Lots())
                    //    .AnyAsync(x => x.ZoneId == zone.ZoneId
                    //        && x.BlockId == block.BlockId
                    //        && x.LotNumber == dto.LotNumber);

                    //if (isLotNumberInUse)
                    //    return Error("LotNumber", $"El número de lote " +
                    //        $"{dto.LotNumber} ya existe en la manzana {block.Name} de {zone.Name}");

                    //if (!string.IsNullOrWhiteSpace(dto.OfficialNumber))
                    //{
                    //    var isOfficialNumberInUse = await Context.Lots
                    //        .Where(Principal.Lots())
                    //        .AnyAsync(x => x.ZoneId == zone.ZoneId
                    //            && x.StreetId == dto.StreetId
                    //            && x.OfficialNumber == dto.OfficialNumber);

                    //    if (isOfficialNumberInUse)
                    //        return Error("OfficialNumber", $"El número oficial " +
                    //            $"{dto.OfficialNumber} ya existe en la calle {street.Name}");
                    //}

                    var settlement = await Context.Settlements
                        .Include(x => x.Business.Tennant)
                        .SingleAsync(x => x.SettlementId == Principal.Settlement.SettlementId);

                    var lot = new Lot(
                        settlement,
                        street,
                        zone,
                        model,
                        block,
                        dto.GroundArea,
                        dto.ConstructionArea,
                        dto.OfficialNumber,
                        dto.LotNumber,
                        Status.Pending,
                        dto.SaleStatus);

                    GetValidationErrors(lot.Validate());

                    new PackageMembership(lot, ruvPackage);
                    new PackageMembership(lot, bankPackage);

                    if (!ModelState.IsValid)
                        return Error();

                    await Context.SaveChangesAsync();

                    await Resync(settlement);

                    transaction.Commit();

                    var output = lot.ToOutput();

                    return OK(output);
                }
                catch (Exception ex)
                {
                    Logger.Log(ex.ToString());
                    return Error();
                }
            }
        }

        [AuthorizeRoles(Roles = "Admin Supervisor SalesAdmin")]
        [Route("~/api/lots/{id:int:min(1)}/dtu")]
        public HttpResponseMessage PostDtu(int id)
        {
            var lot = Context.Lots
                .Where(Principal.Lots())
                .SingleOrDefault(x => x.LotId == id);

            if (lot == null)
                return Error404();

            lot.SetDtu();

            Context.SaveChanges();

            return OK(true);

        }





        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/lots/{id:int:min(1)}/salestatus")]
        public HttpResponseMessage PutSaleStatus(int id, PutSaleStatusInput dto)
        {
            var lot = Context.Lots
                .Where(Principal.Lots())
                .SingleOrDefault(x => x.LotId == id);

            if (lot == null)
                return Error404();

            var updated = lot.PutSaleStatus(dto.SaleStatus, dto.ActionDate);

            if (!updated)
                return Error("No se puede cambiar el estado");

            Context.SaveChanges();

            return OK(true);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin")]
        [Route("~/api/lots/{id:int:min(1)}/finish")]
        public HttpResponseMessage PutFinishLot(int id)
        {
            using (var _transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    var lot = Context.Lots
                        .Include(x => x.PackageMemberships)
                        .Include(x => x.Zone.Settlement)
                        .Include(x => x.Street)
                        .Include(x => x.Model)
                        .Include(x => x.Report.ReportMemberships)
                        .Include(x => x.LastReport.ReportMemberships)
                        .Where(Principal.Lots())
                        .SingleOrDefault(x => x.LotId == id);

                    if (lot == null)
                        return Error404();

                    if (lot.CurrentPeriodInitialPercentage >= 100)
                        return Error("Lote terminado");

                    var period = GetCurrentPeriod();

                    Report previousReport = null;

                    var folio = 1;

                    if (lot.ReportId.HasValue)
                    {
                        folio = lot.Report.Folio + 1;

                        if (lot.Report.PeriodId != period.PeriodId)
                        {
                            previousReport = lot.Report;
                        }
                        else if (lot.LastReportId.HasValue)
                        {
                            previousReport = lot.LastReport;
                        }
                    }

                    var settlementLots = Context.Lots
                        .Where(x => x.SettlementId == lot.SettlementId)
                        .ToList();

                    var buildingConcepts = Context.BuildingConcepts
                        .Where(x => x.ModelId == lot.ModelId)
                        .ToList();

                    var packages = Context.Packages
                        .Include(x => x.PackageMemberships)
                        .Where(x => x.ZoneId == lot.ZoneId)
                        .ToList();

                    var institutions = Context.Institutions
                        .Where(x => x.ZoneId == lot.ZoneId)
                        .ToList();


                    var zoneLots = settlementLots
                        .Where(x => x.ZoneId == lot.ZoneId)
                        .ToList();

                    Report report;

                    if (lot.Report != null && lot.Report.PeriodId == period.PeriodId)
                    {
                        report = lot.Report;

                        var reportMembershipImages = Context.ReportMembershipImages
                            .Where(x => x.ReportMembership.ReportId == report.ReportId)
                            .ToList();

                        var imagesToDelete = reportMembershipImages
                            .ToList();

                        foreach (var item in imagesToDelete)
                        {
                            Context.Entry(item).State = EntityState.Deleted;
                        }

                        var membershipsToDelete = report.ReportMemberships.ToList();

                        foreach (var item in membershipsToDelete)
                        {
                            Context.Entry(item).State = EntityState.Deleted;
                        }

                        report.SetImage("last-report.png");
                        report.UpdatePreviousReportPercentage();
                        Context.SaveChanges();
                    }
                    else
                    {
                        report = new Report(lot, Principal.Settlement, Principal.User, period, folio, "last-report.png");
                    }

                    //create or update the report and add the memberships
                    foreach (var buildingConcept in buildingConcepts)
                    {
                        var previousPercentage = 0.0;

                        var membership = new ReportMembership(
                            report,
                            buildingConcept,
                            previousPercentage,
                            100,
                            null);

                        //report.ReportMemberships.Add(membership);
                    }

                    GetValidationErrors(report.Validate());

                    if (!ModelState.IsValid) return null;

                    var lotPreviousPercentage = lot.Percentage;

                    lot.UpdatePercentageWithReport(report);

                    /* BEGIN UPDATE ZONE PERCENTAGE --------------------------------------------- */

                    var previousZonePercentage = zoneLots.Count > 0 ? lotPreviousPercentage / zoneLots.Count : 0;
                    var newZonePercentage = zoneLots.Count > 0 ? lot.Percentage / zoneLots.Count : 0;

                    lot.Zone.UpdatePercentage(newZonePercentage - previousZonePercentage);
                    /* END UPDATE ZONE PERCENTAGE */

                    /* BEGIN UPDATE PACKAGE PERCENTAGES --------------------------------------------- */
                    foreach (var package in packages)
                    {
                        var packageMembership = package.PackageMemberships
                            .SingleOrDefault(x => x.LotId == lot.LotId);

                        if (packageMembership == null)
                            continue;

                        var previousPackagePercentage = lotPreviousPercentage / package.PackageMemberships.Count;
                        var newPackagePercentage = lot.Percentage / package.PackageMemberships.Count;

                        //_track(package);

                        package.UpdatePercentage(newPackagePercentage - previousPackagePercentage);
                    }
                    /* END UPDATE PACKAGE PERCENTAGES */

                    /* BEGIN UPDATE INSTITUTION PERCENTAGE --------------------------------------------- */
                    foreach (var institution in institutions)
                    {
                        var institutionLots = new List<Lot>();

                        var institutionPackages = packages
                            .Where(x => x.InstitutionId == institution.InstitutionId)
                            .ToList();

                        foreach (var institutionPackage in institutionPackages)
                        {
                            foreach (var packageMembership in institutionPackage.PackageMemberships)
                            {
                                var institutionLot = zoneLots
                                    .SingleOrDefault(x => x.LotId == packageMembership.LotId);

                                institutionLots.Add(institutionLot);
                            }
                        }

                        var previousInstitutionPercentage = institutionLots.Count > 0 ?
                            lotPreviousPercentage / institutionLots.Count : 0;

                        var newInstitutionPercentage = institutionLots.Count > 0 ?
                            lot.Percentage / institutionLots.Count : 0;

                        institution.UpdatePercentage(newInstitutionPercentage - previousInstitutionPercentage);
                    }
                    /* END UPDATE INSTITUTION PERCENTAGES */

                    /* BEGIN UPDATE SETTLEMENT PERCENTAGE --------------------------------------------- */

                    var previousSettlementPercentage = settlementLots.Count > 0 ? lotPreviousPercentage / settlementLots.Count : 0;
                    var newSettlementPercentage = settlementLots.Count > 0 ? lot.Percentage / settlementLots.Count : 0;

                    lot.Settlement.UpdatePercentage(newSettlementPercentage - previousSettlementPercentage);
                    /* END UPDATE SETTLEMENT PERCENTAGE */

                    Context.SaveChanges();
                    _transaction.Commit();


                    var output = new Domain.ReportContext.ReportOutput
                    {
                        ReportId = report.ReportId,

                        CreationDate = report.CreationDate,
                        LastUpdate = report.LastUpdate,

                        LotId = lot.LotId,
                        LotPercentage = lot.GetPercentage(),

                        ZoneId = lot.Zone.ZoneId,
                        ZonePercentage = lot.Zone.GetPercentage(),

                        SettlementId = lot.Zone.Settlement.SettlementId,
                        SettlementPercentage = lot.Zone.Settlement.GetPercentage(),

                        Packages = new List<Domain.PackageContext.Output>(),
                        Institutions = new List<Domain.InstitutionContext.Output>(),
                    };

                    foreach (var packageMembership in lot.PackageMemberships)
                    {
                        var package = packages
                            .Single(x => x.PackageId == packageMembership.PackageId);

                        output.Packages.Add(new Domain.PackageContext.Output
                        {
                            PackageId = packageMembership.PackageId,
                            InstitutionId = package.InstitutionId,

                            Name = package.Name,
                            Percentage = package.GetPercentage(),
                        });
                    }

                    foreach (var institution in institutions)
                    {
                        output.Institutions.Add(new Domain.InstitutionContext.Output
                        {
                            InstitutionId = institution.InstitutionId,
                            Name = institution.Name,
                            Percentage = institution.GetPercentage(),
                        });
                    }

                    return OK(output);
                }
                catch (Exception ex)
                {
                    Logger.Log(ex);
                    return Error();
                }
            }
        }

        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/lots/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> Put(int id, PutInput dto)
        {
            var lot = await Context.Lots
                .Include(x => x.Block)
                .Include(x => x.Model)
                .Include(x => x.Street)
                .Include(x => x.Zone)
                .Where(Principal.Lots())
                .SingleOrDefaultAsync(x => x.LotId == id);

            if (lot == null)
                return Error404();

            if (dto.SaleStatus == SaleStatus.None)
                return Error("SaleStatus", "Estado de venta inválido");

            //var isLotNumberInUse = await Context.Lots
            //    .Where(Principal.Lots())
            //    .AnyAsync(x => x.ZoneId == lot.ZoneId
            //        && x.LotId != lot.LotId
            //        && x.BlockId == lot.BlockId
            //        && x.LotNumber == dto.LotNumber);

            //if (isLotNumberInUse)
            //    return Error("LotNumber", $"El número de lote " +
            //        $"{dto.LotNumber} ya existe en la manzana {lot.Block.Name} de {lot.Zone.Name}");

            //if (!string.IsNullOrWhiteSpace(dto.OfficialNumber))
            //{
            //    var isOfficialNumberInUse = await Context.Lots
            //        .Where(Principal.Lots())
            //        .AnyAsync(x => x.ZoneId == lot.ZoneId
            //            && x.LotId != lot.LotId
            //            && x.StreetId == lot.StreetId
            //            && x.OfficialNumber == dto.OfficialNumber);

            //    if (isOfficialNumberInUse)
            //        return Error("OfficialNumber", $"El número oficial " +
            //            $"{dto.OfficialNumber} ya existe en la calle {lot.Street.Name}");
            //}

            var now = TimeZoner.Now;

            lot.Update(
                dto.OfficialNumber,
                dto.LotNumber,
                dto.GroundArea,
                dto.ConstructionArea);

            lot.PutSaleStatus(dto.SaleStatus, now);

            GetValidationErrors(lot.Validate());

            if (!ModelState.IsValid)
                return Error();

            Context.SaveChanges();

            return OK(true);
        }

        [AuthorizeRoles(Roles = "Supervisor SalesAdmin Admin")]
        [Route("~/api/lots/{id:int:min(1)}/model")]
        public async Task<HttpResponseMessage> PutModel(int id, PutModelInput dto)
        {
            try
            {
                var now = TimeZoner.Now;

                var lot = await Context.Lots
                    .Include(x => x.Model)
                    .Include(x => x.Settlement)
                    .Where(Principal.Lots())
                    .SingleOrDefaultAsync(x => x.LotId == id);

                if (lot == null)
                    return Error404();

                var hasReports = await Context.Reports
                    .AnyAsync(x => x.LotId == lot.LotId);

                if (hasReports)
                    return Error("No puedes cambiar el modelo de un lote con reportes.");

                var model = await Context.Models
                    .Where(Principal.Models())
                    .SingleOrDefaultAsync(x => x.ModelId == dto.ModelId);

                if (model == null)
                    return Error("Modelo no encontrado");

                lot.UpdateModel(model);

                GetValidationErrors(lot.Validate());

                if (!ModelState.IsValid)
                    return Error();

                await Context.SaveChangesAsync();

                await Context.LoadPeriodHistory(lot.Settlement);

                lot.Settlement.Resync(now);

                await Context.SaveChangesAsync();

                return OK(true);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return Error();
            }
        }

        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/lots/{id:int:min(1)}/packages")]
        public async Task<HttpResponseMessage> PutPackages(int id, PutPackagesInput dto)
        {
            var lot = await Context.Lots
                .Include(x => x.Zone)
                .Include(x => x.PackageMemberships)
                .Where(Principal.Lots())
                .SingleOrDefaultAsync(x => x.LotId == id);

            if (lot == null)
                return Error404();

            //if (lot.Status != Status.Pending)
            //    return Error("No se puede cambiar los paquetes de un lote con reportes");

            await Context.Packages
                .Include(x => x.Institution)
                .Where(x => x.ZoneId == lot.ZoneId)
                .LoadAsync();

            var ruv = lot.Zone.Packages
                .SingleOrDefault(x => x.Institution.Name == "RUV"
                    && x.PackageId == dto.RUV);

            if (ruv == null)
                return Error("Paquete RUV no encontrado");

            var bank = lot.Zone.Packages
                .SingleOrDefault(x => x.Institution.Name == "Bancario"
                    && x.PackageId == dto.Bancario);

            if (bank == null)
                return Error("Paquete Bancario no encontrado");

            var lotRuv = lot.PackageMemberships
                .SingleOrDefault(x => x.Package.Institution.Name == "RUV");

            var lotBank = lot.PackageMemberships
                .SingleOrDefault(x => x.Package.Institution.Name == "Bancario");

            if (lotRuv != null)
            {
                if (lotRuv.Package != ruv)
                {
                    Context.PackageMemberships.Remove(lotRuv);
                    new PackageMembership(lot, ruv);
                }
            }

            if (lotBank != null)
            {
                if (lotBank.Package != bank)
                {
                    Context.PackageMemberships.Remove(lotBank);
                    new PackageMembership(lot, bank);
                }
            }

            await Context.SaveChangesAsync();

            await Resync(lot.Settlement);

            return OK(true);
        }




        [AuthorizeRoles(Roles = "SalesAdmin Admin")]
        [Route("~/api/lots/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> Delete(int id)
        {
            var lot = await Context.Lots
                    .Include(x => x.Settlement)
                    .Where(Principal.Lots())
                    .SingleOrDefaultAsync(x => x.LotId == id);

            if (lot == null)
                return Error404();

            var settlement = await Context.Settlements
                .SingleOrDefaultAsync(x => x.SettlementId == lot.SettlementId);

            await Context.Reports
                .Where(x => x.LotId == lot.LotId)
                .LoadAsync();

            await Context.ReportMemberships
                .Where(x => x.Report.LotId == lot.LotId)
                .LoadAsync();

            await Context.ReportMembershipImages
                .Where(x => x.ReportMembership.Report.LotId == lot.LotId)
                .LoadAsync();

            await Context.MapImages
                .Where(x => x.LotId == lot.LotId)
                .LoadAsync();

            await Context.PackageMemberships
                .Where(x => x.LotId == lot.LotId)
                .LoadAsync();

            var mapImages = lot.MapImages
                .ToList();

            foreach (var mapImage in mapImages)
                mapImage.RemoveLot();

            foreach (var report in lot.Reports)
            {
                foreach (var reportMembership in report.ReportMemberships)
                    Context.ReportMembershipImages.RemoveRange(reportMembership.ReportMembershipImages);

                Context.ReportMemberships.RemoveRange(report.ReportMemberships);
            }

            Context.Reports.RemoveRange(lot.Reports);
            Context.PackageMemberships.RemoveRange(lot.PackageMemberships);
            await Context.SaveChangesAsync();

            //must save twice (problems with domain??)
            Context.Lots.Remove(lot);
            await Context.SaveChangesAsync();





            await Resync(settlement);
            return OK(true);
            //using (var transaction = Context.Database.BeginTransaction())
            //transaction.Commit();
        }
    }
}