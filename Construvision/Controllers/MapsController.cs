﻿using Domain;
using Domain.MapContext;
using Domain.MapContext.Image;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class MapsController : BaseApiController
    {
        public MapsController()
        {
            Message404 = "Mapa no encontrado.";
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/maps/{id:int:min(1)}")]
        public HttpResponseMessage Get(int id)
        {
            var map = Context.Maps
                .Where(Principal.Maps())
                .SingleOrDefault(x => x.MapId == id);

            if (map == null)
                return Error404();

            var paths = Context.MapImages
                .Where(x => x.MapId == map.MapId)
                .Select(x => new Domain.MapContext.Image.Output
                {
                    MapImageId = x.MapImageId,
                    LotId = x.LotId,
                    Path = x.Path,
                    Type = x.Type,
                })
                .ToList();

            var markers = Context.Markers
                .Where(x => x.MapId == map.MapId)
                .Select(x => new Domain.MapContext.Marker.Output
                {
                    MarkerId = x.MarkerId,
                    X = x.X,
                    Y = x.Y,
                    Label = x.Label,
                })
                .ToList();

            var output = new Domain.MapContext.Output
            {
                MapId = map.MapId,
                Name = map.Name,
                Height = map.Height,
                Width = map.Width,
                Paths = paths,
                Markers = markers
            };

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin")]
        [Route("~/api/maps")]
        public HttpResponseMessage PostMap(PostInput dto)
        {
            var settlement = Context.Settlements
                .Where(Principal.Settlements())
                .SingleOrDefault(x => x.SettlementId == Principal.Settlement.SettlementId);

            if (dto.Paths.Count == 0)
                return Error("Paths", "Listado de paths vacio");

            var existingMap = Context.Maps
                .Include(x => x.MapImages)
                .SingleOrDefault(x =>
                    x.Name == dto.Name &&
                    x.SettlementId == Principal.Settlement.SettlementId);

            Map map;

            if (existingMap != null)
            {
                if (existingMap.MapImages.Count > 0)
                    return Error("El mapa " + dto.Name + " ya tiene paths.");

                map = existingMap;
            }
            else
            {
                map = new Map(settlement, dto.Name, dto.Width, dto.Height);
            }

            foreach (var path in dto.Paths)
            {
                new MapImage(map, path);
            }

            Context.SaveChanges();

            return OK(true);
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin Manager")]
        [Route("~/api/mapimages")]
        public HttpResponseMessage PostMapImage(PostTypeInput dto)
        {
            var mapImage = Context.MapImages
               .SingleOrDefault(x =>
                   x.Map.SettlementId == Principal.Settlement.SettlementId &&
                   x.MapImageId == dto.MapImageId);

            if (mapImage == null)
                return Error("Imagen de mapa no encontrada.");

            mapImage.SetType(dto.Type);

            Context.SaveChanges();

            return OK(true);
        }


        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/maps/{id:int:min(1)}")]
        public HttpResponseMessage PutMap(int id, PutMapInput dto)
        {
            var map = Context.Maps
                .Where(Principal.Maps())
                .SingleOrDefault(x => x.MapId == id);

            if (map == null)
                Error404();

            map.Update(dto.Name);

            GetValidationErrors(map.Validate());

            if (!ModelState.IsValid)
                return Error();

            Context.SaveChanges();

            return OK(true);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/mapimages/{id:int:min(1)}/assignlot")]
        public HttpResponseMessage PutMapImage(int id, PutInput dto)
        {
            var mapImage = Context.MapImages
                .Where(Principal.MapImages())
                .SingleOrDefault(x => x.MapImageId == id);

            if (mapImage == null)
                return Error("Imagen de Mapa no encontrada.");

            var lot = Context.Lots
                .Where(Principal.Lots())
                .SingleOrDefault(x => x.LotId == dto.LotId);

            if (lot == null)
                return Error("Lote no encontrado.");

            mapImage.AssignLot(lot);

            GetValidationErrors(mapImage.Validate());

            if (!ModelState.IsValid)
                return Error();

            Context.SaveChanges();
            return OK(true);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/maps/{id:int:min(1)}")]
        public HttpResponseMessage Delete(int id)
        {
            var map = Context.Maps
                .Where(Principal.Maps())
                .SingleOrDefault(x => x.MapId == id);

            if (map == null)
                return Error404();

            var mapHasMapImages = Context.MapImages
                .Any(x => x.MapId == map.MapId);

            if (mapHasMapImages)
                return Error("Mapa con PATHS no puede ser eliminado.");

            Context.SaveChanges();

            return OK(true);
        }
    }
}
