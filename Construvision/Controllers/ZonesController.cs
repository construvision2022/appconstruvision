﻿using Domain;
using Domain.ZoneContext;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Construvision.Controllers
{
    public class ZonesController : BaseApiController
    {
        public ZonesController()
        {
            Message404 = "Cerrada no encontrada.";
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones")]
        public HttpResponseMessage GetAll()
        {
            var output = Context.Zones
                .Include(x => x.Map)
                .Where(Principal.Zones())
                .AsEnumerable()
                .Select(x => new Output
                {
                    ZoneId = x.ZoneId,
                    Name = x.Name,
                    Alias = x.Alias,

                    Percentage = x.GetPercentage(2),
                    StartingPercentage = x.GetStartingPercentage(2),

                    MapId = x.MapId,
                    SettlementId = x.SettlementId,
                })
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones/{id:int:min(1)}")]
        public HttpResponseMessage GetOne(int id)
        {
            var zone = Context.Zones
                .Include(x => x.ZonePeriods.Select(xx => xx.Period))
                .Include(x => x.Map)
                .Where(Principal.Zones())
                .SingleOrDefault(x => x.ZoneId == id);

            if (zone == null)
                return Error404();

            var zonePeriods = Context.ZonePeriods
                .Include(x => x.Period)
                .Where(x => x.ZoneId == zone.ZoneId)
                .OrderByDescending(x => x.PeriodId)
                .Take(3)
                .AsEnumerable()
                .Select(x => new Domain.PeriodContext.HistoryOutput
                {
                    PeriodId = x.PeriodId,
                    StartingPercentage = x.GetStartingPercentage(),
                    EndingPercentage = x.GetEndingPercentage(),
                    StartDate = x.Period.StartDate,
                    EndDate = x.Period.EndDate,
                })
                .ToList();

            var output = new Output
            {
                ZoneId = zone.ZoneId,

                Name = zone.Name,
                Alias = zone.Alias,
                Percentage = zone.GetPercentage(),
                StartingPercentage = zone.GetStartingPercentage(),

                MapId = zone.MapId,
                SettlementId = zone.SettlementId,
                LastPeriods = zone.ZonePeriods.OrderByDescending(x => x.PeriodId)
                .Take(3)
                .Select(x => new Domain.PeriodContext.HistoryOutput
                {
                    PeriodId = x.PeriodId,
                    StartingPercentage = x.GetStartingPercentage(),
                    EndingPercentage = x.GetEndingPercentage(),
                    StartDate = x.Period.StartDate,
                    EndDate = x.Period.EndDate,
                })
                .ToList()
            };

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones/{id:int:min(1)}/map")]
        public HttpResponseMessage GetMap(int id)
        {
            var zone = Context.Zones
                .Include(x => x.Map)
                .Where(Principal.Zones())
                .SingleOrDefault(x => x.ZoneId == id);

            if (zone == null)
                return Error404();

            if (zone.Map == null)
                return Error("Zona sin mapa asignado.");

            var map = zone.Map;

            var paths = Context.MapImages
                .Where(x => x.MapId == map.MapId)
                .Select(x => new Domain.MapContext.Image.Output
                {
                    MapImageId = x.MapImageId,
                    LotId = x.LotId,
                    Path = x.Path,
                    Type = x.Type,
                })
                .ToList();

            var markers = Context.Markers
                .Where(x => x.MapId == map.MapId)
                .Select(x => new Domain.MapContext.Marker.Output
                {
                    MarkerId = x.MarkerId,
                    X = x.X,
                    Y = x.Y,
                    Label = x.Label,

                })
                .ToList();

            var output = new Domain.MapContext.Output
            {
                MapId = map.MapId,
                Name = map.Name,
                Height = map.Height,
                Width = map.Width,
                Paths = paths,
                Markers = markers
            };

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones/{id:int:min(1)}/streets")]
        public HttpResponseMessage GetStreets(int id)
        {
            var output = Context.Streets
                .Where(Principal.Streets())
                .Where(x =>
                    x.ZoneId.HasValue &&
                    x.ZoneId == id)
                .Select(x => new Domain.StreetContext.Output
                {
                    StreetId = x.StreetId,
                    Name = x.Name
                }).ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones/{id:int:min(1)}/lots")]
        public HttpResponseMessage GetLots(int id)
        {
            var output = Context.Lots
                .Where(Principal.Lots())
                .Where(x => x.ZoneId == id)
                .AsEnumerable()
                .OrderBy(x => x.LotNumber.PadLeft(4, '0'))
                .Select(x => x.ToOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones/{id:int:min(1)}/blocks")]
        public HttpResponseMessage GetBlocks(int id)
        {
            var output = Context.Blocks
                .Where(Principal.Blocks())
                .Where(x => x.ZoneId == id)
                .OrderBy(x => x.Name)
                .Select(x => new Domain.BlockContext.Output
                {
                    BlockId = x.BlockId,
                    Name = x.Name,
                    Color = x.Color
                }).ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones/{id:int:min(1)}/models")]
        public HttpResponseMessage GetModels(int id)
        {
            var output = Context.Models
                .Where(Principal.Models())
                .Where(x => x.ZoneId == id)
                .OrderBy(x => x.Name)
                .AsEnumerable()
                .Select(x => x.ToOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones/{id:int:min(1)}/history")]
        public async Task<HttpResponseMessage> GetHistory(int id)
        {
            var zone = await Context.Zones
                .Where(Principal.Zones())
                .SingleOrDefaultAsync(x => x.ZoneId == id);

            if (zone == null)
                return Error404();

            var zonePeriods = await Context.ZonePeriods
                .Include(x => x.Period)
                .Where(x => x.ZoneId == zone.ZoneId)
                .OrderByDescending(x => x.PeriodId)
                .ToListAsync();

            var output = zonePeriods
                .Select(x => x.ToHistoryOutput())
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones/{id:int:min(1)}/reports")]
        public HttpResponseMessage GetReports([FromUri]int id, [FromUri]int periodId = 0)
        {
            var settlement = Principal.Settlement;

            var zone = Context.Zones
                .Where(Principal.Zones())
                .SingleOrDefault(x => x.ZoneId == id);

            if (zone == null)
                return Error404();

            Period period;

            if (periodId == 0)
            {
                period = GetCurrentPeriod();
            }
            else
            {
                period = Context.Periods
                    .SingleOrDefault(x => x.PeriodId == periodId);
            }

            if (period == null)
                return Error("Periodo no encontrado.");

            var output = Context.Reports
                .Where(Principal.Reports())
                .Where(x =>
                    x.Lot.ZoneId == zone.ZoneId &&
                    x.PeriodId == period.PeriodId)
                .AsEnumerable()
                .Select(x => new Domain.ReportContext.Output
                {
                    ReportId = x.ReportId,
                    LotId = x.LotId,

                    ImageCode = x.ImageCode,
                    Percentage = x.GetPercentage(),
                    LastUpdate = x.LastUpdate,
                    CreationDate = x.CreationDate,
                    PreviousReportPercentage = x.PreviousReportPercentage,
                })
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones/{id:int:min(1)}/packages")]
        public HttpResponseMessage GetPackages(int id)
        {
            var zone = Context.Zones
                .Where(Principal.Zones())
                .SingleOrDefault(x => x.ZoneId == id);

            if (zone == null)
                return Error404();

            var output = Context.Packages
                .Include(x => x.PackagePeriods.Select(xx => xx.Period))
                .OrderBy(x => x.Name)
                .Where(x => x.ZoneId == zone.ZoneId)
                .Select(x => new Domain.PackageContext.Output
                {
                    PackageId = x.PackageId,

                    Name = x.Name,
                    Percentage = x.Percentage,
                    InstitutionName = x.Institution.Name,
                    InstitutionPercentage = x.Institution.Percentage,

                    InstitutionId = x.InstitutionId,


                    LastPeriods = x.PackagePeriods
                    .OrderByDescending(xx => xx.PeriodId)
                    .Take(3)
                    .Select(xxx => new Domain.PeriodContext.HistoryOutput
                    {
                        PeriodId = xxx.PeriodId,
                        EndingPercentage = xxx.EndingPercentage,
                        StartingPercentage = xxx.StartingPercentage,
                        EndDate = xxx.Period.EndDate,
                        StartDate = xxx.Period.StartDate
                    })
                    .ToList()
                })
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Supervisor Admin SalesAdmin Seller")]
        [Route("~/api/zones/{id:int:min(1)}/institutions")]
        public HttpResponseMessage GetInstitutions(int id)
        {
            var zone = Context.Zones
                .Where(Principal.Zones())
                .SingleOrDefault(x => x.ZoneId == id);

            if (zone == null)
                return Error404();

            var output = Context.Institutions
                .Include(x => x.Packages.Select(xx => xx.PackageMemberships))
                .Include(x => x.Packages.Select(xx => xx.PackagePeriods))
                .Include(x => x.InstitutionPeriods.Select(xx => xx.Period))
                .OrderBy(x => x.Name)
                .Where(x => x.ZoneId == zone.ZoneId)
                .AsEnumerable()
                .Select(institution => new Domain.InstitutionContext.Output
                {
                    InstitutionId = institution.InstitutionId,
                    Name = institution.Name,
                    Percentage = institution.GetPercentage(),
                    StartingPercentage = institution.GetStartingPercentage(),

                    LastPeriods = institution.InstitutionPeriods
                    .OrderByDescending(x => x.PeriodId)
                    .Take(3)
                    .Select(x => new Domain.PeriodContext.HistoryOutput
                    {
                        PeriodId = x.PeriodId,
                        EndDate = x.Period.EndDate,
                        StartDate = x.Period.StartDate,
                        EndingPercentage = x.EndingPercentage,
                        StartingPercentage = x.StartingPercentage
                    })
                    .ToList(),

                    Packages = institution.Packages.Select(package => new Domain.PackageContext.Output
                    {
                        Name = package.Name,
                        PackageId = package.PackageId,
                        Percentage = package.GetPercentage(),
                        InstitutionId = package.InstitutionId,
                        StartingPercentage = package.GetStartingPercentage(),
                        Memberships = package.PackageMemberships.Select(x => x.LotId).ToList(),

                        LastPeriods = package.PackagePeriods
                        .OrderByDescending(x => x.PeriodId)
                        .Take(3)
                        .Select(x => new Domain.PeriodContext.HistoryOutput
                        {
                            PeriodId = x.PeriodId,
                            EndingPercentage = x.EndingPercentage,
                            StartingPercentage = x.StartingPercentage
                        })
                        .ToList()
                    }).ToList(),
                })
                .ToList();

            return OK(output);
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin")]
        [Route("~/api/zones")]
        public async Task<HttpResponseMessage> Post(PostInput dto)
        {
            try
            {
                var settlement = await Context.Settlements
                    .Include(x => x.Business.Tennant)
                    .SingleOrDefaultAsync(x => x.SettlementId == Principal.Settlement.SettlementId);

                var isNameInUse = await Context.Zones
                    .Where(Principal.Zones())
                    .AnyAsync(x => x.Name.Equals(dto.Name, StringComparison.OrdinalIgnoreCase));

                if (isNameInUse)
                    return Error("Name", "Ya existe una cerrada con ese nombre en " + settlement.Name);

                var zone = new Zone(settlement, dto.Name);

                GetValidationErrors(zone.Validate());

                if (!ModelState.IsValid)
                    return Error();

                await Context.SaveChangesAsync();

                var output = zone.ToOutput();

                return OK(output);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
                return Error();
            }
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin")]
        [Route("~/api/zones/{id:int:min(1)}")]
        public async Task<HttpResponseMessage> Put(int id, PutInput dto)
        {
            try
            {
                var settlement = await Context.Settlements
                    .Include(x => x.Business.Tennant)
                    .SingleOrDefaultAsync(x => x.SettlementId == Principal.Settlement.SettlementId);

                var zone = await Context.Zones
                    .Where(Principal.Zones())
                    .SingleOrDefaultAsync(x => x.ZoneId == id);

                if (zone == null)
                    return Error404();

                zone.Update(dto.Name);
                GetValidationErrors(zone.Validate());

                if (!ModelState.IsValid)
                    return Error();

                await Context.SaveChangesAsync();

                return OK(true);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
                return Error();
            }
        }

        [AuthorizeRoles(Roles = "Admin SalesAdmin")]
        [Route("~/api/zones/{id:int:min(1)}/image")]
        public async Task<HttpResponseMessage> PutImage(int id, PutImageInput dto)
        {
            try
            {
                var settlement = await Context.Settlements
                    .Include(x => x.Business.Tennant)
                    .SingleOrDefaultAsync(x => x.SettlementId == Principal.Settlement.SettlementId);

                var zone = await Context.Zones
                    .Where(Principal.Zones())
                    .SingleOrDefaultAsync(x => x.ZoneId == id);

                if (zone == null)
                    return Error404();

                zone.UpdateImage(dto.ImageCode);
                GetValidationErrors(zone.Validate());

                if (!ModelState.IsValid)
                    return Error();

                await Context.SaveChangesAsync();

                return OK(true);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
                return Error();
            }
        }
    }
}
