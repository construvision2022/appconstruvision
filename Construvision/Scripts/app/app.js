﻿var app = {

    title: 'Construvisión',
    error: 'Ha sucedido un error inesperado al procesar su petición',
    loading: false,

    container: null,
    Sammy: null,
    db: {},
    tools: {},
    routes: [],
    components: {},
    redirect: function () {

        if (!app.session)
            window.location = '#/login';
        else if (!app.session.sessionType)
            window.location = '#/sesiones';
        else if (app.session.sessionType == 7)
            window.location = '#/manager';
        else if (app.session.sessionType == 2 || app.session.sessionType == 5)
            window.location = '#/admin';
        else
            window.location = '#/panel';


    },
    session: {
        authorizationToken: null,
        sessionId: null,
        sessionType: null,
        user: null
    },
    forms: {
        inputText: function (inputOptions, options) {

            options = options || {};

            var getInputHTML = function (inputOption) {

                if (options.readonly)
                    inputOption.readonly = true;

                if (options.disabled)
                    inputOption.disabled = true;

                if (options.required)
                    inputOption.required = true;

                var message = '';

                inputOption.displayName = inputOption.displayName || inputOption.name;
                inputOption.placeholder = inputOption.placeholder || inputOption.displayName;

                var data_validation_html = [];

                if (inputOption.required) {

                    if (typeof inputOption.required === 'string') {
                        message = inputOption.required;
                    } else {
                        message = inputOption.displayName + ' es requerido';
                    }

                    data_validation_html.push('data-val-required="' + message + '" ');
                }

                message = '';
                if (inputOption.number) {

                    if (typeof inputOption.number === 'string') {
                        message = inputOption.number;
                    } else {
                        message = inputOption.displayName + ' debe ser un número';
                    }

                    data_validation_html.push('data-val-number="' + message + '" ');
                }

                if (inputOption.digits) {

                    if (typeof inputOption.digits === 'string') {
                        message = inputOption.digits;
                    } else {
                        message = inputOption.displayName + ' debe ser un dígito';
                    }

                    data_validation_html.push('data-val-digits="' + message + '" ');
                }

                if (inputOption.email) {
                    data_validation_html.push('data-val-email="Formato incorrecto." ');

                } else if (inputOption.length) {
                    data_validation_html.push('' +
                        'data-val-length="' + inputOption.length.message + '" ' +
                        'data-val-length-min="' + inputOption.length.min + '" ' +
                        'data-val-length-max="' + inputOption.length.max + '" ');

                } else if (inputOption.range) {
                    data_validation_html.push('' +
                        'data-val-range="' + inputOption.range.message + '" ' +
                        'data-val-range-min="' + inputOption.range.min + '" ' +
                        'data-val-range-max="' + inputOption.range.max + '" ');

                } else {
                    data_validation_html.push('' +
                        'data-val-regex="' + inputOption.displayName + ' contiene caractéres inválidos." ' +
                        'data-val-regex-pattern="' + app.tools.inputRegex + '" ');
                }

                return '' +
                    '<div class="form-group">' +
                    '<label for="' + inputOption.name + '">' + inputOption.displayName + '</label>' +
                    '<input type="text" class="form-control" placeholder="' + inputOption.placeholder + '" ' +
                    (inputOption.name ? 'name = "' + inputOption.name + '" ' : '') +
                    (inputOption.readonly ? 'readonly="" ' : '') +
                    (inputOption.disabled ? 'disabled="" ' : '') +
                    (inputOption.value ? 'value="' + (inputOption.value || '') + '" ' : '') +
                    data_validation_html.join('') + ' data-val="true">' +
                    app.forms.validationErrorDiv(inputOption.name) + '</div>';
            };

            var buffer = '';
            $.each(inputOptions, function (i, inputOption) {
                buffer += getInputHTML(inputOption);
            });

            return buffer;
        },
        textArea: function (options) {
            return `
            <div class="form-group">
                <label for="${options.name}">${options.displayName}</label>
                <textarea name="${options.name}" class="form-control" style="resize: none;"
                    rows="2">${options.value || ''}</textarea>
            </div>
            `;
        },
        validationErrorDiv: function (name) {
            return `
            <div class="form-error">
                <span data-valmsg-for="${name}" data-valmsg-replace="true" class="field-validation-error"></span>
            </div>
            `;
        },
        optionsTd: function (options) {

            var buffer = '';
            $.each(options, function (i, option) {
                buffer += '<li>' + option + '</li>';
            });

            return `
            <td class="text-center" style="width: 52px;">
                <div class="input-group-btn">
                    <div class="dropup">
                        <button data-toggle="dropdown" class="btn btn-xs btn-default dropdown-toggle">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            ${buffer}
                        </ul>
                    </div>
                </div>
            </td>
            `;
        },
        formButton: function (message) {
            return `<button type="submit" class="btn btn-success" data-loading-text="Cargando...">${message}</button>`;
        }
    },
    services: {
        fetch: async function (url) {

            try {
                return await $.get(window.baseUrl + url);
            } catch (e) {

                app.parseErrors(e);
            }
        },
        ajaxAsync: async function (params) {

            try {

                var response = await $.ajax({
                    contentType: 'application/json',
                    url: window.baseUrl + params.url,
                    method: params.method || 'POST',
                    data: JSON.stringify(params.data)
                });

                return response;

            } catch (e) {
                app.log(e.responseJson);
                app.parseErrors(e, params.validator);
            }
        },
        validateForm: function (form) {
            $.validator.unobtrusive.parse(form);
            var validator = form.data('validator');
            return validator;
        }
    },
    funcs: {
        lotAddress: function (lot) {
            return `${lot.StreetName} #${lot.OfficialNumber} [${lot.BlockName} L-${lot.LotNumber}]`;
            //return `${lot.StreetName} #${lot.OfficialNumber} [L-${lot.LotNumber} ${lot.BlockName}]`;
        }
    },
    parseErrors: function (response, validator) {

        if (!response) {
            alertify.alert(app.error);
            return;
        }

        var errors = {};
        var getModelStateErrors = function (modelState) {
            $.each(modelState, function (key, value) {
                key = key.replace('dto.', '');
                errors[key] = value;
            });
        };

        if (!response.responseJSON) {

            if (!response.responseText) {
                alertify.alert(app.error);
                return;
            }

            var parsed;
            try { parsed = JSON.parse(response.responseText); }
            catch (e) {
                alertify.alert(app.error);
                return;
            }

            if (!parsed.ModelState && parsed.Message) {
                alertify.alert(parsed.Message);
                return;
            }

            if (parsed.ModelState) {
                getModelStateErrors(parsed.ModelState);
            }
        }

        if (response.responseJSON) {

            if (response.responseJSON.ModelState) {

                getModelStateErrors(response.responseJSON.ModelState);

            } else if (response.responseJSON.Message) {
                alertify.alert(response.responseJSON.Message);
                return;
            }
        }

        if (validator && Object.keys(errors).length) {
            validator.showErrors(errors);
            return;
        }

        alertify.alert(app.error);
    },
    pageMessage: function (data) {

        if (!data)
            data = {};

        if (typeof data === 'string')
            data = { message: data };

        var html = `
        <div class="row">
            <div class="col-xs-12 text-center">
                <br>
                <br>
                <i class="fa ${data.icon || 'fa-info'} big-icon"></i>
                <br>
                <h3 class="big-text">
                    ${data.message || 'Error'}
                </h3>
                <br>
                ${data.href ?
                `<a class="btn btn-default" href="${data.href}">
                    ${data.text}
                </a>` :
                `<a class="btn btn-default redirect" href="#">${'Regresar' || data.text}</a>`}
            </div>
        </div>
        `;

        app.Sammy.swap(html);
    },
    navbar: function (style) {

        var navbar = $('#navbar');

        var html = `
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#/login">Login</a></li>
        </ul>
        `;

        if (style == 2) {
            html = `
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#/logout">Logout</a></li>
            </ul>
            `;
        }

        if (style == 3) {

            html = `
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#" class="redirect">
                        ${app.session.user.Name}
                    </a>
                </li>
                <li>
                    <a href="#" class="redirect">
                        <strong>
                            ${app.enums.SessionTypes.GetString(app.session.sessionType)}
                        </strong>
                    </a>
                </li>
                <li>
                    <a href="#/logout">Logout</a>
                </li>
            </ul>
            `;
        }

        navbar.html(html);
    },

    logging: true,
    log: function (message) {

        if (app.logging && message)
            console.info(message)
    },

    registerTools: function () {

        app.tools.loading = '<div class="centered-text"><img alt="Cargando..." src="/content/images/loading.gif"></div>'
        app.tools.inputRegex = '^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡ _#$%=\^*&.,;:)(@+-]*$'
        app.tools.parseModelState = function (modelState) {
            var errors = {}
            var list = []

            _.each(modelState, function (key, value) {
                key = key.replace('dto.', '')

                if (key == '')
                    this.list.push(value)
                else
                    this.errors[key] = value
            }, this)

            return {
                errors: errors,
                list: list
            }
        }
        app.tools.show404 = function () {
            app.Sammy.swap('<h1 class="well text-center">Tu página no existe - Error 404</h1>')
        }
        app.tools.centeredMessage = function (message) {
            return '' +
                '<div style="width: 100%" class="centered-text div-table">' +
                '<span class="span-vcenter">' +
                message +
                '</span>' +
                '</div>'
        }
        app.tools.garbageCollector = setInterval(function () {

            $.each(app.db, function (key, value) {

                var item = app.db[key]

                if (item.isComponent) {

                    var id = item.$el.prop('id')
                    var isAliveDOM = $('#' + id).length

                    if (!isAliveDOM) {
                        app.log('deleting component ' + key)
                        app.db[key].close()
                        delete app.db[key]
                    }
                }
            })

        }, 5000)
        app.tools.mergeObjects = function (obj1, obj2) {
            var obj3 = {};
            for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
            for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
            return obj3;
        }
        app.tools.cleanObj = function (objectToClean) {
            for (var x in objectToClean) if (objectToClean.hasOwnProperty(x)) delete objectToClean[x];
        }
        app.tools.imageUploadObject = function (formData, progressDOM) {

            //TODO: CHANGE TO CROP!!
            return {
                url: window.baseUrl + 'api/images/mobile-upload',
                type: 'POST',
                mimeType: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: formData,
                xhr: function () {

                    var request = $.ajaxSettings.xhr()

                    progressDOM.removeClass('hidden')

                    if (!request.upload) return request

                    request.upload.onprogress = function (event) {
                        var percentage = Math.floor((event.loaded / event.total) * 100)
                        progressDOM.html(percentage + '%')
                    }

                    request.upload.onload = function (event) {
                        app.log('upload complete')
                    }

                    return request
                },
            }
        }
        app.tools.modalModelState = function (modelState) {

            if (!modelState) {
                alertify.alert(app.error);
                return false
            }

            var list = []

            $.each(modelState, function (key, value) {
                list.push(value)
            })

            if (list.length > 0) {
                var buffer = ''
                for (var i = 0; i < list.length; i++)
                    buffer += list[i] + '<br>'

                app.tools.modal.title.html(app.title)
                app.tools.modal.body.html(buffer)
                app.tools.modal.show()
            }
        }
        app.tools.modal = {

            instance: $('#modal'),
            dialog: $('.modal-dialog'),
            body: $('.modal-body'),
            title: $('.modal-title'),

            show: function (size) {

                switch (size) {

                    default:
                        size = 'modal-md'
                        break

                    case 'modal-xs':
                    case 'modal-sm':
                    case 'modal-md':
                    case 'modal-lg':
                        break
                }

                this.dialog
                    .removeClass()
                    .addClass('modal-dialog')
                    .addClass(size)

                this.instance.modal({
                    keyboard: true,
                    show: true
                })
            },
            hide: function () {
                this.instance.modal('hide')
            },

            error: function (body, size) {

                if (!size) size = 'modal-sm'

                app.tools.modal.body.html(body)

                app.tools.modal.show(size)
            }
        }

        alertify.defaults.movable = false;
        alertify.defaults.glossary.ok = 'Ok';
        alertify.defaults.glossary.cancel = 'Cancelar';
        alertify.alert().set({ 'title': app.title });
        alertify.confirm().set({ 'title': app.title });
        alertify.prompt().set({ 'title': app.title });
    },
    registerPlugins: function () {

        if (!window.moment)
            return;

        moment.locale('es', {
            months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
            monthsShort: 'Ene_Feb_Mar_Abr_May_Jun_Jul_Ago_Sep_Oct_Nov_Dic'.split('_'),
            weekdays: 'Domingo_Lunes_Martes_Miércoles_Jueves_Viernes_Sábado'.split('_'),
            weekdaysShort: 'Dom._Lun._Mar._Mié._Jue._Vie._Sáb.'.split('_'),
            weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sá'.split('_'),
            longDateFormat: {
                LT: 'H:mm',
                LTS: 'LT:ss',
                L: 'DD/MM/YYYY',
                LL: 'D [de] MMMM [de] YYYY',
                LLL: 'D [de] MMMM [de] YYYY LT',
                LLLL: 'dddd, D [de] MMMM [de] YYYY LT'
            },
            calendar: {
                sameDay: function () {
                    return '[hoy a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                },
                nextDay: function () {
                    return '[mañana a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                },
                nextWeek: function () {
                    return 'dddd [a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                },
                lastDay: function () {
                    return '[ayer a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                },
                lastWeek: function () {
                    return '[el] dddd [pasado a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                },
                sameElse: 'L'
            },
            relativeTime: {
                future: 'en %s',
                past: 'hace %s',
                s: 'unos segundos',
                m: 'un minuto',
                mm: '%d minutos',
                h: 'una hora',
                hh: '%d horas',
                d: 'un día',
                dd: '%d días',
                M: 'un mes',
                MM: '%d meses',
                y: 'un año',
                yy: '%d años'
            },
            ordinalParse: /\d{1,2}º/,
            ordinal: '%dº',
            week: {
                dow: 1,
                doy: 4
            },
        });
    },
    registerJqueryExtensions: function () {

        $.fn.serializeObject = function () {
            var o = {}
            var a = this.serializeArray()
            $.each(a, function () {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]]
                    }
                    o[this.name].push(this.value || '')
                }
                else {
                    o[this.name] = this.value || ''
                }
            })
            return o
        }
        $.fn.extend({
            animateCss: function (animationName, callback) {
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                this.addClass('animated ' + animationName).one(animationEnd, function () {
                    $(this).removeClass('animated ' + animationName);
                    if (callback) {
                        callback();
                    }
                });
                return this;
            }
        });
        $.ajaxSetup({
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Token ' + (app.session.authorizationToken || ''))
                request.setRequestHeader('SessionId', (app.session.sessionId || ''))
                request.setRequestHeader('SessionType', (app.session.sessionType || ''))
            }
        });

        var $document = $(document);

        $document.ajaxError(function (event, jqxhr, settings, exception) {
            if (jqxhr.status == 402) {
                window.location = "#/logout"
            }

            if (jqxhr.status == 404) {



                app.pageMessage({
                    icon: 'fa fa-times',
                    message: 'No puedes crear reportes en un lote terminado',
                    href: `#/sesiones`,
                    text: 'Regresar'
                });
            }

            if (jqxhr.status == 417) {
                localStorage.removeItem('SessionId')
                localStorage.removeItem('SessionType')
                window.location = '#/sesiones'
            }

            if (jqxhr.status == 500) {
                alertify.alert(app.error)
            }
        });

        $document.on('click', '.redirect', function () {
            app.redirect();
        });
    },
    registerBootstrapExtensions: function () {

        app.tools.modal.instance.off('hidden.bs.modal')

        app.tools.modal.instance.on('hidden.bs.modal', function (event) {
            app.tools.modal.title.html(app.title)
            app.tools.modal.body.html('Cargando')

            app.tools.modal.dialog.removeClass().addClass('modal-dialog')
        })
    },
    registerUnderscoreExtensions: function () {
        _.templateSettings = {
            interpolate: /\{\{(.+?)\}\}/g
        }
    },

    readLocalStorage: function () {

        var authorizationToken, sessionId, sessionType, user

        authorizationToken = localStorage.getItem('AuthorizationToken')
        if (authorizationToken) {
            this.session.authorizationToken = authorizationToken
        } else {
            localStorage.removeItem('AuthorizationToken')
        }

        sessionId = localStorage.getItem('SessionId')
        if (sessionId && !isNaN(sessionId)) {
            this.session.sessionId = sessionId
        }
        else {
            localStorage.removeItem('SessionId')
        }

        sessionType = localStorage.getItem('SessionType')
        if (sessionType) {
            this.session.sessionType = sessionType
        } else {
            localStorage.removeItem('SessionType')
        }

        user = localStorage.getItem('User')
        user = JSON.parse(user);

        if (user) {
            this.session.user = user
        }
        else {
            localStorage.removeItem('User')
        }
    },
    registerRoutes: function () {

        app.Sammy = $.sammy('#' + app.container.prop('id'), function () {

            var sammy = this;

            $.each(app.routes, function (index, route) {

                sammy.get(route.route, route.callback);

            });

            sammy.swap = function (html, callback) {

                var that = this;

                html = `<div id="event-bubble">${html}</div>`;

                var $el = that.$element();

                $el.fadeOut(150, function () {

                    $el.empty().html(html).fadeIn(150, function () {

                        var bubble = $el.find('#event-bubble');

                        if (callback) {
                            callback(bubble);
                        }
                    });
                });
            };
            sammy.notFound = function (context) {


                if (window.location.hash) {
                    app.tools.show404()
                }
                else {
                    window.location = '#/login'
                }
            };
            sammy._checkFormSubmission = function (form) { return false; };
            //sammy.debug = true;
        });
    },

    start: function () {

        this.container = $('#app-container');

        this.db = {};

        this.registerTools();
        this.registerPlugins();
        this.registerJqueryExtensions();
        this.registerBootstrapExtensions();
        this.registerUnderscoreExtensions();
        this.registerRoutes();

        this.readLocalStorage();

        if (!this.session.authorizationToken) {
            window.location = '#/login';
        }
        else {

            if (!this.session.sessionType) {
                window.location = '#/sesiones';
                app.navbar(2);
            }
            else {
                app.navbar(3);
            }
        }

        app.Sammy.run();
    },
    enums: {
        SaleStatus: {
            Available: 1,
            Purchased: 2,
            Signed: 3,
            Paid: 4,
            Delivered: 5,
            Integrated: 6,

            GetString: function (status) {

                if (typeof status === 'string')
                    status = parseInt(status);

                switch (status) {
                    default: return '--';
                    case app.enums.SaleStatus.Available: return 'Disponible';
                    case app.enums.SaleStatus.Purchased: return 'Vendido';
                    case app.enums.SaleStatus.Signed: return 'Firmado';
                    case app.enums.SaleStatus.Paid: return 'Pagado';
                    case app.enums.SaleStatus.Delivered: return 'Entregado';
                    case app.enums.SaleStatus.Integrated: return 'Integrado';
                }

            }
        },
        LotStatus: {
            Pending: 1,
            InConstruction: 2,
            Finished: 3,

            GetString: function (status) {

                if (typeof status === 'string')
                    status = parseInt(status);

                switch (status) {
                    default: return '--';
                    case app.enums.LotStatus.Pending: return 'Terreno';
                    case app.enums.LotStatus.InConstruction: return 'En construcción';
                    case app.enums.LotStatus.Finished: return 'Terminado';
                }

            }
        },
        SessionTypes: {
            Admin: 2,
            Supervisor: 4,
            SalesAdmin: 5,
            Seller: 6,
            Manager: 7,

            GetString: function (status) {

                if (typeof status === 'string')
                    status = parseInt(status);

                switch (status) {
                    default: return '--';
                    case app.enums.SessionTypes.Admin: return 'Administrador';
                    case app.enums.SessionTypes.Supervisor: return 'Supervisor';
                    case app.enums.SessionTypes.SalesAdmin: return 'Administrador de ventas';
                    case app.enums.SessionTypes.Seller: return 'Vendedor';
                    case app.enums.SessionTypes.Manager: return 'Manager';
                }

            }
        }
    }
}
