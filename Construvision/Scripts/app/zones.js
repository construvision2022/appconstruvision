﻿(function () {

    if (!app)
        throw 'the app module is required';

    var list = async function (context) {

        document.title = `${app.title} - Cerradas`;

        var zones = await app.services.fetch('api/zones');

        if (!zones) {
            alertify.error(app.error);
            return false;
        }

        var buffer = '';
        $.each(zones, function (i, zone) {
            buffer += `
            <tr>
                <td>${zone.Name}</td>
                <td>${zone.Percentage}%</td>
                ${app.forms.optionsTd([
                    `<a href="#/cerradas/${zone.ZoneId}/lotes">Lotes</a>`,
                    `<a href="#/cerradas/${zone.ZoneId}/paquetes">Paquetes</a>`,
                    `<a href="#/cerradas/${zone.ZoneId}/historial">Historial</a>`,
                    `<a href="#/cerradas/${zone.ZoneId}/imagen">Subir imagen</a>`
                ])}
            </tr>
            `;
        });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Cerradas</h3>
            </div>
            <div class="col-xs-12">
                <a href="#/cerradas/crear" class="btn btn-success">Crear</a>
                <a href="#" class="redirect-link btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Porcentaje</th>
                            <th></th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var lots = async function (context) {

        document.title = `${app.title} - Cerradas`;

        var zone = await app.services.fetch(`api/zones/${context.params.id}`);

        if (!zone) {
            alertify.error(app.error);
            return false;
        }

        var lots = await app.services.fetch(`api/admin/zones/${zone.ZoneId}/lots`);

        if (!lots) {
            alertify.error(app.error);
            return false;
        }

        var buffer = '';
        $.each(lots, function (i, lot) {

            buffer += `
            <tr>
                <td>${lot.BlockName} L-${lot.LotNumber}</td>
                <td>${lot.StreetName} #${lot.OfficialNumber || 'N/A'}</td>
                <td>${lot.ModelName}</td>
                <td>${lot.GroundArea || '--'}</td>
                <td>${lot.ConstructionArea || '--'}</td>
                <td>${lot.RUV || '--'}</td>
                <td>${lot.Bancario || '--'}</td>
                <td>${app.enums.LotStatus.GetString(lot.Status)}</td>
                <td>${app.enums.SaleStatus.GetString(lot.SaleStatus)}</td>
                <td>${lot.Percentage}%</td>
                <td class="text-center">
                    <i class="fa ${lot.Dtu ? 'fa-check text-success' : 'fa-times text-danger'}"></i>
                </td>
                ${app.forms.optionsTd([
                    `<a href="#/lotes/${lot.LotId}/reporte">Crear reporte</a>`,
                    `<a href="#/lotes/${lot.LotId}/actualizar">Actualizar</a>`,
                    `<a href="#/lotes/${lot.LotId}/paquetes">Paquetes</a>`,
                    `<a href="#/lotes/${lot.LotId}/modelo">Cambiar modelo</a>`,
                    `<a href="#/lotes/${lot.LotId}/eliminar?zone=${zone.ZoneId}">Eliminar</a>`
                ])}
            </tr>
            `;
        });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Lotes de ${zone.Name}</h3>
            </div>
            <div class="col-xs-12">
                <a href="#/lotes/crear" class="btn btn-success">Crear</a>
                <a href="#" class="redirect btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th># de lote</th>
                            <th>Dirección</th>
                            <th>Modelo</th>
                            <th>Terreno</th>
                            <th>Const.</th>
                            <th>RUV</th>
                            <th>Bancario</th>
                            <th>Estado lote</th>
                            <th>Estado venta</th>
                            <th>%</th>
                            <th>DTU</th>
                            <th></th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var history = async function (context) {

        document.title = `${app.title} - Historial`;

        var zone = await app.services.fetch('api/zones/' + context.params.id);

        if (!zone)
            return app.pageMessage();

        var history = await app.services.fetch(`api/zones/${context.params.id}/history`);

        if (!history)
            return app.pageMessage();

        if (!history.length)
            return app.pageMessage('No hay avances en ' + zone.Name);

        var buffer = '';
        $.each(history, function (i, item) {

            buffer += `
            <tr>
                <td>${moment.utc(item.StartDate).format('DD/MMMM/YY')}</td>
                <td>${moment.utc(item.EndDate).format('DD/MMMM/YY')}</td>
                <td>${item.StartingPercentage}% ~ ${item.EndingPercentage}%</td>
            </tr>
            `;
        });

        var html = `
        <div class="row">
            <div class="col-xs-12">
                <a href="#/cerradas" class="btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <h3>${zone.Name} (${zone.Percentage}%)</h3>
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Fecha inicial</th>
                            <th>Fecha final</th>
                            <th>%</th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>
        </div>
        `;

        app.Sammy.swap(html, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var post = async function (context) {

        document.title = `${app.title} - Crear cerrada`;

        var inputsBuffer = app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true }
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Crear cerrada</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Crear')}
                    <a href="#/cerradas" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')
            var validator = app.services.validateForm(form);
            var inputs = form[0].elements;

            var nameInput = $(inputs.Name);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var created = await app.services.ajaxAsync({
                    url: `api/zones`,
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!created) {
                    button.button('reset');
                    return;
                }

                window.location = '#/cerradas';
            });

            nameInput.focus();
        });
    };
    var put = async function (context) {

        document.title = `${app.title} - Actualizar cerrada`;

        var zone = await app.services.fetch('api/zones/' + context.params.id);

        if (!zone) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true, value: zone.Name }

        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar cerrada</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Actualizar')}
                    <a href="#/cerradas" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/zones/${zone.ZoneId}`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                window.location = '#/cerradas';
            });

        });
    };
    var putImage = async function (context) {

        document.title = `${app.title} - Imagen de cerrada`;

        var zone = await app.services.fetch('api/zones/' + context.params.id);

        if (!zone)
            return false;

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Imagen de cerrada</h3>
            </div>
            <div class="col-xs-12">
                <h3>${zone.Name}</h3>
                <form>
                    <div class="form-group">
                        <input type="file" name="file" class="form-control">
                    </div>
                    <div class="form-group">
                        <div id="messageDiv"></div>
                    </div>
                    ${app.forms.formButton('Subir Imagen')}
                    <a href="#/cerradas" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]');
            var messageDiv = bubble.find('#messageDiv');

            var fileInput = $(form[0].elements.file);

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                var inputFiles = fileInput[0].files;

                if (!inputFiles.length) {
                    alertify.alert('No hay archivo seleccionado');
                    return;
                }

                var file = inputFiles[0];

                var allowedMimeTypes = [
                    'image/bmp',
                    'image/gif',
                    'image/jpeg',
                    'image/png',
                ];

                if (allowedMimeTypes.indexOf(file.type) == -1) {
                    alertify.alert('Tipo de archivo no soportado');
                    return;
                }

                var maxFileSize = 3;
                var sizeKB = file.size / 1024;
                var sizeMB = (sizeKB / 1024).toFixed(3);


                if (sizeMB >= maxFileSize) {
                    alertify.alert('Peso maximo permitido: ' + maxFileSize + ' Megabytes');
                    return;
                }

                button.button('loading');
                messageDiv.html('Cargando...');

                var formData = new FormData();
                formData.append('file', file);

                var uploadZoneImage = async function (data) {

                    var updated = await app.services.ajaxAsync({
                        url: `api/zones/${zone.ZoneId}/image`,
                        method: 'PUT',
                        data: data,
                        validator: validator
                    });

                    app.loading = false;

                    if (!updated) {
                        button.button('reset');
                        messageDiv.html('');
                        alertify.error('Error');
                        return;
                    }

                    window.location = '#/cerradas';
                };

                var request = $.ajax({
                    url: baseUrl + `api/images/mobile-upload`,
                    type: 'POST',
                    mimeType: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    data: formData,
                    xhr: function () {

                        var xhr = $.ajaxSettings.xhr();

                        xhr.upload.onprogress = function (event) {

                            var percentage = Math.floor(event.loaded / event.total * 100);

                            if (isNaN(percentage))
                                return;

                            messageDiv.html(percentage + '%');

                        };

                        return xhr;
                    }
                });

                request.done(function (response, textStatus) {

                    var json = JSON.parse(response);

                    uploadZoneImage({ ImageCode: json.FileName });
                });

                request.fail(function (response) {
                    app.parseErrors(response);
                    button.button('reset');
                    app.loading = false;
                });

            });

        });
    };


    app.routes.push({ route: '#/cerradas/?', callback: list });
    app.routes.push({ route: '#/cerradas/crear/?', callback: post });
    app.routes.push({ route: '#/cerradas/:id/actualizar/?', callback: put });
    app.routes.push({ route: '#/cerradas/:id/historial/?', callback: history });
    app.routes.push({ route: '#/cerradas/:id/lotes/?', callback: lots });
    app.routes.push({ route: '#/cerradas/:id/imagen/?', callback: putImage });

})();