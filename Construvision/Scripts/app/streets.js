﻿(function () {

    if (!app)
        throw 'the app module is required';

    var list = async function (context) {

        document.title = `${app.title} - Calles`;

        var streets = await app.services.fetch('api/streets');

        if (!streets) {
            alertify.error(app.error);
            return false;
        }

        var buffer = '';
        $.each(streets, function (i, street) {
            buffer += `
            <tr>                
                <td>${street.ZoneName}</td>
                <td>${street.Name}</td>
                ${app.forms.optionsTd([
                    `<a href="#/calles/${street.StreetId}/actualizar">Actualizar</a>`
                ])}
            </tr>
            `;
        });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Calles</h3>
            </div>
            <div class="col-xs-12">
                <a href="#/calles/crear" class="btn btn-success">Crear</a>
                <a href="#" class="redirect btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Cerrada</th>
                            <th>Nombre</th>
                            <th></th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>                    
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var post = async function (context) {

        document.title = `${app.title} - Crear calle`;

        var zones = await app.services.fetch('api/zones');

        if (!zones) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true }
        ]);

        var zonesHTML = '';
        $.each(zones, function (i, zone) {
            zonesHTML += `<option value="${zone.ZoneId}">${zone.Name}</option>`;
        });

        inputsBuffer += `
        <div class="form-group">
            <label for="ZoneId">Cerrada</label>
            <select class="form-control" name="ZoneId" data-val="true" data-val-required="Cerrada es requerida">
                <option value="">-- Selecciona cerrada --</option>
                ${zonesHTML}
            </select>
            ${app.forms.validationErrorDiv('ZoneId')}
        </div>
        `;

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Crear calle</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}                    
                    ${app.forms.formButton('Crear')}
                    <a href="#/calles" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')
            var validator = app.services.validateForm(form);
            var inputs = form[0].elements;

            var nameInput = $(inputs.Name);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var created = await app.services.ajaxAsync({
                    url: `api/streets`,
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!created) {
                    button.button('reset');
                    return;
                }

                window.location = '#/calles';
            });

            nameInput.focus();
        });
    };
    var put = async function (context) {

        document.title = `${app.title} - Actualizar calle`;

        var street = await app.services.fetch('api/streets/' + context.params.id);

        if (!street) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { displayName: 'Cerrada', disabled: true, value: street.ZoneName },
            { name: 'Name', displayName: 'Nombre', required: true, value: street.Name }

        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar calle</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Actualizar')}
                    <a href="#/calles" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/streets/${street.StreetId}`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                window.location = '#/calles';
            });

        });
    };    

    app.routes.push({ route: '#/calles/?', callback: list });
    app.routes.push({ route: '#/calles/crear/?', callback: post });
    app.routes.push({ route: '#/calles/:id/actualizar/?', callback: put });

})();