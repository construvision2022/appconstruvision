﻿(function () {

    if (!app)
        throw 'the app module is required';

    var list = async function (context) {

        document.title = `${app.title} - Residenciales`;

        var settlements = await app.services.fetch('api/settlements');

        if (!settlements) {
            alertify.error(app.error);
            return false;
        }

        var buffer = '';
        $.each(settlements, function (i, settlement) {
            buffer += `
            <tr>
                <td>${settlement.Name}</td>
                <td>${settlement.Percentage}%</td>
                ${app.forms.optionsTd([
                    `<a href="#/residenciales/${settlement.SettlementId}/actualizar">Actualizar</a>`,
                    `<a href="#/residenciales/${settlement.SettlementId}/usuarios">Usuarios</a>`,
                    `<a href="#/residenciales/${settlement.SettlementId}/historial">Historial</a>`,
                    `<a href="#/residenciales/${settlement.SettlementId}/imagen">Subir imagen</a>`
                ])}
            </tr>
            `;
        });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Residenciales</h3>
            </div>
            <div class="col-xs-12">
                <a href="#/residenciales/crear" class="btn btn-success">Crear</a>
                <a href="#" class="redirect btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Porcentaje</th>
                            <th></th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var history = async function (context) {

        document.title = `${app.title} - Historial`;

        var settlement = await app.services.fetch('api/settlements/' + context.params.id);

        if (!settlement)
            return app.pageMessage();

        var history = await app.services.fetch(`api/settlements/${context.params.id}/history`);

        if (!history)
            return app.pageMessage();

        if (!history.length)
            return app.pageMessage('No hay avances en ' + settlement.Name);

        var buffer = '';
        $.each(history, function (i, item) {

            buffer += `
            <tr>
                <td>${moment.utc(item.StartDate).format('DD/MMMM/YY')}</td>
                <td>${moment.utc(item.EndDate).format('DD/MMMM/YY')}</td>
                <td>${item.StartingPercentage}% ~ ${item.EndingPercentage}%</td>
            </tr>
            `;
        });

        var html = `
        <div class="row">
            <div class="col-xs-12">
                <a href="#/residenciales" class="btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <h3>${settlement.Name} (${settlement.Percentage}%)</h3>
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Fecha inicial</th>
                            <th>Fecha final</th>
                            <th>%</th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>
        </div>
        `;

        app.Sammy.swap(html, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var post = async function (context) {

        document.title = `${app.title} - Crear residencial`;

        var inputsBuffer = app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true }
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Crear residencial</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Crear')}
                    <a href="#/residenciales" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')
            var validator = app.services.validateForm(form);
            var inputs = form[0].elements;

            var nameInput = $(inputs.Name);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var created = await app.services.ajaxAsync({
                    url: `api/settlements`,
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!created) {
                    button.button('reset');
                    return;
                }

                window.location = '#/residenciales';
            });

            nameInput.focus();
        });
    };
    var put = async function (context) {

        document.title = `${app.title} - Actualizar residencial`;

        var settlement = await app.services.fetch('api/settlements/' + context.params.id);

        if (!settlement)
            return app.pageMessage();

        var inputsBuffer = app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true, value: settlement.Name }
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar residencial</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Actualizar')}
                    <a href="#/residenciales" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/settlements/${settlement.SettlementId}`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                window.location = '#/residenciales';
            });

        });
    };
    var putImage = async function (context) {

        document.title = `${app.title} - Imagen de residencial`;

        var settlement = await app.services.fetch('api/settlements/' + context.params.id);

        if (!settlement)
            return false;

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Imagen de residencial</h3>
            </div>
            <div class="col-xs-12">
                <h3>${settlement.Name}</h3>
                <div class="well well-xs text-center">
                    <img src="content/images/uploads/${settlement.ImageCode || 'not-found.jpg'}"
                        title="${settlement.Name}"
                        class="image-responsive">
                </div>
                <form>
                    <div class="form-group">
                        <input type="file" name="file" class="form-control">
                    </div>
                    <div class="form-group">
                        <div id="messageDiv"></div>
                    </div>
                    ${app.forms.formButton('Subir Imagen')}
                    <a href="#/residenciales" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]');
            var messageDiv = bubble.find('#messageDiv');

            var fileInput = $(form[0].elements.file);

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                var inputFiles = fileInput[0].files;

                if (!inputFiles.length) {
                    alertify.alert('No hay archivo seleccionado');
                    return;
                }

                var file = inputFiles[0];

                var allowedMimeTypes = [
                    'image/bmp',
                    'image/gif',
                    'image/jpeg',
                    'image/png',
                ];

                if (allowedMimeTypes.indexOf(file.type) == -1) {
                    alertify.alert('Tipo de archivo no soportado');
                    return;
                }

                var maxFileSize = 3;
                var sizeKB = file.size / 1024;
                var sizeMB = (sizeKB / 1024).toFixed(3);


                if (sizeMB >= maxFileSize) {
                    alertify.alert('Peso maximo permitido: ' + maxFileSize + ' Megabytes');
                    return;
                }

                button.button('loading');
                messageDiv.html('Cargando...');

                var formData = new FormData();
                formData.append('file', file);

                var updateSettlementImage = async function (data) {

                    var updated = await app.services.ajaxAsync({
                        url: `api/settlements/${settlement.SettlementId}/image`,
                        method: 'PUT',
                        data: data,
                        validator: validator
                    });

                    app.loading = false;

                    if (!updated) {
                        button.button('reset');
                        messageDiv.html('');
                        alertify.error('Error');
                        return;
                    }

                    window.location = '#/residenciales';
                };

                var request = $.ajax({
                    url: baseUrl + `api/images/mobile-upload`,
                    type: 'POST',
                    mimeType: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    data: formData,
                    xhr: function () {

                        var xhr = $.ajaxSettings.xhr();

                        xhr.upload.onprogress = function (event) {

                            var percentage = Math.floor(event.loaded / event.total * 100);

                            if (isNaN(percentage))
                                return;

                            messageDiv.html(percentage + '%');

                        };

                        return xhr;
                    }
                });

                request.done(function (response, textStatus) {

                    var json = JSON.parse(response);

                    updateSettlementImage({ ImageCode: json.FileName });
                });

                request.fail(function (response) {
                    app.parseErrors(response);
                    button.button('reset');
                    app.loading = false;
                });

            });

        });
    };

    app.routes.push({ route: '#/residenciales/?', callback: list });
    app.routes.push({ route: '#/residenciales/crear/?', callback: post });
    app.routes.push({ route: '#/residenciales/:id/actualizar/?', callback: put });
    app.routes.push({ route: '#/residenciales/:id/historial/?', callback: history });
    app.routes.push({ route: '#/residenciales/:id/imagen/?', callback: putImage });

})();