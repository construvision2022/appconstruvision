﻿(function () {

    if (!app)
        throw 'the app module is required';

    var list = async function (context) {

        app.Sammy.swap(`<h2 class="text-center">Cargando...</h2>`);

        var settlement = await app.services.fetch('api/settlements/' + context.params.id);
        if (!settlement)
            return false;

        var users = await app.services.fetch('api/settlements/' + settlement.SettlementId + '/users');
        if (!users)
            return false;

        document.title = `${app.title} - Usuarios de ${settlement.Name}`;

        var getUserHTML = function (user, type) {
            return `
            <tr>                
                <td>${user.Email}</td>
                <td>${user.Name}</td>
                <td>${user.FirstLastName}</td>
                <td>${user.SecondLastName}</td>
                <td>${user.MobilePhone}</td>
                <td>${user.LocalPhone}</td>
                <td><strong>${type}</strong></td>
                ${app.forms.optionsTd([
                    `<a href="#/usuarios/sesiones?email=${user.Email}">Asignar sesiones</a>`,
                    `<a href="#/usuarios/${user.UserId}/actualizar">Actualizar</a>`,
                    `<a href="#/usuarios/${user.UserId}/password">Contraseña</a>`,
                    `<a href="#/usuarios/${user.UserId}/eliminar">Eliminar</a>`
                ])}
            </tr>
            `;
        };
        var getLongTDHTML = function (message) {
            return `
            <tr class="text-center">
                <td colspan="20" style="font-size: 16px;">
                    <strong>${message}<strong>
                </td>
            </tr>
            `;
        };

        var buffer = '';
        if (users.Managers.length) {
            buffer += getLongTDHTML('MANAGERS');
            $.each(users.Managers, function (i, user) {
                buffer += getUserHTML(user, 'Managers');
            });
        }

        if (users.Admins.length) {
            buffer += getLongTDHTML('ADMINISTRADORES');
            $.each(users.Admins, function (i, user) {
                buffer += getUserHTML(user, 'Admin');
            });
        }

        if (users.SalesAdmins.length) {
            buffer += getLongTDHTML('ADMINISTRADORES DE VENTAS');
            $.each(users.SalesAdmins, function (i, user) {
                buffer += getUserHTML(user, 'Admin de ventas');
            });
        }

        if (users.Sellers.length) {
            buffer += getLongTDHTML('VENDEDORES');
            $.each(users.Sellers, function (i, user) {
                buffer += getUserHTML(user, 'Vendedores');
            });
        }

        if (users.Supervisors.length) {
            buffer += getLongTDHTML('SUPERVISORES');
            $.each(users.Supervisors, function (i, user) {
                buffer += getUserHTML(user, 'Supervisor');
            });
        }

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Usuarios</h3>
            </div>
            <div class="col-xs-12">
                <a href="#/residenciales/${settlement.SettlementId}/usuarios/crear" 
                    class="btn btn-success">Crear</a>
                <a href="#/residenciales" class="btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Nombre</th>
                            <th>Primer apellido</th>
                            <th>Segundo apellido</th>
                            <th>Teléfono móvil</th>
                            <th>Teléfono local</th>
                            <th>Tipo</th>
                            <th></th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>                    
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var search = async function (context) {

        document.title = `${app.title} - Buscar usuario`;

        var templateHTML = app.forms.inputText([            
            { name: 'Email', displayName: 'Email', required: true, value: (context.params.email || '') },
        ]);

        templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Buscar usuario</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${templateHTML}
                    ${app.forms.formButton('Buscar')}
                    <a href="#/manager" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        var drawTable = function (bubble, response) {

            if (!response || !response.Sessions || !response.Sessions.length)
                return app.pageMessage('El usuario no tiene sesiones actualmente');

            var sessionsBuffer = '';
            $.each(response.Sessions, function (i, session) {

                var type = app.enums.SessionTypes.GetString(session.SessionType);
                sessionsBuffer += `
                <tr>
                    <td>${session.Description || '--'}</td>
                    <td>${session.Alias || '--'}</td>
                    <td>${session.Location || '--'}</td>
                    <td><strong>${type}</strong></td>
                    ${app.forms.optionsTd([
                        `<a href="#/usuarios/sesiones?email=${response.User.Email}">Asignar sesiones</a>`,
                        `<a href="#/usuarios/${response.User.UserId}/actualizar">Actualizar</a>`,
                        `<a href="#/usuarios/${response.User.UserId}/password">Contraseña</a>`,
                        `<a href="#/usuarios/${response.User.UserId}/eliminar">Eliminar</a>`
                    ])}
                </tr>
                `;
            });

            templateHTML = `
            <div class="row">
                <div class="col-xs-12">
                    <button id="restart" class="btn btn-default">
                        Buscar otro
                    </button>
                    <button id="assign" class="btn btn-primary">
                        Asignar roles a usuario
                    </button>
                </div>
                <div class="col-xs-12">
                    <h3>Sesiones</h3>
                    <hr>
                    <table class="table table-hover table-condensed">
                        <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Alias</th>
                                <th>Localidad</th>
                                <th>Tipo</th>
                                <th></th>
                            <tr>
                        </thead>
                        <tbody>
                            ${sessionsBuffer}
                        </tbody>
                    </table>
                </div>
            </div>
            `;

            bubble.html(templateHTML);

            var restartButton = bubble.find('#restart');
            restartButton.on('click', function () {
                window.location = `#/usuarios/buscar?email=${response.User.Email}`;
                app.Sammy.refresh();
            });

            var assignButton = bubble.find('#assign');
            assignButton.on('click', function () {
                window.location = '#/usuarios/sesiones?email=' + response.User.Email;
            });
        };

        app.Sammy.swap(templateHTML, function (bubble) {            

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]');
            var validator = app.services.validateForm(form);
            var emailInput = $(form[0].elements);
            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                var data = form.serializeObject();

                button.button('loading');
                app.loading = true;

                var response = await app.services.ajaxAsync({
                    url: `api/sessions/user`,
                    data: { Email: data.Email },
                    validator: validator
                });

                app.loading = false;

                if (!response) {
                    button.button('reset');
                    return false;
                    //return app.pageMessage('Error al cargar las sesiones del usuario');
                }

                drawTable(bubble, response);
            });

            emailInput.select();
        });
    };    
    var sessions = async function (context) {

        document.title = `${app.title} - Asignar sesión a usuario`;

        var settlements = await app.services.fetch('api/settlements/');
        if (!settlements) {
            alertify.error(app.error);
            return false;
        }

        var settlementsHTML = '';
        $.each(settlements, function (i, settlement) {
            settlementsHTML += `
            <option value="${settlement.SettlementId}">
                ${settlement.Name}
            </option>
            `;
        });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Asignar sesión a usuario</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${app.forms.inputText([{ name: 'Email', displayName: 'Email', required: true, value: (context.params.email || '') }])}
                    <div class="form-group">
                        <label>Tipo de usuario</label>
                        <select name="Type" class="form-control"
                            data-val="true" data-val-required="Tipo de usuario es requerido.">
                            <option value="">-- Selecciona tipo de usuario --</option>
                            <option value="7">Manager</option>
                            <option value="2">Admin</option>
                            <option value="5">Admin de ventas</option>
                            <option value="6">Vendedor</option>
                            <option value="4">Supervisor</option>
                        </select>
                        ${app.forms.validationErrorDiv('Type')}
                    </div>
                    <div class="form-group" style="display: none">
                        <label>Residencial</label>
                        <select name="SettlementId" class="form-control">
                            <option value="">-- Selecciona residencial --</option>
                            ${settlementsHTML}
                        </select>
                        ${app.forms.validationErrorDiv('SettlementId')}
                    </div>
                    <div class="form-group" style="display: none">
                        <label>Cerrada</label>
                        <select name="ZoneId" class="form-control">
                            <option value="">-- Selecciona residencial --</option>
                        </select>
                        ${app.forms.validationErrorDiv('ZoneId')}
                    </div>
                    ${app.forms.formButton('Crear')}
                    <a href="#/residenciales" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;
        
        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]');
            var validator = app.services.validateForm(form);

            var zoneIdInput = $(form[0].elements.ZoneId);
            var settlementIdInput = $(form[0].elements.SettlementId);
            var typeInput = $(form[0].elements.Type);

            typeInput.on('change', function (event) {

                var type = this.value;

                if (!type)
                    return false;

                type = parseInt(type);

                if (type == 7) {
                    settlementIdInput.val('').closest('div').hide();
                    zoneIdInput.val('').closest('div').hide();
                }
                else if (type == 2 || type == 5) {
                    settlementIdInput.val('').closest('div').show();
                    zoneIdInput.val('').closest('div').hide();
                }
                else {
                    settlementIdInput.val('').closest('div').show();
                    zoneIdInput.val('').closest('div').show();
                }
            });
            settlementIdInput.on('change', async function (event) {

                var settlementId = parseInt(settlementIdInput.val());

                if (isNaN(settlementId))
                    return;

                var zones = await app.services.fetch('api/settlements/' + settlementId + '/zones');

                if (!zones) {
                    alertify.error(app.error);
                    return false;
                }

                var zonesHTML = '';
                $.each(zones, function (i, zone) {
                    zonesHTML += `<option value="${zone.ZoneId}">${zone.Name}</option>`;
                });

                zoneIdInput.html(zonesHTML);
            });

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                var data = form.serializeObject();

                var type = parseInt(typeInput.val());
                if (!type || isNaN(type))
                    return false;

                if (type == 4 || type == 6) {

                    delete data.SettlementId;
                    if (!data.ZoneId) {
                        validator.showErrors({ ZoneId: 'Cerrada es requerida' });
                        return false;
                    }
                }
                else if (type == 2 || type == 5) {

                    delete data.ZoneId;
                    if (!data.SettlementId) {
                        validator.showErrors({ SettlementId: 'Residencial es requerido' });
                        return false;
                    }
                }
                else {

                    delete data.SettlementId;
                    delete data.ZoneId;
                }

                app.loading = true;
                button.button('loading');

                var created = await app.services.ajaxAsync({
                    url: `api/user/sessions`,
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!created) {
                    button.button('reset');
                    return;
                }

                alertify.success('Sesión creada');

                window.location = `#/usuarios/buscar?email=${data.Email}`;
            });

            typeInput.focus();
        });
    };
    var post = async function (context) {

        document.title = `${app.title} - Crear usuario`;

        var settlement = await app.services.fetch('api/settlements/' + context.params.id);
        if (!settlement) {
            alertify.error(app.error);
            return false;
        }

        var zones = await app.services.fetch('api/settlements/' + settlement.SettlementId + '/zones');
        if (!zones) {
            alertify.error(app.error);
            return false;
        }

        var zonesHTML = '';
        $.each(zones, function (i, zone) {
            zonesHTML += `<option value="${zone.ZoneId}">${zone.Name}</option>`;
        });

        var lengthObj = {
            min: 10,
            max: 10,
            message: 'Teléfono a 10 dígitos'
        };

        var templateHTML = app.forms.inputText([
            { name: 'Residencial', disabled: true, value: settlement.Name },
            { name: 'Email', displayName: 'Email', required: true },
            { name: 'Name', displayName: 'Nombre', required: true },
            { name: 'FirstLastName', displayName: 'Primer apellido', required: true },
            { name: 'SecondLastName', displayName: 'Segundo apellido', required: true },
            { name: 'MobilePhone', displayName: 'Teléfono móvil', length: lengthObj },
            { name: 'LocalPhone', displayName: 'Teléfono local', length: lengthObj },
        ]);

        templateHTML += `
        <div class="form-group">
            <label>Tipo de usuario</label>
            <select name="Type" class="form-control">
                <option value="">-- Selecciona tipo de usuario --</option>
                <option value="2">Admin</option>
                <option value="5">Admin de ventas</option>
                <option value="6">Vendedor</option>
                <option value="4">Supervisor</option>
                <option value="7">Manager</option>
            </select>
            ${app.forms.validationErrorDiv('Type')}
        </div>
        <div class="form-group" style="display: none">
            <label>Cerrada</label>
            <select name="ZoneId" class="form-control">
                <option value="">-- Selecciona cerrada --</option>
                ${zonesHTML}
            </select>            
            ${app.forms.validationErrorDiv('ZoneId')}
        </div>
        `;

        templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Crear usuario</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${templateHTML}
                    ${app.forms.formButton('Crear')}
                    <a href="#/residenciales" class="btn btn-default">Regresar</a>
                </form>
            </div>            
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]');
            var validator = app.services.validateForm(form);

            var zoneIdInput = $(form[0].elements.ZoneId);
            var typeInput = $(form[0].elements.Type);

            typeInput.on('change', function (event) {

                var type = this.value;

                if (!type)
                    return false;

                type = parseInt(type);

                if (type == 4 || type == 6)
                    zoneIdInput.closest('div').show();
                else
                    zoneIdInput.val('').closest('div').hide();
            });

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                var data = form.serializeObject();

                var type = parseInt(typeInput.val());
                if (!type || isNaN(type))
                    return false;

                if (type == 4 || type == 6) {
                    if (!data.ZoneId) {
                        validator.showErrors({ ZoneId: 'Cerrada es requerida' });
                        return false;
                    }
                }

                app.loading = true;
                button.button('loading');

                var created = await app.services.ajaxAsync({
                    url: `api/settlements/${settlement.SettlementId}/user`,
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!created) {
                    button.button('reset');
                    return;
                }

                alertify.success('Usuario creado');
                window.location = `#/residenciales/${settlement.SettlementId}/usuarios`;
            });
        });
    };
    var put = async function (context) {

        document.title = `${app.title} - Actualizar usuario`;

        var user = await app.services.fetch('api/users/' + context.params.id);

        if (!user) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { name: 'Email', displayName: 'Email', required: true, value: user.Email },
            { name: 'Name', displayName: 'Nombre', required: true, value: user.Name },
            { name: 'FirstLastName', displayName: 'Primer apellido', required: true, value: user.FirstLastName },
            { name: 'SecondLastName', displayName: 'Segundo apellido', required: true, value: user.SecondLastName },
            { name: 'MobilePhone', displayName: 'Teléfono móvil', value: user.MobilePhone },
            { name: 'LocalPhone', displayName: 'Teléfono local', value: user.LocalPhone },
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar usuario</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Actualizar')}
                    <a href="#/residenciales" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/users/${user.UserId}`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                alertify.success('Usuario actualizado');
                window.location = '#/residenciales';
            });

        });
    };
    var putPassword = async function (context) {

        document.title = `${app.title} - Actualizar contraseña`;

        var user = await app.services.fetch('api/users/' + context.params.id);

        if (!user) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([            
            { name: 'Password', displayName: 'Contraseña', required: true },
            { name: 'RePassword', displayName: 'Repite la contraseña', required: 'Debes repetir la contraseña' },
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar contraseña</h3>
            </div>
            <div class="col-xs-12">
                <h4>${user.Email} | ${user.Name} ${user.FirstLastName} ${user.SecondLastName}</h4>
                <hr>
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Actualizar contraseña')}
                    <a href="#/residenciales" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                var data = form.serializeObject();

                if (data.Password.length < 6) {
                    validator.showErrors({ Password: 'Mínimo 6 caracteres' });
                    return false;
                }

                if (data.Password != data.RePassword) {
                    validator.showErrors({ Password: 'Las contraseñas no coinciden' });
                    return false;
                }

                app.loading = true;
                button.button('loading');

                var updated = await app.services.ajaxAsync({
                    url: `api/users/${user.UserId}/password`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                alertify.success('Contraseña actualizada');

                window.location = '#/residenciales';
            });

        });
    };
    var del = async function (context) {

        document.title = `${app.title} - Eliminar usuario`;

        var user = await app.services.fetch('api/users/' + context.params.id);

        if (!user) {
            alertify.error(app.error);
            return false;
        }

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Eliminar usuario</h3>
            </div>
            <div class="col-xs-12">
                <h4>${user.Email} | ${user.Name} ${user.FirstLastName} ${user.SecondLastName}</h4>
                <hr>
                <form>                    
                    ${app.forms.formButton('Eliminar')}
                    <a href="#/residenciales" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                var data = form.serializeObject();

                app.loading = true;
                button.button('loading');

                var deleted = await app.services.ajaxAsync({
                    url: `api/users/${user.UserId}`,
                    method: 'DELETE',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!deleted) {
                    button.button('reset');
                    return;
                }

                alertify.success('Usuario eliminado');
                window.location = '#/residenciales';
            });

        });
    };

    app.routes.push({ route: '#/residenciales/:id/usuarios/?', callback: list });    
    app.routes.push({ route: '#/residenciales/:id/usuarios/crear/?', callback: post });
    app.routes.push({ route: '#/usuarios/:id/actualizar/?', callback: put });
    app.routes.push({ route: '#/usuarios/:id/eliminar/?', callback: del });
    app.routes.push({ route: '#/usuarios/:id/password/?', callback: putPassword });
    app.routes.push({ route: '#/usuarios/sesiones/?', callback: sessions });
    app.routes.push({ route: '#/usuarios/buscar/?', callback: search });
})();