﻿(function () {

    if (!app)
        throw 'the app module is required';

    var list = async function (context) {

        document.title = `${app.title} - Manzanas`;

        var blocks = await app.services.fetch('api/blocks');

        if (!blocks) {
            alertify.error(app.error);
            return false;
        }

        var buffer = '';
        $.each(blocks, function (i, block) {
            buffer += `
            <tr>                
                <td>${block.ZoneName}</td>
                <td>${block.Name}</td>
                ${app.forms.optionsTd([
                    `<a href="#/manzanas/${block.BlockId}/actualizar">Actualizar</a>`
                ])}
            </tr>
            `;
        });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Manzanas</h3>
            </div>
            <div class="col-xs-12">
                <a href="#/manzanas/crear" class="btn btn-success">Crear</a>
                <a href="#" class="redirect btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Cerrada</th>
                            <th>Nombre</th>
                            <th></th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>                    
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var post = async function (context) {

        document.title = `${app.title} - Crear manzana`;

        var zones = await app.services.fetch('api/zones');

        if (!zones) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true },
            { name: 'Color', displayName: 'Color', placeholder: 'Ej. #A5B5C5 para revisar colores entrar a http://www.color-hex.com' }
        ]);

        var zonesHTML = '';
        $.each(zones, function (i, zone) {
            zonesHTML += `<option value="${zone.ZoneId}">${zone.Name}</option>`;
        });

        inputsBuffer += `
        <div class="form-group">
            <label for="ZoneId">Cerrada</label>
            <select class="form-control" name="ZoneId" data-val="true" data-val-required="Cerrada es requerida">
                <option value="">-- Selecciona cerrada --</option>
                ${zonesHTML}
            </select>
            ${app.forms.validationErrorDiv('ZoneId')}
        </div>
        `;

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Crear manzana</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}                    
                    ${app.forms.formButton('Crear')}
                    <a href="#/manzanas" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')
            var validator = app.services.validateForm(form);
            var inputs = form[0].elements;

            var nameInput = $(inputs.Name);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var created = await app.services.ajaxAsync({
                    url: `api/blocks`,
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!created) {
                    button.button('reset');
                    return;
                }

                window.location = '#/manzanas';
            });

            nameInput.focus();
        });
    };
    var put = async function (context) {

        document.title = `${app.title} - Actualizar manzana`;

        var block = await app.services.fetch('api/blocks/' + context.params.id);

        if (!block) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { displayName: 'Cerrada', disabled: true, value: block.ZoneName },
            { name: 'Name', displayName: 'Nombre', required: true, value: block.Name },
            {
                name: 'Color', displayName: 'Color',
                placeholder: 'Ej. #A5B5C5 para revisar colores entrar a http://www.color-hex.com', value: block.Color
            }
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar manzana</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Actualizar')}
                    <a href="#/manzanas" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/blocks/${block.BlockId}`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                window.location = '#/manzanas';
            });

        });
    };

    app.routes.push({ route: '#/manzanas/?', callback: list });
    app.routes.push({ route: '#/manzanas/crear/?', callback: post });
    app.routes.push({ route: '#/manzanas/:id/actualizar/?', callback: put });

})();