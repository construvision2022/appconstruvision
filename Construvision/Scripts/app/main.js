﻿(function () {

    if (!app)
        throw 'the app module is required';

    var Map = function (options) {

        var map = this;

        var getActionValue = function () {

            var actionIdInput = $('#action-select');
            var action = parseInt(actionIdInput.val());

            if (!isNaN(action))
                return action;

        };
        var getLotDetailsHTML = function (lot) {
            return `
            <ul class="list-unstyled">
                <li>Dirección: ${lot.Address}</li>
                <li>Manzana: [${lot.Block} L:${lot.LotNumber}]</li>
                <li>Modelo: ${lot.ModelName}</li>
                <li>Avance de obra: ${lot.Percentage}%</li>
                <li>Área de terreno: ${lot.GroundArea || '--'}</li>
                <li>Área de construcción: ${lot.ConstructionArea || '--'}</li>
                <li>Estatus de lote: ${lot.StatusName}</li>
                <li>Estatus de venta: ${lot.SaleStatusName}</li>
                <li>Fecha venta: ${lot.SaleDate_iso || '--'}</li>
            </ul>
            `;
        };

        var assignMapImage = function (code) {
            var templateHTML = `
            <form>
                <div class="form-group">
                    <label for="BlockFilter">Manzana</label>
                    <select id="BlockFilter" class="form-control">
                        <option value="" selected>-- Selecciona una manzana --</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="LotId">Número</label>
                    <select id="LotId" name="LotId" class="form-control">
                        <option value="">-- Selecciona número --</option>
                    </select>
                </div>
                <input type='hidden' id='MapImageId' name='MapImageId'>
                <div style='width:100%; text-align:center'>
                    ${app.forms.formButton('Guardar')}
                    <button class='btn btn-default' id='close-modal'>Cancelar</button>
                </div>
            </form>
            `;

            var assignTemplate = _.template(templateHTML);

            app.tools.modal.title.html('Asignar lote a mapa.');
            app.tools.modal.body.html(assignTemplate());

            var form = app.tools.modal.body.find('form');
            var button = form.find('button[type="submit"]');

            var buffer = '';
            var blocksFilter = $('#BlockFilter');
            var lotId = $('#LotId');

            $.each(map.blocks, function (i, block) {
                buffer += '<option value="' + block.BlockId + '">' + block.Name + '</option>'
            });
            blocksFilter.html(buffer);

            if (window.elblockid)
                blocksFilter.val(window.elblockid);

            blocksFilter.on('change', async function (event) {

                var blockId = blocksFilter.val();

                if (!blockId)
                    return false;

                window.elblockid = blockId;

                var lots = await app.services.fetch(`api/blocks/${blockId}/unassignedlots`)

                if (!lots || !lots.length)
                    return false;

                buffer = ''
                $.each(lots, function (i, lot) {
                    buffer += `<option value="${lot.LotId}">${lot.LotNumber}</option>`
                });

                lotId.html(buffer);
            })
            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                app.loading = true;
                button.button('loading');

                var lid = lotId.val();

                if (!lid) {
                    alertify.error('Debes seleccionar un lote.');
                    return false;
                }

                var lot = map.lots[lid];

                console.log("L-" + lot.LotNumber + " -> PATH-" + code);

                var lotAssigned = await app.services.ajaxAsync({
                    url: `api/mapimages/${code}/assignlot`,
                    method: 'PUT',
                    data: { LotId: lid },
                });

                app.loading = false;

                if (lotAssigned) {
                    alertify.success('Lote asignado.');
                    app.Sammy.refresh();
                }
                else {
                    alertify.error('No se pudo asignar el lote');
                }

                app.tools.modal.hide();
            });

            $('#close-modal').on('click', function (event) {
                event.preventDefault();
                app.tools.modal.hide();
            });

            app.tools.modal.show();

            blocksFilter.trigger('change');
        };
        var updateLotAreas = function (code) {

            var lot = map.lots_hash[code];

            if (!lot) {
                alertify.error('Lote no encontrado!');
                return;
            }

            var inputsBuffer = app.forms.inputText([
                {
                    name: 'GroundArea', displayName: 'Área de terreno', number: true, value: lot.GroundArea,
                    range: { message: 'Área de terreno debe ser entre 1 y 9999', min: 1, max: 9999 }
                },
                {
                    name: 'ConstructionArea', displayName: 'Área de construcción', number: true, value: lot.ConstructionArea,
                    range: { message: 'Área de construcción debe ser entre 1 y 9999', min: 1, max: 9999 }
                },
                { name: 'LotNumber', displayName: 'Número de lote', number: true, value: lot.LotNumber },
                { name: 'OfficialNumber', displayName: 'Número oficial', number: true, value: lot.OfficialNumber },
            ]);

            var templateHTML = `
            <form>
                <h5>Estado de venta:
                    ${app.enums.SaleStatus.GetString(lot.SaleStatus)}
                </h5>
                <h5>Estado:
                    ${app.enums.LotStatus.GetString(lot.Status)}
                </h5>
                <h5>Porcentaje:
                    ${lot.Percentage}%
                </h5>
                <h5>Actualizar lote ------- </h5>
                <hr>
                ${inputsBuffer}
                <div class="form-group">
                    <label>Estado de venta</label>
                    <select name="SaleStatus" class="form-control">
                        <option>-- Seleccione estado de venta --</option>
                        <option value="1">Disponible</option>
                        <option value="2">Vendido</option>
                        <option value="3">Firmado</option>
                        <option value="4">Pagado</option>
                        <option value="5">Entregado</option>
                        <option value="6">Integrado</option>
                    </select>
                </div>
                <div style='width:100%; text-align:center'>
                    ${app.forms.formButton('Actualizar')}
                    <button class='btn btn-default' id='close-modal'>Cancelar</button>
                </div>
            </form>
            `;

            app.tools.modal.title.html('Actualizar lote');
            app.tools.modal.body.html(templateHTML);

            var form = app.tools.modal.body.find('form');
            var button = form.find('button[type="submit"]');
            var validator = app.services.validateForm(form);

            var inputs = form[0].elements;
            var groundAreaInput = $(inputs.GroundArea);
            var constructionAreaInput = $(inputs.ConstructionArea);
            var saleStatusInput = $(inputs.SaleStatus);

            saleStatusInput.val(lot.SaleStatus);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/lots/${lot.LotId}`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (updated) {

                    alertify.success('Lote actualizado');

                    lot.GroundArea = data.GroundArea;
                    lot.ConstructionArea = data.ConstructionArea;
                    lot.SaleStatus = data.SaleStatus;
                    lot.LotNumber = data.LotNumber;
                    lot.OfficialNumber = data.OfficialNumber;

                    var original = map.lots[lot.LotId];
                    var street = map.streets[original.StreetId];

                    lot.Address = street.Name + ' #' + lot.OfficialNumber;

                    var newSaleStatus = parseInt(data.SaleStatus);

                    switch (newSaleStatus) {
                        default: lot.SaleStatusName = '--'; break;
                        case app.enums.SaleStatus.Available: lot.SaleStatusName = 'Disponible'; break;
                        case app.enums.SaleStatus.Purchased: lot.SaleStatusName = 'Vendido'; break;
                        case app.enums.SaleStatus.Signed: lot.SaleStatusName = 'Firmado'; break;
                        case app.enums.SaleStatus.Paid: lot.SaleStatusName = 'Pagado'; break;
                        case app.enums.SaleStatus.Delivered: lot.SaleStatusName = 'Entregado'; break;
                        case app.enums.SaleStatus.Integrated: lot.SaleStatusName = 'Integrado'; break;
                    }

                    $('#lots-panel').html(getLotDetailsHTML(lot));

                    map.render();

                    app.tools.modal.hide();

                    return;
                }

                button.button('reset');
                alertify.error('No se pudo actualizar el lote');
            });

            $('#close-modal').on('click', function (event) {
                event.preventDefault();
                app.tools.modal.hide();
            });

            app.tools.modal.show();
        };
        var updateLotModel = function (code) {

            var lot = map.lots_hash[code];

            if (!lot) {
                alertify.error('Lote no encontrado!');
                return;
            }

            var inputsBuffer = app.forms.inputText([
                { displayName: 'Cerrada', readonly: true, value: lot.ZoneName },
                { displayName: 'Estado de venta', readonly: true, value: app.enums.SaleStatus.GetString(lot.SaleStatus) },
                { displayName: 'Estado', readonly: true, value: app.enums.LotStatus.GetString(lot.Status) },
                { displayName: 'Porcentaje', readonly: true, value: `${lot.Percentage}%` },
                { displayName: 'LotNumber', displayName: 'Número de lote', readonly: true, value: `${lot.LotNumber}` },
                { displayName: 'OfficialNumber', displayName: 'Número oficial', readonly: true, value: `${lot.OfficialNumber}` },
                { displayName: 'GroundArea', displayName: 'Área de terreno', readonly: true, value: `${lot.GroundArea}` },
                { displayName: 'ConstructionArea', displayName: 'Área de construcción', readonly: true, value: `${lot.ConstructionArea}` },
            ]);

            var modelsHTML = '<option value="">-- Selecciona modelo --</option>';
            $.each(map.models, function (i, model) {
                modelsHTML += `
                <option value="${model.ModelId}"
                    ${model.ModelId == lot.ModelId ? 'selected' : ''}>
                    ${model.Name}
                </option>
                `;
            });

            var templateHTML = `
            <div class="row">
                <div class="col-xs-12">
                    <form>
                        ${inputsBuffer}
                        <div class="form-group">
                            <label>Modelo</label>
                            <select name="ModelId" class="form-control"
                                data-val="true"
                                data-val-required="Modelo es requerido">
                                ${modelsHTML}
                            </select>
                            ${app.forms.validationErrorDiv('ModelId')}
                        </div>
                        ${app.forms.formButton('Actualizar modelo')}
                    </form>
                </div>
            </div>
            `;

            app.tools.modal.title.html('Actualizar modelo del lote.');
            app.tools.modal.body.html(templateHTML);

            var form = app.tools.modal.body.find('form');
            var button = form.find('button[type="submit"]');

            var validator = app.services.validateForm(form);

            var inputs = form[0].elements;
            var modelIdInput = $(inputs.ModelId);

            if (lot.ModelId)
                modelIdInput.val(lot.ModelId);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/lots/${lot.LotId}/model`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated)
                    return;

                alertify.success('Modelo del lote actualizado');
                app.tools.modal.hide();
                app.Sammy.refresh();

            });

            $('#close-modal').on('click', function (event) {
                event.preventDefault();
                app.tools.modal.hide();
            });

            app.tools.modal.show();
        };
        var deleteLot = function (code) {

            var lot = map.lots_hash[code];

            if (!lot) {
                alertify.error('Lote no encontrado!');
                return;
            }

            var templateHTML = `
                <form>
                    <div style='width:100%; text-align:center'>
                        ${app.forms.formButton('Eliminar')}
                        <button class='btn btn-default' id='close-modal'>Cancelar</button>
                    </div>
                </form>
                `;

            app.tools.modal.title.html('Eliminar lote.');
            app.tools.modal.body.html(templateHTML);

            var form = app.tools.modal.body.find('form');

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                app.loading = true;

                var deleted = await app.services.ajaxAsync({
                    url: `api/lots/${lot.LotId}`,
                    method: 'DELETE'
                });

                app.loading = false;

                if (deleted) {
                    alertify.success('Lote eliminado');
                    app.Sammy.refresh();
                }
                else
                    alertify.error('No se pudo eliminar el lote');

                app.tools.modal.hide();
            });

            $('#close-modal').on('click', function (event) {
                event.preventDefault();
                app.tools.modal.hide();
            });
            app.tools.modal.show();
        };
        var createReport = async function (code) {

            var lot = map.lots_hash[code];

            if (!lot) {
                alertify.error('Lote no encontrado!');
                return;
            }

            if (lot.Status == 3) {
                alertify.alert('No puedes crear reportes en un lote terminado');
                return;
            }

            var templateHTML = `
            <div id="report-container">
                <div class="col-xs-12 text-center">
                    <h4>Cargando...</h4>
                </div>
            </div>
            <div class="clearfix"></div>
            `;

            app.tools.modal.title.html(`Crear reporte ${lot.Address}`);
            app.tools.modal.body.html(templateHTML);
            app.tools.modal.show();

            var lotStatus = await app.services.fetch(`api/lots/${lot.LotId}/status`);

            if (!lotStatus) {
                app.tools.modal.body.html('')
                app.tools.modal.hide();
                return;
            }

            var component = new app.components.ReportCreator({
                containerId: '#report-container',
                lot: lot,
                concepts: lotStatus.Concepts,
                onSubmit: function (response) {

                    lot.Percentage = response.LotPercentage;
                    lot.Status = response.LotStatus;
                    switch (response.LotStatus) {
                        case app.enums.LotStatus.Pending: lot.StatusName = 'Terreno'; break;
                        case app.enums.LotStatus.InConstruction: lot.StatusName = 'En construcción'; break;
                        case app.enums.LotStatus.Finished: lot.StatusName = 'Terminado'; break;
                    }

                    $('#lots-panel').html(getLotDetailsHTML(lot));
                    app.tools.modal.hide();
                    map.render();
                }

            });

            $('#close-modal').on('click', function (event) {
                event.preventDefault();
                app.tools.modal.hide();
            });
        };

        map.initialize = function (options) {

            map.isComponent = true;

            options = options || {};

            map.$el = options.$el;

            if (!options.$el) throw new Error('$el');
            if (!options.zone) throw new Error('zone');
            if (!options.lots) throw new Error('lots');
            if (!options.models) throw new Error('models');
            if (!options.blocks) throw new Error('blocks');
            if (!options.streets) throw new Error('streets');
            if (!options.paths) throw new Error('paths');

            this.map = null;
            this.regionValues = {};
            this.regionScales = {};
            this.map_paths = {};
            this.lots_hash = {};
            this.defaultLabels = {
                'Lote': '#4a8bc2',
                'Área Común': '#00e500',
                'Otro': '#f0f0f0'
            };
            this.width = options.width;
            this.height = options.height;

            map.zone = options.zone;

            var lots = _(options.lots).indexBy(function (lot) {
                return lot.LotId
            });
            var models = _(options.models).indexBy(function (model) {
                return model.ModelId
            });
            var blocks = _(options.blocks).indexBy(function (block) {
                return block.BlockId
            });
            var streets = _(options.streets).indexBy(function (street) {
                return street.StreetId
            });
            var paths = _(options.paths).indexBy(function (path) {
                return path.MapImageId
            });

            map.blocks = blocks;
            map.lots = lots;
            map.models = models;
            map.streets = streets;

            map.multiselect = false;
            map.defaultColor = options.defaultColor || '#f3fbe7';

            _.each(paths, function (path) {

                var address, lot, model, block, street, status

                if (path.Type == 1) {

                    lot = lots[path.LotId]

                    if (lot) {

                        if (lot.ModelId)
                            model = models[lot.ModelId];

                        if (lot.BlockId)
                            block = blocks[lot.BlockId];

                        if (lot.StreetId)
                            street = streets[lot.StreetId];

                        address = lot.OfficialNumber ?
                            street ? street.Name + ' #' + lot.OfficialNumber :
                                "Sin calle " + lot.OfficialNumber :
                            street ? street.Name + " S/N" :
                                "Sin calle S/N";

                        var item = {
                            LotId: path.LotId,

                            Address: address,
                            LotNumber: lot.LotNumber,
                            OfficialNumber: lot.OfficialNumber,
                            GroundArea: lot.GroundArea,
                            ConstructionArea: lot.ConstructionArea,
                            SaleDate: lot.SaleDate,
                            SaleDate_iso: lot.SaleDate_iso,
                            Status: lot.Status,
                            SaleStatus: lot.SaleStatus,
                            Percentage: lot.Percentage,

                            Street: street ? street.Name : 'No asignada',
                            Block: block ? block.Name : 'No asignado',
                            BlockId: block ? block.BlockId : null,

                            ZoneName: map.zone.Name
                        };

                        switch (lot.Status) {
                            default: item.StatusName = '--'; break;
                            case app.enums.LotStatus.Pending: item.StatusName = 'No vendido'; break;
                            case app.enums.LotStatus.InConstruction: item.StatusName = 'En construcción'; break;
                            case app.enums.LotStatus.Finished: item.StatusName = 'Terminado'; break;
                        }

                        switch (lot.SaleStatus) {
                            default: item.SaleStatusName = '--'; break;
                            case app.enums.SaleStatus.Available: item.SaleStatusName = 'Disponible'; break;
                            case app.enums.SaleStatus.Purchased: item.SaleStatusName = 'Vendido'; break;
                            case app.enums.SaleStatus.Signed: item.SaleStatusName = 'Firmado'; break;
                            case app.enums.SaleStatus.Paid: item.SaleStatusName = 'Pagado'; break;
                            case app.enums.SaleStatus.Delivered: item.SaleStatusName = 'Entregado'; break;
                            case app.enums.SaleStatus.Integrated: item.SaleStatusName = 'Integrado'; break;
                        }

                        if (model) {
                            item.ModelId = model.ModelId;
                            item.ModelName = model.Name;
                        } else {
                            item.ModelName = '--';
                        }

                        map.lots_hash[path.MapImageId] = item;

                    } else {

                        map.map_paths[path.MapImageId] = {
                            path: path.Path,
                            name: 'Lote no asignado.',
                        }

                    }
                }

                map.map_paths[path.MapImageId] = {
                    path: path.Path,
                    name: address || (path.Type == 2 ? 'Área común' : 'Lote no asignado.'),
                }

            }, this);

            map.getRegionLabels = function (type) {
                switch (type) {
                    case 0: return 'Otro';
                    case 1: return 'Lote';
                    case 2: return 'Área Común';
                }
            };

            var buildingStatusHash = {
                1: {
                    StatusType: 1,
                    StatusName: 'Terreno',
                    Color: '#a1cde3'
                },
                2: {
                    StatusType: 2,
                    StatusName: 'En construcción',
                    Color: '#e2d77f'
                },
                3: {
                    StatusType: 3,
                    StatusName: 'Terminado',
                    Color: '#5cde83'
                }
            };
            var saleStatusHash = {
                1: {
                    StatusType: 1,
                    StatusName: 'Disponible',
                    Color: '#F00'
                },
                2: {
                    StatusType: 2,
                    StatusName: 'Vendido',
                    Color: '#0F0'
                },
                3: {
                    StatusType: 3,
                    StatusName: 'Firmado',
                    Color: '#00F'
                },
                4: {
                    StatusType: 4,
                    StatusName: 'Pagado',
                    Color: '#FF0'
                },
                5: {
                    StatusType: 5,
                    StatusName: 'Entregado',
                    Color: '#F0F'
                },
                6: {
                    StatusType: 6,
                    StatusName: 'Integrado',
                    Color: '#0FF'
                }
            };

            //1: Default
            map._handle0 = function () {
                _.each(paths, function (path) {
                    map.regionValues[path.MapImageId] = map.getRegionLabels(path.Type)
                }, this)

                map.regionScales = app.tools.mergeObjects({}, map.defaultLabels)

            };
            //2: Models
            map._handle1 = function () {
                _.each(paths, function (path) {
                    switch (path.Type) {
                        case 1:
                            var lot = map.lots_hash[path.MapImageId]
                            map.regionValues[path.MapImageId] = lot.ModelId
                            break;
                        case 2:
                            map.regionValues[path.MapImageId] = 'Área Común';
                            break;

                        default: map.regionValues[path.MapImageId] = 'Otro'; break;
                    }
                }, this)
                _.each(models, function (model) {
                    map.regionScales[model.ModelId] = model.Color
                }, this)

                map.regionScales = app.tools.mergeObjects(map.regionScales, map.defaultLabels)

            };
            //3: Model
            map._handle2 = function (id) {
                _.each(paths, function (path) {
                    switch (path.Type) {
                        case 1:
                            var lot = map.lots_hash[path.MapImageId]
                            map.regionValues[path.MapImageId] = lot.ModelId
                            break;
                        case 2:
                            map.regionValues[path.MapImageId] = 'Área Común';
                            break;

                        default:
                            map.regionValues[path.MapImageId] = 'Otro';
                            break;
                    }
                }, this)
                _.each(models, function (model) {
                    if (model.ModelId == id) {
                        map.regionScales[model.ModelId] = model.Color
                    } else {
                        map.regionScales[model.ModelId] = map.defaultColor
                    }
                }, this)

                map.regionScales = app.tools.mergeObjects(map.regionScales, map.defaultLabels)
            };
            //4: Blocks
            map._handle3 = function () {
                _.each(paths, function (path) {
                    switch (path.Type) {
                        case 1:
                            var lot = map.lots_hash[path.MapImageId]
                            map.regionValues[path.MapImageId] = lot.BlockId
                            break;
                        case 2:
                            map.regionValues[path.MapImageId] = 'Área Común';
                            break;
                        default: map.regionValues[path.MapImageId] = 'Otro'; break;
                    }
                }, this)
                _.each(blocks, function (block) {
                    map.regionScales[block.BlockId] = block.Color
                }, this)

                map.regionScales = app.tools.mergeObjects(map.regionScales, map.defaultLabels)
            };
            //5: Block
            map._handle4 = function (id) {
                _.each(paths, function (path) {
                    switch (path.Type) {
                        case 1:
                            var lot = map.lots_hash[path.MapImageId]
                            map.regionValues[path.MapImageId] = lot.BlockId
                            break;
                        case 2:
                            map.regionValues[path.MapImageId] = 'Área Común';
                            break;

                        default: map.regionValues[path.MapImageId] = 'Otro'; break;
                    }
                }, this)
                _.each(blocks, function (block) {
                    if (block.BlockId == id) {
                        map.regionScales[block.BlockId] = block.Color
                    } else {
                        map.regionScales[block.BlockId] = map.defaultColor
                    }
                }, this)

                map.regionScales = app.tools.mergeObjects(map.regionScales, map.defaultLabels)
            };
            //6: Building status
            map._handle5 = function () {

                $.each(paths, function (i, path) {

                    if (path.Type == 1) {
                        var lot = map.lots_hash[path.MapImageId];

                        if (lot) {
                            map.regionValues[path.MapImageId] = lot.Status;
                            return;
                        }

                        console.log(`MapImageId ${path.MapImageId} has a Type of '1' and no Lot`);

                    } else if (path.Type == 2)
                        map.regionValues[path.MapImageId] = 'Área Común';
                    else
                        map.regionValues[path.MapImageId] = 'Otro';

                });

                $.each(buildingStatusHash, function (i, item) {
                    map.regionScales[item.StatusType] = item.Color;
                });

                map.regionScales = app.tools.mergeObjects(map.regionScales, map.defaultLabels);
            };
            //7: Sale status
            map._handle6 = function (typeId) {

                _.each(paths, function (path) {
                    switch (path.Type) {
                        case 1:
                            var lot = map.lots_hash[path.MapImageId]
                            map.regionValues[path.MapImageId] = lot.SaleStatus;
                            break;
                        case 2:
                            map.regionValues[path.MapImageId] = 'Área Común';
                            break;

                        default: map.regionValues[path.MapImageId] = 'Otro'; break;
                    }
                }, this);

                _.each(saleStatusHash, function (item) {
                    if (item.StatusType == typeId) {
                        map.regionScales[item.StatusType] = item.Color
                    } else {
                        map.regionScales[item.StatusType] = map.defaultColor
                    }
                }, this)

                map.regionScales = app.tools.mergeObjects(map.regionScales, map.defaultLabels)
            };
        };

        map.close = function () {

            if (map.instance && map.instance.remove) {
                map.instance.remove()
                map.instance = null
            }
        };

        map.render = function (options) {

            map.close();

            app.tools.cleanObj(map.regionScales);
            app.tools.cleanObj(map.regionValues);

            options = options || {}
            var filter = options.filter || (map.filter || 0)

            //1: Default
            //2: Models
            //3: Model
            //4: Blocks
            //5: Block
            //6: Building status
            //7: Sale status

            switch (filter) {
                case 0: map._handle0(); break;
                case 1: map._handle1(); break;
                case 2: map._handle2(options.modelId); break;
                case 3: map._handle3(); break;
                case 4: map._handle4(options.blockId); break;
                case 5: map._handle5(); break;
                case 6: map._handle6(options.type); break;
                case 7: map._handle7(options.type); break;
            };

            $.fn.vectorMap('addMap', 'map', {
                width: map.width,
                height: map.height,
                paths: map.map_paths
            });

            map.$el.vectorMap({
                map: 'map',
                showTooltip: true,
                backgroundColor: 'transparent',
                regionsSelectable: true,
                regionsSelectableOne: !map.multiselect,
                //markers: map.markers,
                series: {
                    regions: [{
                        values: map.regionValues,
                        scale: map.regionScales
                    }]
                },
                regionStyle: {
                    selected: {
                        fill: 'blue',
                        stroke: 'none',
                        "fill-opacity": 1,
                        "stroke-opacity": 1,
                        "stroke-width": 0,
                    }
                },
                regionLabelStyle: {
                    initial: {
                        'font-size': 10
                    },
                    hover: {
                        cursor: 'pointer'
                    }
                },
                markerStyle: {
                    initial: {
                        fill: '#F8E23B',
                        stroke: '#383f47'
                    }
                },
                markerLabelStyle: {
                    initial: {
                        'font-family': 'Verdana',
                        'font-size': 16,
                        'font-weight': 'bold',
                        cursor: 'default',
                        fill: 'white',
                        stroke: '#333',
                    }
                },
                labels: {
                    regions: {
                        render: function (code) {

                            var lot = map.lots_hash[code];

                            if (!lot)
                                return false;

                            return `${lot.LotNumber} | ${lot.Percentage}%`;
                        }
                    }
                },

                onRegionTipShow: map.onRegionTipShow,
                onRegionClick: map.onRegionClick,
                onRegionSelected: map.onRegionSelected,
                //onViewPortChange: map.onViewPortChange,
            })

            map.instance = map.$el.vectorMap('get', 'mapObject');
        };

        map.action = async function (event, action, code) {

            var lot = map.lots_hash[code]

            switch (action) {

                case 1: return assignMapImage(code);
                case 2: return updateLotAreas(code);
                case 3: return updateLotModel(code);
                case 4: return deleteLot(code);
                case 5: return createReport(code);
            }
        };

        map.onRegionClick = function (event, code) {

            var action = getActionValue();

            if (action) {

                if (action == 1)
                    return true;

                return map.lots_hash[code] != null;
            }

        };

        map.onRegionSelected = function (event, code, isSelected, selectedRegions) {

            if (!isSelected)
                return false;

            var action = getActionValue();
            if (action == 1) {
                map.action(event, action, code);
                return;
            }

            var lot = map.lots_hash[code];

            if (!lot)
                return false;

            app.log('path: ' + code + ' | lot: ' + lot.LotId);

            $('#lots-panel').html(getLotDetailsHTML(lot));

            map.action(event, action, code);

            console.log("Path -> " + code);
        };

        map.onRegionTipShow = function (event, el, code) {

            var html = el.html();

            if (!html)
                return false;

            var lot = map.lots_hash[code];

            if (!lot)
                return false;

            var output = `
            <i class="fa fa-location-arrow fa-fw"></i>${html}
            <br>
            <i class="fa fa-home fa-fw"></i>
            Manzana: [${lot.Block} L:${lot.LotNumber}]
            <br>
            <i class="fa fa-home fa-fw"></i>
            Modelo: ${lot.ModelName}
            <br>
            ${lot.SaleDate ? `<br>Venta: ${lot.SaleDate_iso}` : ''}
            <i class="fa fa-check-circle fa-fw"></i>
            Avance de obra: ${lot.Percentage || '0'}%
            <br>
            <i class="fa fa-square-o fa-fw"></i>
            Terreno: ${lot.GroundArea || '--'}
            <br>
            <i class="fa fa-square fa-fw"></i>
            Construcción: ${lot.ConstructionArea || '--'}
            `;

            el.html(output);
        };

        map.initialize(options);
    };

    app.components.ReportCreator = function (options) {

        if (!options.containerId)
            throw 'ReportCreator: bad containerId';

        var c = {
            containerId: options.containerId,
            onSubmit: options.onSubmit || $.noop,
            lot: options.lot,
            concepts: options.concepts,
            fileCode: 'not-found.jpg',
        };

        var getConceptById = function (id) {
            var output = null;
            $.each(options.concepts, function (i, concept) {

                if (concept.BuildingConceptId == id) {
                    output = concept;
                    return false;
                }

            });
            return output;
        };

        c.container = $(c.containerId);

        var drawComponent = function () {

            var conceptsHTML = `
            <div class="col-xs-12 well">
                <div class="form-group">
                    <input id="file-input" type="file" class="form-control">
                </div>
                <div class="form-group">
                    <div id="file-message" class="text-danger"></div>
                </div>
            </div>
            <div class="col-xs-12 text-center well"
                ${c.fileCode == 'not-found.jpg' ? 'style="display: none" ' : ''}>
                <img id="file-image" class="img-responsive"
                src="/content/images/uploads/${c.fileCode}">
            </div>
            <div class="clearfix"></div>
            `;

            $.each(c.concepts, function (i, concept) {

                var colorClass = '';
                if (concept.Percentage == 100)
                    colorClass = 'text-success';
                else if (concept.Percentage == 0)
                    colorClass = 'text-danger';

                conceptsHTML += `
                <div class="well well-sm">
                    <div class="${colorClass}">
                        ${concept.Name}: <strong>${concept.Percentage}%</strong>
                    </div>
                    <input type="range"
                        data-id="${concept.BuildingConceptId}"
                        value="${concept.Percentage || 0}"
                        step="25"
                        min="0"
                        max="100"
                        class="form-control range">
                </div>
                `;

            });

            conceptsHTML += `
            <div class="col-xs-12">
                <br>
                <button id="save-button" class="btn btn-success" data-loading-text="Cargando...">
                    Guardar
                </button>
            </div>
            `;

            conceptsHTML = `
            <div id="component-bubble" class="col-xs-12">
                ${conceptsHTML}
            </div>`;

            c.container.html(conceptsHTML);

            var bubble = c.container.find('#component-bubble');

            var fileInput = bubble.find('#file-input');
            var fileMessage = bubble.find('#file-message');

            bubble.on('change', '.range', function (event) {

                var $input = $(this);

                var percentage = parseInt($input.val());

                if (isNaN(percentage))
                    return false;

                var id = $input.data('id');

                var concept = getConceptById(id);

                if (concept.LastPeriodPercentage > percentage) {

                    drawComponent();

                    //$input.val(concept.LastPeriodPercentage);
                    //var div = $input.closest('div');
                    //var strong = div.find('strong');
                    //strong.html(concept.LastPeriodPercentage + '%');

                    alertify.error(`Mínimo para ${concept.Name}: ${concept.LastPeriodPercentage}%`);
                    return false;
                }

                concept.Percentage = percentage;

                $.each(c.concepts, function (i, item) {

                    if (item.BuildingConceptId == concept.BuildingConceptId)
                        return true;

                    if (!item.CriticalRoute)
                        return true;

                    if (item.CriticalRoute < concept.CriticalRoute)
                        item.Percentage = 100;

                    if (item.CriticalRoute > concept.CriticalRoute)
                        item.Percentage = 0;
                });

                drawComponent();

            });
            bubble.on('change', '#file-input', async function () {

                if (app.loading)
                    return false;

                var inputFiles = fileInput[0].files;

                if (!inputFiles.length) {
                    fileMessage.html('No hay archivo seleccionado');
                    return;
                }

                var file = inputFiles[0];

                var allowedMimeTypes = [
                    'image/bmp',
                    'image/gif',
                    'image/jpeg',
                    'image/png',
                ];

                if (allowedMimeTypes.indexOf(file.type) == -1) {
                    fileMessage.html('Tipo de archivo no soportado');
                    return;
                }

                var maxFileSize = 3;
                var sizeKB = file.size / 1024;
                var sizeMB = (sizeKB / 1024).toFixed(3);

                if (sizeMB >= maxFileSize) {
                    fileMessage.html('Peso maximo permitido: ' + maxFileSize + ' Megabytes');
                    return;
                }

                app.loading = true;
                fileMessage.html('Cargando...');

                var formData = new FormData();
                formData.append('file', file);

                var request = $.ajax({
                    url: baseUrl + `api/images/mobile-upload`,
                    type: 'POST',
                    mimeType: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    data: formData,
                    xhr: function () {

                        var xhr = $.ajaxSettings.xhr();

                        xhr.upload.onprogress = function (event) {

                            var percentage = Math.floor(event.loaded / event.total * 100);

                            if (isNaN(percentage))
                                return;

                            fileMessage.html(percentage + '%');

                        };

                        return xhr;
                    }
                });

                request.done(function (response, textStatus) {
                    var json = JSON.parse(response);
                    c.fileCode = json.FileName;
                    var image = bubble.find('#file-image');
                    image.attr('src', '/content/images/uploads/' + c.fileCode);
                    image.closest('div').show();
                });

                request.fail(function (response) {
                    app.parseErrors(response);
                });

                request.always(function () {
                    app.loading = false;
                    fileMessage.html('');
                });

            });
            bubble.on('click', '#save-button', async function () {

                if (app.loading)
                    return false;

                var button = $(this);

                button.button('loading');
                app.loading = true;

                var data = {
                    LotId: c.lot.LotId,
                    FileName: c.fileCode,
                    BuildingConcepts: c.concepts
                };

                var response = await app.services.ajaxAsync({
                    url: 'api/reports',
                    data: data
                });

                app.loading = false;

                if (!response) {
                    button.button('reset');
                    return false;
                }

                alertify.success('Reporte creado.');

                c.onSubmit(response);
            });
        };

        drawComponent();

        return c;
    };
    app.components.Creator = function (form) {

        if (!form)
            throw Error('bad textAreas');

        var that = this;

        var textAreaError = function (textArea, message) {
            textArea.addClass('input-validation-error');
            textArea.closest('.form-group').animateCss('bounceIn', function () {
                textArea.removeClass('bounceIn');
                textArea.removeClass('input-validation-error');
            });
            throw Error(message);
        };

        var getTextAreaData = function (textArea, name, options) {

            textArea = $(textArea);

            var items = [];
            options = options || {
                required: false
            };

            var text = textArea.val();

            if (!text && options.fill) {

                text = '\n';
            }

            if (options.required && !text) {
                textAreaError(textArea, `Columna ${name} es inválida`);
            }

            if (!text) {

                if (!options.fill) {
                    textAreaError(textArea, `Columna ${name} es inválida`);
                }

                for (var i = 0; i < options.elements; i++) {
                    items.push('\n');
                }

                return items;
            }

            var arr = text.split('\n');

            if (!arr.length)
                textAreaError(textArea, `Columna ${name} es inválida`);

            var counter = options.elements || arr.length;
            for (var ix = 0; ix < counter; ix++) {
                var item = arr[ix];

                if (item == undefined && options.fill) {

                    item = options.defaultValue;

                } else {

                    if (!item) {

                        //TODO: should remove all empty lines and update the textarea's inner html

                        if (options.fill) {
                            item = options.defaultValue;
                        }

                        if (!item && options.required) {
                            textAreaError(textArea, `Elemento #${ix + 1} de ${name} es inválido`);
                        }

                    } else {

                        item = item.trim();
                        item = item.toLowerCase();
                        item = item.replace('\t', '');

                        if (item.length == 1) {
                            item = item.charAt(0).toUpperCase();
                        } else {
                            item = item.charAt(0).toUpperCase() + item.slice(1);
                        }
                    }
                }

                if (options.required && !item) {
                    textAreaError(textArea, `Elemento #${ix + 1} de ${name} es inválido`);
                }

                if (ix + 1 <= counter)
                    items.push(item);
            }

            return items;
        };

        that.create = function () {

            var data = {};

            var requiredConfig = {
                required: true
            };

            var textAreas = form[0].elements;

            data.Settlements = getTextAreaData(textAreas.settlements, 'Residenciales', requiredConfig);
            data.Zones = getTextAreaData(textAreas.zones, 'Cerradas', requiredConfig);
            data.Blocks = getTextAreaData(textAreas.blocks, 'Manzanas', requiredConfig);
            data.Streets = getTextAreaData(textAreas.streets, 'Calles', requiredConfig);
            data.Models = getTextAreaData(textAreas.models, 'Modelos', requiredConfig);
            data.LotNumbers = getTextAreaData(textAreas.lotNumbers, 'Números de lote', requiredConfig);
            data.RUVPackages = getTextAreaData(textAreas.ruvPackages, 'Paquetes RUV', requiredConfig);
            data.BankPackages = getTextAreaData(textAreas.bankPackages, 'Paquetes Bancarios', requiredConfig);

            data.OfficialNumbers = getTextAreaData(textAreas.officialNumbers, 'Números oficiales', {
                required: false,
                fill: true,
                defaultValue: 'N/A',
                elements: data.Settlements.length
            });

            return data;
        };
    };

    var login = function (context) {

        if (app.session.authorizationToken) {
            app.redirect();
            return;
        }

        var email = localStorage.getItem('last_email');

        var html = `
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3">
                <h3 style="text-align: center;">
                    <i class="text-muted fa fa-list-alt"></i> Inicio de sesión
                </h3>
                <form>
                    <div class="form-group">
                        <label for="Email">Email</label>
                        <input class="form-control" name="Email" type="text"
                            data-val="true" data-val-email="Formato incorrecto." data-val-required="Email es requerido"
                            placeholder="Email" value="${email || ''}">
                        ${app.forms.validationErrorDiv('Email')}
                    </div>
                    <div class="form-group">
                        <label for="Password">Contraseña</label>
                        <input class="form-control" name="Password" type="password"
                            data-val="true" data-val-required="Contraseña es requerida."
                            data-val-length="Minimo 6 caractéres" data-val-length-min="6" placeholder="Contraseña">
                        ${app.forms.validationErrorDiv('Password')}
                    </div>
                    ${app.forms.formButton('Entrar')}
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(html, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            var inputs = form[0].elements;

            var emailInput = $(inputs.Email);
            var passwordInput = $(inputs.Password);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var response = await app.services.ajaxAsync({
                    url: 'api/login',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!response) {
                    emailInput.select();
                    passwordInput.val('');
                    button.button('reset');
                    return false;
                }

                app.session.user = {
                    Email: response.Email,
                    Name: response.Name,
                    FirstLastName: response.FirstLastName,
                    SecondLastName: response.SecondLastName,
                };

                app.session.authorizationToken = response.Token;

                app.navbar(2);

                localStorage.setItem('last_email', response.Email);
                localStorage.setItem('User', JSON.stringify(app.session.user));
                localStorage.setItem('AuthorizationToken', app.session.authorizationToken);

                window.location = '#/sesiones';
            });

            emailInput.select();
        });
    };
    var logout = async function (context) {

        var response = await app.services.ajaxAsync({
            url: `api/logout`
        });

        app.session.authorizationToken = null
        app.session.user = null
        app.session.sessionId = null
        app.session.sessionType = null

        localStorage.removeItem('AuthorizationToken')
        localStorage.removeItem('SessionId')
        localStorage.removeItem('SessionType')
        localStorage.removeItem('User')

        app.navbar(1);

        window.location = '#/login'
    };
    var sessions = async function (context) {

        if (!app.session || !app.session.authorizationToken) {
            app.pageMessage({
                icon: 'fa fa-times',
                message: 'Debes iniciar sesión',
                href: `#/login`,
                text: 'Login'
            });
            return;
        }

        var sessions = await app.services.fetch('api/sessions');

        if (!sessions)
            return app.pageMessage('No se pudieron cargar las sesiones');

        var imagesFolder = '/Content/images/uploads/';

        var buffer = '';
        $.each(sessions, function (index, session) {

            buffer += `
            <div class="col-xs-6 col-md-4 col-lg-3 thumbnail session-item"
                data-id="${session.SessionId}" data-type="${session.SessionType}">
                <img title="${session.Description}" class="img-responsive"
                src="${imagesFolder}${session.ImageCode || 'not-found.jpg'}"
                onerror="this.onerror=null;this.src='${imagesFolder}not-found.jpg';"/>
                <h3 class="centered-text">${session.Description}</h3>
            </div>
            `;
        });
        console.log(sessions);
        app.Sammy.swap(`<div class="row">${buffer}</div>`, function (bubble) {

            bubble.on('click', '.session-item', function (event) {

                var div = $(this);

                var data = div.data();

                localStorage.setItem('SessionId', data.id);
                localStorage.setItem('SessionType', data.type);

                app.readLocalStorage();

                setTimeout(function () {

                    app.navbar(3);
                    app.redirect();

                }, 100);

            });
        });

    };
    var maps = async function (context) {

        document.title = `${app.title} - Mapas`;

        var getTemplateHTML = function () {

            var getTitleHTML = function (id, label) {
                return `
                <h4 class="panel-title">
                    <a>
                        ${label}
                        <span id='${id}' class="badge pull-right hidden"
                            style='font-style:normal; font-weight:600; background:white;
                            color:#404040; border:1px solid #ddd'>--</span>
                    </a>
                </h4>
                `;
            };

            return `
            <div class="row">
                <div class="col-xs-12">
                    <a href="#" class="btn btn-default redirect">Regresar</a>
                </div>
                <div class="col-xs-12">
                    <hr>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-4 whitebg">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="zone-select">Cerrada</label>
                            <select id="zone-select" class="form-control"></select>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="action-select">Acción</label>
                            <select id="action-select" class="form-control">
                                <option value="" selected disabled>-- Seleccione una opción --</option>
                                <option value="5">Crear Reporte</option>
                                <option value="2">Actualizar lote</option>
                                <option value="3">Actualizar modelo</option>
                                <option value="1">Asignar lote a imagen de mapa</option>
                                <option value="4">Eliminar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="panel-group" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab"
                                data-toggle="collapse" data-parent="#accordion" href="#building-status-collapse">
                                    ${getTitleHTML('building-status-total', 'Estados de construcción')}
                                </div>
                                <div id="building-status-collapse" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body fontsm" id='building-status-panel'>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Estado</th>
                                                    <th>Lotes</th>
                                                <tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab"
                                    data-toggle="collapse" data-parent="#accordion" href="#sale-status-collapse">
                                    ${getTitleHTML('sale-status-total', 'Estados de venta')}
                                </div>
                                <div id="sale-status-collapse" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body fontsm" id='sale-status-panel'>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Estado</th>
                                                    <th>Lotes</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab"
                                    data-toggle="collapse" data-parent="#accordion" href="#models-collapse">
                                    ${getTitleHTML('models-total', 'Modelos')}
                                </div>
                                <div id="models-collapse" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body fontsm" id='models-panel'>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Modelo</th>
                                                    <th>Lotes</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab"
                                    data-toggle="collapse" data-parent="#accordion" href="#blocks-collapse">
                                    ${getTitleHTML('blocks-total', 'Manzanas')}
                                </div>
                                <div id="blocks-collapse" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body fontsm" id='blocks-panel'>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Manzana</th>
                                                    <th>Lotes</th>
                                                <tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab"
                                    data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                    <h4 class="panel-title">
                                        <a>Información de Lote</a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse in" role="tabpanel">
                                    <div class="panel-body fontsm" id='lots-panel'>
                                        Selecciona un lote para ver su información...
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-8 whitebg">
                    <div id="map-container" style="height: 700px; background:#ddd"></div>
                </div>
            </div>
            `;
        };

        var zones = await app.services.fetch('api/zones');

        if (!zones) {
            alertify.error(app.error);
            return false;
        }

        app.Sammy.swap(getTemplateHTML(), function (bubble) {

            var zonesBuffer = '<option selected disabled>-- Selecciona una cerrada --</option>';
            $.each(zones, function (i, zone) {
                zonesBuffer += `<option value="${zone.ZoneId}">${zone.Name}</option>`;
            });

            var actionInput = bubble.find('#action-select');
            var zoneIdInput = bubble.find('#zone-select');

            zoneIdInput.html(zonesBuffer);

            zoneIdInput.on('change', async function (event) {

                var value = zoneIdInput.val()

                if (!$.isNumeric(value))
                    return;

                zoneIdInput.prop('disabled', true);

                if (app.db.map)
                    app.db.map.close();

                var id = parseInt(value, 10);

                var zone = $.grep(zones, function (zone) {
                    return zone.ZoneId == id;
                })[0];

                if (!zone)
                    throw 'zone not found!';

                window.elzoneid = zone.ZoneId;

                var requests = [
                    app.services.fetch(`api/zones/${id}/lots`),
                    app.services.fetch(`api/zones/${id}/models`),
                    app.services.fetch(`api/zones/${id}/blocks`),
                    app.services.fetch(`api/zones/${id}/streets`),
                    app.services.fetch(`api/zones/${id}/map`)
                ];

                var responses = await Promise.all(requests);

                var lots = responses[0];
                var models = responses[1];
                var blocks = responses[2];
                var streets = responses[3];
                var map = responses[4];

                if (!lots || !models || !blocks || !streets) {
                    alertify.alert('Error al cargar datos');
                    app.redirect();
                    return;
                }

                if (!map) {
                    alertify.alert('Error al cargar el mapa');
                    app.redirect();
                    return;
                }

                var buildingStatusHash = {
                    1: {
                        StatusType: 1,
                        StatusName: 'Terreno',
                        Color: '#a1cde3'
                    },
                    2: {
                        StatusType: 2,
                        StatusName: 'En construcción',
                        Color: '#e2d77f'
                    },
                    3: {
                        StatusType: 3,
                        StatusName: 'Terminado',
                        Color: '#5cde83'
                    }
                };
                var saleStatusHash = {
                    1: {
                        StatusType: 1,
                        StatusName: 'Disponible',
                        Color: '#F00'
                    },
                    2: {
                        StatusType: 2,
                        StatusName: 'Vendido',
                        Color: '#0F0'
                    },
                    3: {
                        StatusType: 3,
                        StatusName: 'Firmado',
                        Color: '#00F'
                    },
                    4: {
                        StatusType: 4,
                        StatusName: 'Pagado',
                        Color: '#FF0'
                    },
                    5: {
                        StatusType: 5,
                        StatusName: 'Entregado',
                        Color: '#F0F'
                    },
                    6: {
                        StatusType: 6,
                        StatusName: 'Integrado',
                        Color: '#0FF'
                    },
                };

                var getTableRowHTML = function (id, color, td1, td2) {
                    return `
                    <tr class='item' data-id="${id}">
                        <td>
                            <div style="height:10px; width:10px; display:inline-block;
                                background-color: ${color}; border:1px solid #ddd;">
                            </div>
                            <span style='display:inline-block; padding-left:7.5px'>
                                ${td1}
                            </span>
                        </td>
                        <td>
                            ${td2}
                        </td>
                    </tr>
                    `;
                };

                var buildingStatusBuffer = '';
                $.each(buildingStatusHash, function (i, item) {

                    var lotsWithThisBuildingStatus = $.grep(lots, function (lot) {
                        return lot.Status == item.StatusType;
                    });

                    buildingStatusBuffer += getTableRowHTML(
                        item.StatusType,
                        item.Color,
                        item.StatusName,
                        lotsWithThisBuildingStatus.length);
                });

                var saleStatusBuffer = '';
                $.each(saleStatusHash, function (i, item) {

                    var lotsWithThisSaleStatus = $.grep(lots, function (lot) {
                        return lot.SaleStatus == item.StatusType;
                    });

                    saleStatusBuffer += getTableRowHTML(
                        item.StatusType,
                        item.Color,
                        item.StatusName,
                        lotsWithThisSaleStatus.length);
                });

                var modelsBuffer = '';
                $.each(models, function (i, model) {

                    var modelLots = $.grep(lots, function (lot) {
                        return lot.ModelId == model.ModelId;
                    });

                    modelsBuffer += getTableRowHTML(
                        model.ModelId,
                        model.Color,
                        model.Name,
                        modelLots.length);
                });

                var blocksBuffer = '';
                $.each(blocks, function (i, block) {

                    var blockLots = $.grep(lots, function (lot) {
                        return lot.BlockId == block.BlockId;
                    });

                    blocksBuffer += getTableRowHTML(
                        block.BlockId,
                        block.Color,
                        block.Name,
                        blockLots.length);
                });

                var $mapContainer = bubble.find('#map-container');

                $mapContainer.html('');

                app.db.map = new Map({
                    $el: $mapContainer,
                    zone: zone,
                    lots: lots,
                    models: models,
                    blocks: blocks,
                    streets: streets,
                    paths: map.Paths,
                    width: map.Width,
                    height: map.Height,
                    filter: 0,
                    defaultColor: '#f3fbe7',
                });

                app.db.map.render({ filter: 0 });

                bubble.find('#building-status-total')
                    .html(Object.keys(buildingStatusHash).length)
                    .removeClass('hidden');

                bubble.find('#sale-status-total')
                    .html(Object.keys(saleStatusHash).length)
                    .removeClass('hidden');

                bubble.find('#models-total')
                    .html(models.length)
                    .removeClass('hidden');

                bubble.find('#blocks-total')
                    .html(blocks.length)
                    .removeClass('hidden');

                var buildingStatusPanel = bubble.find('#building-status-panel');
                var saleStatusPanel = bubble.find('#sale-status-panel');
                var modelPanel = bubble.find('#models-panel');
                var blocksPanel = bubble.find('#blocks-panel');

                buildingStatusPanel.find('tbody').html(buildingStatusBuffer);
                saleStatusPanel.find('tbody').html(saleStatusBuffer);
                modelPanel.find('tbody').html(modelsBuffer);
                blocksPanel.find('tbody').html(blocksBuffer);

                buildingStatusPanel.find('.item').on('click', function (e) {

                    var row = $(e.currentTarget);

                    var id = row.data('id');

                    app.db.map.render({ filter: 5, type: id })

                });
                saleStatusPanel.find('.item').on('click', function (e) {

                    var row = $(e.currentTarget);

                    var id = row.data('id');

                    app.db.map.render({ filter: 6, type: id })

                });
                modelPanel.find('.item').on('click', function (e) {

                    var row = $(e.currentTarget);

                    var id = row.data('id');

                    app.db.map.render({ filter: 2, modelId: id });
                });
                blocksPanel.find('.item').on('click', function (e) {

                    var row = $(e.currentTarget);

                    var id = row.data('id');

                    app.db.map.render({ filter: 4, blockId: id })
                });

                zoneIdInput.prop('disabled', false);
            });

            actionInput.on('change', async function (event) {

                var actionIdInput = $('#action-select');

                var action = parseInt(actionIdInput.val());

                if (!isNaN(action))
                    window.elaction = action;
            });

            if (window.elzoneid)
                zoneIdInput.val(window.elzoneid).change();

            if (window.elaction)
                actionInput.val(window.elaction);

            zoneIdInput.focus();
        });
    };
    var sendReport = async function (context) {

        document.title = `${app.title} - Enviar reporte`;

        var html = `
        <div class="row">
            <div class="col-xs-12 text-center">
                <br>
                <br>
                <i style="color: #071C2D" class="fa fa-exclamation big-icon"></i>
                <br>
                <h3 class="big-text" style="color: #071C2D">
                    Enviar reporte de avance de obras del periodo actual
                </h3>
                <br>
                <button id="send" data-loading-text="Cargando..." class="btn btn-success">
                    Enviar
                </button>
            </div>
        </div>
        `;

        app.Sammy.swap(html, function (bubble) {

            var button = bubble.find('#send');

            button.on('click', function (event) {

                alertify.confirm('Enviar reporte', async function () {

                    if (app.loading)
                        return;

                    app.loading = true;
                    button.button('loading');

                    var response = await app.services.ajaxAsync({
                        url: `api/send-report`,
                        data: { Send: true }
                    });

                    app.loading = false;

                    if (!response) {
                        button.button('reset');
                        return false;
                    }

                    app.pageMessage({
                        icon: 'fa fa-check',
                        message: 'El reporte se está generando y se enviara en unos momentos.',
                        href: `#/manager`,
                        text: 'Regresar'
                    });

                });

            });

        });

    };
    var creator = function (context) {

        document.title = `${app.title} - Creador`;

        var textAreaFunc = function (options) {
            return `
            <div class="col-md-6 col-lg-4">
                <div class="form-group">
                    <label>${options.label}</label>
                    <textarea name="${options.name}" rows="3" class="form-control no-resize"></textarea>
                </div>
            </div>
            `;
        };

        var html = `
        <form class="well">
            <div class="row">
                ${textAreaFunc({ label: 'Residenciales', name: 'settlements' })}
                ${textAreaFunc({ label: 'Cerradas', name: 'zones' })}
            </div>
            <div class="row"><br></div>
            <div class="row">
                ${textAreaFunc({ label: 'Manzanas', name: 'blocks' })}
                ${textAreaFunc({ label: 'Modelos', name: 'models' })}
                ${textAreaFunc({ label: 'Calles', name: 'streets' })}
            </div>
            <div class="row"><br></div>
            <div class="row">
                ${textAreaFunc({ label: 'Números de lote', name: 'lotNumbers' })}
                ${textAreaFunc({ label: 'Números oficiales', name: 'officialNumbers' })}
            </div>
            <div class="row"><br></div>
            <div class="row">
                ${textAreaFunc({ label: 'Paquetes RUV', name: 'ruvPackages' })}
                ${textAreaFunc({ label: 'Paquetes Bancarios', name: 'bankPackages' })}
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        ${app.forms.formButton('Procesar')}
                        <a href="#/manager" class="btn btn-default">Regresar</a>
                    </div>
                </div>
            </div>
        </form>
        <div class="row"><hr></div>
        <div class="row">
            <div id="table-container" class="col-xs-12">
            </div>
        </div>
        `;

        app.Sammy.swap(html, async function (bubble) {

            var tableContainer = bubble.find('#table-container');
            var form = bubble.find('form');
            var button = form.find('button[type="submit"]');

            form.on('submit', function (event) {

                if (app.loading)
                    return false;

                app.loading = true;
                button.button('loading');

                var data = null;
                var pc = new app.components.Creator(form);

                try {

                    data = pc.create();

                } catch (e) {

                    alertify.error(e.message);
                    app.loading = false;
                    button.button('reset');
                    return false;
                }

                if (!data.Settlements.length) {
                    alertify.error('Listado inválido');
                    app.loading = false;
                    button.button('reset');
                    return false;
                }

                var func = function () {

                    var rows = '';
                    for (var i = 0; i < data.Settlements.length; i++) {

                        rows += `
                        <tr>
                            <td>${i + 1}</td>
                            <td>${data.Settlements[i]}</td>
                            <td>${data.Zones[i]}</td>
                            <td>${data.Blocks[i]}</td>
                            <td>${data.Streets[i]}</td>
                            <td>${data.Models[i]}</td>
                            <td>${data.OfficialNumbers[i]}</td>
                            <td>${data.LotNumbers[i]}</td>
                            <td>${data.RUVPackages[i]}</td>
                            <td>${data.BankPackages[i]}</td>
                        </tr>
                        `;
                    }

                    var tableHTML = `
                    <div class="well">
                        <button id="save" class="btn btn-primary" data-loading-text="Cargando...">
                            Guardar
                        </button>
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <th>Indice</th>
                                        <th>Residenciales</th>
                                        <th>Cerrada</th>
                                        <th>Manzanas</th>
                                        <th>Calles</th>
                                        <th>Modelos</th>
                                        <th>Números oficiales</th>
                                        <th>Números de lote</th>
                                        <th>Paquetes RUV</th>
                                        <th>Paquetes Bancarios</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    ${rows}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    `;

                    tableContainer.html(tableHTML);

                    var table = tableContainer.find('table');
                    var save = tableContainer.find('#save');

                    save.on('click', async function (event) {

                        if (app.loading)
                            return false;

                        app.loading = true;
                        save.button('loading');

                        var response = await app.services.ajaxAsync({
                            url: `api/creator`,
                            method: 'POST',
                            data: data
                        });

                        app.loading = false;

                        if (!response) {
                            save.button('reset');
                            return false;
                        }

                        var responseMessageHTML = `
                        <div class="row">
                            <div class="col-xs-12">
                                <h2>Residenciales creadas: ${response.Settlements}</h2>
                                <h2>Cerradas creadas: ${response.Zones}</h2>
                                <h2>Calles creadas: ${response.Streets}</h2>
                                <h2>Manzanas creadas: ${response.Blocks}</h2>
                                <h2>Modelos creados: ${response.Models}</h2>
                                <h2>Lotes creados: ${response.Lots}</h2>
                                <h2>Paquetes RUV creados: ${response.RUVPackages}</h2>
                                <h2>Paquetes Bancarios creados: ${response.BankPackages}</h2>
                            </div>
                            <div class="col-xs-12">
                                <button id="restart-button" class="btn btn-success">
                                    Volver a crear
                                </button>
                            </div>
                        </div>
                        `;

                        app.Sammy.swap(responseMessageHTML, function (bubble) {

                            bubble.on('click', '#restart-button', function () {
                                app.Sammy.refresh();
                            });
                        });
                    });

                    button.button('reset');
                    app.loading = false;
                };

                setTimeout(func, 500);

            });
        });
    };
    var panel = function (context) {

        document.title = `${app.title} - Panel`;

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Panel</h3>
                <div class="well">
                    <ul class="list-unstyled">
                        <li><a href="#/mapa">Mapas</a></li>
                    </ul>
                </div>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

        });
    };
    var admin = function (context) {

        document.title = `${app.title} - Admin`;

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Panel</h3>
                <div class="well">
                    <ul class="list-unstyled">
                        <li><a href="#/cerradas">Cerradas</a></li>
                        <li><a href="#/modelos">Modelos</a></li>
                        <li><a href="#/calles">Calles</a></li>
                        <li><a href="#/manzanas">Manzanas</a></li>
                        <li><a href="#/mapa">Mapas</a></li>
                        <li><a href="#/lotes">Lotes</a></li>
                    </ul>
                </div>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

        });
    };
    var manager = async function (context) {
        document.title = `${app.title} - Manager`;

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Panel</h3>
                <div class="well">
                    <ul class="list-unstyled">
                        <li><a href="#/residenciales">Residenciales</a></li>
                        <li><a href="#/enviar-reporte">Enviar reporte</a></li>
                        <li><a href="#/creador">Creador</a></li>
                        <li><a href="#/usuarios/buscar">Buscar usuario</a></li>
                    </ul>
                </div>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

        });
    };

    app.routes.push({ route: '#/login/?', callback: login });
    app.routes.push({ route: '#/logout/?', callback: logout });
    app.routes.push({ route: '#/sesiones/?', callback: sessions });
    app.routes.push({ route: '#/mapa/?', callback: maps });
    app.routes.push({ route: '#/enviar-reporte/?', callback: sendReport });
    app.routes.push({ route: '#/creador/?', callback: creator });
    app.routes.push({ route: '#/panel/?', callback: panel });
    app.routes.push({ route: '#/admin/?', callback: admin });
    app.routes.push({ route: '#/manager/?', callback: manager });

})();