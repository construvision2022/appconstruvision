﻿(function () {

    if (!app)
        throw 'the app module is required';

    var list = async function (context) {

        document.title = `${app.title} - Modelos`;

        var models = await app.services.fetch('api/models');

        if (!models) {
            alertify.error(app.error);
            return false;
        }

        var getColorIcon = function (color) {

            if (!color)
                return '--';

            return `<div style="
                        float: left;
                        width: 22px;
                        height: 22px;
                        margin-right: 3px;
                        border: 1px solid rgba(0, 0, 0, .5);
                        background-color: ${color}"></div> ${color}`;
        };
        var getFiles = function (model) {

            var pdf = model.File ? `<a href="/content/files/${model.File}" target="_blank">PDF</a>` : '';
            var image = model.ImageCode ? `<a href="/content/images/uploads/${model.ImageCode}" target="_blank">Imagen</a>` : '';

            if (pdf && image)
                return `${pdf} | ${image}`;
            else if (pdf)
                return pdf;
            else if (image)
                return image;
            else
                return '--';
        };

        var buffer = '';
        $.each(models, function (i, model) {

            var options = [
                `<a href="#/modelos/${model.ModelId}/actualizar">Actualizar</a>`,
                `<a href="#/modelos/${model.ModelId}/pdf">PDF</a>`,
                `<a href="#/modelos/${model.ModelId}/imagen">Imagen</a>`,
                `<a href="#/modelos/${model.ModelId}/reset">Reiniciar</a>`
            ];

            if (model.DefaultData) {                
                options.push(`<a href="#/modelos/${model.ModelId}/setup">
                    <i class="fa fa-exclamation-circle text-danger"></i> Setup</a>`);
            }

            buffer += `
            <tr ${model.DefaultData ? 'class="danger" title="Model sin conceptos de construcción"' : ''}>
                <td>${model.ZoneName}</td>
                <td>${model.Name}</td>
                <td>${model.Floors || '--'}</td>
                <td>${model.Area || '--'}</td>
                <td>${model.GroundArea || '--'}</td>
                <td>${getColorIcon(model.Color)}</td>            
                <td>${getFiles(model)}</td>            
                ${app.forms.optionsTd(options)}
            </tr>
            `;
        });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Modelos</h3>
            </div>
            <div class="col-xs-12">
                <a href="#/modelos/crear" class="btn btn-success">Crear</a>
                <a href="#" class="redirect btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>                                
                            <th>Cerrada</th>
                            <th>Nombre</th>
                            <th>Pisos</th>
                            <th>Área de const.</th>
                            <th>Área de terreno</th>
                            <th>Color</th>
                            <th>Archivos</th>
                            <th></th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>                    
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var post = async function (context) {

        document.title = `${app.title} - Crear modelo`;

        var zones = await app.services.fetch('api/zones');

        if (!zones) {
            alertify.error(app.error)
            return;
        }

        var zonesHTML = '';
        $.each(zones, function (i, zone) {
            zonesHTML += `<option value="${zone.ZoneId}">${zone.Name}</option>`;
        });

        var inputsBuffer = `
        <div class="form-group">
            <label for="ZoneId">Cerrada</label>
            <select class="form-control" name="ZoneId" data-val="true" data-val-required="Cerrada es requerida">
                <option value="">-- Selecciona cerrada --</option>
                ${zonesHTML}
            </select>
        </div>
        `;

        inputsBuffer += app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true },
            { name: 'Floors', displayName: 'Pisos', number: true },
            { name: 'Area', displayName: 'Área de const.', number: true, },
            { name: 'GroundArea', displayName: 'Área de terreno', number: true },
            {
                name: 'Color', displayName: 'Color',
                placeholder: 'Ej. #A5B5C5 para revisar colores entrar a http://www.color-hex.com'
            }
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Crear modelo</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    <div id="building-concepts-container">
                    </div>
                    ${app.forms.formButton('Crear')}
                    <a href="#/modelos" class="btn btn-default">Regresar</a>
                </form>
            </div>
            <div class="col-xs-12"><br></div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            var component = new BuildingConceptCreator({
                containerId: '#building-concepts-container'
            });

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                if (!component.data.length) {
                    alertify.alert('Debes crear conceptos de construcción');
                    return false;
                }

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                data.BuildingConcepts = component.data;

                var created = await app.services.ajaxAsync({
                    url: `api/models`,
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!created) {
                    button.button('reset');
                    return;
                }

                window.location = '#/modelos';
            });
        });
    };
    var put = async function (context) {

        document.title = `${app.title} - Actualizar modelo`;

        var model = await app.services.fetch('api/models/' + context.params.id);

        if (!model) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { name: 'ZoneName', displayName: 'Cerrada', disabled: true, value: model.ZoneName },
            { name: 'Name', displayName: 'Nombre', required: true, value: model.Name },
            { name: 'Floors', displayName: 'Pisos', number: true, value: model.Floors },
            { name: 'Area', displayName: 'Área de const.', number: true, value: model.Area },
            { name: 'GroundArea', displayName: 'Área de terreno', number: true, value: model.GroundArea },
            {
                name: 'Color', displayName: 'Color',
                placeholder: 'Ej. #A5B5C5 para revisar colores entrar a http://www.color-hex.com', value: model.Color
            }
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar modelo</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Actualizar')}
                    <a href="#/modelos" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/models/${model.ModelId}`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                window.location = '#/modelos';
            });

        });
    };
    var putPDF = async function (context) {

        document.title = `${app.title} - PDF de modelo`;

        var model = await app.services.fetch('api/models/' + context.params.id);

        if (!model)
            return false;

        var modelPDFHTML = '';

        if (model.File) {
            modelPDFHTML = `
            <div class="col-xs-12">
                <a href="/content/files/${model.File}"
                    class="btn btn-primary" target="_blank">
                    Ver PDF
                </a>
                <hr>
            </div>
            `;
        }


        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>PDF de modelo</h3>
            </div>
            ${modelPDFHTML}                
            <div class="col-xs-12">
                <form>
                    <div class="form-group">
                        <input type="file" name="file" class="form-control">
                    </div>
                    <div class="form-group">
                        <div id="messageDiv"></div>
                    </div>
                    ${app.forms.formButton('Subir PDF')}
                    <a href="#/modelos" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]');
            var messageDiv = bubble.find('#messageDiv');

            var fileInput = $(form[0].elements.file);

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                var inputFiles = fileInput[0].files;

                if (!inputFiles.length) {
                    alertify.alert('No hay archivo seleccionado');
                    return;
                }

                var file = inputFiles[0];

                if (file.type != 'application/pdf') {
                    alertify.alert('Solo PDF');
                    return;
                }

                var maxFileSize = 3;
                var sizeKB = file.size / 1024;
                var sizeMB = (sizeKB / 1024).toFixed(3);


                if (sizeMB >= maxFileSize) {
                    alertify.alert('Peso maximo permitido: ' + maxFileSize + ' Megabytes');
                    return;
                }

                app.loading = true;
                button.button('loading');
                messageDiv.html('Cargando...');

                var formData = new FormData();
                formData.append('file', file);

                var updateModelPDF = async function (response) {

                    var json = JSON.parse(response);

                    var updated = await app.services.ajaxAsync({
                        url: `api/models/${model.ModelId}/pdf`,
                        method: 'PUT',
                        data: json,
                        validator: validator
                    });

                    app.loading = false;

                    if (!updated) {
                        button.button('reset');
                        messageDiv.html('');
                        alertify.error('Error');
                        return;
                    }

                    window.location = '#/modelos';
                };

                var request = $.ajax({
                    url: baseUrl + `api/files/upload`,
                    type: 'POST',
                    mimeType: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    data: formData,
                    xhr: function () {

                        var xhr = $.ajaxSettings.xhr();

                        xhr.upload.onprogress = function (event) {

                            var percentage = Math.floor(event.loaded / event.total * 100);

                            if (isNaN(percentage))
                                return;

                            messageDiv.html(percentage + '%');

                        };

                        return xhr;
                    }
                });

                request.done(function (response, textStatus) {
                    updateModelPDF(response);
                });

                request.fail(function (response) {
                    app.parseErrors(response);
                    button.button('reset');
                    app.loading = false;
                });
            });

        });
    };
    var putImage = async function (context) {

        document.title = `${app.title} - Imagen de modelo`;

        var model = await app.services.fetch('api/models/' + context.params.id);

        if (!model)
            return false;

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Imagen de modelo</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    <div class="form-group">
                        <input type="file" name="file" class="form-control">
                    </div>
                    <div class="form-group">
                        <div id="messageDiv"></div>
                    </div>
                    ${app.forms.formButton('Subir Imagen')}
                    <a href="#/modelos" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]');
            var messageDiv = bubble.find('#messageDiv');

            var fileInput = $(form[0].elements.file);

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                var inputFiles = fileInput[0].files;

                if (!inputFiles.length) {
                    alertify.alert('No hay archivo seleccionado');
                    return;
                }

                var file = inputFiles[0];

                var allowedMimeTypes = [
                    'image/bmp',
                    'image/gif',
                    'image/jpeg',
                    'image/png',
                ];

                if (allowedMimeTypes.indexOf(file.type) == -1) {
                    alertify.alert('Tipo de archivo no soportado');
                    return;
                }

                var maxFileSize = 3;
                var sizeKB = file.size / 1024;
                var sizeMB = (sizeKB / 1024).toFixed(3);


                if (sizeMB >= maxFileSize) {
                    alertify.alert('Peso maximo permitido: ' + maxFileSize + ' Megabytes');
                    return;
                }

                button.button('loading');
                messageDiv.html('Cargando...');

                var formData = new FormData();
                formData.append('file', file);

                var updateModelImage = async function (response) {

                    var json = JSON.parse(response);

                    var updated = await app.services.ajaxAsync({
                        url: `api/models/${model.ModelId}/image`,
                        method: 'PUT',
                        data: json,
                        validator: validator
                    });

                    app.loading = false;

                    if (!updated) {
                        button.button('reset');
                        messageDiv.html('');
                        alertify.error('Error');
                        return;
                    }

                    window.location = '#/modelos';
                };

                var request = $.ajax({
                    url: baseUrl + `api/images/mobile-upload`,
                    type: 'POST',
                    mimeType: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    data: formData,
                    xhr: function () {

                        var xhr = $.ajaxSettings.xhr();

                        xhr.upload.onprogress = function (event) {

                            var percentage = Math.floor(event.loaded / event.total * 100);

                            if (isNaN(percentage))
                                return;

                            messageDiv.html(percentage + '%');

                        };

                        return xhr;
                    }
                });

                request.done(function (response, textStatus) {
                    updateModelImage(response);
                });

                request.fail(function (response) {
                    app.parseErrors(response);
                    button.button('reset');
                    app.loading = false;
                });

            });

        });
    };
    var setup = async function (context) {

        document.title = `${app.title} - Setup modelo`;

        var model = await app.services.fetch('api/models/' + context.params.id);

        if (!model) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true, value: model.Name },
            { name: 'Floors', displayName: 'Pisos', number: true, value: model.Floors },
            { name: 'Area', displayName: 'Área de const.', number: true, value: model.Area },
            { name: 'GroundArea', displayName: 'Área de terreno', number: true, value: model.GroundArea },
            {
                name: 'Color', displayName: 'Color', value: model.Color,
                placeholder: 'Ej. #A5B5C5 para revisar colores entrar a http://www.color-hex.com'
            }
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Setup</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    <div id="building-concepts-container">
                    </div>
                    ${app.forms.formButton('Setup')}
                    <a href="#/modelos" class="btn btn-default">Regresar</a>
                </form>
            </div>
            <div class="col-xs-12"><br></div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            var component = new BuildingConceptCreator({
                containerId: '#building-concepts-container'
            });

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                if (!component.data.length) {
                    alertify.alert('Debes crear conceptos de construcción');
                    return false;
                }

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                data.BuildingConcepts = component.data;

                var updated = await app.services.ajaxAsync({
                    url: `api/models/${model.ModelId}`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                window.location = '#/modelos';
            });
        });
    };
    var reset = async function (context) {

        document.title = `${app.title} - Reiniciar modelo`;

        var model = await app.services.fetch('api/models/' + context.params.id);

        if (!model) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true, value: model.Name }
        ], { disabled: true });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Reset</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}                    
                    ${app.forms.formButton('Reset')}
                    <a href="#/modelos" class="btn btn-default">Regresar</a>
                </form>
            </div>
            <div class="col-xs-12"><br></div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var reset = await app.services.ajaxAsync({
                    url: `api/models/${model.ModelId}/reset`,
                    method: 'PUT',
                    data: { Reset: true },
                    validator: validator
                });

                app.loading = false;

                if (!reset) {
                    button.button('reset');
                    return;
                }

                window.location = '#/modelos';
            });
        });
    };

    var BuildingConceptCreator = function (options) {

        if (!options.containerId)
            throw 'BuildingConceptCreator: bad containerId';

        var c = {
            containerId: options.containerId,
            data: [],
            onSubmit: options.onSubmit
        };

        var getDataTable = function () {

            if (!c.data.length) {

                return `
                <h4>Debes crear conceptos de construcción</h4>
                <button id="show-modal" type="button" class="btn btn-primary">
                    Crear concepto
                </button>
                `;
            }

            var total = new Big(0);
            var buffer = '';
            $.each(c.data, function (i, buildingConcept) {
                total = total.plus(buildingConcept.Percentage);
                buffer += `
                <tr>
                    <td>${buildingConcept.Name}</td>
                    <td>${buildingConcept.Percentage}</td>
                    <td>${buildingConcept.Order}</td>
                    <td>${buildingConcept.CriticalRoute || '--'}</td>
                    <td class="text-center" style="width: 1px;">
                        <button type="button" data-index="${i}" class="btn btn-xs btn-danger delete">
                            &times;
                        </button>
                    </td>
                </tr>
                `;
            });

            return `
            <button id="show-modal" type="button" class="btn btn-primary">
                Crear concepto
            </button>            
            <br>
            <h4>Sumatoria de porcentajes: <strong>${total}</strong></h4>
            <table class="table table-first table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Porcentaje</th>
                        <th>Orden</th>
                        <th>Ruta crítica</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>${buffer}</tbody>
            </table>
            `;
        };
        var getFormHTML = function () {

            var inputsBuffer = app.forms.inputText([
                { name: 'Name', displayName: 'Nombre', required: true },
                { name: 'Percentage', displayName: 'Porcentaje', number: true, required: true },
                {
                    name: 'Order', displayName: 'Orden', digits: true, required: true,
                    range: { min: 1, max: 99, message: 'Orden debe ser entre 1 y 99' }
                },
                {
                    name: 'CriticalRoute', displayName: 'Ruta crítica', digits: true,
                    placeholder: 'Si un concepto no está dentro de la ruta crítica, dejar este campo vacío',
                    range: { min: 1, max: 99, message: 'Ruta crítica debe ser entre 1 y 99' }
                }
            ]);

            return `
            <div class="row">
                <div class="col-xs-12">
                    <form>
                        ${inputsBuffer}
                        ${app.forms.formButton('Crear')}
                        <button id="back" type="button" class="btn btn-default">
                            Cancelar
                        </button>
                    </form>
                </div>
            </div>
            `;

            return output;
        };
        var drawComponent = function () {

            if (!c.container) {
                c.container = $(c.containerId);
            }

            c.container.html(`
            <div class="col-xs-12 well well-sm">
                ${getDataTable()}                
            </div>            
            `);

            var showModalButton = c.container.find('#show-modal');
            showModalButton.on('click', function () {

                app.tools.modal.title.html('Crear concepto de construcción.');
                app.tools.modal.body.html(getFormHTML());
                app.tools.modal.show();

                var form = app.tools.modal.body.find('form');
                var button = form.find('button[type="submit"]')
                var backButton = form.find('#back');

                var validator = app.services.validateForm(form);
                var inputs = form[0].elements;

                var nameInput = $(inputs.Name);
                var percentageInput = $(inputs.Percentage);
                var orderInput = $(inputs.Order);
                var criticalRouteInput = $(inputs.CriticalRoute);

                backButton.on('click', function (event) {
                    app.tools.modal.hide();
                });

                form.on('submit', function (event) {

                    event.preventDefault();

                    if (!form.valid())
                        return false;

                    var buildingConcept = {
                        Name: nameInput.val(),
                        Percentage: parseFloat(percentageInput.val()),
                        Order: orderInput.val(),
                        CriticalRoute: criticalRouteInput.val()
                    };

                    //if (buildingConcept.Percentage < .0001) {
                    //    validator.showErrors({ Percentage: 'Porcentaje mínimo .0001' });
                    //    return false;
                    //}

                    if (buildingConcept.Percentage > 1) {
                        validator.showErrors({ Percentage: 'Porcentaje máximo 1' });
                        return false;
                    }

                    var error = false;
                    $.each(c.data, function (i, item) {

                        if (item.Name == buildingConcept.Name) {
                            error = true;
                            validator.showErrors({ Name: 'Nombre repetido' });
                        }

                        if (item.Order == buildingConcept.Order) {
                            error = true;
                            validator.showErrors({ Order: 'Orden repetido' });
                        }

                    });

                    if (error)
                        return false;

                    var total = new Big(0);

                    $.each(c.data, function (i, item) {
                        total = total.plus(item.Percentage);
                    });

                    total = total.toString();
                    
                    if (total > 1) {
                        validator.showErrors({ Percentage: 'Los porcentajes deben sumar 1' });
                        return false;
                    }

                    c.data.push(buildingConcept);

                    alertify.success('Concepto de construcción creado');

                    app.tools.modal.hide();

                    drawComponent();
                });

                nameInput.select();
            });
            var table = c.container.find('table');
            table.on('click', '.delete', function (event) {
                var index = $(this).data('index');
                c.data.splice(index, 1);
                alertify.success('Concepto de construcción eliminado');
                drawComponent();
            });
        };
        drawComponent();

        return c;
    };

    app.routes.push({ route: '#/modelos/?', callback: list });
    app.routes.push({ route: '#/modelos/crear/?', callback: post });
    app.routes.push({ route: '#/modelos/:id/actualizar/?', callback: put });
    app.routes.push({ route: '#/modelos/:id/pdf/?', callback: putPDF });
    app.routes.push({ route: '#/modelos/:id/imagen/?', callback: putImage });
    app.routes.push({ route: '#/modelos/:id/setup/?', callback: setup });
    app.routes.push({ route: '#/modelos/:id/reset/?', callback: reset });

})();