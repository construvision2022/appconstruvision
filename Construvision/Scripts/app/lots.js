﻿(function () {

    if (!app)
        throw 'the app module is required';

    var list = async function (context) {

        document.title = `${app.title} - Lotes`;

        var lots = await app.services.fetch('api/admin/lots');

        if (!lots)
            return app.pageMessage();

        var buffer = '';
        $.each(lots, function (i, lot) {

            buffer += `
            <tr>                
                <td>${lot.ZoneName}</td>
                <td>${lot.BlockName} L-${lot.LotNumber}</td>
                <td>${lot.StreetName} #${lot.OfficialNumber || 'N/A'}</td>
                <td>${lot.ModelName}</td>
                <td>${lot.GroundArea || '--'}</td>
                <td>${lot.ConstructionArea || '--'}</td>
                <td>${lot.RUV || '--'}</td>
                <td>${lot.Bancario || '--'}</td>
                <td>${app.enums.LotStatus.GetString(lot.Status)}</td>
                <td>${app.enums.SaleStatus.GetString(lot.SaleStatus)}</td>
                <td>${lot.Percentage}%</td>                
                <td class="text-center">
                    <i class="fa ${lot.Dtu ? 'fa-check text-success' : 'fa-times text-danger'}"></i>
                </td>                
                ${app.forms.optionsTd([
                    `<a href="#/lotes/${lot.LotId}/reporte">Crear reporte</a>`,
                    `<a href="#/lotes/${lot.LotId}/actualizar">Actualizar</a>`,
                    `<a href="#/lotes/${lot.LotId}/paquetes">Paquetes</a>`,
                    `<a href="#/lotes/${lot.LotId}/modelo">Cambiar modelo</a>`,                    
                    `<a href="#/lotes/${lot.LotId}/eliminar">Eliminar</a>`
                ])}
            </tr>
            `;
        });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Lotes</h3>
            </div>
            <div class="col-xs-12">
                <a href="#/lotes/crear" class="btn btn-success">Crear</a>
                <a href="#" class="redirect btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Cerrada</th>
                            <th># de lote</th>
                            <th>Dirección</th>
                            <th>Modelo</th>
                            <th>Terreno</th>
                            <th>Const.</th>                            
                            <th>RUV</th>
                            <th>Bancario</th>
                            <th>Estado lote</th>
                            <th>Estado venta</th>
                            <th>%</th>                            
                            <th>DTU</th>                            
                            <th></th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>                    
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var post = async function (context) {

        document.title = `${app.title} - Crear lote`;

        var zones = await app.services.fetch('api/zones');

        if (!zones) {
            window.location = '#/lotes';
            return;
        }

        var zonesHTML = '';
        $.each(zones, function (i, zone) {
            zonesHTML += `<option value="${zone.ZoneId}">${zone.Name}</option>`;
        });

        var inputsBuffer = `        
        <div class="form-group">
            <label for="ZoneId">Cerrada</label>
            <select class="form-control" name="ZoneId" data-val="true" data-val-required="Cerrada es requerida">
                <option value="">-- Selecciona cerrada --</option>
                ${zonesHTML}
            </select>
        </div>
        <div class="form-group">
            <label>Calle</label>
            <select name="StreetId" class="form-control">
                <option value="">-- Selecciona cerrada --</option>
            </select>
        </div>
        <div class="form-group">
            <label>Manzana</label>
            <select name="BlockId" class="form-control">
                <option value="">-- Selecciona cerrada --</option>
            </select>
        </div>
        <div class="form-group">
            <label>Modelo</label>
            <select name="ModelId" class="form-control">
                <option value="">-- Selecciona cerrada --</option>
            </select>
        </div>
        <div class="form-group">
            <label>RUV</label>
            <select name="RUV" class="form-control">
                <option value="">-- Selecciona cerrada --</option>
            </select>
        </div>
        <div class="form-group">
            <label>Bancario</label>
            <select name="Bank" class="form-control">
                <option value="">-- Selecciona cerrada --</option>
            </select>
        </div>
        <div class="form-group">
            <label>Estado de venta</label>
            <select name="SaleStatus" class="form-control" data-val="true"
                data-val-required="Estado de venta es requerido">
                <option value="">-- Seleccione estado de venta --</option>
                <option value="1">Disponible</option>                        
                <option value="2">Vendido</option>                        
                <option value="3">Firmado</option>                        
                <option value="4">Pagado</option>                        
                <option value="5">Entregado</option>                        
                <option value="6">Integrado</option>                        
            </select>
            ${app.forms.validationErrorDiv('SaleStatus')}
        </div>
        `;

        inputsBuffer += app.forms.inputText([
            { name: 'LotNumber', displayName: 'Número de lote', required: true },
            { name: 'OfficialNumber', displayName: 'Número oficial' },
            { name: 'GroundArea', displayName: 'Área de terreno', number: true, },
            { name: 'ConstructionArea', displayName: 'Área de const.', number: true }
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Crear lote</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}                    
                    ${app.forms.formButton('Crear')}
                    <a href="#/lotes" class="btn btn-default">Regresar</a>
                </form>
                <br>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')
            var validator = app.services.validateForm(form);
            var inputs = form[0].elements;

            var blockIdInput = $(inputs.BlockId);
            var streetIdInput = $(inputs.StreetId);
            var modelIdInput = $(inputs.ModelId);

            var zonesInput = $(inputs.ZoneId);
            var ruvInput = $(inputs.RUV);
            var bankInput = $(inputs.Bank);

            zonesInput.on('change', async function (event) {

                var zoneId = parseInt(zonesInput.val());

                if (isNaN(zoneId))
                    return;

                var responses = await Promise.all([
                    app.services.fetch(`api/zone/${zoneId}/packages/select`),
                    app.services.fetch(`api/zones/${zoneId}/streets`),
                    app.services.fetch(`api/zones/${zoneId}/blocks`),
                    app.services.fetch(`api/zones/${zoneId}/models`),
                ]);

                var packages = responses[0];
                var streets = responses[1];
                var blocks = responses[2];
                var models = responses[3];

                if (!packages || !streets || !blocks || !models) {
                    window.location = '#/lotes';
                    return;
                }

                var streetsHTML = '<option value="">-- Selecciona calle --</option>';
                $.each(streets, function (i, street) {
                    streetsHTML += `<option value="${street.StreetId}">${street.Name} </option>`;
                });

                var blocksHTML = '<option value="">-- Selecciona manzana --</option>';
                $.each(blocks, function (i, block) {
                    blocksHTML += `<option value="${block.BlockId}">${block.Name}</option>`;
                });

                var modelsHTML = '<option value="">-- Selecciona modelo --</option>';
                $.each(models, function (i, model) {
                    modelsHTML += `<option value="${model.ModelId}">${model.Name}</option>`;
                });

                var ruvHTML = '<option value="">-- Selecciona paquete --</option>';
                $.each(packages.RUV, function (i, package) {
                    ruvHTML += `<option value="${package.PackageId}">${package.PackageName}</option>`;
                });

                var bankHTML = '<option value="">-- Selecciona paquete --</option>';
                $.each(packages.Bank, function (i, package) {
                    bankHTML += `<option value="${package.PackageId}">${package.PackageName}</option>`;
                });

                var blockIdInput = $(inputs.BlockId);
                var streetIdInput = $(inputs.StreetId);
                var modelIdInput = $(inputs.ModelId);


                ruvInput.html(ruvHTML);
                bankInput.html(bankHTML);
                blockIdInput.html(blocksHTML);
                streetIdInput.html(streetsHTML);
                modelIdInput.html(modelsHTML);
            });

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var created = await app.services.ajaxAsync({
                    url: `api/lots`,
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!created) {
                    button.button('reset');
                    return;
                }

                window.location = '#/lotes';
            });

            zonesInput.focus();
        });
    };
    var postReport = async function (context) {

        var lot = await app.services.fetch('api/lots/' + context.params.id);

        if (!lot)
            return app.pageMessage('Lote no encontrado.');

        if (lot.Status == 3) {
            app.pageMessage({
                icon: 'fa fa-times',
                message: 'No puedes crear reportes en un lote terminado',
                href: `#/cerradas/${lot.ZoneId}/lotes`,
                text: 'Regresar'
            });
            return;
        }

        var lotStatus = await app.services.fetch(`api/lots/${lot.LotId}/status`);

        if (!lotStatus)
            return app.pageMessage('Lote no encontrado.');

        var zones = await app.services.fetch('api/zones');

        if (!zones) {
            alertify.error(app.error);
            return false;
        }

        var zone = zones[0];

        var templateHTML = `
        <div class="col-xs-12"><br></div>
        <div class="col-xs-12">
            <a href="#/cerradas/${zone.ZoneId}/lotes" class="btn btn-default">
                Regresar
            </a>
            <hr>
            <h4>
                ${app.funcs.lotAddress(lot)} - ${zone.Name}                
            </h4>
            ${lot.LastFrontImage ? `
            <a href="/content/images/uploads/${lot.LastFrontImage}" target="_blank">
                Última imagen de reporte
            </a><br><br>` : ''}
        </div>
        <div id="report-container" class="row">
            <div class="col-xs-12 text-center">
                <h4>Cargando...</h4>
            </div>
        </div>
        <div class="col-xs-12"><br></div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var component = new app.components.ReportCreator({
                containerId: '#report-container',
                lot: lot,
                concepts: lotStatus.Concepts,
                onSubmit: function (lot) {

                    if (lot.LotStatus == 3)
                        window.location = `#/cerradas/${lot.ZoneId}/lotes`;
                    else
                        app.Sammy.refresh();
                }
            });
        });
    };
    var put = async function (context) {

        document.title = `${app.title} - Actualizar lote`;

        var lot = await app.services.fetch('api/lots/' + context.params.id);

        if (!lot)
            return app.pageMessage('Lote no encontrado');

        var inputsBuffer = app.forms.inputText([
            { name: 'LotNumber', displayName: 'Número de lote', value: lot.LotNumber },
            { name: 'OfficialNumber', displayName: 'Número oficial', value: lot.OfficialNumber },
            { name: 'GroundArea', displayName: 'Área de terreno', number: true, value: lot.GroundArea },
            { name: 'ConstructionArea', displayName: 'Área de const.', number: true, value: lot.ConstructionArea }
        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar lote - ${app.funcs.lotAddress(lot)}</h3>                
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    <div class="form-group">
                        <label>Estado de venta</label>
                        <select name="SaleStatus" class="form-control">
                            <option>-- Seleccione estado de venta --</option>
                            <option value="1">Disponible</option>                        
                            <option value="2">Vendido</option>                        
                            <option value="3">Firmado</option>                        
                            <option value="4">Pagado</option>                        
                            <option value="5">Entregado</option>                        
                            <option value="6">Integrado</option>                        
                        </select>
                    </div>
                    ${app.forms.formButton('Actualizar')}
                    <a href="#/lotes" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')
            var inputs = form[0].elements;

            var validator = app.services.validateForm(form);

            var saleStatusInput = $(inputs.SaleStatus);
            saleStatusInput.val(lot.SaleStatus);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/lots/${lot.LotId}`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                window.location = '#/lotes';
            });

        });
    };
    var putModel = async function (context) {

        document.title = `${app.title} - Actualizar modelo de lote`;

        var lot = await app.services.fetch('api/lots/' + context.params.id);

        if (!lot)
            return app.pageMessage('Lote no encontrado');

        var models = await app.services.fetch(`api/zones/${lot.ZoneId}/models`);

        if (!models) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { displayName: 'Cerrada', disabled: true, value: lot.ZoneName },
            { displayName: 'Estado de venta', disabled: true, value: app.enums.SaleStatus.GetString(lot.SaleStatus) },
            { displayName: 'Estado', disabled: true, value: app.enums.LotStatus.GetString(lot.Status) },
            { displayName: 'Porcentaje', disabled: true, value: `${lot.Percentage}%` },
            { displayName: 'Número de lote', disabled: true, value: lot.LotNumber },
            { displayName: 'Número oficial', disabled: true, value: lot.OfficialNumber },
            { displayName: 'Área de terreno', disabled: true, value: lot.GroundArea },
            { displayName: 'Área de const.', disabled: true, value: lot.ConstructionArea },
        ]);

        var modelsHTML = '<option value="">-- Selecciona modelo --</option>';
        $.each(models, function (i, model) {
            modelsHTML += `
            <option value="${model.ModelId}" 
                ${model.ModelId == lot.ModelId ? 'selected' : ''}>
                ${model.Name}
            </option>
`;
        });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar modelo de lote - ${app.funcs.lotAddress(lot)}</h3>
            </div>
            <div class="col-xs-12">
                <form>
                ${inputsBuffer}
                    <div class="form-group">
                        <label>Modelo</label>
                        <select name="ModelId" class="form-control"
                            data-val="true"
                            data-val-required="Modelo es requerido">
                            ${modelsHTML}
                        </select>
                        ${app.forms.validationErrorDiv('ModelId')}
                    </div>                    
                    ${app.forms.formButton('Actualizar modelo')}
                    <a href="#/lotes" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/lots/${lot.LotId}/model`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                window.location = '#/lotes';
            });

        });
    };
    var putPackages = async function (context) {

        document.title = `${app.title} - Actualizar paquetes`;

        var lot = await app.services.fetch('api/lots/' + context.params.id + '/?packages=true');

        if (!lot)
            return app.pageMessage('Lote no encontrado');

        var zone = await app.services.fetch('api/zones/' + lot.ZoneId);

        if (!zone) {
            alertify.error(app.error);
            return;
        }

        var packages = await app.services.fetch(`api/zone/${lot.ZoneId}/packages/select`);

        var ruvHTML = '<option value="">-- Selecciona paquete RUV --</option>';
        $.each(packages.RUV, function (i, package) {
            ruvHTML += `<option value="${package.PackageId}">${package.PackageName}</option>`;
        });

        var bankHTML = '<option value="">-- Selecciona paquete Bancario --</option>';
        $.each(packages.Bank, function (i, package) {
            bankHTML += `<option value="${package.PackageId}">${package.PackageName}</option>`;
        });

        var inputsBuffer = `
        <div class="form-group">
            <label>RUV</label>
            <select name="RUV" class="form-control" data-val="true" data-val-required="Paquete RUV es requerido">            
                ${ruvHTML}
            </select>
            ${app.forms.validationErrorDiv('RUV')}
        </div>
        <div class="form-group">
            <label>Bancario</label>
            <select name="Bancario" class="form-control" data-val="true" data-val-required="Paquete Bancario es requerido">
                ${bankHTML}                
            </select>
            ${app.forms.validationErrorDiv('Bancario')}
        </div>
        `;

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar paquetes - ${app.funcs.lotAddress(lot)}</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}                    
                    ${app.forms.formButton('Actualizar paquetes')}
                    <a href="#/lotes" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')
            var validator = app.services.validateForm(form);
            var inputs = form[0].elements;

            var ruvInput = $(inputs.RUV);
            var bankInput = $(inputs.Bancario);

            if (lot.RUVId)
                ruvInput.val(lot.RUVId);

            if (lot.BancarioId)
                bankInput.val(lot.BancarioId);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var created = await app.services.ajaxAsync({
                    url: `api/lots/${lot.LotId}/packages`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!created) {
                    button.button('reset');
                    return;
                }

                window.location = '#/lotes';
            });
        });
    };    
    var del = async function (context) {

        document.title = `${app.title} - Eliminar lote`;

        var lot = await app.services.fetch('api/lots/' + context.params.id);

        if (!lot)
            return app.pageMessage('Lote no encontrado');

        var returnHash = context.params.zone ?
            `#/cerradas/${lot.ZoneId}/lotes` :
            '#/lotes';

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Eliminar lote - ${app.funcs.lotAddress(lot)}</h3>                
            </div>
            <div class="col-xs-12">
                <form>
                    <button id="delete-button" class="btn btn-danger" data-loading-text="Cargando...">
                        Eliminar
                    </button>                    
                    ${`<a class="btn btn-default" href="${returnHash}">Regresar</a>`}
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var button = bubble.find('#delete-button');

            button.on('click', function () {

                if (app.loading)
                    return;

                var onConfirm = async function () {

                    if (app.loading)
                        return;

                    app.loading = true;
                    button.button('loading');

                    var deleted = await app.services.ajaxAsync({
                        url: `api/lots/${lot.LotId}`,
                        method: 'DELETE'
                    });

                    app.loading = false;

                    if (!deleted) {
                        button.button('reset');
                        return;
                    }

                    window.location = returnHash;

                }

                alertify.confirm(`Confirma eliminar ${app.funcs.lotAddress(lot)}`, onConfirm);
            });

        });
    };

    app.routes.push({ route: '#/lotes/?', callback: list });
    app.routes.push({ route: '#/lotes/crear/?', callback: post });
    app.routes.push({ route: '#/lotes/:id/reporte/?', callback: postReport });
    app.routes.push({ route: '#/lotes/:id/actualizar/?', callback: put });
    app.routes.push({ route: '#/lotes/:id/paquetes/?', callback: putPackages });
    app.routes.push({ route: '#/lotes/:id/modelo/?', callback: putModel });    
    app.routes.push({ route: '#/lotes/:id/eliminar/?', callback: del });

})();