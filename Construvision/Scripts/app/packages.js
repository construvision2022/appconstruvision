﻿(function () {

    if (!app)
        throw 'the app module is required';

    var list = async function (context) {

        document.title = `${app.title} - Paquetes`;

        var zone = await app.services.fetch(`api/zones/${context.params.id}`);
        if (!zone) {
            alertify.error(app.error);
            return false;
        }

        var packages = await app.services.fetch(`api/zones/${context.params.id}/packages`);
        if (!packages) {
            alertify.error(app.error);
            return false;
        }

        var buffer = '';
        $.each(packages, function (i, package) {
            buffer += `
            <tr>
                <td>${package.Name}
                    (${package.InstitutionName} - ${package.InstitutionPercentage.toFixed(2)}%)
                </td>
                <td>${package.Percentage.toFixed(2)}%</td>
                ${app.forms.optionsTd([
                    `<a href="#/paquetes/${package.PackageId}/actualizar">Actualizar</a>`,
                    `<a href="#/paquetes/${package.PackageId}/historial">Historial</a>`
                ])}
            </tr>
            `;
        });

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Paquetes de ${zone.Name}</h3>
            </div>
            <div class="col-xs-12">
                <a href="#/cerradas/${zone.ZoneId}/paquetes/crear" class="btn btn-success">Crear</a>
                <a href="#/cerradas" class="btn btn-default">Regresar</a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Porcentaje</th>
                            <th></th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var table = bubble.find('table');

        });
    };
    var post = async function (context) {

        document.title = `${app.title} - Crear paquete`;

        var zone = await app.services.fetch('api/zones/' + context.params.id);
        if (!zone) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true }
        ]);

        inputsBuffer += `
        <div class="form-group">
            <label>Institución</label>
            <select name="Institution" class="form-control">
                <option value="">-- Selecciona institución --</option>
                <option value="1">RUV</option>
                <option value="2">Bancario</option>
            </select>
        </div>
        `;

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Crear paquete de ${zone.Name}</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Crear')}
                    <a href="#/cerradas/${zone.ZoneId}/paquetes" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')
            var validator = app.services.validateForm(form);
            var inputs = form[0].elements;

            var nameInput = $(inputs.Name);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var created = await app.services.ajaxAsync({
                    url: `api/zone/${zone.ZoneId}/packages`,
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!created) {
                    button.button('reset');
                    return;
                }

                window.location = `#/cerradas/${zone.ZoneId}/paquetes`;
            });

            nameInput.focus();
        });
    };
    var put = async function (context) {

        document.title = `${app.title} - Actualizar paquete`;

        var package = await app.services.fetch('api/packages/' + context.params.id);
        if (!package) {
            alertify.error(app.error);
            return false;
        }

        var inputsBuffer = app.forms.inputText([
            { name: 'Name', displayName: 'Nombre', required: true, value: package.Name }

        ]);

        var templateHTML = `
        <div class="row">
            <div class="col-xs-12">
                <h3>Actualizar paquete</h3>
            </div>
            <div class="col-xs-12">
                <form>
                    ${inputsBuffer}
                    ${app.forms.formButton('Actualizar')}
                    <a href="#/cerradas/${package.ZoneId}/paquetes" class="btn btn-default">Regresar</a>
                </form>
            </div>
        </div>
        `;

        app.Sammy.swap(templateHTML, function (bubble) {

            var form = bubble.find('form');
            var button = form.find('button[type="submit"]')

            var validator = app.services.validateForm(form);

            form.on('submit', async function (event) {

                event.preventDefault();

                if (app.loading)
                    return false;

                if (!form.valid())
                    return false;

                app.loading = true;
                button.button('loading');

                var data = form.serializeObject();

                var updated = await app.services.ajaxAsync({
                    url: `api/packages/${package.PackageId}`,
                    method: 'PUT',
                    data: data,
                    validator: validator
                });

                app.loading = false;

                if (!updated) {
                    button.button('reset');
                    return;
                }

                window.location = `#/cerradas/${package.ZoneId}/paquetes`;
            });

        });
    };
    var history = async function (context) {

        document.title = `${app.title} - Historial`;

        var package = await app.services.fetch('api/packages/' + context.params.id);

        if (!package)
            return app.pageMessage();

        var history = await app.services.fetch(`api/packages/${context.params.id}/history`);

        if (!history)
            return app.pageMessage();

        if (!history.length)
            return app.pageMessage('No hay avances en ' + package.Name);

        var buffer = '';
        $.each(history, function (i, item) {

            buffer += `
            <tr>
                <td>${moment.utc(item.StartDate).format('DD/MMMM/YY')}</td>
                <td>${moment.utc(item.EndDate).format('DD/MMMM/YY')}</td>
                <td>${item.StartingPercentage}% ~ ${item.EndingPercentage}%</td>
            </tr>
            `;
        });

        var html = `
        <div class="row">
            <div class="col-xs-12">
                <a href="#/cerradas/${package.ZoneId}/paquetes" class="btn btn-default">
                    Regresar
                </a>
            </div>
            <div class="col-xs-12"><br></div>
            <div class="col-xs-12">
                <h3>${package.Name} (${package.Percentage}%)</h3>
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Fecha inicial</th>
                            <th>Fecha final</th>
                            <th>%</th>
                        <tr>
                    </thead>
                    <tbody>
                        ${buffer}
                    </tbody>
                </table>
            </div>
        </div>
        `;

        app.Sammy.swap(html, function (bubble) {

            var table = bubble.find('table');

        });
    };

    app.routes.push({ route: '#/cerradas/:id/paquetes/?', callback: list });
    app.routes.push({ route: '#/cerradas/:id/paquetes/crear/?', callback: post });
    app.routes.push({ route: '#/paquetes/:id/actualizar/?', callback: put });
    app.routes.push({ route: '#/paquetes/:id/historial/?', callback: history });

})();