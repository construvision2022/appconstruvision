﻿/// <reference path="jquery-3.3.1.js" />
/// <autosync enabled="true" />
/// <reference path="app.js" />
/// <reference path="backbone.min.js" />
/// <reference path="backboneapp.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="bootstrap-datepicker.js" />
/// <reference path="jquery.blueimp-gallery.min.js" />
/// <reference path="jquery.js" />
/// <reference path="jquery.signalr-2.2.0.min.js" />
/// <reference path="jquery.validate.min.js" />
/// <reference path="jquery.validate.unobtrusive.min.js" />
/// <reference path="jquery-jvectormap-2.0.2.min.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <reference path="moment.min.js" />
/// <reference path="moment-with-locales.js" />
/// <reference path="sammy-0.7.5.min.js" />
/// <reference path="underscore.min.js" />
/// <reference path="underscore.string.js" />
