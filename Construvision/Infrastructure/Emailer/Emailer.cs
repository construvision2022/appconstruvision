﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Hosting;
using Domain;
using Domain.EmailContext;
using Newtonsoft.Json;

namespace Construvision
{
    public class Emailer
    {
        static HttpClient HttpClient;
        static Emailer()
        {
            HttpClient = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        public Emailer()
        {
            SenderName = "Construvision SA de CV";
            SenderEmail = "info@construvisionapp.com";
        }

        string Subject { set; get; }
        string Body { set; get; }
        string InnerBody { set; get; }
        string SenderName { set; get; }
        string SenderEmail { set; get; }

        public async void Send(Email email)
        {
            try
            {
                if (WebConfigData.Environment == "Debug")
                {
                    if (!App.Instance.Developers.Contains(email.AuxEmail))
                    {
                        App.Instance.EventLog.Push("stopped email: " + email.AuxEmail);
                        return;
                    }
                }

                LayoutModel layoutDataModel = null;
                await Task.Run(() => layoutDataModel = Serializer.Deserialize<LayoutModel>(email.LayoutData));

                //if (email.EmailTemplate != EmailTemplates.NewExcelExport)
                //    return;

                ExcelExportModel model = null;
                await Task.Run(() => model = Serializer.Deserialize<ExcelExportModel>(email.Data));

                Subject = model.GetEmailSubject();

                await SetEmailBodyFromTemplateAndModel(layoutDataModel, model);

                var plainTextBody = Regex.Replace(InnerBody, @"<(.|\n)*?>", string.Empty);
                plainTextBody = plainTextBody.Replace("\r\n\r\n", "\r\n");
                plainTextBody = plainTextBody.Replace("\r\n", "");
                plainTextBody = plainTextBody.Trim();
                plainTextBody = plainTextBody
                    .Replace("&aacute;", "a")
                    .Replace("&eacute;", "e")
                    .Replace("&iacute;", "i")
                    .Replace("&oacute;", "o")
                    .Replace("&uacute;", "u");

                var data = new
                {
                    content = new
                    {
                        from = layoutDataModel.BusinessName + " <" + layoutDataModel.BusinessSenderEmail + ">",
                        subject = Subject,
                        html = Body,
                        text = plainTextBody,
                    },
                    recipients = new[]
                    {
                        new
                        {
                            address = email.AuxEmail,
                            metadata = new { id = email.EmailId },
                        },
                    },
                };

                var payLoad = string.Empty;
                await Task.Run(() => payLoad = JsonConvert.SerializeObject(data));

                var uri = new Uri("https://api.sparkpost.com/api/v1/transmissions");

                var message = new HttpRequestMessage(HttpMethod.Post, uri);

                message.Headers.TryAddWithoutValidation("Authorization", layoutDataModel.EmailSenderPassword);

                message.Content = new StringContent(payLoad, Encoding.UTF8);

                var response = await HttpClient.SendAsync(message);

                var responseContent = await response.Content.ReadAsStringAsync();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    App.Instance.EventLog.Push($"sparkpost | {response.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
        }

        async Task<string> ReadFile(string templateName)
        {
            var templatePath = HostingEnvironment.MapPath("~/EmailTemplates/" + templateName);

            var templateText = string.Empty;
            using (var fileStream = new FileStream(templatePath, FileMode.Open, FileAccess.Read))
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                templateText = await streamReader.ReadToEndAsync();
            }

            return templateText;
        }
        async Task<bool> SetEmailBodyFromTemplateAndModel(LayoutModel layoutData, IEmailModel model)
        {
            var layoutText = await ReadFile("Layout.html");
            var templateText = await ReadFile(model.GetEmailTemplate().ToString() + ".html");
            var fullTemplate = layoutText.Replace("{Template}", templateText);

            var properties = layoutData.GetType().GetProperties();
            var modelProperties = model.GetType().GetProperties();

            var tempvar = string.Empty;
            foreach (var item in properties)
            {
                //Layout fill
                tempvar = Convert.ToString(item.GetValue(layoutData, null));
                fullTemplate = fullTemplate.Replace("{{" + item.Name + "}}", tempvar);
                templateText = templateText.Replace("{{" + item.Name + "}}", tempvar);
            }

            fullTemplate = fullTemplate.Replace("{{BaseUrl}}", WebConfigData.BaseUrl);
            templateText = templateText.Replace("{{BaseUrl}}", WebConfigData.BaseUrl);

            foreach (var item in modelProperties)
            {
                //Template fill
                tempvar = Convert.ToString(item.GetValue(model, null));
                fullTemplate = fullTemplate.Replace("{" + item.Name + "}", tempvar);
                templateText = templateText.Replace("{" + item.Name + "}", tempvar);
            }

            InnerBody = templateText;
            Body = fullTemplate;

            return true;
        }
    }
}