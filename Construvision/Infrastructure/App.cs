﻿using Domain;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Construvision
{
    public class App
    {
        public static App Instance { private set; get; }
        static App()
        {

        }

        public App()
        {
            Instance = this;

            Chronjobs = new ChronJobs.Context(new List<ChronJobs.BaseChronJob>
            {
                //new ChronJobs.SendEmail(3),
                //new ChronJobs.Pinger(20),
                //new ChronJobs.PeriodCloser(300),
                //new ChronJobs.GeneratePeriodExcel(25),
            });

            Developers = new List<string>()
            {
                "danielaguirrelopez42@gmail.com",
                "fex@yopmail.com",
                "manager@yopmail.com",
            };

            ExcelReportRecipients = new List<string>
            {
                //"danielaguirrelopez42@gmail.com",
                //"jacobourquides@gmail.com",

                "jfelix@construvision.com",
                "kwakida@construvision.com",
                "kooichi@hotmail.com",
                "kooichiwakida@icloud.com",
                "kooichi@icloud.com",
            };

            EventLog = new ConcurrentStack<string>();

            RunInitialTasks();
        }

        public ChronJobs.Context Chronjobs { private set; get; }
        public ConcurrentStack<string> EventLog { private set; get; }
        public List<string> Developers { private set; get; }
        public List<string> ExcelReportRecipients { private set; get; }

        async void RunInitialTasks()
        {
            var output = new Dictionary<string, Stack<string>>();

            var scriptServices = new List<ScriptServices.BaseScriptService>
            {
                //new ScriptServices.InitialCreate(),


                //new ScriptServices.PeriodCloser(),

                //new ScriptServices.RunSomeSQL(),
                //new ScriptServices.SyncService(),
                //new ScriptServices.GeneratePeriodExcel(),

                //might need this one...
                //new ScriptServices.FixModels(),
            };

            if (!scriptServices.Any())
                return;

            var start = TimeZoner.Now;

            foreach (var scriptService in scriptServices)
            {
                await scriptService.Run();
            }

            var end = TimeZoner.Now;

            var seconds = (end - start).TotalSeconds;

            EventLog.Push("app init after " + seconds);
        }
    }
}