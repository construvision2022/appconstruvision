﻿using Domain;
using System;

namespace Construvision
{
    public static class Logger
    {
        public static void Log(string message, LogTypes type = LogTypes.Service)
        {
            try
            {
                using (var context = new EFContext())
                {
                    context.Logs.Add(new Log(message));
                    context.SaveChanges();
                }
            }
            catch
            {

            }
        }
        public static void Log(Exception ex, LogTypes type = LogTypes.Service)
        {
            using (var context = new EFContext())
            {
                context.Logs.Add(new Log(ex.ToString()));
                context.SaveChanges();
            }
        }        
    }
}