﻿using Domain;
using Domain.SessionContext;
using System;
using System.Linq.Expressions;
using System.Security.Principal;

namespace Construvision
{
    public class CustomPrincipal : IPrincipal, IIdentity, IActor
    {
        public Session Session { private set; get; }

        public int Id { private set; get; }
        public SessionTypes SessionType { private set; get; }

        public Manager Manager { private set; get; }
        public Admin Admin { private set; get; }
        public SalesAdmin SalesAdmin { private set; get; }
        public Supervisor Supervisor { private set; get; }        
        public Seller Seller { private set; get; }
        public Settlement Settlement { private set; get; }
        public User User { private set; get; }
        public Business Business { private set; get; }

        public string IP { set; get; }
        public string PathAndQuery { set; get; }

        public CustomPrincipal(Session session, SessionTypes type, string pathAndQuery)
        {
            Session = session ?? throw new InvalidCredentialsException();
            User = session.User;
            SessionType = type;
            PathAndQuery = pathAndQuery;
        }

        public void WithSupervisor(Supervisor supervisor)
        {
            if (supervisor == null) throw new ArgumentNullException("supervisor");

            Id = supervisor.SupervisorId;
            Supervisor = supervisor;
            Settlement = supervisor.Zone.Settlement;
            Business = User.Business;
        }
        public void WithAdmin(Admin admin)
        {
            if (admin == null) throw new ArgumentNullException("admin");

            Id = admin.AdminId;
            Admin = admin;
            Settlement = admin.Settlement;
            Business = User.Business;
        }
        public void WithManager(Manager manager)
        {
            if (manager == null) throw new ArgumentNullException("manager");

            Id = manager.ManagerId;
            Manager = manager;
            Business = User.Business;
        }
        public void WithSalesAdmin(SalesAdmin salesAdmin)
        {
            if (salesAdmin == null) throw new ArgumentNullException("salesAdmin");

            Id = salesAdmin.SalesAdminId;
            SalesAdmin = salesAdmin;
            Settlement = salesAdmin.Settlement;
            Business = User.Business;
        }        
        public void WithSeller(Seller seller)
        {
            if (seller == null) throw new ArgumentNullException("seller");

            Id = seller.SellerId;
            Settlement = seller.Zone.Settlement;
            Seller = seller;
            Business = User.Business;
        }


        public IIdentity Identity { get { return this; } }
        public bool IsInRole(string role) { throw new NotImplementedException(); }

        public string Name { set; get; }
        public string AuthenticationType { get { return "Token"; } }
        public bool IsAuthenticated { set; get; }

        IActor GetCurrentActor()
        {
            switch (SessionType)
            {
                default:
                case SessionTypes.None: throw new InvalidCredentialsException();

                case SessionTypes.Seller: return Seller;                
                case SessionTypes.Admin: return Admin;
                case SessionTypes.SalesAdmin: return SalesAdmin;
                case SessionTypes.Supervisor: return Supervisor;
                case SessionTypes.Manager: return Manager;
            }
        }

        public Expression<Func<Admin, bool>> Admins()
        {
            return GetCurrentActor().Admins();
        }
        public Expression<Func<Lot, bool>> Lots()
        {
            return GetCurrentActor().Lots();
        }
        public Expression<Func<Street, bool>> Streets()
        {
            return GetCurrentActor().Streets();
        }
        public Expression<Func<Block, bool>> Blocks()
        {
            return GetCurrentActor().Blocks();
        }
        public Expression<Func<Model, bool>> Models()
        {
            return GetCurrentActor().Models();
        }
        public Expression<Func<Zone, bool>> Zones()
        {
            return GetCurrentActor().Zones();
        }
        public Expression<Func<Institution, bool>> Institutions()
        {
            return GetCurrentActor().Institutions();
        }
        public Expression<Func<Package, bool>> Packages()
        {
            return GetCurrentActor().Packages();
        }
        public Expression<Func<Map, bool>> Maps()
        {
            return GetCurrentActor().Maps();
        }
        public Expression<Func<MapImage, bool>> MapImages()
        {
            return GetCurrentActor().MapImages();
        }
        public Expression<Func<Settlement, bool>> Settlements()
        {
            return GetCurrentActor().Settlements();
        }
        public Expression<Func<Report, bool>> Reports()
        {
            return GetCurrentActor().Reports();
        }
        public Expression<Func<ReportMembership, bool>> ReportMemberships()
        {
            return GetCurrentActor().ReportMemberships();
        }
    }
}
