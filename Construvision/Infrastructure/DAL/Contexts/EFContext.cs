﻿using Backend.Domain.ConnectionContext;
using Backend.Migrations;
using Domain;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading.Tasks;

namespace Construvision
{
    public class EFContext : DbContext
    {
        public EFContext() : base("name=EFContext")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.ValidateOnSaveEnabled = false;
        }

        public async Task<bool> LoadPeriodHistory(Settlement settlement)
        {
            var now = TimeZoner.Now;

            await Settlements
                .Include(x => x.Business.Tennant)
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await Lots
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await Streets
               .Where(x => x.SettlementId == settlement.SettlementId)
               .LoadAsync();

            await Models
               .Where(x => x.SettlementId == settlement.SettlementId)
               .LoadAsync();

            await Zones
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();



            await Institutions
                .Where(x => x.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await Packages
                .Where(x => x.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await PackageMemberships
                .Where(x => x.Package.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();




            await Periods
                .Where(x => x.TennantId == settlement.Business.TennantId)
                .LoadAsync();

            await SettlementPeriods
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await InstitutionPeriods
                .Where(x => x.Institution.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await PackagePeriods
                .Where(x => x.Package.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await ZonePeriods
                .Where(x => x.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();





            await Reports
                .Where(x => x.Lot.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await ReportMemberships
                .Where(x => x.Report.Lot.SettlementId == settlement.SettlementId)
                .LoadAsync();

            return true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EFContext, Configuration>());

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            #region UserContext

            modelBuilder.Entity<Connection>()
               .ToTable("Connections");

            modelBuilder.Entity<Notification>()
                .HasKey(x => x.NotificationId)
               .ToTable("Notifications");

            modelBuilder.Entity<NotificationMembership>()
               .ToTable("NotificationMemberships");

            modelBuilder.Entity<SmartLink>()
               .ToTable("SmartLinks");

            modelBuilder.Entity<Session>()
               .ToTable("Sessions");

            modelBuilder.Entity<User>()
               .ToTable("Users");

            modelBuilder.Entity<Manager>()
               .ToTable("Managers");

            modelBuilder.Entity<Admin>()
               .ToTable("Admins");

            modelBuilder.Entity<Supervisor>()
               .ToTable("Supervisors");

            modelBuilder.Entity<SalesAdmin>()
               .ToTable("SalesAdmins");

            modelBuilder.Entity<Seller>()
               .ToTable("Sellers");

            #endregion

            #region SettlementContext

            modelBuilder.Entity<Tennant>()
               .ToTable("Tenants");

            modelBuilder.Entity<Business>()
               .ToTable("Businesses");

            modelBuilder.Entity<Settlement>()
               .ToTable("Settlements");

            modelBuilder.Entity<Model>()
               .ToTable("Models");

            modelBuilder.Entity<Zone>()
               .ToTable("Zones");

            modelBuilder.Entity<Street>()
               .ToTable("Streets");

            modelBuilder.Entity<Block>()
               .ToTable("Blocks");

            modelBuilder.Entity<Lot>()
               .ToTable("Lots");

            modelBuilder.Entity<Lot>()
                .HasOptional(x => x.Report)
                .WithMany()
                .HasForeignKey(x => x.ReportId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lot>()
                .HasOptional(x => x.LastReport)
                .WithMany()
                .HasForeignKey(x => x.LastReportId)
                .WillCascadeOnDelete(false);

            #endregion

            #region ReportContext

            modelBuilder.Entity<BuildingConcept>()
                .ToTable("BuildingConcepts");

            modelBuilder.Entity<Report>()
                .ToTable("Reports");

            modelBuilder.Entity<Report>()
                .HasRequired(x => x.Lot)
                .WithMany(x => x.Reports)
                .HasForeignKey(x => x.LotId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ReportMembership>()
                .ToTable("ReportMemberships");

            modelBuilder.Entity<ReportMembershipImage>()
                .ToTable("ReportMembershipImages");

            modelBuilder.Entity<Institution>()
                .ToTable("Institutions");

            modelBuilder.Entity<Package>()
                .ToTable("Packages");

            modelBuilder.Entity<PackageMembership>()
                .ToTable("PackagesMemberships");

            #endregion

            #region SystemContext

            modelBuilder.Entity<Email>()
               .ToTable("Emails")
               .HasKey(x => x.EmailId);

            modelBuilder.Entity<Log>()
               .ToTable("Logs");

            modelBuilder.Entity<Period>()
                .ToTable("Periods");

            modelBuilder.Entity<PackagePeriod>()
                .ToTable("PackagePeriods");

            modelBuilder.Entity<ZonePeriod>()
                .ToTable("ZonePeriods");

            modelBuilder.Entity<SettlementPeriod>()
                .ToTable("SettlementPeriods");

            modelBuilder.Entity<InstitutionPeriod>()
                .ToTable("InstitutionPeriods");

            modelBuilder.Entity<ExcelExport>()
                .ToTable("ExcelExports");
            #endregion

            #region MapContext
            modelBuilder.Entity<Marker>()
               .ToTable("Markers");

            modelBuilder.Entity<MapImage>()
               .ToTable("MapImages");

            modelBuilder.Entity<Map>()
               .ToTable("Maps");

            #endregion
        }

        #region UserContext

        public DbSet<Connection> Connections { set; get; }
        public DbSet<Notification> Notifications { set; get; }
        public DbSet<NotificationMembership> NotificationMemberships { set; get; }
        public DbSet<SmartLink> SmartLinks { set; get; }
        public DbSet<Session> Sessions { set; get; }
        public DbSet<User> Users { set; get; }
        public DbSet<Admin> Admins { set; get; }
        public DbSet<Manager> Managers { set; get; }
        public DbSet<Supervisor> Supervisors { set; get; }
        public DbSet<SalesAdmin> SalesAdmins { set; get; }
        public DbSet<Seller> Sellers { set; get; }

        #endregion

        #region SettlementContext

        public DbSet<Tennant> Tenants { set; get; }
        public DbSet<Business> Businesses { set; get; }
        public DbSet<Settlement> Settlements { set; get; }
        public DbSet<Model> Models { set; get; }
        public DbSet<Zone> Zones { set; get; }
        public DbSet<Block> Blocks { set; get; }
        public DbSet<Street> Streets { set; get; }
        public DbSet<Lot> Lots { set; get; }

        #endregion

        #region ReportContext

        public DbSet<BuildingConcept> BuildingConcepts { set; get; }
        public DbSet<Report> Reports { set; get; }
        public DbSet<ReportMembership> ReportMemberships { set; get; }
        public DbSet<ReportMembershipImage> ReportMembershipImages { set; get; }
        public DbSet<Package> Packages { set; get; }
        public DbSet<PackageMembership> PackageMemberships { set; get; }
        public DbSet<Institution> Institutions { set; get; }

        #endregion

        #region SystemContext
        public DbSet<Email> Emails { set; get; }
        public DbSet<Log> Logs { set; get; }
        public DbSet<Period> Periods { set; get; }
        public DbSet<ZonePeriod> ZonePeriods { set; get; }
        public DbSet<PackagePeriod> PackagePeriods { set; get; }
        public DbSet<SettlementPeriod> SettlementPeriods { set; get; }
        public DbSet<InstitutionPeriod> InstitutionPeriods { set; get; }
        public DbSet<ExcelExport> ExcelExports { set; get; }
        #endregion

        #region MapContext
        public DbSet<Marker> Markers { set; get; }

        public DbSet<MapImage> MapImages { set; get; }
        public DbSet<Map> Maps { set; get; }

        #endregion
    }
}