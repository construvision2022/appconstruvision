﻿using System.Collections.Generic;

namespace Backend.Common.SparkPost.Models
{
    public class WebhookWrapper
    {
        public Msys msys { set; get; }
        //TODO: Add the different types here 
    }

    public class Msys
    {
        public MessageEvent message_event { set; get; }
    }


    public class RcptMeta
    {
        public string id { set; get; }
    }

    public class MessageEvent
    {
        //EVENT: Bounce (first event handled, so default fields are all here)
        public string type { set; get; }
        public string bounce_class { set; get; }
        public string campaign_id { set; get; }
        public string customer_id { set; get; }
        public string delv_method { set; get; }
        public string device_token { set; get; }
        public string error_code { set; get; }
        public string event_id { set; get; }
        public string friendly_from { set; get; }
        public string ip_address { set; get; }
        public string message_id { set; get; }
        public string msg_from { set; get; }
        public string msg_size { set; get; }
        public string num_retries { set; get; }
        public RcptMeta rcpt_meta { set; get; }
        public List<string> rcpt_tags { set; get; }
        public string rcpt_to { set; get; }
        public string raw_rcpt_to { set; get; }
        public string rcpt_type { set; get; }
        public string raw_reason { set; get; }
        public string reason { set; get; }
        public string routing_domain { set; get; }
        public string sms_coding { set; get; }
        public string sms_dst { set; get; }
        public string sms_dst_npi { set; get; }
        public string sms_dst_ton { set; get; }
        public string sms_src { set; get; }
        public string sms_src_npi { set; get; }
        public string sms_src_ton { set; get; }
        public string subaccount_id { set; get; }
        public string subject { set; get; }
        public string template_id { set; get; }
        public string template_version { set; get; }
        public int timestamp { set; get; }
        public string transmission_id { set; get; }

        //EVENT: Delay
        public string queue_time { set; get; }
        public List<string> sms_remoteids { set; get; }
        public int sms_segments { set; get; }

        //EVENT: Delivery
    }
}