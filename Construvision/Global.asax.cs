﻿using Domain;
using Newtonsoft.Json;
using System.Globalization;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using System.Web.Routing;

namespace Construvision
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            //AreaRegistration.RegisterAllAreas();
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);

            var config = GlobalConfiguration.Configuration;

            var routes = RouteTable.Routes;
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();
            config.MapHttpAttributeRoutes();

            var cors = new EnableCorsAttribute(origins: "*", headers: "*", methods: "*");
            config.EnableCors(cors);

            var jsonFormatterSettings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                DateParseHandling = DateParseHandling.DateTime,
                Culture = CultureInfo.CurrentCulture,
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = Formatting.None,
            };

            config.Formatters.JsonFormatter.SerializerSettings = jsonFormatterSettings;
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            //GlobalHost.DependencyResolver.Register(typeof(IPerformanceCounterManager), () => new TempPerformanceCounterManager());

            //--------------------------------------------------------------------------------
            //This must be the final line where 'config' is referenced -----------------------
            //--------------------------------------------------------------------------------
            //--------------------------------------------------------------------------------
            config.EnsureInitialized();
            //--------------------------------------------------------------------------------

            WebConfigData.BaseUrl = WebConfigurationManager.AppSettings["BaseUrl"];
            WebConfigData.Environment = WebConfigurationManager.AppSettings["Environment"];
            WebConfigData.BrokerUrl = WebConfigurationManager.AppSettings["BrokerUrl"];
            WebConfigData.BrokerEndPoint = WebConfigurationManager.AppSettings["BrokerEndPoint"];
            WebConfigData.BrokerToken = WebConfigurationManager.AppSettings["BrokerToken"];

            new App();

            //new RTContext();
        }

        //protected void Application_BeginRequest()
    }
}
/* Global.asax methods
-----Request-----
Application_BeginRequest
Application_AuthenticateRequest
Application_AuthorizeRequest
Application_ResolveRequestCache
Application_AcquireRequestState
Application_PreRequestHandlerExecute
Application_PreSendRequestHeaders
Application_PreSendRequestContent
<<code is executed>>
Application_PostRequestHandlerExecute
Application_ReleaseRequestState
Application_UpdateRequestCache
Application_EndRequest
 

-----Application----
Application_Init
Application_Start
Application_Disposed
Application_End
Application_Error
 */
