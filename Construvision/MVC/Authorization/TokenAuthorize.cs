﻿using Construvision.Controllers;
using Domain;
using Domain.SessionContext;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Construvision
{
    public class TokenAuthorize : AuthorizeAttribute
    {
        public TokenAuthorize()
        {

        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var authorizationHeader = actionContext.Request.Headers.Authorization;

            if (authorizationHeader == null)
                return false;

            var token = authorizationHeader.Parameter;
            var scheme = authorizationHeader.Scheme;

            if (string.IsNullOrWhiteSpace(scheme) || string.IsNullOrWhiteSpace(token))
                return false;

            if (scheme != "Token")
                return false;

            var context = new EFContext();

            var session = context.Sessions
                .Include(x => x.User.Business)
                .SingleOrDefault(x =>
                    !x.IsExpired &&
                    x.Token == token);

            if (session == null)
                return false;

            int.TryParse(actionContext.Request.GetHeader("SessionId"), out int sessionId);

            var header = actionContext.Request.GetHeader("SessionType");

            SessionTypes sessionType = SessionTypes.None;
            Enum.TryParse(header, out sessionType);


            //if (sessionType == SessionTypes.None) return false;

            var now = TimeZoner.Now;

            if (session.LastActivity.AddMinutes(3) < now)
            {
                session.UpdateLastActivity(now);

                context.SaveChanges();
            }

            var pathAndQuery = actionContext.Request.RequestUri.PathAndQuery;

            var principal = new CustomPrincipal(session, sessionType, pathAndQuery);

            switch (sessionType)
            {
                case SessionTypes.None:
                    break;

                case SessionTypes.Manager:
                    principal.WithManager(GetManager(context, session.UserId, sessionId));
                    break;

                case SessionTypes.Admin:
                    principal.WithAdmin(GetAdmin(context, session.UserId, sessionId));
                    break;

                case SessionTypes.SalesAdmin:
                    principal.WithSalesAdmin(GetSalesAdmin(context, session.UserId, sessionId));
                    break;

                case SessionTypes.Supervisor:
                    principal.WithSupervisor(GetSupervisor(context, session.UserId, sessionId));
                    break;

                case SessionTypes.Seller:
                    principal.WithSeller(GetSeller(context, session.UserId, sessionId));
                    break;
            }

            if (HttpContext.Current != null)
                HttpContext.Current.User = principal;

            actionContext.RequestContext.Principal = principal;

            var controller = actionContext.ControllerContext.Controller as BaseApiController;

            controller.OnCreate(context, principal);

            return true;
        }


        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var httpResponse = new HttpResponseMessage(HttpStatusCode.PaymentRequired)
            {
                ReasonPhrase = "Unauthorized"
            };

            actionContext.Response = httpResponse;

            //base.HandleUnauthorizedRequest(actionContext);
        }

        Manager GetManager(EFContext dbContext, int userId, int managerId)
        {
            try
            {
                return dbContext.Managers
                    .Single(x =>
                        x.UserId == userId &&
                        x.ManagerId == managerId);
            }
            catch
            {
                throw new InvalidCredentialsException();
            }
        }
        Admin GetAdmin(EFContext dbContext, int userId, int adminId)
        {
            try
            {
                return dbContext.Admins
                    .Include(x => x.Settlement.Business)
                    .Single(x =>
                        x.UserId == userId &&
                        x.AdminId == adminId);
            }
            catch
            {
                throw new InvalidCredentialsException();
            }
        }
        SalesAdmin GetSalesAdmin(EFContext dbContext, int userId, int salesAdminId)
        {
            try
            {
                return dbContext.SalesAdmins
                    .Include(x => x.Settlement.Business)
                    .Single(x =>
                        x.UserId == userId &&
                        x.SalesAdminId == salesAdminId);
            }
            catch
            {
                throw new InvalidCredentialsException();
            }
        }
        Supervisor GetSupervisor(EFContext dbContext, int userId, int supervisorId)
        {
            try
            {
                return dbContext.Supervisors
                    .Include(x => x.Zone.Settlement.Business)
                    .Single(x =>
                        x.UserId == userId &&
                        x.SupervisorId == supervisorId);
            }
            catch
            {
                throw new InvalidCredentialsException();
            }
        }
        Seller GetSeller(EFContext dbContext, int userId, int sellerId)
        {
            try
            {
                return dbContext.Sellers
                    .Include(x => x.Zone.Settlement.Business)
                    .Single(x =>
                        x.UserId == userId &&
                        x.SellerId == sellerId);
            }
            catch
            {
                throw new InvalidCredentialsException();
            }
        }
    }
}