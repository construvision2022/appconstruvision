﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Construvision
{
    public class AuthorizeRoles : AuthorizeAttribute
    {
        public AuthorizeRoles()
        {

        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var principal = Thread.CurrentPrincipal as CustomPrincipal;
            return Roles.Split(' ').Contains(principal.SessionType.ToString());
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var httpResponse = new HttpResponseMessage(HttpStatusCode.PaymentRequired);
            httpResponse.ReasonPhrase = "Unauthorized";
            actionContext.Response = httpResponse;
            //base.HandleUnauthorizedRequest(actionContext);
        }
    }
}