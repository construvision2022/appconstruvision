﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Construvision
{
    public static class Extensions
    {
        public static HttpResponseMessage SetCacheHeaders(this HttpResponseMessage response, TimeSpan time)
        {
            response.Headers.CacheControl = new CacheControlHeaderValue
            {
                Public = true,
                MaxAge = time,
            };

            return response;
        }

        public static string GetHeader(this HttpRequestMessage request, string key)
        {
            IEnumerable<string> keys = null;

            if (!request.Headers.TryGetValues(key, out keys)) return null;

            return keys.First();
        }
    }
}