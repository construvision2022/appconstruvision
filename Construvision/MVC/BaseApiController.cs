﻿using Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Construvision.Controllers
{
    [EnableCors("*", "*", "*", SupportsCredentials = true)]
    [TokenAuthorize]
    [ModelStateValidationFilter]
    [UnhandledExceptionFilter]
    public abstract class BaseApiController : ApiController
    {
        public BaseApiController()
        {

        }

        public void OnCreate(EFContext context, CustomPrincipal principal)
        {
            Context = context;
            Principal = principal;
        }

        protected EFContext Context;
        protected CustomPrincipal Principal;

        public string Message404 = "No encontrado";
        public HttpResponseMessage OK(object output = null)
        {
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
        protected HttpResponseMessage Error(string error = "")
        {
            if (!string.IsNullOrWhiteSpace(error))
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error);

            if (ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");

            if (ModelState.Count == 1)
            {
                var key = ModelState.Keys.First();

                if (ModelState.Values.Count == 1)
                {
                    var firstValue = ModelState.Values.First();
                    var firstError = firstValue.Errors.First();

                    if (string.IsNullOrWhiteSpace(key))
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, firstError.ErrorMessage);
                }
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }
        protected HttpResponseMessage Error(string key, string error)
        {
            ModelState.AddModelError(key, error);
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }
        protected HttpResponseMessage Error404()
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, Message404);
        }
        protected void GetValidationErrors(Dictionary<string, string> dictionary)
        {
            foreach (var item in dictionary)
            {
                ModelState.AddModelError(item.Key, item.Value);
            }
        }
        protected Period GetCurrentPeriod()
        {
            var date = TimeZoner.Now.Date;

            var period = Context.Periods
                .SingleOrDefault(x =>
                x.StartDate <= date
                    && x.EndDate >= date);

            if (period == null)
            {
                ModelState.AddModelError(string.Empty, "Periodo no encontrado");
                return null;
            }

            return period;
        }
        protected async Task<Period> GetCurrentPeriodAsync()
        {
            var date = TimeZoner.Now.Date;

            return await Context.Periods
                .SingleOrDefaultAsync(x =>
                    x.StartDate <= date
                    && x.EndDate >= date);
        }
        protected bool ImageFileExists(string fileName)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    var basePath = HostingEnvironment.MapPath("~/Content/images/uploads");
                    var filePath = Path.Combine(basePath, fileName);
                    return System.IO.File.Exists(filePath);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }

            return false;
        }
        protected async Task<bool> Resync(Settlement settlement)
        {
            var now = TimeZoner.Now;

            await Context.LoadPeriodHistory(settlement);

            settlement.Resync(now);

            await Context.SaveChangesAsync();

            return true;
        }
    }
}