﻿using Domain;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace Construvision.Controllers
{
    public class UnhandledExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var request = context.Request;
            var response = new HttpResponseMessage(HttpStatusCode.BadRequest);


            LogException(context);

            var type = context.Exception.GetType();

            if (type == typeof(HttpResponseException))
            {
                var httpResponseException = context.Exception as HttpResponseException;

                if (httpResponseException.Response.StatusCode == HttpStatusCode.UnsupportedMediaType)
                {
                    response.Content = new StringContent("Missing Content-Type Header.");
                }

                goto hell;
            }


            if (type == typeof(DomainException))
            {
                response.Content = new StringContent("Error in server.");
                goto hell;
            }

            if (type == typeof(FileNotFoundException))
            {
                response.Content = new StringContent("File not found.");
                goto hell;
            }

            if (type == typeof(NullReferenceException))
            {
                response.Content = new StringContent("Missing required parameters.");
                goto hell;
            }

            if (type == typeof(NotImplementedException))
            {
                response.StatusCode = HttpStatusCode.NotImplemented;
                goto hell;
            }

            if (type == typeof(InvalidCredentialsException))
            {
                response.StatusCode = HttpStatusCode.PaymentRequired;
                response.ReasonPhrase = "Unauthorized";
                //httpResponse.Headers.Add("", "");

                goto hell;
            }

            if (type == typeof(MissingFieldException))
            {
                response.StatusCode = HttpStatusCode.ExpectationFailed;
                response.Headers.Add("Expect", "Expected ID");
                goto hell;
            }



            hell:
            context.Response = response;
        }

        void LogException(HttpActionExecutedContext context)
        {
            using (var dbContext = new EFContext())
            {
                try
                {
                    var exception = context.Exception.ToString();

                    dbContext.Logs.Add(new Log(exception));
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}