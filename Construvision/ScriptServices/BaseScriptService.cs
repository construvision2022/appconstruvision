﻿using System.Threading.Tasks;

namespace Construvision.ScriptServices
{
    public abstract class BaseScriptService
    {
        protected EFContext Context { set; get; }

        public BaseScriptService()
        {
            Context = new EFContext();
        }

        public abstract Task<bool> Run();
    }
}