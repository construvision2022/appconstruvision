﻿using Domain;
using Domain.EmailContext;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace Construvision.ScriptServices
{
    public class GeneratePeriodExcel : BaseScriptService
    {
        public GeneratePeriodExcel()
        {

        }

        bool firstHeaders = false;
        int currentRow = 0;
        ExcelRange headersGlobalRange;

        LayoutModel GetLayoutModel(Tennant tenant, Business business, Settlement settlement, EFContext context = null)
        {
            if (settlement == null) throw new ArgumentNullException("settlement");
            if (business == null) throw new ArgumentNullException("settlement.Business");
            if (tenant == null) throw new ArgumentNullException("settlement.Tenant");

            if (context == null)
                context = new EFContext();

            var output = new LayoutModel
            {
                BusinessSenderEmail = tenant.SenderEmail,
                BusinessName = business.Name,
                BusinessAddress = business.Address,
                Logo = tenant.Logo,
                Banner = business.Banner,
                InvitationBanner = business.InvitationBanner,
                PlatformLink = tenant.PlatformLink,
                BusinessAndroidLink = tenant.AndroidLink,
                BusinessiOSLink = tenant.IOSLink,
                HiddenBanner = !string.IsNullOrWhiteSpace(business.Banner) ? "inline-block" : "none",
                BusinessColor = tenant.Color,
                ImageFolder = tenant.ImageFolder,
                EmailSenderCredential = tenant.EmailSenderCredential,
                EmailSenderPassword = tenant.EmailSenderPassword,


                SettlementContactName = "Construvision",
                SettlementContactEmail = "Construvision",
                SettlementMobilePhone = "Construvision",
                SettlementLocalPhone = "Construvision",
            };

            return output;
        }

        bool GenerateExcel(
            ExcelPackage excelPackage,
            List<Period> periods,
            List<Settlement> settlements,
            List<Lot> lots,
            List<PackageMembership> lotsPackages,
            List<PackageMembership> bankPackages,
            List<Model> models,
            List<Report> reports,
            List<ReportMembership> reportMemberships)
        {
            excelPackage.Workbook.Properties.Author = "Construvision";
            excelPackage.Workbook.Properties.Title = "Reporte avances al " + periods.FirstOrDefault().EndDate.ToConstruTime();
            excelPackage.Workbook.Properties.Comments = "Generado por sistema";
            //excelPackage.PrinterSettings.FitToPage = true;

            foreach (var period in periods)
            {
                //Crear pestaña con infromacion de cada residencial por periodo
                CreatePeriodWorksheet(excelPackage, period, settlements, lots, lotsPackages, bankPackages, models, reports, reportMemberships);
            }

            /* ----- SAVE EXCEL ----- */
            excelPackage.Save();

            return true;
        }

        void CreatePeriodWorksheet(
            ExcelPackage excelPackage,
            Period period,
            List<Settlement> settlements,
            List<Lot> lots,
            List<PackageMembership> lotsPackages,
            List<PackageMembership> bankPackages,
            List<Model> models,
            List<Report> reports,
            List<ReportMembership> reportMemberships)
        {
            var excelWorkSheet = excelPackage.Workbook.Worksheets.Add(period.EndDate.ToConstruTime());
            excelWorkSheet.View.ShowGridLines = true;
            excelWorkSheet.View.ShowHeaders = true;

            var allCells = excelWorkSheet.Cells;
            allCells.Style.Font.Name = "Century Gothic";
            allCells.Style.WrapText = true;
            allCells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            var startingColumn = 3;
            currentRow = 2;

            // Crear y llenar cuadro con informacion de residencial
            FillHeaderInfo(excelWorkSheet, startingColumn);

            foreach (var settlement in settlements)
            {
                var settlementLots = lots
                    .Where(x => x.SettlementId == settlement.SettlementId)
                    .OrderBy(x => x.Zone.Name.Length)
                    .ThenBy(x => x.Zone.Name)
                    .ThenBy(x => x.Block.Name.Length)
                    .ThenBy(x => x.Block.Name)
                    .ThenBy(x => x.LotNumber.Length)
                    .ThenBy(x => x.LotNumber)
                    .ToList();

                var settlementLotPackages = lotsPackages.Where(x => x.Lot.SettlementId == settlement.SettlementId).ToList();
                var settlementBankPackages = lotsPackages.Where(x => x.Lot.SettlementId == settlement.SettlementId).ToList();

                //Llenar ponderaciones de 2 plantas
                var twoFloorsModel = models
                    .Where(x =>
                        x.SettlementId == settlement.SettlementId &&
                        x.Floors == 2)
                    .FirstOrDefault();

                var currentColumn = startingColumn + 1;
                if (twoFloorsModel != null)
                {
                    var twoFloorsLots = settlementLots.Where(x => x.Model.Floors == 2).ToList();
                    FillLots(currentColumn, excelWorkSheet, twoFloorsModel, twoFloorsLots, reports, settlementLotPackages, settlementBankPackages, reportMemberships, period);
                }

                //Llenar ponderaciones de 1 planta
                var oneFloorModel = models
                    .Where(x =>
                        x.SettlementId == settlement.SettlementId &&
                        x.Floors == 1)
                    .FirstOrDefault();

                currentColumn = startingColumn + 1;
                if (oneFloorModel != null)
                {
                    var oneFloorLots = settlementLots.Where(x => x.Model.Floors == 1).ToList();
                    FillLots(currentColumn, excelWorkSheet, oneFloorModel, oneFloorLots, reports, settlementLotPackages, settlementBankPackages, reportMemberships, period);
                }
            }

            headersGlobalRange.AutoFilter = true;


            //Dar estilo a pestaña
            allCells.AutoFitColumns();
            excelWorkSheet.Column(1).Width = 2;   //Row mini
            excelWorkSheet.Column(2).Width = 6;   //Consecutivo
            excelWorkSheet.Column(3).Width = 30;  //Fracc
            excelWorkSheet.Column(4).Width = 22;  //Zona
            excelWorkSheet.Column(5).Width = 8;   //Manzana
            excelWorkSheet.Column(6).Width = 6;   //# lote
            excelWorkSheet.Column(7).Width = 22;  //Paquete ruv
            excelWorkSheet.Column(8).Width = 22;  //Paquete bank
            excelWorkSheet.Column(9).Width = 26;  //Modelo
            excelWorkSheet.Column(10).Width = 6;  //Numero viviendas
            excelWorkSheet.Column(11).Width = 4;  //Tipo?
            excelWorkSheet.Column(12).Width = 12; //Avance

            excelWorkSheet.Column(35).Width = 12; //Fecha
            excelWorkSheet.Column(36).Width = 12; //Fecha
            excelWorkSheet.Column(37).Width = 12; //Fecha

            excelWorkSheet.View.ZoomScale = 70;
        }

        void FillHeaderInfo(ExcelWorksheet excelWorkSheet, int startingColumn)
        {
            //CREAR CUADRO DE INFORMACION DE RESIDENCIAL
            var rowHeadersRange = excelWorkSheet.Cells["D" + currentRow + ":AE" + (currentRow + 5)];
            rowHeadersRange.Style.Font.Bold = true;
            rowHeadersRange.Style.Border.BorderAround(ExcelBorderStyle.Double, Color.Black);
            rowHeadersRange.Style.WrapText = false;

            //Nombre constructora.
            excelWorkSheet.Cells["D" + (currentRow + 2) + ":AE" + (currentRow + 2)].Merge = true;
            var buildingBusinessNameCell = excelWorkSheet.Cells[(currentRow + 2), (startingColumn + 1)];
            buildingBusinessNameCell.Value = "REPORTE DE AVANCE - CONSTRUVISION S.A. DE C.V.";
            buildingBusinessNameCell.Style.WrapText = false;

            //Nombre de fracc.
            excelWorkSheet.Cells["D" + (currentRow + 3) + ":AE" + (currentRow + 3)].Merge = true;
            var settlementNameCell = excelWorkSheet.Cells[(currentRow + 3), (startingColumn + 1)];
            settlementNameCell.Value = TimeZoner.Now.ToLongDateString();
            settlementNameCell.Style.WrapText = false;

            //ESQUEMA DE CRÉDITO: FINANCIAMIENTO A PROMOTORES DE VIVIENDA MEDIA RESIDENCIAL
            excelWorkSheet.Cells["D" + (currentRow + 4) + ":AE" + (currentRow + 4)].Merge = true;
            var legendCell = excelWorkSheet.Cells[(currentRow + 4), (startingColumn + 1)];
            legendCell.Value = "ESQUEMA DE CRÉDITO: FINANCIAMIENTO A PROMOTORES DE VIVIENDA MEDIA RESIDENCIAL";
            legendCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            legendCell.Style.WrapText = false;

            //Hermosillo
            excelWorkSheet.Cells["D" + (currentRow + 5) + ":AE" + (currentRow + 5)].Merge = true;
            var locationCell = excelWorkSheet.Cells[(currentRow + 5), (startingColumn + 1)];
            locationCell.Value = "HERMOSILLO, SONORA";
            locationCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            locationCell.Style.WrapText = false;

            currentRow += 6;
        }

        void FillLots(
            int currentColumn,
            ExcelWorksheet excelWorkSheet,
            Model model,
            List<Lot> lots,
            List<Report> reports,
            List<PackageMembership> settlementLotPackages,
            List<PackageMembership> settlementBankPackages,
            List<ReportMembership> reportMemberships,
            Period period)
        {
            var percentageFormating = "0.00";
            //var percentageFormating = "0.00\\%";
            var buildingConcepts = model.BuildingConcepts.OrderBy(x => x.Order);
            var buildingConceptsCount = model.BuildingConcepts.Count();
            var numberOfHeaders = buildingConceptsCount + 14;
            var excelColumn = ExcelColumn(numberOfHeaders);

            var range = $"B{currentRow}:{excelColumn}{currentRow}";
            var headersRange = excelWorkSheet.Cells[range];
            headersRange.Style.Font.Bold = true;
            headersRange.Style.Font.Size = 10;
            headersRange.Style.TextRotation = 90;
            headersRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
            headersRange.Style.Border.Left.Style = ExcelBorderStyle.Medium;
            headersRange.Style.Border.Right.Style = ExcelBorderStyle.Medium;
            headersRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;

            if (firstHeaders == false)
            {
                firstHeaders = true;
                headersGlobalRange = headersRange;
            }

            excelWorkSheet.Cells[(currentRow), currentColumn - 2].Value = "LISTA TOTAL";
            excelWorkSheet.Cells[(currentRow), currentColumn - 1].Value = "FRACC";
            excelWorkSheet.Cells[(currentRow), currentColumn + 0].Value = "ZONA";
            excelWorkSheet.Cells[(currentRow), currentColumn + 1].Value = "MANZANA";
            excelWorkSheet.Cells[(currentRow), currentColumn + 2].Value = "LOTE";
            excelWorkSheet.Cells[(currentRow), currentColumn + 3].Value = "PAQUETE RUV";
            excelWorkSheet.Cells[(currentRow), currentColumn + 4].Value = "PAQUETE BANCARIO";
            excelWorkSheet.Cells[(currentRow), currentColumn + 5].Value = "MODELO";
            excelWorkSheet.Cells[(currentRow), currentColumn + 6].Value = "NÚMERO DE VIVIENDAS";
            excelWorkSheet.Cells[(currentRow), currentColumn + 7].Value = "TIPO";
            excelWorkSheet.Cells[(currentRow), currentColumn + 8].Value = "AVANCE";

            foreach (var buildingConcept in buildingConcepts)
            {
                var headerCell = excelWorkSheet.Cells[currentRow, currentColumn + 9];
                headerCell.Value = buildingConcept.Name;
                excelWorkSheet.Row(currentRow).Height = 80;

                var basePercentageCell = excelWorkSheet.Cells[(currentRow + 1), currentColumn + 9];
                basePercentageCell.Value = buildingConcept.Percentage * 100;
                basePercentageCell.Style.Font.Bold = true;
                basePercentageCell.Style.Font.Size = 10;
                basePercentageCell.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                basePercentageCell.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                basePercentageCell.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                basePercentageCell.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                basePercentageCell.Style.Numberformat.Format = percentageFormating;

                currentColumn += 1;
            }

            //Agrega header de inicio de construccion y fecha de termino al final de conceptos.
            var buildintStartDateCell = excelWorkSheet.Cells[currentRow, currentColumn + 9];
            buildintStartDateCell.Value = "FECHA DE INICIO";
            //buildintStartDateCell.Style.TextRotation = 0;

            currentColumn += 1;

            var buildintEndDateCell = excelWorkSheet.Cells[currentRow, currentColumn + 9];
            buildintEndDateCell.Value = "FECHA DE TERMINACIÓN";
            buildintStartDateCell.Style.TextRotation = 0;

            currentRow += 2;
            var i = 1;

            foreach (var lot in lots)
            {
                var row = excelWorkSheet.Row(currentRow);
                row.Height = 15;

                var lotRange = excelWorkSheet.Cells[$"B{currentRow}:{excelColumn}{currentRow}"];
                lotRange.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                lotRange.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                lotRange.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                lotRange.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                var lotPackage = settlementLotPackages.SingleOrDefault(x => x.LotId == lot.LotId)?.Package.Name;
                var bankPackage = settlementBankPackages.SingleOrDefault(x => x.LotId == lot.LotId)?.Package.Name;
                var lotPercentage = 0.00;

                excelWorkSheet.Cells[currentRow, 2].Value = i;
                excelWorkSheet.Cells[currentRow, 3].Value = lot.Settlement.Name;
                excelWorkSheet.Cells[currentRow, 4].Value = lot.Zone.Alias;
                excelWorkSheet.Cells[currentRow, 5].Value = lot.Block.Name;
                excelWorkSheet.Cells[currentRow, 6].Value = lot.LotNumber;
                excelWorkSheet.Cells[currentRow, 7].Value = lotPackage ?? "SIN PAQUETE";
                excelWorkSheet.Cells[currentRow, 8].Value = bankPackage ?? "SIN PAQUETE";
                excelWorkSheet.Cells[currentRow, 9].Value = lot.Model.Name;
                excelWorkSheet.Cells[currentRow, 10].Value = i;
                excelWorkSheet.Cells[currentRow, 11].Value = 42;
                var lotPercentageCell = excelWorkSheet.Cells[currentRow, 12];
                lotPercentageCell.Style.Font.Bold = true;
                lotPercentageCell.Style.Numberformat.Format = percentageFormating;

                currentColumn = 13;

                //Calculos
                var lotReport = reports.SingleOrDefault(x =>
                    x.LotId == lot.LotId &&
                    x.PeriodId == period.PeriodId);

                //Si no hay reporte en periodo
                if (lotReport == null)
                {
                    //Buscar si tiene algun reporte pasado y obtener el ultimo
                    lotReport = reports.Where(x =>
                        x.LotId == lot.LotId &&
                        x.CreationDate < period.StartDate)
                    .FirstOrDefault();

                    if (lotReport != null)
                    {
                        var memberships = reportMemberships.Where(x => x.ReportId == lotReport.ReportId).OrderBy(x => x.BuildingConcept.Order);
                        foreach (var membership in memberships)
                        {
                            if (membership.Percentage > 0)
                            {
                                lotPercentage += membership.LotPercentage;

                                excelWorkSheet.Cells[currentRow, currentColumn].Value = membership.Percentage;
                                excelWorkSheet.Cells[currentRow, currentColumn].Style.Font.Color.SetColor(Color.Red);
                                excelWorkSheet.Cells[currentRow, currentColumn].Style.Numberformat.Format = percentageFormating;
                            }

                            currentColumn++;
                        }
                    }
                }
                //Si hay reporte en periodo
                else
                {
                    var memberships = reportMemberships.Where(x => x.ReportId == lotReport.ReportId).OrderBy(x => x.BuildingConcept.Order);
                    foreach (var membership in memberships)
                    {
                        if (membership.Percentage > 0)
                        {
                            lotPercentage += membership.LotPercentage;

                            excelWorkSheet.Cells[currentRow, currentColumn].Value = membership.Percentage;
                            excelWorkSheet.Cells[currentRow, currentColumn].Style.Font.Color.SetColor(Color.Red);
                            excelWorkSheet.Cells[currentRow, currentColumn].Style.Numberformat.Format = percentageFormating;
                        }

                        currentColumn++;
                    }
                }

                //LLENAR PORCENTAJE DE LOTE
                lotPercentage = lotPercentage > 100 ? 100 : lotPercentage;
                lotPercentageCell.Value = lotPercentage;

                //FECHA DE INICIO DE OBRA SOLO SI ES MENOR AL PERIODO ACTUAL Y PORCENTAJE DE LOTE ES DIFERENTE DE 0 EN ESE PERIODO
                if (lot.BuildingStartDate.HasValue && lot.BuildingStartDate.Value <= period.EndDate && lotPercentage != 0)
                {
                    excelWorkSheet.Cells[currentRow, currentColumn].Value = lot.BuildingStartDate.Value.ToConstruTime2();
                }

                //FECHA DE TERMINO DE OBRA SOLO SI ES MENOR AL PERIODO ACTUAL Y PORCENTAJE DE LOTE ES 100% EN ESE PERIODO.
                if (lot.BuildingEndDate.HasValue && lot.BuildingEndDate.Value <= period.EndDate && lotPercentage == 100)
                {
                    excelWorkSheet.Cells[currentRow, (currentColumn + 1)].Value = lot.BuildingEndDate.Value.ToConstruTime2();
                }

                i++;
                currentRow++;
            }

            //currentRow += 4;
        }

        string ExcelColumn(int column)
        {
            string columnString = "";
            decimal columnNumber = column;
            while (columnNumber > 0)
            {
                decimal currentLetterNumber = (columnNumber - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                columnString = currentLetter + columnString;
                columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
            }
            return columnString;
        }

        public override async Task<bool> Run()
        {
            var excelReportIds = await Context.ExcelExports
                .Where(x => !x.EndDate.HasValue)
                .Select(x => x.ExcelExportId)
                .ToListAsync();

            Context = null;

            foreach (var excelReportId in excelReportIds)
            {
                try
                {
                    var context = new EFContext();
                    var emailer = new Emailer();

                    /* ----- LOAD DATA ----- */
                    var excel = await context.ExcelExports
                        .Where(x => !x.EndDate.HasValue)
                        .SingleOrDefaultAsync(x => x.ExcelExportId == excelReportId);

                    if (excel == null)
                        continue;

                    var business = await context.Businesses
                        .Include(x => x.Tennant)
                        .SingleOrDefaultAsync(x => x.BusinessId == excel.BusinessId);

                    var periods = await context.Periods
                        .Where(x =>
                            x.StartDate >= excel.RangeStartDate &&
                            x.EndDate <= excel.RangeEndDate)
                        .ToListAsync();

                    if (periods.Count == 0)
                    {
                        Logger.Log("La solicitud de excel export con ID " + excel.ExcelExportId + " no produce ningun periodo en su rango.");
                        continue;
                    }

                    var settlements = await context.Settlements
                        .Where(x => x.BusinessId == business.BusinessId)
                        .ToListAsync();

                    var defaultSettlement = settlements.First();

                    await context.Zones
                        .LoadAsync();

                    await context.Blocks
                        .LoadAsync();

                    var lots = await context.Lots
                        .ToListAsync();

                    await context.Packages
                        .LoadAsync();

                    var ruvLotsPackages = await context.PackageMemberships
                        .Where(x => x.Package.Institution.Name == "RUV")
                        .ToListAsync();

                    var banklotsPackages = await context.PackageMemberships
                        .Where(x => x.Package.Institution.Name == "Bancario")
                        .ToListAsync();

                    var models = await context.Models
                        .ToListAsync();

                    var buildingConcepts = await context.BuildingConcepts
                        .ToListAsync();

                    var reports = await context.Reports
                        .OrderByDescending(x => x.CreationDate)
                        .ToListAsync();

                    var reportMemberships = await context.ReportMemberships
                        .ToListAsync();

                    /* ----- FILE TEMPLATE ----- */
                    var fileName = Guid.NewGuid().ToString("N") + ".xlsx";
                    var basePath = HostingEnvironment.MapPath("~/content/files/exports/");
                    var filePath = Path.Combine(basePath, fileName);
                    var newFile = new FileInfo(filePath);
                    var excelPackage = new ExcelPackage(newFile);

                    var excelCreated = GenerateExcel(
                        excelPackage,
                        periods,
                        settlements,
                        lots,
                        ruvLotsPackages,
                        banklotsPackages,
                        models,
                        reports,
                        reportMemberships);

                    if (!excelCreated)
                        throw new InvalidDataException();

                    excel.End(fileName);

                    var model = new ExcelExportModel()
                    {
                        StartDate = excel.RangeStartDate.ToConstruTime2(),
                        EndDate = excel.RangeEndDate.ToConstruTime2(),
                        FileName = excel.FileName
                    };

                    var emails = new List<Email>();

                    var layoutData = GetLayoutModel(business.Tennant, business, defaultSettlement);

                    var layoutDataString = string.Empty;
                    await Task.Run(() => layoutDataString = Serializer.Serialize(layoutData));

                    foreach (var emailAddress in App.Instance.ExcelReportRecipients)
                    {
                        var emailContent = string.Empty;
                        await Task.Run(() => emailContent = Serializer.Serialize(model));

                        var email = new Email(
                                defaultSettlement,
                                emailAddress,
                                Guid.NewGuid().ToString("N"),
                                emailContent,
                                layoutDataString,
                                EmailTemplates.NewExcelExport,
                                TimeZoner.Now);

                        email.End();

                        emails.Add(email);
                    }

                    context.Emails.AddRange(emails);

                    await context.SaveChangesAsync();

                    foreach (var email in emails)
                    {
                        emailer.Send(email);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(ex, LogTypes.ChronJob_Excel);
                    continue;
                }
            }

            return true;
        }
    }
}