﻿using Domain;
using Domain.SettlementContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Construvision.ScriptServices
{
    public class PeriodCloser : BaseScriptService
    {
        ResyncPeriodsOutput Result = new ResyncPeriodsOutput();

        public override async Task<bool> Run()
        {
            var now = TimeZoner.Now;

            var tenant = await Context.Tenants
                .Include(x => x.Businesses)
                .Include(x => x.Periods)
                .FirstOrDefaultAsync();

            if (tenant == null)
                return false;

            tenant.CreatePeriods(now);

            var pastPeriod = tenant.GetPastPeriod();

            if (pastPeriod == null)
                return false;

            if (pastPeriod.SendDate.HasValue)
                return false;

            var settlementIds = new List<int>();

            settlementIds = await Context.Settlements
                .Select(x => x.SettlementId)
                .ToListAsync();

            try
            {
                App.Instance.Chronjobs.SetSkipping(true);

                foreach (var settlementId in settlementIds)
                {
                    using (var context = new EFContext())
                    {
                        try
                        {
                            var settlement = await context.Settlements
                                .Include(x => x.Business.Tennant)
                                .SingleAsync(x => x.SettlementId == settlementId);

                            if (!await Load(context, settlement))
                                continue;

                            var result = settlement.Resync(now);

                            Result.Merge(result);

                            await context.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            Logger.Log(ex);
                            continue;
                        }
                    }
                }

                pastPeriod.SetSendDate(TimeZoner.Now);

                foreach (var business in tenant.Businesses)
                {
                    new ExcelExport(
                        business,
                        pastPeriod.StartDate,
                        pastPeriod.EndDate);
                }

                await Context.SaveChangesAsync();

                return true;
            }
            catch (Exception bigEx)
            {
                Logger.Log(bigEx, LogTypes.ScriptService);
                return false;
            }
            finally
            {
                App.Instance.Chronjobs.SetSkipping(false);
            }
        }

        async Task<bool> Load(EFContext context, Settlement settlement)
        {
            var now = TimeZoner.Now;

            await context.Settlements
                .Include(x => x.Business.Tennant.Periods)
                .Include(x => x.Lots)
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.Zones
                .Include(x => x.Streets)
                .Include(x => x.Models)
                .Include(x => x.Institutions)
                .Include(x => x.Packages)
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.PackageMemberships
                .Where(x => x.Package.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.SettlementPeriods
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.ZonePeriods
                .Where(x => x.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.InstitutionPeriods
                .Where(x => x.Institution.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.PackagePeriods
                .Where(x => x.Package.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.Reports
                .Where(x => x.Lot.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.ReportMemberships
                .Where(x => x.Report.Lot.SettlementId == settlement.SettlementId)
                .LoadAsync();

            return true;
        }
    }
}