﻿using System;
using System.Threading.Tasks;

namespace Construvision.ScriptServices
{

    public class RunSomeSQL : BaseScriptService
    {
        public RunSomeSQL()
        {
        }

        public override async Task<bool> Run()
        {
            try
            {
                var commands = GetSQLCommands();

                await Context.Database.ExecuteSqlCommandAsync(commands);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return false;
            }
        }

        string GetSQLCommands()
        {
            return $@"
            ";
        }
    }
}