﻿using Domain;
using Domain.SettlementContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Construvision.ScriptServices
{
    public class SyncService : BaseScriptService
    {
        public SyncService()
        {
        }

        ResyncPeriodsOutput Result = new ResyncPeriodsOutput();

        public override async Task<bool> Run()
        {
            var settlementIds = new List<int>();

            settlementIds = await Context.Settlements
                .Select(x => x.SettlementId)
                .ToListAsync();

            Context = null;

            try
            {
                App.Instance.Chronjobs.SetSkipping(true);

                var now = TimeZoner.Now;

                foreach (var settlementId in settlementIds)
                {
                    using (var context = new EFContext())
                    {
                        try
                        {
                            var settlement = await context.Settlements
                                .Include(x => x.Business.Tennant)
                                .SingleAsync(x => x.SettlementId == settlementId);

                            if (!await context.LoadPeriodHistory(settlement))
                                continue;

                            var result = settlement.Resync(now);

                            Result.Merge(result);

                            await context.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            Logger.Log(ex);
                            continue;
                        }
                    }
                }

                return true;
            }
            catch (Exception bigEx)
            {
                Logger.Log(bigEx, LogTypes.ScriptService);
                return false;
            }
            finally
            {
                App.Instance.Chronjobs.SetSkipping(false);
            }
        }
    }
}
