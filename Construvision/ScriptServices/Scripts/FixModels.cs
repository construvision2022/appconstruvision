﻿using Domain;
using Domain.EmailContext;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace Construvision.ScriptServices
{
    public class FixModels : BaseScriptService
    {
        string Error;

        public FixModels()
        {
        }

        public override async Task<bool> Run()
        {
            var abedul = await Fix("abedul.csv", 33);

            if (!abedul)
            {
                var abedulError = Error;
            }

            var claveles = await Fix("claveles.csv", 34);

            if (!claveles)
            {
                var clavelesError = Error;
            }

            return string.IsNullOrWhiteSpace(Error);
        }

        public async Task<bool> Fix(string fileName, int zoneId)
        {
            var lines = await ReadFile(fileName, zoneId);

            if (lines == null)
                return false;

            using (var context = new EFContext())
            {
                var zone = await LoadZone(context, zoneId);

                if (zone == null)
                    return false;

                var now = TimeZoner.Now;

                var thisMonth = new DateTime(now.Year, now.Month, 1);

                var oldModels = new List<Model>();

                try
                {
                    var counter = -1;

                    foreach (var line in lines)
                    {
                        counter++;

                        if (counter == 0)
                            continue;

                        var values = line.Split(',').ToList();

                        var registry = new RegistryData(values);

                        var lot = zone.Lots
                            .SingleOrDefault(x => (x.Block.Name + " L-" + x.LotNumber) == registry.BlockAddress);

                        if (lot == null)
                        {
                            Error = $"No se encontro el lote con la direccion de manzana  {registry.BlockAddress}";
                            return false;
                        }

                        if (2507 == lot.LotId)
                        {

                        }

                        var blockAddress = lot.Block.Name + " L-" + lot.LotNumber;
                        if (registry.BlockAddress != blockAddress)
                        {
                            Error = $"Lote con ID {registry.LotId} tiene una direccion de manzana diferente";
                            return false;
                        }

                        var oldModel = zone.Models
                            .SingleOrDefault(x => x.Name.Equals(registry.OldModel, StringComparison.OrdinalIgnoreCase));

                        if (oldModel == null)
                        {
                            Error = $"No se encontro el modelo {registry.OldModel} en la cerrada {zone.Name}";
                            return false;
                        }

                        var newModel = zone.Models
                            .SingleOrDefault(x => x.Name.Equals(registry.NewModel, StringComparison.OrdinalIgnoreCase));

                        if (newModel == null)
                        {
                            newModel = new Model(
                                zone,
                                registry.NewModel,
                                oldModel.Color,
                                oldModel.Area,
                                oldModel.GroundArea,
                                oldModel.Floors);

                            foreach (var buildingConcept in oldModel.BuildingConcepts)
                            {
                                new BuildingConcept(
                                    newModel,
                                    buildingConcept.Name,
                                    buildingConcept.Percentage,
                                    buildingConcept.Order,
                                    buildingConcept.CriticalRoute);
                            }

                            await context.SaveChangesAsync();
                        }

                        if (oldModel != newModel)
                        {
                            if (registry.BlockAddress == "M-970 L-5")
                            {

                            }

                            var reports = lot.Reports
                                .OrderBy(x => x.Folio)
                                .ToList();

                            lot.UpdateModel(newModel);

                            if (!reports.Any())
                                continue;

                            if (newModel.BuildingConcepts.Count != oldModel.BuildingConcepts.Count)
                            {
                                Error = $"modelo {oldModel.Name} tiene diferentes conceptos de construccion que modelo {newModel.Name}";
                                return false;
                            }

                            foreach (var oldBuildingConcept in oldModel.BuildingConcepts)
                            {
                                var newBuildingConcept = newModel.BuildingConcepts
                                    .SingleOrDefault(x => x.Name.Equals(oldBuildingConcept.Name, StringComparison.OrdinalIgnoreCase));

                                if (newBuildingConcept == null)
                                {
                                    Error = $"Concepto de construcción {oldBuildingConcept.Name} no encontrado en modelo {newModel.Name}";
                                    return false;
                                }

                                if (newBuildingConcept.Order != oldBuildingConcept.Order)
                                {
                                    Error = $"Conceptos de construcción {newBuildingConcept.Name} tienen orden diferente.";
                                    return false;
                                }

                                if (newBuildingConcept.CriticalRoute != oldBuildingConcept.CriticalRoute)
                                {
                                    Error = $"Conceptos de construcción {newBuildingConcept.Name} tienen ruta critica diferente.";
                                    return false;
                                }
                            }

                            foreach (var report in reports)
                            {
                                foreach (var membership in report.ReportMemberships)
                                {
                                    var newBuildingConcept = newModel.BuildingConcepts
                                        .SingleOrDefault(x => x.Name.Equals(membership.BuildingConcept.Name, StringComparison.OrdinalIgnoreCase));

                                    membership.UpdateBuildingConcept(newBuildingConcept);
                                }
                            }
                        }

                        var newStreet = zone.Streets
                            .SingleOrDefault(x => x.Name.Equals(registry.NewStreetName, StringComparison.OrdinalIgnoreCase));

                        if (newStreet == null)
                            newStreet = new Street(zone.Settlement, zone, registry.NewStreetName);

                        if (newStreet != lot.Street)
                        {
                            lot.UpdateStreet(newStreet);
                            lot.UpdateOfficialNumber(registry.NewOfficialNumber);
                        }
                    }

                    oldModels = oldModels.Distinct().ToList();

                    foreach (var oldModel in oldModels)
                        context.BuildingConcepts.RemoveRange(oldModel.BuildingConcepts);

                    context.Models.RemoveRange(oldModels);

                    await context.SaveChangesAsync();

                    return true;
                }
                catch (Exception ex)
                {
                    Error = ex.ToString();
                    return false;
                }
            }
        }

        async Task<Zone> LoadZone(EFContext context, int zoneId)
        {
            var zone = await context.Zones
                .Include(x => x.Settlement.Business)
                .SingleOrDefaultAsync(x => x.ZoneId == zoneId);

            if (zone == null)
            {
                Error = $"<h1>Cerrada ID: {zoneId} no encontrada</h1>";
                return null;
            }

            await context.Tenants
                .Include(x => x.Periods)
                .Where(x => x.TennantId == zone.Settlement.Business.TennantId)
                .LoadAsync();

            await context.Blocks
                .Where(x => x.ZoneId == zone.ZoneId)
                .LoadAsync();

            await context.Models
                .Where(x => x.ZoneId == zone.ZoneId)
                .LoadAsync();

            await context.Streets
                .Where(x => x.ZoneId == zone.ZoneId)
                .LoadAsync();

            await context.Lots
                .Where(x => x.ZoneId == zone.ZoneId)
                .LoadAsync();

            await context.BuildingConcepts
                .Where(x => x.Model.ZoneId == zone.ZoneId)
                .LoadAsync();

            await context.Reports
                .Where(x => x.Lot.ZoneId == zone.ZoneId)
                .LoadAsync();

            await context.ReportMemberships
                .Where(x => x.Report.Lot.ZoneId == zone.ZoneId)
                .LoadAsync();

            return zone;
        }

        async Task<string[]> ReadFile(string fileName, int zoneId)
        {
            var path = HostingEnvironment.MapPath("~/" + fileName);

            if (!File.Exists(path))
            {
                Error = "Archivo no encontrado: " + path;
                return null;
            }

            var data = new string[4];
            await Task.Run(() => data = File.ReadAllLines(path));

            if (data == null)
            {
                Error = "FixModels: Data = null";
                return null;
            }

            if (data.Length == 0)
            {
                Error = "FixModels: Data.length = 0";
                return null;
            }

            return data;
        }

        class RegistryData
        {
            public int LotId;

            public string BlockAddress;
            public string NewAddress;
            public string OldAddress;
            public string NewModel;
            public string OldModel;

            public string NewStreetName;
            public string NewOfficialNumber;

            public RegistryData(List<string> values)
            {
                BlockAddress = values[0];
                LotId = int.Parse(values[1]);
                NewAddress = values[2];
                OldAddress = values[3];
                NewModel = values[4];
                OldModel = values[5];

                var split = NewAddress.Split(' ');

                NewStreetName = string.Empty;

                foreach (var item in split)
                {
                    if (item.StartsWith("#"))
                    {
                        NewOfficialNumber = item.Replace("#", "");
                    }
                    else
                    {
                        NewStreetName += item + " ";
                    }
                }

                NewStreetName = NewStreetName.Trim();
            }
        }
    }
}