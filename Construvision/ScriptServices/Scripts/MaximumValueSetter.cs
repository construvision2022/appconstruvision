﻿using Domain;
using Domain.SettlementContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Construvision.ScriptServices
{
    public class MaximumValueSetter : BaseScriptService
    {
        public async override Task<bool> Run()
        {
            try
            {
                var now = TimeZoner.Now;

                var settlementIds = await Context.Settlements
                    .Select(x => x.SettlementId)
                    .ToListAsync();

                //Context = null;

                foreach (var settlementId in settlementIds)
                {
                    try
                    {
                        var context = new EFContext();

                        var settlement = await context.Settlements
                            .SingleAsync(x => x.SettlementId == settlementId);

                        if (!await Load(context, settlement))
                            continue;

                        if (!settlement.Lots.Any())
                            continue;

                        settlement.Lots
                            .ForEach(x => x.Model.SetMaxPercentage());

                        await context.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex, LogTypes.Service);
                        continue;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogTypes.Service);
                return false;
            }
        }


        static async Task<bool> Load(EFContext context, Settlement settlement)
        {
            var now = TimeZoner.Now;

            await context.Settlements
                .Include(x => x.Business.Tennant)
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.Lots
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.Streets
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.Models
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.Zones
                .Where(x => x.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.Institutions
                .Where(x => x.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.Packages
                .Include(x => x.PackageMemberships)
                .Where(x => x.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            await context.BuildingConcepts
                .Where(x => x.Model.Zone.SettlementId == settlement.SettlementId)
                .LoadAsync();

            return true;
        }
    }
}