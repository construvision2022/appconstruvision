﻿using Backend.Domain;
using Domain;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Construvision.ScriptServices
{
    public class InitialCreate : BaseScriptService
    {
        public InitialCreate()
        {
        }

        public override async Task<bool> Run()
        {
            try
            {
                if (await Context.Tenants.AnyAsync())
                    return true;

                var tenant = Creator.CreateDomain(TimeZoner.Now);

                Context.Tenants.Add(tenant);

                await Context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return false;
            }
        }
    }
}