﻿using Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace Construvision.ChronJobs
{
    public class Context
    {
        public static string CacheHeartBeatKey = "Cache_HeartBeat";
        static void CacheItemRemoved(string key, object value, CacheItemRemovedReason reason)
        {
            if (key == CacheHeartBeatKey)
            {
                App.Instance.Chronjobs.AddCacheHeartBeatKey(5);

                if (App.Instance.Chronjobs.IsSkipping)
                    return;

                foreach (var item in App.Instance.Chronjobs.Jobs)
                {
                    Task.Run(() => { item.Ping(); });
                }
            }
        }

        public Context(List<BaseChronJob> jobs)
        {
            Jobs = jobs;
            AddCacheHeartBeatKey(5);
        }

        public bool IsSkipping { private set; get; }
        public List<BaseChronJob> Jobs { private set; get; }

        void AddCacheHeartBeatKey(int duration)
        {
            if (HttpRuntime.Cache[CacheHeartBeatKey] == null)
            {
                HttpRuntime.Cache.Add(
                    CacheHeartBeatKey,
                    TimeZoner.Now.AddSeconds(duration),
                    null,
                    TimeZoner.Now.AddSeconds(duration),
                    Cache.NoSlidingExpiration,
                    CacheItemPriority.NotRemovable,
                    CacheItemRemoved);
            }
        }
        public void SetSkipping(bool state)
        {
            IsSkipping = state;
        }
    }
}