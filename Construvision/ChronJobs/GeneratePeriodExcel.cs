﻿using Domain;
using Domain.EmailContext;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace Construvision.ChronJobs
{
    public class GeneratePeriodExcel : BaseChronJob
    {
        public GeneratePeriodExcel(int wait) : base("GeneratePeriodExcel", wait)
        {

        }

        public override async Task<bool> Job()
        {
            var service = new ScriptServices.GeneratePeriodExcel();
            return await service.Run();
        }
    }
}