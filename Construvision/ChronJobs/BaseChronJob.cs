﻿using Domain;
using Domain.EmailContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Construvision.ChronJobs
{
    public abstract class BaseChronJob
    {
        public const int __initialLoadingCycles = 3;

        public string Name { private set; get; }
        public int Counter { private set; get; }
        public int Wait { private set; get; }
        public int Timer { private set; get; }

        public bool Working { private set; get; }

        public DateTime Last { private set; get; }
        public DateTime Start { private set; get; }

        public List<string> Log { private set; get; }




        public BaseChronJob(string name, int wait)
        {
            if (wait < 1) wait = 1;

            var now = TimeZoner.Now;

            Name = name;
            Counter = 0;
            Start = Last = now;
            Wait = wait;

            Timer = __initialLoadingCycles;

            Log = new List<string>();
        }
        public async void Ping()
        {
            if (Working)
            {
                Log.Add("Working...");
            }
            else if (Timer > 0)
            {
                Timer--;
            }
            else
            {
                Working = true;

                await Job();

                Last = TimeZoner.Now;

                Timer = Wait;

                Counter++;

                Working = false;
            }
        }

        public abstract Task<bool> Job();

        protected LayoutModel GetLayoutModel(Tennant tenant, Business business, Settlement settlement, EFContext dbContext = null)
        {
            if (settlement == null) throw new ArgumentNullException("settlement");
            if (business == null) throw new ArgumentNullException("settlement.Business");
            if (tenant == null) throw new ArgumentNullException("settlement.Tenant");

            if (dbContext == null)
                dbContext = new EFContext();

            var output = new LayoutModel
            {
                BusinessSenderEmail = tenant.SenderEmail,
                BusinessName = business.Name,
                BusinessAddress = business.Address,
                Logo = tenant.Logo,
                Banner = business.Banner,
                InvitationBanner = business.InvitationBanner,
                PlatformLink = tenant.PlatformLink,
                BusinessAndroidLink = tenant.AndroidLink,
                BusinessiOSLink = tenant.IOSLink,
                HiddenBanner = !string.IsNullOrWhiteSpace(business.Banner) ? "inline-block" : "none",
                BusinessColor = tenant.Color,
                ImageFolder = tenant.ImageFolder,
                EmailSenderCredential = tenant.EmailSenderCredential,
                EmailSenderPassword = tenant.EmailSenderPassword,


                SettlementContactName = "Construvision",
                SettlementContactEmail = "Construvision",
                SettlementMobilePhone = "Construvision",
                SettlementLocalPhone = "Construvision"
            };

            return output;
        }
    }
}