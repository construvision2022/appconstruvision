﻿using Domain;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Construvision.ChronJobs
{
    public class Pinger : BaseChronJob
    {
        static HttpClient HttpClient = new HttpClient();

        public Pinger(int wait) : base("Pinger", wait)
        {

        }

        public override async Task<bool> Job()
        {
            if (WebConfigData.Environment == "Debug")
                return false;

            try
            {
                await HttpClient.GetAsync(WebConfigData.BaseUrl);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogTypes.ChronJob_Pinger);
                return false;
            }
        }
    }
}