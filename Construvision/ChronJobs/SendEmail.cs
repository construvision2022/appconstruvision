﻿using Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace Construvision.ChronJobs
{
    public class SendEmail : BaseChronJob
    {
        public SendEmail(int wait) : base("SendEmail", wait)
        {

        }

        public override async Task<bool> Job()
        {
            var emailBag = new List<Email>();

            using (var context = new EFContext())
            {
                try
                {
                    var settlements = await context.Settlements
                        .Include(x => x.Business)
                        .ToListAsync();

                    foreach (var settlement in settlements)
                    {
                        var emails = await context.Emails
                            .Include(x => x.User)
                            .Where(x =>
                                x.SettlementId == settlement.SettlementId &&
                                !x.EndDate.HasValue &&
                                x.Schedule < TimeZoner.Now)
                            .ToListAsync();

                        if (emails.Count == 0)
                            continue;

                        emailBag.AddRange(emails);

                        foreach (var email in emails)
                        {
                            email.End();
                        }

                        await context.SaveChangesAsync();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(ex, LogTypes.ChronJob_SendEmail);

                    return false;
                }
            }

            try
            {
                var emailer = new Emailer();

                foreach (var email in emailBag)
                {
                    if (email.User != null && email.User.IsBadEmail)
                        continue;

                    emailer.Send(email);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogTypes.ChronJob_SendEmail);
                return false;
            }

            return true;
        }
    }
}