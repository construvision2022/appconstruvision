﻿using System.Threading.Tasks;

namespace Construvision.ChronJobs
{
    public class PeriodCloser : BaseChronJob
    {
        public PeriodCloser(int wait) : base("PeriodCloser", wait)
        {

        }

        public async override Task<bool> Job()
        {
            var script = new ScriptServices.PeriodCloser();
            return await script.Run();
        }
    }
}