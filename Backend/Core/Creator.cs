﻿using Domain;
using Domain.LotContext;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Backend.Domain
{
    public static class Creator
    {
        public static Tennant CreateDomain(DateTime now)
        {
            var startOfYear = new DateTime(now.Year, 1, 1);

            var pastYear = startOfYear.AddYears(-1);

            var tenant = new Tennant("TestTenant", pastYear);

            tenant.CreatePeriods(now);

            var business = new Business(
                tenant, "TestBusiness", "Test Business", "Just a test business",
                "TSTBSNSS", "6621467913", "example.com", now);

            business.UpdateAddress("Calle #123");

            var managerUser = new User(
                business, "Manager", "Yop", "Mail",
                "manager@yopmail.com", "6623456789", "6621423159", null);

            new Manager(managerUser, business);

            var adminUser = new User(
                business, "Admin", "Yop", "Mail",
                "admin@yopmail.com", "6622519846", "6622154329", null);

            var salesAdminUser = new User(
                business, "SalesAdmin", "Yop", "Mail",
                "salesadmin@yopmail.com", "6622529846", "6622153329", null);

            var supervisorUser = new User(
               business, "Super", "Yop", "Mail",
               "supervisor@yopmail.com", "6621452198", "6623561911", null);

            var settlement = new Settlement(
                business, adminUser,
                "TestSettlement", "TestSettlement", "Just a test settlement",
                "TSTSTTLMNT", "83100");

            new Admin(adminUser, settlement);
            new SalesAdmin(salesAdminUser, settlement);

            var zone = new Zone(settlement, "TestZone");

            var bancarioInstitution = zone.Institutions.Single(x => x.Name == "Bancario");
            var ruvInstitution = zone.Institutions.Single(x => x.Name == "RUV");

            new Supervisor(supervisorUser, zone);

            var map = new Map(settlement, "TestMap", 362, 442);

            new MapImage(map, "m5.00 406.64l0.00 -30.69l176.08 0.00l176.08 0.00l0.00 30.69l0.00 30.69l-176.08 0.00l-176.08 0.00l0.00 -30.69z");
            new MapImage(map, "m354.78 406.64l0.00 -28.31l-173.70 0.00l-173.70 0.00l0.00 28.31l0.00 28.31l173.70 0.00l173.70 0.00l0.00 -28.31z");
            new MapImage(map, "m8.44 296.17c0.00 -27.22 -0.05 -30.82 -0.40 -30.82c-0.35 0.00 -0.40 -3.35 -0.40 -28.57c0.00 -25.22 0.05 -28.57 0.40 -28.57c0.35 0.00 0.40 -2.93 0.40 -24.74c0.00 -21.81 -0.05 -24.74 -0.40 -24.74c-0.35 0.00 -0.40 -4.44 -0.40 -38.36c0.00 -33.93 0.05 -38.36 0.40 -38.36c0.35 0.00 0.40 -4.45 0.40 -38.50l0.00 -38.50l172.11 0.00l172.11 0.00l0.00 161.00l0.00 161.00l-172.11 0.00l-172.11 0.00l0.00 -30.82z");

            zone.AssignMap(map);

            var ruv10 = new Package(zone, ruvInstitution, "RUV 10");
            var ruv0 = new Package(zone, ruvInstitution, "No establecido");
            var bank10 = new Package(zone, bancarioInstitution, "Bancario 10");
            var bank0 = new Package(zone, bancarioInstitution, "No establecido");

            settlement.CreateSummaryPeriods();

            var street = new Street(settlement, zone, "TestStreet");

            var model = new Model(
                zone, "TestModel", "#F00",
                100, 100, 1);

            new BuildingConcept(model, "TestBuildingConcept", 1, 1, 1);

            var block = new Block(settlement, zone, "TestBlock", "#00F");

            Lot createLot(int lotNumber)
            {
                var lot = new Lot(
                settlement,
                street,
                zone,
                model,
                block,
                model.GroundArea,
                model.Area,
                lotNumber.ToString(),
                lotNumber.ToString(),
                Status.Pending,
                SaleStatus.Available);

                new PackageMembership(lot, ruv10);
                new PackageMembership(lot, bank10);

                if (LotImagesHash.ContainsKey(lot.LotNumber))
                {
                    var path = LotImagesHash[lot.LotNumber];
                    var mapImage = new MapImage(map, path);
                    mapImage.AssignLot(lot);
                }

                return lot;
            };

            for (int i = 1; i <= 10; i++)
                createLot(i);

            return tenant;
        }

        static Dictionary<string, string> LotImagesHash = new Dictionary<string, string>
        {
            { "1", "m180.15 44.69l0.00 -37.31l-84.67 0.00l-84.67 0.00l0.00 37.31l0.00 37.31l84.67 0.00l84.67 0.00l0.00 -37.31z" },
            { "2", "m350.28 44.69l0.00 -37.31l-83.48 0.00l-83.48 0.00l0.00 37.31l0.00 37.31l83.48 0.00l83.48 0.00l0.00 -37.31z" },
            { "3", "m180.15 120.36l0.00 -35.98l-84.67 0.00l-84.67 0.00l0.00 35.98l0.00 35.98l84.67 0.00l84.67 0.00l0.00 -35.98z" },
            { "4", "m350.02 120.36l0.00 -35.98l-83.34 0.00l-83.34 0.00l0.00 35.98l0.00 35.98l83.34 0.00l83.34 0.00l0.00 -35.98z" },
            { "5", "m180.15 183.46l0.00 -24.74l-84.67 0.00l-84.67 0.00l0.00 24.74l0.00 24.74l84.67 0.00l84.67 0.00l0.00 -24.74z" },
            { "6", "m350.28 183.46l0.00 -24.74l-83.48 0.00l-83.48 0.00l0.00 24.74l0.00 24.74l83.48 0.00l83.48 0.00l0.00 -24.74z" },
            { "7", "m180.15 236.77l0.00 -26.19l-84.67 0.00l-84.67 0.00l0.00 26.19l0.00 26.19l84.67 0.00l84.67 0.00l0.00 -26.19z" },
            { "8", "m349.49 236.77l0.00 -26.19l-83.08 0.00l-83.08 0.00l0.00 26.19l0.00 26.19l83.08 0.00l83.08 0.00l0.00 -26.19z" },
            { "9", "m180.15 294.98l0.00 -29.63l-84.67 0.00l-84.67 0.00l0.00 29.63l0.00 29.63l84.67 0.00l84.67 0.00l0.00 -29.63z" },
            { "10", "m350.28 294.98l0.00 -29.63l-83.48 0.00l-83.48 0.00l0.00 29.63l0.00 29.63l83.48 0.00l83.48 0.00l0.00 -29.63z" },
        };
    }
}
