﻿using System.Collections.Generic;

namespace Domain
{
    public class PaginationResult<T>
    {
        //public Int32 Pages { set; get; }
        public List<T> Items { set; get; }
        public int Pages { set; get; }
        public int TotalItems { set; get; }
    }
}
