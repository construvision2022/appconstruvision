﻿namespace Domain
{
    public class SettlementSelectionOutput
    {
        public int AdminId { set; get; }
        public string Name { set; get; }
        public string Location { set; get; }

        public string ImageCode { set; get; }
    }
}
