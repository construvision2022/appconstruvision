﻿namespace Domain
{
    public class ResidentSelectionOutput
    {
        public int ResidentId { set; get; }
        public string Address { set; get; }

        public string ImageCode { set; get; }
    }
}
