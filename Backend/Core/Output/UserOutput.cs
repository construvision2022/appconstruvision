﻿namespace Domain
{
    public class UserOutput
    {
        public int UserId { set; get; }
        
        public string Email { set; get; }
        public string Name { set; get; }
        public string FirstLastName { set; get; }
        public string SecondLastName { set; get; }
    }
}
