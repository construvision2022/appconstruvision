﻿using System;
using System.Linq.Expressions;

namespace Domain
{
    public interface IActor
    {
        Expression<Func<Admin, bool>> Admins();

        Expression<Func<Lot, bool>> Lots();
        Expression<Func<Street, bool>> Streets();
        Expression<Func<Block, bool>> Blocks();
        Expression<Func<Model, bool>> Models();
        Expression<Func<Zone, bool>> Zones();
        Expression<Func<Institution, bool>> Institutions();
        Expression<Func<Package, bool>> Packages();

        Expression<Func<Map, bool>> Maps();
        Expression<Func<MapImage, bool>> MapImages();
        Expression<Func<Settlement, bool>> Settlements();

        Expression<Func<Report, bool>> Reports();
        Expression<Func<ReportMembership, bool>> ReportMemberships();
    }
}
