﻿namespace Domain.EmailContext
{
    public interface IEmailModel
    {
        string GetEmailSubject();
        
        EmailTemplates GetEmailTemplate();
    }
}
