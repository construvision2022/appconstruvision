﻿namespace Domain
{
    public static class WebConfigData
    {
        public static string BaseUrl { set; get; }
        public static string Environment { set; get; }
        
        public static string BrokerUrl { set; get; }
        public static string BrokerEndPoint { set; get; }
        public static string BrokerToken { set; get; }
    }
}