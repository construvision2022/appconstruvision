﻿using Newtonsoft.Json;
using System;
using System.Text;

namespace Domain
{
    public static class Serializer
    {
        public static string Serialize(object data)
        {
            return JsonConvert.SerializeObject(data);
        }

        public static T Deserialize<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        public static string EncodeTo64(string data)
        {
            return Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(data));
        }

        public static string DecodeFrom64(string data)
        {
            return UTF8Encoding.UTF8.GetString(Convert.FromBase64String(data));
        }
    }
}
