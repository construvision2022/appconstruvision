﻿using System;
using System.Text.RegularExpressions;

namespace Domain
{
    public static class Extensions
    {
        public static string ToConstruTime(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return date.Day.ToString("00") + " " + date.Month.ToString("00") + " " + date.Year.ToString("0000");
        }
        public static string ToConstruTime2(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return date.Day.ToString("00") + "-" + GetMonthNameInSpanish(date.Month) + "-" + date.ToString("yy");
        }
        public static string ToJacobsTime(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return date.Day.ToString("00") + "/" + GetMonthNameInSpanish(date.Month) + "/" + date.Year;
        }

        public static string ToJacobsTime2(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return date.Day.ToString() + " de " + GetMonthNameInSpanish(date.Month);
        }


        public static string ToJacobsTimeDayless(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return GetMonthNameInSpanish(date.Month) + "/" + date.Year;
        }
        public static string ToJacobsTimeJacobPowers(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return GetMonthNameInSpanish(date.Month) + "/" + date.ToString("yy");
        }
        public static string ToJacobsPartialDate(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return GetMonthNameInSpanishFull(date.Month) + " del " + date.ToString("yyyy");
        }
        public static string ToJacobsTimeOnlyMonth(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return GetMonthNameInSpanishFull(date.Month);
        }
        public static string ToJacobsFullTime(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return string.Format("{0}/{1}/{2} {3}:{4}", new string[] { 
                date.Day.ToString("00"),
                GetMonthNameInSpanish(date.Month),
                date.Year.ToString(),
                date.Hour.ToString("00"),
                date.Minute.ToString("00"),
            });

        }
        public static string ToJacobsFullDate(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return string.Format("{0}/{1}/{2}", new string[] { 
                date.Day.ToString("00"),
                GetMonthNameInSpanishFull(date.Month),
                date.Year.ToString(),
            });

        }
        public static string ToFullDateTime(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return string.Format("{0} de {1} de {2} a las {3}:{4} hrs", new string[] { 
                date.Day.ToString("00"),
                GetMonthNameInSpanishFull(date.Month),
                date.Year.ToString(),
                date.Hour.ToString("00"),
                date.Minute.ToString("00"),
            });

        }
        public static string ToIntTime(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return date.ToString("yyMMdd");
        }
        public static string ToIntTimeLong(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return date.ToString("yyyyMMdd");
        }
        public static string ToJacobsTimeStarUp(this DateTime date)
        {
            if (date == null) throw new ArgumentNullException("date");

            return GetMonthNameInSpanishFull(date.Month) + " del " + date.ToString("yyyy");
        }


        public static string ToMexPhoneFormat(this string phone)
        {
            if (string.IsNullOrWhiteSpace(phone)) return string.Empty;
            try { return Regex.Replace(phone, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3"); }
            catch { return phone; }
        }

        public static string GetMonthNameInSpanish(int month)
        {
            var mutex = string.Empty;

            switch (month)
            {
                case 1: mutex = "Ene"; break;
                case 2: mutex = "Feb"; break;
                case 3: mutex = "Mar"; break;
                case 4: mutex = "Abr"; break;
                case 5: mutex = "May"; break;
                case 6: mutex = "Jun"; break;
                case 7: mutex = "Jul"; break;
                case 8: mutex = "Ago"; break;
                case 9: mutex = "Sep"; break;
                case 10: mutex = "Oct"; break;
                case 11: mutex = "Nov"; break;
                case 12: mutex = "Dic"; break;
            }

            return mutex;
        }

        public static string GetMonthNameInSpanishFull(int month)
        {
            var mutex = string.Empty;

            switch (month)
            {
                case 1: mutex = "Enero"; break;
                case 2: mutex = "Febrero"; break;
                case 3: mutex = "Marzo"; break;
                case 4: mutex = "Abril"; break;
                case 5: mutex = "Mayo"; break;
                case 6: mutex = "Junio"; break;
                case 7: mutex = "Julio"; break;
                case 8: mutex = "Agosto"; break;
                case 9: mutex = "Septiembre"; break;
                case 10: mutex = "Octubre"; break;
                case 11: mutex = "Noviembre"; break;
                case 12: mutex = "Diciembre"; break;
            }

            return mutex;
        }
    }
}