﻿using CryptSharp;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Domain
{
    public static class Cryptography
    {
        public static string Encrypt(string text)
        {
            var rijndaelManaged = new RijndaelManaged();
            EncryptorTransform = rijndaelManaged.CreateEncryptor(Key, Vector);
            DecryptorTransform = rijndaelManaged.CreateDecryptor(Key, Vector);
            UTFEncoder = new UTF8Encoding();

            return EncryptToString(text);
        }
        public static string Decrypt(string text)
        {
            var rijndaelManaged = new RijndaelManaged();
            EncryptorTransform = rijndaelManaged.CreateEncryptor(Key, Vector);
            DecryptorTransform = rijndaelManaged.CreateDecryptor(Key, Vector);
            UTFEncoder = new UTF8Encoding();

            return DecryptString(text);
        }

        public static string EncryptCookie(string text)
        {
            var rijndaelManaged = new RijndaelManaged();
            EncryptorTransform = rijndaelManaged.CreateEncryptor(CookieKey, CookieVector);
            DecryptorTransform = rijndaelManaged.CreateDecryptor(CookieKey, CookieVector);
            UTFEncoder = new UTF8Encoding();

            return EncryptToString(text);
        }
        public static string DecryptCookie(string text)
        {
            var rijndaelManaged = new RijndaelManaged();
            EncryptorTransform = rijndaelManaged.CreateEncryptor(CookieKey, CookieVector);
            DecryptorTransform = rijndaelManaged.CreateDecryptor(CookieKey, CookieVector);
            UTFEncoder = new UTF8Encoding();

            return DecryptString(text);
        }

        #region RIJNDAEL

        private static ICryptoTransform EncryptorTransform, DecryptorTransform;
        private static UTF8Encoding UTFEncoder;
        private static string EncryptToString(string TextValue)
        {
            return ByteArrToString(EncryptToByteArray(TextValue));
        }
        private static Byte[] EncryptToByteArray(string TextValue)
        {
            var bytes = UTFEncoder.GetBytes(TextValue);
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream, EncryptorTransform, CryptoStreamMode.Write);
            cryptoStream.Write(bytes, 0, bytes.Length);
            cryptoStream.FlushFinalBlock();
            memoryStream.Position = 0;
            var encrypted = new Byte[memoryStream.Length];
            memoryStream.Read(encrypted, 0, encrypted.Length);
            cryptoStream.Close();
            memoryStream.Close();

            return encrypted;
        }
        private static string DecryptString(string EncryptedString)
        {
            return DecryptToByteArray(StrToByteArray(EncryptedString));
        }
        private static string DecryptToByteArray(Byte[] EncryptedValue)
        {
            MemoryStream encryptedStream = new MemoryStream();
            CryptoStream decryptStream = new CryptoStream(encryptedStream, DecryptorTransform, CryptoStreamMode.Write);
            decryptStream.Write(EncryptedValue, 0, EncryptedValue.Length);
            decryptStream.FlushFinalBlock();
            encryptedStream.Position = 0;
            Byte[] decryptedBytes = new Byte[encryptedStream.Length];
            encryptedStream.Read(decryptedBytes, 0, decryptedBytes.Length);
            encryptedStream.Close();

            return UTFEncoder.GetString(decryptedBytes);
        }
        private static Byte[] StrToByteArray(string text)
        {
            Byte vaule;
            var ByteArray = new Byte[text.Length / 3];
            var i = 0;
            var j = 0;
            do
            {
                vaule = Byte.Parse(text.Substring(i, 3));
                ByteArray[j++] = vaule;
                i += 3;
            }
            while (i < text.Length);
            return ByteArray;
        }
        private static string ByteArrToString(Byte[] ByteArray)
        {
            Byte value;
            var text = string.Empty;
            for (int i = 0; i <= ByteArray.GetUpperBound(0); i++)
            {
                value = ByteArray[i];
                if (value < (Byte)10) text += "00" + value.ToString();
                else if (value < (Byte)100) text += "0" + value.ToString();
                else text += value.ToString();
            }
            return text;
        }

        private static Byte[] GenerateEncryptionKey()
        {
            var rijndaelManaged = new RijndaelManaged();
            rijndaelManaged.GenerateKey();
            return rijndaelManaged.Key;
        }
        private static Byte[] GenerateEncryptionVector()
        {
            var rijndaelManaged = new RijndaelManaged();
            rijndaelManaged.GenerateIV();
            return rijndaelManaged.IV;
        }

        #endregion

        #region Keys and Vectors

        private static Byte[] Vector = { 224, 113, 137, 093, 063, 102, 078, 062, 250, 081, 093, 242, 166, 068, 129, 074 };
        private static Byte[] Key = { 254, 047, 132, 020, 180, 092, 167, 229, 051, 007, 020, 051, 072, 222, 088, 014, 208, 034, 233, 038, 212, 071, 060, 028, 157, 055, 220, 111, 070, 062, 116, 081, };

        private static Byte[] CookieVector = { 018, 120, 042, 004, 034, 128, 102, 037, 001, 061, 251, 197, 026, 079, 004, 154 };
        private static Byte[] CookieKey = { 198, 035, 078, 067, 030, 022, 102, 117, 196, 176, 022, 245, 181, 240, 141, 197, 049, 082, 239, 113, 219, 143, 192, 207, 083, 092, 230, 089, 177, 120, 058, 066 };

        #endregion

        public static string CreateSalt()
        {
            //var guid = Guid.NewGuid();
            //var guidArray = guid.ToByteArray();
            //var base64String = Convert.ToBase64String(guidArray);
            //return "ISalt:" + base64String;

            return Crypter.Blowfish.GenerateSalt();
        }

        public static string BCrypt(string password, string salt)
        {
            return Crypter.Blowfish.Crypt(password, salt);
        }

        public static string SHA256(string data)
        {
            return Crypter.Sha512.Crypt(data);
        }
    }
}