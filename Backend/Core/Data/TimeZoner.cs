﻿using System;

namespace Domain
{
    public static class TimeZoner
    {
        public static DateTime Now
        {
            get
            {
                return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("US Mountain Standard Time"));
            }
        }
    }
}
