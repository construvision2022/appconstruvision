﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class QueryInput
    {
        [Display(Name = "Consulta")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        public string Query { set; get; }        
    }
}
