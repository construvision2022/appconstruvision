﻿namespace Domain
{
    public class PagingInput
    {
        public int PageSize { set; get; }
        public int PageNumber { set; get; }
    }
}
