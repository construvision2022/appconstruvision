﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class ExcelExport
    {
        public int ExcelExportId { private set; get; }

        public string FileName { private set; get; }
        public DateTime CreationDate { private set; get; }
        public DateTime? EndDate { private set; get; }

        public DateTime RangeStartDate { private set; get; }
        public DateTime RangeEndDate { private set; get; }

        public int BusinessId { private set; get; }
        public Business Business { private set; get; }

        ExcelExport() { }
        public ExcelExport(Business business, DateTime startDate, DateTime endDate)
        {
            Business = business ?? throw new DomainException();
            BusinessId = business.BusinessId;

            RangeStartDate = startDate;
            RangeEndDate = endDate;

            CreationDate = TimeZoner.Now;

            Business.ExcelReports.Add(this);
        }

        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (RangeStartDate > RangeEndDate) validator.Add("Periods", "Fecha de inicio no puede ser mayor a fecha final.");
        }
        public void End(string fileName)
        {
            FileName = fileName;
            EndDate = TimeZoner.Now;
        }
    }
}