﻿using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;

namespace Domain.ImageUploadContext
{
    public class PostCropInput
    {
        [Display(Name = "Nombre de archivo")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string FileName { set; get; }

        [Display(Name = "Alto")]
        [Required(ErrorMessage = "{0} es requerido.")]
        [Min(10, ErrorMessage = "Mínimo 10.")]
        public double Height { set; get; }

        [Display(Name = "Ancho")]
        [Required(ErrorMessage = "{0} es requerido.")]
        [Min(10, ErrorMessage = "Mínimo 10.")]
        public double Width { set; get; }

        [Display(Name = "Coordenada X")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public double X { set; get; }

        [Display(Name = "Coordenada Y")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public double Y { set; get; }
    }
}