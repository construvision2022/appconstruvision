﻿namespace Domain.ImageUploadContext
{
    public class Output
    {
        public string FileName { set; get; }

        public double Height { set; get; }

        public double Width { set; get; }
    }
}