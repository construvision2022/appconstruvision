﻿using System.Collections.Generic;

namespace Domain.InstitutionContext
{
    public class Output
    {
        public int InstitutionId { set; get; }

        public string Name { set; get; }
        public double Percentage { set; get; }
        public double StartingPercentage { set; get; }

        public List<PackageContext.Output> Packages { set; get; }
        public List<PeriodContext.HistoryOutput> LastPeriods { set; get; }
    }
}