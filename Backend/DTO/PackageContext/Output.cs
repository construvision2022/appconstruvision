﻿using System.Collections.Generic;

namespace Domain.PackageContext
{
    public class Output
    {
        public int PackageId { set; get; }


        public string Name { set; get; }
        public double Percentage { set; get; }
        public double StartingPercentage { set; get; }
        public string InstitutionName { set; get; }
        public double InstitutionPercentage { set; get; }

        public List<int> Memberships { set; get; }
        public List<PeriodContext.HistoryOutput> LastPeriods { set; get; }

        public int InstitutionId { set; get; }        
        public int ZoneId { set; get; }
    }
}
