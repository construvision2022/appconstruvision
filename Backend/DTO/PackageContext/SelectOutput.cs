﻿using System.Collections.Generic;

namespace Domain.PackageContext
{
    public class SelectOutput
    {
        public List<SelectItem> RUV { set; get; } = new List<SelectItem>();
        public List<SelectItem> Bank { set; get; } = new List<SelectItem>();
    }
}
