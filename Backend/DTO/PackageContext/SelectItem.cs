﻿using System.Collections.Generic;

namespace Domain.PackageContext
{
    public class SelectItem
    {
        public int PackageId { set; get; }
        public string PackageName { set; get; }
        public string InstitutionName { set; get; }
        public int InstitutionId { set; get; }
    }
}
