﻿using System.ComponentModel.DataAnnotations;

namespace Domain.PackageContext
{
    public class PostInput
    {
        [Display(Name = "Institución")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int Institution { set; get; }

        [Display(Name = "Nombre")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Name { set; get; }
    }
}