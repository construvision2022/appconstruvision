﻿using System.Collections.Generic;

namespace Domain.CreatorContext
{
    public class Output
    {
        public int Settlements { set; get; }
        public int Zones { set; get; }
        public int Blocks { set; get; }
        public int Models { set; get; }
        public int Streets { set; get; }        
        public int Lots { set; get; }
        public int RUVPackages { set; get; }
        public int BankPackages { set; get; }

        public string Error { set; get; }
        public bool OK => string.IsNullOrWhiteSpace(Error);
    }
}
