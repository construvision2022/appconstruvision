﻿using System.Collections.Generic;

namespace Domain.CreatorContext
{
    public class PostInput
    {
        public List<string> Settlements { set; get; }
        public List<string> Zones { set; get; }
        public List<string> Blocks { set; get; }
        public List<string> Streets { set; get; }
        public List<string> Models { set; get; }
        public List<string> LotNumbers { set; get; }
        public List<string> OfficialNumbers { set; get; }        
        public List<string> RUVPackages { set; get; }
        public List<string> BankPackages { set; get; }
    }
}
