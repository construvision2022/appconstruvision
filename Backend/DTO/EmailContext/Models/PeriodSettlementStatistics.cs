﻿namespace Domain.EmailContext
{
    public class PeriodSettlementStatistics : IEmailModel
    {
        public string PeriodEndDate { set; get; }

        public string ReportCode { set; get; }

        public string GetEmailSubject()
        {
            return "Falta un dia para la generación del reporte quincenal";
        }

        public EmailTemplates GetEmailTemplate()
        {
            return EmailTemplates.PeriodExpiring;
        }
    }
}
