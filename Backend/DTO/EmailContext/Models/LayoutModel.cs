﻿namespace Domain.EmailContext
{
    public class LayoutModel
    {
        public string BusinessSenderEmail { set; get; }
        public string Logo { set; get; }
        public string Banner { set; get; }
        public string InvitationBanner { set; get; }
        public string BusinessName { set; get; }
        public string EmailSenderCredential { set; get; }
        public string EmailSenderPassword { set; get; }
        public string BusinessAddress { set; get; }
        public string PlatformLink { set; get; }
        public string ImageFolder { set; get; }
        public string BusinessiOSLink { set; get; }
        public string BusinessAndroidLink { set; get; }
        public string HiddenBanner { set; get; }
        public string BusinessColor { set; get; }


        public string SettlementContactName { set; get; }
        public string SettlementContactEmail { set; get; }
        public string SettlementMobilePhone { set; get; }
        public string SettlementLocalPhone { set; get; }
    }
}