﻿namespace Domain.EmailContext
{
    public class ReportModel : IEmailModel
    {
        public string ResidentName { set; get; }

        public string Address { set; get; }

        public string ResidentReportCode { set; get; }

        public string GetEmailSubject()
        {
            return "Se ha generado un nuevo reporte de su casa";
        }

        public EmailTemplates GetEmailTemplate()
        {
            return EmailTemplates.NewReport;
        }
    }
}
