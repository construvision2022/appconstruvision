﻿namespace Domain.EmailContext
{
    public class WelcomeModel : IEmailModel
    {
        public string ResidentName { set; get; }

        public string Address { set; get; }

        public string SettlementName { set; get; }

        public string ResidentEmail { set; get; }

        public string Password { set; get; }

        public string GetEmailSubject()
        {
            return "Descarga la aplicacion de Construvision";
        }

        public EmailTemplates GetEmailTemplate()
        {
            return EmailTemplates.Welcome;
        }
    }
}
