﻿namespace Domain.EmailContext
{
    public class PeriodExpiringModel : IEmailModel
    {
        public string SupervisorName { set; get; }

        public string PeriodEndDate { set; get; }

        public string GetEmailSubject()
        {
            return "Falta un dia para la generación del reporte quincenal";
        }

        public EmailTemplates GetEmailTemplate()
        {
            return EmailTemplates.PeriodExpiring;
        }
    }
}
