﻿namespace Domain.EmailContext
{
    public class ExcelExportModel : IEmailModel
    {
        public string AdminName { set; get; }

        public string StartDate { set; get; }
        public string EndDate { set; get; }

        public string FileName { set; get; }

        public string GetEmailSubject()
        {
            return "Reporte de avance de construccion para " + AdminName;
        }

        public EmailTemplates GetEmailTemplate()
        {
            return EmailTemplates.NewExcelExport;
        }
    }
}
