﻿using System;
using System.Collections.Generic;

namespace Domain.ReportContext
{
    public class DetailOutput
    {
        public int ReportId { set; get; }
        public int LotId { set; get; }

        public string ImageCode { set; get; }
        public double Percentage { set; get; }
        public double PreviousReportPercentage { set; get; }
        public DateTime CreationDate { set; get; }
        public DateTime? LastUpdate { set; get; }

        public List<Membership.Output> BuildingConcepts { set; get; }
    }
}

