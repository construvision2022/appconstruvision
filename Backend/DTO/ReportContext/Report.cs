﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class Report
    {
        public int ReportId { private set; get; }
        public int Folio { private set; get; }
        public DateTime CreationDate { private set; get; }
        public DateTime? LastUpdate { private set; get; }
        public DateTime? PublishDate { private set; get; }
        public double ReportPercentage { private set; get; }
        public string AdminReportGuid { private set; get; }
        public string ResidentReportGuid { private set; get; }
        public string ImageCode { private set; get; }
        public double PreviousReportPercentage { private set; get; }

        public int LotId { private set; get; }
        public Lot Lot { private set; get; }

        public int UserId { private set; get; }
        public User User { private set; get; }

        public int PeriodId { private set; get; }
        public Period Period { private set; get; }

        public List<ReportMembership> ReportMemberships { private set; get; }

        void Init()
        {
            ReportMemberships = new List<ReportMembership>();
        }
        Report() { Init(); }
        public Report(Lot lot, Settlement settlement, User user, Period period, int folio, string imageCode)
        {
            Lot = lot ?? throw new DomainException();
            LotId = lot.LotId;

            User = user ?? throw new DomainException();
            UserId = user.UserId;

            Period = period ?? throw new DomainException();
            PeriodId = period.PeriodId;

            Folio = folio;
            ImageCode = imageCode;

            if (lot.Report != null)
                PreviousReportPercentage = lot.Report.ReportPercentage;

            CreationDate = TimeZoner.Now;

            Init();

            Lot.Reports.Add(this);
            Period.Reports.Add(this);
        }

        public Dictionary<string, string> Validate()
        {
            var validator = new Dictionary<string, string>();

            if (Folio < 1) validator.Add("Folio", "Error al generar el folio");

            if (ReportMemberships == null) validator.Add(string.Empty, "Conceptos de construcción requeridos.");

            return validator;
        }
        public void SetImage(string imageCode)
        {
            ImageCode = imageCode;
            LastUpdate = TimeZoner.Now;
        }
        public void SetReportPercentage(double percentage)
        {
            //if (percentage < 0 || percentage > 100) throw new ArgumentOutOfRangeException("percentage");
            ReportPercentage = percentage;
        }
        public void UpdatePreviousReportPercentage()
        {
            PreviousReportPercentage = ReportPercentage;
        }
        public double GetPercentage(int decimals = 1)
        {
            return (double)Math.Round(ReportPercentage, decimals);
        }

        public override string ToString()
        {
            var output = $"{ReportId} | {ReportPercentage.ToString("00.0")}%";

            if (Lot != null)
                output += $" | {Lot.GetAddres()}";

            if (Period != null)
                output += $" | {Period.StartDate.ToString("dd/MM/yy")}~{Period.EndDate.ToString("dd/MM/yy")}";

            return output;
        }
    }
}