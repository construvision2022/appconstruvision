﻿using System;

namespace Domain
{
    public class ReportMembershipImage
    {
        public int ReportMembershipImageId { private set; get; }
        public string ImageCode { private set; get; }

        public int ReportMembershipId { private set; get; }
        public ReportMembership ReportMembership { private set; get; }

        ReportMembershipImage() { }
        public ReportMembershipImage(ReportMembership reportMembership, string imageCode)
        {
            ReportMembership = reportMembership ?? throw new DomainException();
            ReportMembershipId = reportMembership.ReportMembershipId;

            ImageCode = imageCode;

            ReportMembership.ReportMembershipImages.Add(this);
        }

        public void Validate()
        {
        }
        public void SetImage(string imageCode)
        {
            ImageCode = imageCode;
        }
    }
}