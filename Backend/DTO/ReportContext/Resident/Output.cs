﻿using System;
using System.Collections.Generic;

namespace Domain.ReportContext.Resident
{
    public class Output
    {
        public int ReportId { set; get; }
        public List<string> LotImages { set; get; }
        public string Address { set; get; }
        public string Block { set; get; }
        public string Zone { set; get; }
        public string LotNumber { set; get; }
        public double CompletePercentage { set; get; }
        public string ResidentReportCode { set; get; }
        public DateTime EndDate { set; get; }
    }
}
