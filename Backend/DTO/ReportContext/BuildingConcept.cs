﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class BuildingConcept
    {
        public int BuildingConceptId { private set; get; }

        public string Name { private set; get; }
        public double Percentage { private set; get; }
        public int Order { private set; get; }
        public int? CriticalRoute { private set; get; }
        public DateTime CreationDate { private set; get; }

        public Model Model { private set; get; }
        public int ModelId { private set; get; }

        BuildingConcept() { }
        public BuildingConcept(Model model, string name, double percentage, int order, int? criticalRoute)
        {
            Model = model ?? throw new DomainException();
            ModelId = model.ModelId;

            Name = name;
            Percentage = percentage;
            Order = order;
            CriticalRoute = criticalRoute;
            CreationDate = TimeZoner.Now;

            Model.BuildingConcepts.Add(this);
        }

        public string Validate()
        {
            if (Order <= 0)
                return "Orden inválido.";

            if (CriticalRoute.HasValue)
                if (CriticalRoute.Value <= 0 || CriticalRoute > 99)
                    return "Ruta critica inválida.";

            if (Percentage > 1)
                return "Porcentaje inválido.";

            return string.Empty;
        }

        public override string ToString()
        {
            return $"{BuildingConceptId} | {Name} | #{Order} | CR: {CriticalRoute} | {Percentage}%";
        }
    }
}