﻿using System;

namespace Domain.ReportContext
{
    public class Output
    {
        public int ReportId { set; get; }
        public int LotId { set; get; }

        public string ImageCode { set; get; }
        public double Percentage { set; get; }
        public double PreviousReportPercentage { set; get; }
        public DateTime CreationDate { set; get; }
        public DateTime? LastUpdate { set; get; }
    }
}

