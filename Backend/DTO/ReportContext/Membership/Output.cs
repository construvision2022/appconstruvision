﻿using System.Collections.Generic;

namespace Domain.ReportContext.Membership
{
    public class Output
    {
        public int BuildingConceptId { set; get; }

        public string Name { set; get; }
        public double Percentage { set; get; }
        public double LastPeriodPercentage { set; get; }
        public int? CriticalRoute { set; get; }
        public string Comment { set; get; }

        public List<Image.Output> Images { set; get; }
    }
}