﻿namespace Domain.ReportContext.Membership.Image
{
    public class Output
    {
        public int ReportMembershipImageId { set; get; }
        public string FileName { set; get; }
    }
}

