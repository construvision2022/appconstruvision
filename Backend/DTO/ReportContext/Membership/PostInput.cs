﻿using DataAnnotationsExtensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.ReportContext.Membership
{
    public class PostInput
    {
        [Display(Name = "Concepto de construccion ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int BuildingConceptId { set; get; }

        [Display(Name = "Porciento")]
        [Min(0, ErrorMessage = "Porcentaje no puede ser menor a 0")]
        [Max(100, ErrorMessage = "Porcentaje no puede ser mayor a 100")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public double Percentage { set; get; }

        [Display(Name = "Comentario")]
        public string Comment { set; get; }

        [Display(Name = "Imagen")]
        public List<Membership.Image.PostInput> Images { set; get; }
    }
}