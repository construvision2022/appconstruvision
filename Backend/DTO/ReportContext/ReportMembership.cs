﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class ReportMembership
    {
        public int ReportMembershipId { private set; get; }

        public double Percentage { private set; get; }
        public double LotPercentage { private set; get; }
        public double PreviousReportPercentage { private set; get; }
        public string Comment { private set; get; }
        public DateTime CreationDate { private set; get; }

        public int ReportId { private set; get; }
        public Report Report { private set; get; }

        public int BuildingConceptId { private set; get; }
        public BuildingConcept BuildingConcept { private set; get; }

        public List<ReportMembershipImage> ReportMembershipImages { private set; get; }


        void Init()
        {
            ReportMembershipImages = new List<ReportMembershipImage>();
        }
        ReportMembership() { Init(); }
        public ReportMembership(Report report, BuildingConcept buildingConcept, double previousReportPercentage, double percent, string comment = "")
        {
            Report = report ?? throw new DomainException();
            ReportId = report.ReportId;

            BuildingConcept = buildingConcept ?? throw new DomainException();
            BuildingConceptId = buildingConcept.BuildingConceptId;

            Percentage = percent;
            LotPercentage = percent * buildingConcept.Percentage;
            PreviousReportPercentage = previousReportPercentage;
            Comment = comment;
            CreationDate = TimeZoner.Now;

            Report.ReportMemberships.Add(this);
        }

        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (Percentage < 0 || Percentage > 100) validator.Add("Percentage", "Porcentaje inválido.");
        }
        public void Finish(BuildingConcept buildingConcept)
        {
            Percentage = 100;
            LotPercentage = 100 * buildingConcept.Percentage;
        }
        public double GetPercentage(int decimals = 1)
        {
            return Math.Round(Percentage, decimals);
        }
        public void UpdateBuildingConcept(BuildingConcept bc)
        {
            BuildingConcept = bc ?? throw new DomainException();
            BuildingConceptId = bc.BuildingConceptId;
        }
    }
}