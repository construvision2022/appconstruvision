﻿using Domain.LotContext;
using System;
using System.Collections.Generic;

namespace Domain.ReportContext
{
    public class ReportOutput
    {
        public int ReportId { set; get; }

        public DateTime CreationDate { set; get; }
        public DateTime? LastUpdate { set; get; }

        public int LotId { set; get; }
        public double LotPercentage { set; get; }
        public Status LotStatus { set; get; }

        public int ZoneId { set; get; }
        public double ZonePercentage { set; get; }

        public int SettlementId { set; get; }
        public double SettlementPercentage { set; get; }

        public List<PackageContext.Output> Packages { set; get; }
        public List<InstitutionContext.Output> Institutions { set; get; }
    }
}

