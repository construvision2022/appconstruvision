﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.ReportContext
{
    public class PostInput
    {
        public List<Membership.PostInput> BuildingConcepts { set; get; }

        [Display(Name = "Lote ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int LotId { set; get; }

        [Display(Name = "Imagen de fachada")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public string FileName { set; get; }

    }
}