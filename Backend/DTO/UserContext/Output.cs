﻿using System.Collections.Generic;

namespace Domain.UserContext
{
    public class Output
    {
        public int UserId { set; get; }

        public string Name { set; get; }
        public string FirstLastName { set; get; }
        public string SecondLastName { set; get; }
        public string Email { set; get; }
        public string MobilePhone { set; get; }
        public string LocalPhone { set; get; }
    }
}
