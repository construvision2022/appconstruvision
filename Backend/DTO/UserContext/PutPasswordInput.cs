﻿using System.ComponentModel.DataAnnotations;

namespace Domain.UserContext
{
    public class PutPasswordInput
    {
        [Display(Name = "Contraseña nueva")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public string Password { set; get; }

        [Display(Name = "Repite la contraseña")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public string RePassword { set; get; }
    }
}