﻿using System.Collections.Generic;

namespace Domain.UserContext
{
    public class UsersOutput
    {
        public List<Output> Admins { set; get; } = new List<Output>();
        public List<Output> SalesAdmins { set; get; } = new List<Output>();
        public List<Output> Sellers { set; get; } = new List<Output>();
        public List<Output> Supervisors { set; get; } = new List<Output>();
        public List<Output> Managers { set; get; } = new List<Output>();
    }
}
