﻿using DataAnnotationsExtensions;
using Domain.SessionContext;
using System.ComponentModel.DataAnnotations;

namespace Domain.UserContext
{
    public class PostInput
    {
        [Display(Name = "Email")]
        [Email(ErrorMessage = "Formato incorrecto.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Email { set; get; }

        [Display(Name = "Nombre")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Name { set; get; }

        [Display(Name = "Apellido paterno")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string FirstLastName { set; get; }

        [Display(Name = "Apellido materno")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string SecondLastName { set; get; }

        [Display(Name = "Teléfono móvil")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        public string MobilePhone { set; get; }

        [Display(Name = "Teléfono local")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        public string LocalPhone { set; get; }

        [Display(Name = "Tipo de usuario")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public SessionTypes Type { set; get; }

        [Display(Name = "Cerrada")]
        public int? ZoneId { set; get; }
    }
}