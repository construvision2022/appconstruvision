﻿using System.ComponentModel.DataAnnotations;

namespace Domain.UserContext
{
    public class PutInput
    {
        [Display(Name = "Email")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Email { set; get; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Name { set; get; }

        [Display(Name = "Primer apellido")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string FirstLastName { set; get; }

        [Display(Name = "Segundo apellido")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string SecondLastName { set; get; }

        [Display(Name = "Teléfono móvil")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string MobilePhone { set; get; }

        [Display(Name = "Teléfono local")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string LocalPhone { set; get; }
    }
}