﻿namespace Domain.BlockContext
{
    public class Output
    {
        public int? BlockId { set; get; }
        public string Name { set; get; }
        public string Color { set; get; }
        public int? SettlementId { set; get; }
        public int? ZoneId { set; get; }
        public string ZoneName { set; get; }
    }
}
