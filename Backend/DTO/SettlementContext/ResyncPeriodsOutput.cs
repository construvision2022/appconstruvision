﻿using Domain;
using System;
using System.Collections.Generic;

namespace Domain.SettlementContext
{
    public class ResyncPeriodsOutput
    {
        public ResyncPeriodsOutput()
        {
            PackagePeriods = new List<PackagePeriod>();
            InstitutionPeriods = new List<InstitutionPeriod>();
            ZonePeriods = new List<ZonePeriod>();
            SettlementPeriods = new List<SettlementPeriod>();
        }

        public List<PackagePeriod> PackagePeriods { set; get; }
        public List<InstitutionPeriod> InstitutionPeriods { set; get; }
        public List<ZonePeriod> ZonePeriods { set; get; }
        public List<SettlementPeriod> SettlementPeriods { set; get; }

        public void Merge(ResyncPeriodsOutput result)
        {
            PackagePeriods.AddRange(result.PackagePeriods);
            InstitutionPeriods.AddRange(result.InstitutionPeriods);
            ZonePeriods.AddRange(result.ZonePeriods);
            SettlementPeriods.AddRange(result.SettlementPeriods);
        }
    }
}
