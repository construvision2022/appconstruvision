﻿using System.ComponentModel.DataAnnotations;

namespace Domain.SettlementContext
{
    public class PutImageInput
    {
        [Display(Name = "Imagen")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public string ImageCode { set; get; }
    }
}