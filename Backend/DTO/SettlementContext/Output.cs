﻿using System.Collections.Generic;

namespace Domain.SettlementContext
{
    public class Output
    {
        public int SettlementId { set; get; }

        public string Name { set; get; }
        public string Alias { set; get; }
        public string Description { set; get; }

        public double Percentage { set; get; }
        public double StartingPercentage { set; get; }

        public string RFC { set; get; }
        public string ZipCode { set; get; }

        public string ImageCode { set; get; }
        public List<PeriodContext.HistoryOutput> LastPeriods { set; get; }
    }
}
