﻿using System;
using System.Collections.Generic;

namespace Domain.SettlementContext
{
    public class Statistics
    {
        public int SettlementId { set; get; }

        public double LastPercentage { set; get; }
        public double CurrentPercentage { set; get; }
        
        public DateTime LastEndDate { set; get; }
        public DateTime EndDate { set; get; }
        
        public int Pending { set; get; }
        public int InConstruction { set; get; }
        public int Finished { set; get; }

        public List<PeriodContext.Output> Periods { set; get; }
        public List<PeriodContext.Output> Institutions { set; get; }
        public List<ReportContext.Output> Reports { set; get; }
    }
}