﻿using System.Collections.Generic;

namespace Domain.ZoneContext
{
    public class Output
    {
        public int ZoneId { set;  get; }
        public string Name { set; get; }

        public string Alias { set; get; }
        public double Percentage { set;get;}
        public double StartingPercentage { set; get; } 

        public int? MapId { set; get; }
        public int? SettlementId { set; get; }
        public List<PeriodContext.HistoryOutput> LastPeriods { set; get; }
    }
}
