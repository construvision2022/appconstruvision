﻿using System.ComponentModel.DataAnnotations;

namespace Domain.ZoneContext
{
    public class PutInput
    {
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public string Name { set; get; }
    }
}