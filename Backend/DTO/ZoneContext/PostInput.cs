﻿using System.ComponentModel.DataAnnotations;

namespace Domain.ZoneContext
{
    public class PostInput
    {
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public string Name { set; get; }
    }
}