﻿using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;

namespace Domain.ResidentContext
{
    public class PostInput
    {
        [Display(Name = "Lote ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int LotId { set; get; }

        [Display(Name = "Email")]
        [Email(ErrorMessage = "Formato incorrecto.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Email { set; get; }

        [Display(Name = "Nombre")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Name { set; get; }

        [Display(Name = "Apellido paterno")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string FirstLastName { set; get; }

        [Display(Name = "Apellido materno")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string SecondLastName { set; get; }

        [Display(Name = "Teléfono móvil")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        public string MobilePhone { set; get; }

        [Display(Name = "Teléfono local")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        public string LocalPhone { set; get; }

        [Display(Name = "Comentarios")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        public string Comments { set; get; }

        [Display(Name = "Típo de residente")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public ResidentTypes Type { set; get; }
    }
}