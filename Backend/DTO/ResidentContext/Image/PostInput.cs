﻿using System.ComponentModel.DataAnnotations;

namespace Domain.ResidentContext.Image
{
    public class PostInput
    {
        [Display(Name = "Nombre de archivo")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string FileName { set; get; }
    }
}
