﻿namespace Domain.ResidentContext
{
    public class Output
    {
        public int ResidentId { set; get; }

        public string Email { set; get; }
        public string Name { set; get; }
        public string FirstLastName { set; get; }
        public string SecondLastName { set; get; }
        public string MobilePhone { set; get; }

        public ResidentTypes Type { set; get; }
        public ResidentStatus Status { set; get; }

        public int LotId { set; get; }
        public int UserId { set; get; }
    }
}
