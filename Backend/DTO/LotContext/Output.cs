﻿using System;

namespace Domain.LotContext
{
    public class Output
    {
        public int LotId { set; get; }
        
        public string LotNumber { set; get; }
        public string OfficialNumber { set; get; }
        
        public string Area { set; get; }
        public double GroundArea { set; get; }
        public double? ConstructionArea { set; get; }

        public DateTime? SaleDate { set; get; }
        public string SaleDate_iso { set; get; }

        public DateTime? PaymentDate { set; get; }
        public string PaymentDate_iso { set; get; }

        public string LastFrontImage { set; get; }

        public Status Status { set; get; }
        public SaleStatus SaleStatus { set; get; }
        public double Percentage { set; get; }
        public bool Dtu { set; get; }
        public bool Updated { set; get; }

        public string RUV { set; get; }
        public int RUVId { set; get; }

        public string Bancario { set; get; }
        public int BancarioId { set; get; }

        public int? ZoneId { set; get; }
        public string ZoneName { set; get; }

        public int? StreetId { set; get; }
        public string StreetName { set; get; }

        public int? BlockId { set; get; }
        public string BlockName { set; get; }

        public int? ModelId { set; get; }
        public string ModelName { set; get; }

        public int? SettlementId { set; get; }
        public DateTime? BuildingStartDate { set; get; }
        public DateTime? BuildingEndDate { set; get; }
        public double StartingPercentage { set; get; }
        public DateTime? LastUpdate { set; get; }
        public DateTime? DtuDate { set; get; }        
    }
}