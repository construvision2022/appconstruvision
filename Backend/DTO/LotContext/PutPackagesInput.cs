﻿using System;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;

namespace Domain.LotContext
{
    public class PutPackagesInput
    {
        [Display(Name = "Paquete RUV")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int RUV { set; get; }

        [Display(Name = "Paquete bancario")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int Bancario { set; get; }
    }
}
