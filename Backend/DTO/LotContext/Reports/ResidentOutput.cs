﻿using System;

namespace Domain.LotContext.Reports
{
    public class ResidentOutput
    {
        public string LotNumber { set; get; }
        public string OfficialNumber { set; get; }
        public string Zone { set; get; }
        public string Block { set; get; }
        public string Street { set; get; }
        public string Model { set; get; }
        public string Settlement { set; get; }
        public double CompletePercentage { set; get; }
        public double Area { set; get; }
        public double GroundArea { set; get; }
        public DateTime? SaleDate { set; get; }
        public string LotImages { set; get; }
    }
}