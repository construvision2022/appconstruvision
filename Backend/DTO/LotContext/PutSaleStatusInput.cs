﻿using System;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;

namespace Domain.LotContext
{
    public class PutSaleStatusInput
    {
        [Display(Name = "Estado de venta")]
        [Required(ErrorMessage = "{0} es requerido")]
        public SaleStatus SaleStatus { set; get; }

        [Display(Name = "Fecha de entrega")]
        [Date(ErrorMessage = "{0} Formato de fecha incorrecto.")]
        public DateTime? ActionDate { set; get; }
    }
}
