﻿using System.ComponentModel.DataAnnotations;

namespace Domain.LotContext
{
    public class PutModelInput
    {
        [Display(Name = "Modelo ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int ModelId { set; get; }
    }
}
