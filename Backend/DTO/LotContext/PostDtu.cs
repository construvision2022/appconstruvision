﻿using System.ComponentModel.DataAnnotations;

namespace Domain.LotContext
{
    public class PostDtu
    {
        [Display(Name = "Anotación")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        public string Annotation { set; get; }
    }
}