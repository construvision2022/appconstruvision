﻿using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;

namespace Domain.LotContext
{
    public class PostInput
    {
        [Display(Name = "Privada ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int ZoneId { set; get; }

        [Display(Name = "Calle ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int StreetId { set; get; }

        [Display(Name = "Manzana ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int BlockId { set; get; }

        [Display(Name = "Modelo ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int ModelId { set; get; }

        [Display(Name = "Paquete RUV")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int RUV { set; get; }

        [Display(Name = "Paquete bancario")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int Bank { set; get; }

        [Display(Name = "Número de lote")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string LotNumber { set; get; }

        [Display(Name = "Número oficial")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        //[Required(ErrorMessage = "{0} es requerido.")]
        public string OfficialNumber { set; get; }

        [Display(Name = "Área de terreno")]
        [Min(1, ErrorMessage = "Debe tener al menos 1 metro cuadrado")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public double GroundArea { set; get; }

        [Display(Name = "Área de construcción")]
        [Min(1, ErrorMessage = "Debe tener al menos 1 metro cuadrado")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public double ConstructionArea { set; get; }

        [Display(Name = "Estado de construcción")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public SaleStatus SaleStatus { set; get; }
    }
}