﻿namespace Domain.LotContext
{
    public class Raw
    {
        public int LotId { set; get; }
        public string LotNumber { set; get; }        
    }
}