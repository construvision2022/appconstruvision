﻿using System.ComponentModel.DataAnnotations;

namespace Domain.LotContext
{
    public class PutInput
    {
        [Display(Name = "Número de lote")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string LotNumber { set; get; }

        [Display(Name = "Número oficial")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        public string OfficialNumber { set; get; }

        [Display(Name = "Área de construcción")]        
        [Required(ErrorMessage = "{0} es requerida.")]
        public double ConstructionArea { set; get; }

        [Display(Name = "Área de terreno")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public double GroundArea { set; get; }

        [Display(Name = "Estado de venta")]
        [Required(ErrorMessage = "{0} es requerido")]
        public SaleStatus SaleStatus { set; get; }
    }
}
