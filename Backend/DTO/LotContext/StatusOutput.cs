﻿using System.Collections.Generic;

namespace Domain.LotContext
{
    public class StatusOutput
    {
        public int? ReportId { set; get; }
        public double CompletePercentage { set; get; }
        public string Folio { set; get; }
        public string Creator { set; get; }
        public string FrontImage { set; get; }
        
        public List<ReportContext.Membership.Output> Concepts {set;get;}
    }
}