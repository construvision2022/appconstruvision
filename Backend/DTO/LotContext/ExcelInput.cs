﻿public class LotRow
{
    public string Settlement { set; get; }
    public string Zone { set; get; }
    public string Model { set; get; }
    public string Block { set; get; }
    public string Street { set; get; }
    public string LotNumber { set; get; }
    public string OfficialNumber { set; get; }
    public double? GroundArea { set; get; }
    public double? ConstructionArea { set; get; }
    public string RuvPackage { set; get; }
    public string BankPackage { set; get; }
}