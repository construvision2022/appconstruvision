﻿namespace Domain.MapContext.Image
{
    public class Output
    {       
        public int MapImageId { set; get; }
        
        public string Path { set; get; }
        
        public Domain.MapContext.Image.ImageTypes Type { set; get; }
        
        public int? LotId { set; get; }
    }
}