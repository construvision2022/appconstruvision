﻿using System.ComponentModel.DataAnnotations;

namespace Domain.MapContext.Image
{
    public class PostTypeInput
    {
        [Display(Name = "Imagen de mapa ID")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public int MapImageId { set; get; }

        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public ImageTypes Type { set; get; }
    }
}
