﻿using DataAnnotationsExtensions;
using System.ComponentModel.DataAnnotations;

namespace Domain.MapContext.Image
{
    public class PutInput
    {
        [Display(Name = "Lote ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int LotId { set; get; }
    }
}
