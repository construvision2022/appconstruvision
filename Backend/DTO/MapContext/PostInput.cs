﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.MapContext
{
    public class PostInput
    {
        [Display(Name = "Nombre")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Name { set; get; }

        [Display(Name = "Ancho")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int Width { set; get; }

        [Display(Name = "Alto")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int Height { set; get; }

        public List<string> Paths { set; get; }
    }
}
