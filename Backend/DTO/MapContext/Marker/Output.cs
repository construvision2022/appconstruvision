﻿using System.Collections.Generic;

namespace Domain.MapContext.Marker
{
    public class Output
    {
        public int MarkerId { set; get; }
        public string Label { set; get; }
        public double X { set; get; }
        public double Y { set; get; }
    }
}