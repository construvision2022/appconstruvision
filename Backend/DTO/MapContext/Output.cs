﻿using System.Collections.Generic;

namespace Domain.MapContext
{
    public class Output
    {
        public int MapId { set; get; }
        public string Name { set; get; }
        public int Width { set; get; }
        public int Height { set; get; }
        public List<Image.Output> Paths { set; get; }
        public List<Marker.Output> Markers { set; get; }
    }
}