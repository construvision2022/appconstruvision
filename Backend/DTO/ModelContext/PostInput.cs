﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.ModelContext
{
    public class PostInput
    {
        [Display(Name = "Privada ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int ZoneId { set; get; }

        [Display(Name = "Nombre")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Name { set; get; }

        [Display(Name = "Pisos")]
        public int Floors { set; get; }

        [Display(Name = "Área")]
        public double Area { set; get; }

        [Display(Name = "Área de terreno")]
        public double GroundArea { set; get; }

        [Display(Name = "Color")]
        public string Color { set; get; }

        [Display(Name = "Conceptos de construcción")]
        public List<PostBuildingConceptInput> BuildingConcepts { set; get; } = new List<PostBuildingConceptInput>();
    }
}
