﻿using System.ComponentModel.DataAnnotations;

namespace Domain.ModelContext
{
    public class PutFileInput
    {
        [Display(Name = "Nombre de archivo")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string FileName { set; get; }
    }
}
