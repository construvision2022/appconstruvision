﻿namespace Domain.ModelContext
{
    public class Output
    {
        public int ModelId { set; get; }

        public string Name { set; get; }
        public double Area { set; get; }
        public double GroundArea { set; get; }
        public int Floors { set; get; }
        public string Color { set; get; }
        public bool DefaultData { set; get; }

        public string ImageCode { set; get; }
        public string File { set; get; }

        public int SettlementId { set; get; }
        public int ZoneId { set; get; }
        public string ZoneName { set; get; }
    }
}
