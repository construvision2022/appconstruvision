﻿using System.ComponentModel.DataAnnotations;

namespace Domain.ModelContext
{
    public class PostImageInput
    {
        [Display(Name = "Modelo ID")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public int ModelId { set; get; }

        [Display(Name = "Nombre de archivo")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string FileName { set; get; }
    }
}
