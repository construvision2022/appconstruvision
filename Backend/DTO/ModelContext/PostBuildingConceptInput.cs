﻿using System.ComponentModel.DataAnnotations;

namespace Domain.ModelContext
{
    public class PostBuildingConceptInput
    {
        [Display(Name = "Nombre")]
        public string Name { set; get; }

        [Display(Name = "Porcentaje")]
        public decimal Percentage { set; get; }

        [Display(Name = "Orden")]
        public int Order { set; get; }

        [Display(Name = "Ruta crítica")]
        public int? CriticalRoute { set; get; }
    }
}
