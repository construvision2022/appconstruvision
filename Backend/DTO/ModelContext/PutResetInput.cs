﻿using System.ComponentModel.DataAnnotations;

namespace Domain.ModelContext
{
    public class PutResetInput
    {
        [Display(Name = "Reset")]
        public bool Reset { set; get; }
    }
}
