﻿using System.ComponentModel.DataAnnotations;

namespace Domain.StreetContext
{
    public class PostInput
    {
        [Display(Name = "Nombre")]
        [RegularExpression(@"^[áéíóúÁÉÍÓÚñÑa-zA-Z0-9/?¿!¡\s_#$%=\^*&.,;:)(@+-]*$", ErrorMessage = "{0} contiene caracteres inválidos.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Name { set; get; }

        [Display(Name = "Zona ID")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public int ZoneId { set; get; }
    }
}