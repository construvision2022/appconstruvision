﻿namespace Domain.StreetContext
{
    public class Output
    {
        public int? StreetId { set; get; }

        public string Name { set; get; }
        public string ZoneName { set; get; }

        public int? ZoneId { set; get; }
        public int? SettlementId { set; get; }
    }
}
