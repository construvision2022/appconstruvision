﻿using System;

namespace Domain.PeriodContext
{
    public class Output
    {
        public int PeriodId { set; get; }

        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
        public double CurrentPercentage { set; get; }
    }
}