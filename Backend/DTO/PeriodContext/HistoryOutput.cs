﻿using System;

namespace Domain.PeriodContext
{
    public class HistoryOutput
    {
        public int PeriodId { set; get; }
        
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }

        public double StartingPercentage { set; get; }
        public double EndingPercentage { set; get; }
    }
}
