﻿namespace Domain.BusinessContext
{
    public class Output
    {
        public string Name { set; get; }
        public string Description { set; get; }
        public string RegisteredName { set; get; }
        public string RFC { set; get; }
        public string Website { set; get; }
        public string Phone { set; get; }
        public string Address { set; get; }
        public string Banner { set; get; }
        public string Logo { set; get; }
    }
}
