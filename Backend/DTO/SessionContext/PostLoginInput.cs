﻿using DataAnnotationsExtensions;
using System.ComponentModel.DataAnnotations;

namespace Domain.SessionContext
{
    public class PostLoginInput
    {
        [Display(Name = "Email")]
        [Email(ErrorMessage = "Formato incorrecto.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Email { set; get; }

        [Display(Name = "Contraseña")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "{0} es requerida.")]
        public string Password { set; get; }
    }
}
