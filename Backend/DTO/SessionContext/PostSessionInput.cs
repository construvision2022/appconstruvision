﻿using DataAnnotationsExtensions;
using System.ComponentModel.DataAnnotations;

namespace Domain.SessionContext
{
    public class PostSessionInput
    {
        [Display(Name = "Email")]
        [Email(ErrorMessage = "Formato incorrecto.")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Email { set; get; }

        [Display(Name = "Cerrada ID")]
        public int ZoneId { set; get; }

        [Display(Name = "Residencial ID")]
        public int SettlementId { set; get; }

        [Display(Name = "Tipo")]
        public SessionTypes Type { set; get; }
    }
}
