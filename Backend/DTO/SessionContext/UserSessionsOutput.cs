﻿using System.Collections.Generic;

namespace Domain.SessionContext
{
    public class UserSessionsOutput
    {
        public UserContext.Output User { set; get; }

        public List<Output> Sessions { set; get; }
    }
}
