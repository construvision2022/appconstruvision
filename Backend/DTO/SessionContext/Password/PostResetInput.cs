﻿using System.ComponentModel.DataAnnotations;

namespace Domain.SessionContext.Password
{
    public class PostResetInput
    {
        [Display(Name = "Token")]
        [Required(ErrorMessage = "{0} es requerido.")]
        public string Token { set; get; }

        [Display(Name = "Contraseña nueva")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public string NewPassword { set; get; }
    }
}