﻿using System.ComponentModel.DataAnnotations;

namespace Domain.SessionContext.Password
{
    public class PostChangeInput
    {
        [Display(Name = "Contraseña anterior")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public string OldPassword { set; get; }

        [Display(Name = "Contraseña nueva")]
        [Required(ErrorMessage = "{0} es requerida.")]
        public string NewPassword { set; get; }
    }
}
