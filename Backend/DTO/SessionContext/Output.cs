﻿namespace Domain.SessionContext
{
    public class Output
    {
        public int SessionId { set; get; }
        public SessionTypes SessionType { set; get; }
        public string Description { set; get; }
        public string Alias { set; get; }
        public int MapId { set; get; }
        public string ImageCode { set; get; }

        public int SettlementId { set; get; }
        public int ZoneId { set; get; }
    }
}
