﻿namespace Domain.SessionContext
{
    public class LoginOutput
    {
        public string Token { set; get; }
        public string Email { set; get; }
        public string Name { set; get; }
        public string FirstLastName { set; get; }
        public string SecondLastName { set; get; }
    }
}