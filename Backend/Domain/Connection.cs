﻿using Domain;
using System;
using System.Collections.Generic;

namespace Backend.Domain.ConnectionContext
{
    public class Connection
    {
        public int ConnectionId { private set; get; }

        public string Token { private set; get; }
        public DateTime CreationDateUTC { set; get; }

        public int? UserId { private set; get; }
        public User User { private set; get; }

        public int? SessionId { private set; get; }
        public Session Session { private set; get; }

        void Init() { }
        Connection() { Init(); }
        public Connection(string token)
        {
            Token = token;
            CreationDateUTC = DateTime.UtcNow;

            Init();
        }

        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Token)) { validator.Add("Token", "Token es requerido."); }
            //if (UserId <= 0) { validator.Error("Usuario no encontrado."); }
        }        
        public void SetSessionWithUser(Session session)
        {
            if (session == null) throw new ArgumentNullException("session");
            if (session.User == null) throw new ArgumentNullException("session.User");

            Session = session;
            SessionId = session.SessionId;

            User = session.User;
            UserId = session.UserId;
        }
    }
}