﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain
{
    public class Supervisor : IActor
    {
        Supervisor()
        {
        }
        public Supervisor(User user, Zone zone)
        {
            User = user ?? throw new DomainException();
            UserId = user.UserId;

            Zone = zone ?? throw new DomainException();
            ZoneId = zone.ZoneId;

            CreationDate = TimeZoner.Now;

            Zone.Supervisors.Add(this);
        }



        public int SupervisorId { private set; get; }

        public DateTime CreationDate { private set; get; }

        public int UserId { private set; get; }
        public User User { private set; get; }

        public int ZoneId { private set; get; }
        public Zone Zone { private set; get; }


        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (UserId <= 0) validator.Add(string.Empty, "Usuario no encontrado.");
            if (ZoneId <= 0) validator.Add(string.Empty, "Zona no encontrada.");
        }
        public Expression<Func<Admin, bool>> Admins()
        {
            throw new NotImplementedException();
        }
        public Expression<Func<Lot, bool>> Lots()
        {
            return lot => lot.ZoneId == ZoneId;
        }
        public Expression<Func<Street, bool>> Streets()
        {
            return street => street.ZoneId == ZoneId;
        }
        public Expression<Func<Block, bool>> Blocks()
        {
            return block => block.ZoneId == ZoneId;
        }
        public Expression<Func<Model, bool>> Models()
        {
            return model => model.ZoneId == ZoneId;
        }
        public Expression<Func<Zone, bool>> Zones()
        {
            return zone => zone.ZoneId == ZoneId;
        }
        public Expression<Func<Institution, bool>> Institutions()
        {
            return institution => institution.ZoneId == ZoneId;
        }
        public Expression<Func<Map, bool>> Maps()
        {
            return map => map.MapId == Zone.MapId;
        }
        public Expression<Func<MapImage, bool>> MapImages()
        {
            return mapImage => mapImage.MapId == Zone.MapId;
        }
        public Expression<Func<Settlement, bool>> Settlements()
        {
            return settlement => settlement.SettlementId == Zone.SettlementId;
        }
        public Expression<Func<Report, bool>> Reports()
        {
            return report =>
                //report.UserId == this.UserId &&
                report.Lot.ZoneId == ZoneId;
        }
        public Expression<Func<ReportMembership, bool>> ReportMemberships()
        {
            return reportMembership => reportMembership.Report.Lot.ZoneId == ZoneId;
        }
        public Expression<Func<Package, bool>> Packages()
        {
            return package => package.ZoneId == ZoneId;
        }
    }
}
