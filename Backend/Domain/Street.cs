﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Street
    {
        public int StreetId { private set; get; }

        public string Name { private set; get; }

        public int SettlementId { private set; get; }
        public Settlement Settlement { private set; get; }

        public int? ZoneId { private set; get; }
        public Zone Zone { private set; get; }

        public List<Lot> Lots { private set; get; }

        void Init()
        {
            Lots = new List<Lot>();
        }
        Street() { Init(); }
        public Street(Settlement settlement, Zone zone, string name)
        {
            Settlement = settlement ?? throw new DomainException();
            SettlementId = settlement.SettlementId;

            Zone = zone ?? throw new DomainException();
            ZoneId = zone.ZoneId;

            Name = name;

            Init();

            Zone.Streets.Add(this);
        }

        public Dictionary<string, string> Validate()
        {
            var d = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Name)) d.Add("Name", "Nombre es requerido.");

            return d;
        }
        public void Update(string name)
        {
            Name = name;
        }
        public StreetContext.Output ToOutput()
        {
            return new StreetContext.Output
            {
                StreetId = StreetId,

                Name = Name,
                ZoneName = Zone?.Name,

                SettlementId = SettlementId,
                ZoneId = ZoneId
            };
        }
    }
}