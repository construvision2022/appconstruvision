﻿using Domain.PeriodContext;
using System;
using System.Collections.Generic;

namespace Domain
{
    public class ZonePeriod
    {
        ZonePeriod() { }
        public ZonePeriod(Zone zone, Period period, double startingPercentage)
        {
            Zone = zone ?? throw new DomainException();
            ZoneId = zone.ZoneId;

            Period = period ?? throw new DomainException(); ;
            PeriodId = period.PeriodId;

            StartingPercentage = startingPercentage;
            EndingPercentage = Zone.Percentage;
            CreationDate = TimeZoner.Now;

            Zone.ZonePeriods.Add(this);
        }



        public int ZonePeriodId { private set; get; }

        public double StartingPercentage { private set; get; }
        public double EndingPercentage { private set; get; }
        public DateTime CreationDate { private set; get; }

        public int ZoneId { private set; get; }
        public Zone Zone { private set; get; }

        public int PeriodId { private set; get; }
        public Period Period { private set; get; }


        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (StartingPercentage > EndingPercentage)
            {
                validator.Add(string.Empty, "Porcentaje inicial no puede ser mayor a porcentaje final.");
            }
        }
        public double GetStartingPercentage(int decimals = 1)
        {
            return Math.Round(StartingPercentage, decimals);
        }
        public double GetEndingPercentage(int decimals = 1)
        {
            return Math.Round(EndingPercentage, decimals);
        }
        public void SetStartingPercentage(double startingPercentage)
        {
            StartingPercentage = startingPercentage;
        }
        public void SetEndingPercentage(double percentage)
        {
            EndingPercentage = percentage;
        }
        public void ResetData()
        {
            StartingPercentage = 0;
            EndingPercentage = 0;
        }

        public HistoryOutput ToHistoryOutput()
        {
            return new HistoryOutput
            {
                PeriodId = PeriodId,

                StartingPercentage = GetStartingPercentage(),
                EndingPercentage = GetEndingPercentage(),

                StartDate = Period.StartDate,
                EndDate = Period.EndDate,
            };
        }

        public override string ToString()
        {
            var output = $"{ZonePeriodId} | {StartingPercentage}% | {EndingPercentage}%";

            if (Period != null)
                output += $" | {Period}";

            if (Zone != null)
                output += $" | {Zone.Name}";

            return output;
        }
    }
}