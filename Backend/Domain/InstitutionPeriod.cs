﻿using Domain.PeriodContext;
using System;
using System.Collections.Generic;

namespace Domain
{
    public class InstitutionPeriod
    {
        InstitutionPeriod() { }
        public InstitutionPeriod(Institution institution, Period period, double startingPerentage)
        {
            Institution = institution ?? throw new DomainException();
            InstitutionId = institution.InstitutionId;

            Period = period ?? throw new DomainException();
            PeriodId = period.PeriodId;

            StartingPercentage = startingPerentage;
            EndingPercentage = institution.Percentage;
            CreationDate = TimeZoner.Now;

            Institution.InstitutionPeriods.Add(this);
        }



        public int InstitutionPeriodId { private set; get; }

        public double StartingPercentage { private set; get; }
        public double EndingPercentage { private set; get; }
        public DateTime CreationDate { private set; get; }

        public Institution Institution { private set; get; }
        public int InstitutionId { private set; get; }

        public int PeriodId { private set; get; }
        public Period Period { private set; get; }


        public void Validate()
        {
            var d = new Dictionary<string, string>();
            if (StartingPercentage > EndingPercentage)
            {
                d.Add(string.Empty, "Porcentaje inicial no puede ser mayor a porcentaje final.");
            }
        }
        public double GetStartingPercentage(int decimals = 1)
        {
            return Math.Round(StartingPercentage, decimals);
        }
        public double GetEndingPercentage(int decimals = 1)
        {
            return Math.Round(EndingPercentage, decimals);
        }
        public void SetStartingPercentage(double startingPercentage)
        {
            StartingPercentage = startingPercentage;
        }
        public void SetEndingPercentage(double endingPercentage)
        {
            EndingPercentage = endingPercentage;
        }
        internal void ResetData()
        {
            StartingPercentage = 0.0;
            EndingPercentage = 0.0;
        }

        public HistoryOutput ToHistoryOutput()
        {
            return new HistoryOutput
            {
                PeriodId = PeriodId,
                StartingPercentage = GetStartingPercentage(),
                EndingPercentage = GetEndingPercentage(),
                StartDate = Period.StartDate,
                EndDate = Period.EndDate,
            };
        }

        public override string ToString()
        {
            var output = $"{InstitutionPeriodId} | {StartingPercentage}% | {EndingPercentage}%";

            if (Period != null)
                output += $" | {Period}";

            if (Institution != null)
                output += $" | {Institution.Name}";

            return output;
        }
    }
}