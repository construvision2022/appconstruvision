﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Session
    {
        public int SessionId { private set; get; }

        public string IP { private set; get; }
        public string UserAgent { private set; get; }
        public string Token { private set; get; }
        public DateTime CreationDate { private set; get; }
        public DateTime LastActivity { private set; get; }
        public bool IsExpired { private set; get; }

        public int UserId { set; get; }
        public User User { set; get; }

        Session() { }
        public Session(User user, string token, string ip, string userAgent, DateTime now)
        {
            User = user ?? throw new DomainException();
            UserId = user.UserId;

            Token = token;
            IP = ip;
            UserAgent = userAgent;
            LastActivity = now;
            CreationDate = now;
        }

        public Dictionary<string, string> Validate()
        {
            var validator = new Dictionary<string, string>();

            if (UserId <= 0) validator.Add(string.Empty, "Usuario no encontrado.");

            if (string.IsNullOrWhiteSpace(Token)) validator.Add(string.Empty, "Token inválido.");
            if (string.IsNullOrWhiteSpace(IP)) validator.Add("IP", "IP invalida.");
            if (string.IsNullOrWhiteSpace(UserAgent)) validator.Add("UserAgent", "UserAgent inválido.");

            return validator;
        }
        public void Delete() { throw new NotImplementedException(); }
        public void Expire()
        {
            IsExpired = true;
        }
        public void UpdateLastActivity(DateTime now)
        {
            LastActivity = now;
        }        
    }
}