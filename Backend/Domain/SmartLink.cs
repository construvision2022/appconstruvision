﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class SmartLink
    {
        public int SmartLinkId { private set; get; }

        public string Code { private set; get; }
        public DateTime CreationDate { private set; get; }
        public DateTime? ActivationDate { private set; get; }
        public DateTime ExpirationDate { private set; get; }
        public SmartLinkTypes SmartLinkType { private set; get; }

        public int UserId { private set; get; }
        public User User { private set; get; }

        SmartLink() { }
        public SmartLink(User user, string code, DateTime expirationDate, SmartLinkTypes type)
        {
            User = user ?? throw new DomainException();
            UserId = user.UserId;

            Code = code;
            ExpirationDate = expirationDate;
            SmartLinkType = type;
            CreationDate = TimeZoner.Now;
        }

        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Code))
                validator.Add("Code", "Código inválido.");

            if (ExpirationDate <= CreationDate)
                validator.Add("ExpirationDate", "Fecha de expiración inválida.");
        }
        public void Activate()
        {
            ActivationDate = TimeZoner.Now;
        }
    }
}