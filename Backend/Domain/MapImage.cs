﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class MapImage
    {
        public int MapImageId { private set; get; }
        public string Path { private set; get; }
        public MapContext.Image.ImageTypes Type { private set; get; }

        public int? LotId { private set; get; }
        public Lot Lot { private set; get; }

        public Map Map { private set; get; }
        public int MapId { private set; get; }

        MapImage() { }
        public MapImage(Map map, string path)
        {
            Map = map ?? throw new DomainException();
            MapId = map.MapId;

            Path = path;
            Type = MapContext.Image.ImageTypes.Path;

            Map.MapImages.Add(this);
        }

        public Dictionary<string, string> Validate()
        {
            //if (this.LotId.HasValue && this.LotId <= 0) validator.Error(String.Empty, "Lote no encontrado.");
            return new Dictionary<string, string>();
        }
        public string GetMapPath()
        {
            var path = (Path).Trim();
            string[] arrCords = (Path).Split(' ');

            string tempPath = "M";

            for (int i = 0; i < arrCords.Length; i++)
            {
                if (arrCords[i] != " ")
                {
                    if (i > 0)
                    {
                        tempPath += "L";
                    }
                    tempPath += arrCords[i].Replace(',', ' ');

                }
            }
            return tempPath;
        }
        public void AssignLot(Lot lot)
        {
            Lot = lot ?? throw new DomainException();
            LotId = lot.LotId;

            Type = MapContext.Image.ImageTypes.Lot;

            Lot.MapImages.Add(this);
        }
        public void RemoveLot()
        {
            if (Lot == null) throw new DomainException();

            Lot.MapImages.Remove(this);

            LotId = null;
            Lot = null;

            Type = MapContext.Image.ImageTypes.Path;
        }
        public void SetType(MapContext.Image.ImageTypes type)
        {
            Type = type;
        }
    }
}
