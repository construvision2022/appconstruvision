﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.SettlementContext;

namespace Domain
{
    public class Package
    {
        public int PackageId { private set; get; }
        public DateTime CreationDate { private set; get; }
        public string Name { private set; get; }
        public double CurrentPeriodInitialPercentage { private set; get; }
        public double Percentage { private set; get; }
        public double MaxPercentage { private set; get; }


        public Zone Zone { private set; get; }
        public int ZoneId { private set; get; }

        public Institution Institution { private set; get; }
        public int InstitutionId { private set; get; }

        public List<PackagePeriod> PackagePeriods { private set; get; }
        public List<PackageMembership> PackageMemberships { private set; get; }


        void Init()
        {
            PackagePeriods = new List<PackagePeriod>();
            PackageMemberships = new List<PackageMembership>();
        }
        Package() { Init(); }
        public Package(Zone zone, Institution institution, string name)
        {
            Zone = zone ?? throw new DomainException();
            ZoneId = zone.ZoneId;

            Institution = institution ?? throw new DomainException();
            InstitutionId = institution.InstitutionId;

            Name = name;
            CreationDate = TimeZoner.Now;

            Init();

            Zone.Packages.Add(this);
            Institution.Packages.Add(this);
        }

        public Dictionary<string, string> Validate()
        {
            return new Dictionary<string, string>();
        }

        public void Update(string name)
        {
            Name = name;
        }
        public void UpdatePercentage(double percentage)
        {
            Percentage += percentage;
        }
        public void SetPercentage(double percentage)
        {
            Percentage = percentage;
        }
        public void UpdateCurrentPeriodInitialPercentage()
        {
            CurrentPeriodInitialPercentage = Percentage;
        }
        public double GetPercentage(int decimals = 1)
        {
            return Math.Round(Percentage, decimals);
        }
        public double GetStartingPercentage(int decimals = 1)
        {
            return Math.Round(CurrentPeriodInitialPercentage, decimals);
        }
        public void ResetData()
        {
            Percentage = 0.0;
            CurrentPeriodInitialPercentage = 0.0;

            foreach (var packagePeriod in PackagePeriods)
                packagePeriod.ResetData();
        }
        public void SetMaxPercentage()
        {
            var lots = PackageMemberships
                .Select(x => x.Lot)
                .ToList();

            if (!lots.Any())
                return;

            var maxPercentage = lots
                .Sum(x => x.Model.MaxPercentage);

            maxPercentage /= lots.Count;

            if (MaxPercentage != maxPercentage)
                MaxPercentage = maxPercentage;
        }
        public void SyncPeriod(ResyncPeriodsOutput result, Period period)
        {
            if (Percentage == MaxPercentage)
                return;

            var lots = PackageMemberships
                .Select(x => x.Lot)
                .ToList();

            if (!lots.Any())
                return;

            var startingPercentage = Percentage;

            Percentage = lots.Sum(x => x.Percentage);
            Percentage /= lots.Count;

            if (Percentage == 0)
                return;

            var packagePeriod = PackagePeriods
                .SingleOrDefault(x => x.Period == period);

            if (packagePeriod == null)
            {
                packagePeriod = new PackagePeriod(this, period, startingPercentage);
                result.PackagePeriods.Add(packagePeriod);
            }
            else
            {
                if (packagePeriod.StartingPercentage != startingPercentage)
                    packagePeriod.SetStartingPercentage(startingPercentage);

                if (packagePeriod.EndingPercentage != Percentage)
                    packagePeriod.SetEndingPercentage(Percentage);
            }
        }
        public PackageContext.Output ToOutput()
        {
            return new PackageContext.Output
            {
                PackageId = PackageId,

                Name = Name,
                Percentage = Percentage,
                StartingPercentage = GetStartingPercentage(),

                InstitutionId = InstitutionId,
                ZoneId = ZoneId,
            };
        }

        public override string ToString()
        {
            var output = $"name: {Name} | current: {Percentage.ToString("F")} | last: {CurrentPeriodInitialPercentage.ToString("F")}";

            if (Zone != null)
                output += $" | {Zone.Name}";

            return output;
        }

    }
}