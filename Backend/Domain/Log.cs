﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Log
    {
        Log() { }
        public Log(string message)
        {
            Message = message;
            CreationDate = TimeZoner.Now;
        }


        public int LogId { private set; get; }

        public string Message { private set; get; }
        public DateTime CreationDate { private set; get; }
    }
}