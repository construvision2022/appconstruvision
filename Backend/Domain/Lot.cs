﻿using Domain.LotContext;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class Lot
    {
        Lot() { Init(); }
        public Lot(
            Settlement settlement,
            Street street,
            Zone zone,
            Model model,
            Block block,
            double? groundArea,
            double? constructionArea,
            string officialNumber,
            string lotNumber,
            Status status = Status.Pending,
            SaleStatus saleStatus = SaleStatus.Available)
        {
            Settlement = settlement ?? throw new DomainException();
            SettlementId = settlement.SettlementId;

            Street = street ?? throw new DomainException();
            StreetId = street.StreetId;

            Zone = zone ?? throw new DomainException();
            ZoneId = zone.ZoneId;

            Model = model ?? throw new DomainException();
            ModelId = model.ModelId;

            Block = block ?? throw new DomainException();
            BlockId = block.BlockId;

            LotNumber = lotNumber;
            OfficialNumber = officialNumber;
            Status = status;
            SaleStatus = saleStatus;
            CreationDate = TimeZoner.Now;

            GroundArea = groundArea ?? Model.GroundArea;
            ConstructionArea = constructionArea ?? Model.Area;

            HasModelArea = GroundArea == Model.GroundArea
                && ConstructionArea == Model.Area;

            Init();

            Settlement.Lots.Add(this);
            Zone.Lots.Add(this);
            Block.Lots.Add(this);
        }

        public int LotId { private set; get; }

        public string LotNumber { private set; get; }
        public string OfficialNumber { private set; get; }
        public double? GroundArea { private set; get; }
        public double? ConstructionArea { private set; get; }
        public double Percentage { private set; get; }
        public bool HasModelArea { private set; get; }
        public Status Status { private set; get; }
        public SaleStatus SaleStatus { private set; get; }
        public bool Dtu { private set; get; }
        public string DtuAnnotations { private set; get; }
        public DateTime CreationDate { private set; get; }
        public DateTime? SaleDate { private set; get; }
        public DateTime? PaymentDate { private set; get; }
        public DateTime? BuildingStartDate { private set; get; }
        public DateTime? BuildingEndDate { private set; get; }
        public DateTime? DtuDate { private set; get; }
        public bool HasCurrentPeriodReport { private set; get; }
        public double CurrentPeriodInitialPercentage { private set; get; }
        public DateTime? LastUpdate { private set; get; }

        public int SettlementId { private set; get; }
        public Settlement Settlement { private set; get; }

        public int ModelId { private set; get; }
        public Model Model { private set; get; }

        public int ZoneId { private set; get; }
        public Zone Zone { private set; get; }

        public int BlockId { private set; get; }
        public Block Block { private set; get; }

        public int StreetId { private set; get; }
        public Street Street { private set; get; }

        public Report Report { private set; get; }
        public int? ReportId { private set; get; }

        public Report LastReport { private set; get; }
        public int? LastReportId { private set; get; }

        public List<Report> Reports { private set; get; }
        public List<PackageMembership> PackageMemberships { private set; get; }
        public List<MapImage> MapImages { private set; get; }

        void Init()
        {
            Reports = new List<Report>();
            PackageMemberships = new List<PackageMembership>();
            MapImages = new List<MapImage>();
        }
        public string GetAddres()
        {
            return Street.Name + " #" + OfficialNumber;
        }
        public double GetPercentage(int decimals = 1)
        {
            return Math.Round(Percentage, decimals);
        }
        public void SetDtu(string annotations = null)
        {
            Dtu = true;
            DtuAnnotations = annotations;
            DtuDate = TimeZoner.Now;
        }
        public void UpdatePercentageWithReport(Report report)
        {
            var tempPercentage = 0.0;
            foreach (var membership in report.ReportMemberships)
            {
                tempPercentage += membership.LotPercentage;
            }

            Percentage = tempPercentage;

            report.SetReportPercentage(Percentage);

            var allBuildingConceptsAt100 = report.ReportMemberships
                .TrueForAll(x => x.Percentage == 100);

            if (Percentage == 0)
            {
                BuildingStartDate = null;
                BuildingEndDate = null;
                Status = Status.Pending;
            }

            if (Percentage > 0 && !allBuildingConceptsAt100)
            {
                if (!BuildingStartDate.HasValue)
                    BuildingStartDate = report.CreationDate;

                Status = Status.InConstruction;
            }

            if (allBuildingConceptsAt100)
            {
                BuildingEndDate = report.CreationDate;

                if (!BuildingStartDate.HasValue)
                    BuildingStartDate = report.CreationDate;

                Status = Status.Finished;
            }

            if (Report != null && Report.Period != report.Period)
            {
                LastReport = Report;
                LastReportId = Report.ReportId;
            }

            Report = report;
            ReportId = report.ReportId;

            HasCurrentPeriodReport = true;
            LastUpdate = report.LastUpdate ?? report.CreationDate;
        }
        public void UpdateStartBuildingDate(DateTime startDate)
        {
            BuildingStartDate = startDate;
        }
        public void UpdateCurrentPeriodInitialPercentage()
        {
            //TODO: is this rounding secure?
            //CurrentPeriodInitialPercentage = Math.Round(Percentage, 1);

            //TODO: check nothing broke
            CurrentPeriodInitialPercentage = Percentage;
        }
        public bool PutSaleStatus(SaleStatus saleStatus, DateTime? date)
        {
            if (saleStatus == SaleStatus)
                return false;

            //switch (saleStatus)
            //{
            //    case SaleStatus.Available:
            //        if (SaleStatus >= SaleStatus.Signed)
            //            return false;
            //        break;

            //    case SaleStatus.Purchased:
            //        if (SaleStatus >= SaleStatus.Signed)
            //            return false;
            //        break;

            //    case SaleStatus.Signed:
            //        if (SaleStatus >= SaleStatus.Paid)
            //            return false;
            //        break;

            //    case SaleStatus.Paid:
            //        if (SaleStatus == SaleStatus.Delivered)
            //            return false;
            //        break;

            //    case SaleStatus.Delivered:
            //        break;
            //}

            SaleStatus = saleStatus;
            return true;
        }
        public void SetFinishedStatus()
        {
            Status = Status.Finished;
        }
        public void UpdatePeriodReportFlag(bool hasCurrentPeriodReport)
        {
            HasCurrentPeriodReport = hasCurrentPeriodReport;
        }
        public void UpdateModel(Model model)
        {
            Model = model ?? throw new DomainException();
            ModelId = model.ModelId;
        }
        public void UpdateStreet(Street street)
        {
            Street?.Lots.Remove(this);

            Street = street ?? throw new DomainException();
            StreetId = Street.StreetId;

            Street.Lots.Add(this);
        }
        public void UpdateOfficialNumber(string officialNumber)
        {
            OfficialNumber = officialNumber;
        }
        public void Update(string officialNumber, string lotNumber, double? groundArea, double? constructionArea)
        {
            LotNumber = lotNumber;
            OfficialNumber = officialNumber;
            GroundArea = groundArea;
            ConstructionArea = constructionArea;

            HasModelArea = GroundArea == Model.GroundArea
                && ConstructionArea == Model.Area;
        }
        public void SoftReset()
        {
            Percentage = 0.0;
            CurrentPeriodInitialPercentage = 0.0;
            HasCurrentPeriodReport = false;

            BuildingStartDate = null;
            BuildingEndDate = null;
            LastUpdate = null;
            Status = Status.Pending;
        }
        public void HardReset()
        {
            SaleStatus = SaleStatus.Available;
            Dtu = false;

            SoftReset();
        }
        public Dictionary<string, string> Validate()
        {
            var validator = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(LotNumber)) validator.Add("Number", "Número es requerido.");

            //TODO: ?????

            return validator;
        }
        public Output ToOutput()
        {
            var output = new Output
            {
                LotId = LotId,
                LotNumber = LotNumber,
                OfficialNumber = OfficialNumber,

                SaleDate = SaleDate,
                PaymentDate = PaymentDate,

                GroundArea = GroundArea ?? 0,
                ConstructionArea = ConstructionArea ?? 0,

                Percentage = GetPercentage(),
                BuildingStartDate = BuildingStartDate,
                BuildingEndDate = BuildingEndDate,

                SettlementId = SettlementId,

                ZoneId = ZoneId,
                ZoneName = Zone?.Name,

                StreetId = StreetId,
                StreetName = Street?.Name,

                BlockId = BlockId,
                BlockName = Block?.Name,

                ModelId = ModelId,
                ModelName = Model?.Name,

                Status = Status,
                SaleStatus = SaleStatus,
                Dtu = Dtu,
                Updated = HasCurrentPeriodReport,
                StartingPercentage = CurrentPeriodInitialPercentage,
                LastUpdate = LastUpdate,
                DtuDate = DtuDate,
            };

            if (Report != null)
                output.LastFrontImage = Report.ImageCode;
            else if (LastReport != null)
                output.LastFrontImage = LastReport.ImageCode;

            if (SaleDate.HasValue)
                output.SaleDate_iso = SaleDate.Value.ToString("d");

            if (PaymentDate.HasValue)
                output.PaymentDate_iso = PaymentDate.Value.ToString("d");

            foreach (var packageMembership in PackageMemberships)
            {
                if (packageMembership.Package == null || packageMembership.Package.Institution == null)
                    continue;

                if (packageMembership.Package.Institution.Name == "RUV")
                {
                    output.RUV = packageMembership.Package.Name;
                    output.RUVId = packageMembership.Package.PackageId;
                }

                if (packageMembership.Package.Institution.Name == "Bancario")
                {
                    output.Bancario = packageMembership.Package.Name;
                    output.BancarioId = packageMembership.Package.PackageId;
                }
            }

            return output;
        }


        public override string ToString()
        {
            return $@"{LotId} | {(Street != null ? GetAddres() : "NULL " + OfficialNumber)} | {Percentage}%";
        }
    }
}