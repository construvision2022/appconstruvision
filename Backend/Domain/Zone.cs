﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.ZoneContext;

namespace Domain
{
    public class Zone
    {
        public int ZoneId { private set; get; }
        public string Name { private set; get; }
        public string Alias { private set; get; }
        public string ImageCode { private set; get; }
        public double Percentage { private set; get; }
        public double CurrentPeriodInitialPercentage { private set; get; }
        public DateTime? BuildingStartDate { private set; get; }
        public double MaxPercentage { private set; get; }

        public int SettlementId { private set; get; }
        public Settlement Settlement { private set; get; }

        public int? MapId { private set; get; }
        public Map Map { private set; get; }

        public List<Block> Blocks { private set; get; }
        public List<Institution> Institutions { private set; get; }
        public List<Lot> Lots { private set; get; }
        public List<Model> Models { private set; get; }
        public List<Package> Packages { private set; get; }
        public List<Seller> Sellers { private set; get; }
        public List<Street> Streets { private set; get; }
        public List<Supervisor> Supervisors { private set; get; }
        public List<ZonePeriod> ZonePeriods { private set; get; }

        void Init()
        {
            Blocks = new List<Block>();
            Institutions = new List<Institution>();
            Lots = new List<Lot>();
            Models = new List<Model>();
            Packages = new List<Package>();
            Sellers = new List<Seller>();
            Streets = new List<Street>();
            Supervisors = new List<Supervisor>();
            ZonePeriods = new List<ZonePeriod>();
        }
        Zone() { Init(); }
        public Zone(Settlement settlement, string name)
        {
            Settlement = settlement ?? throw new DomainException();
            SettlementId = settlement.SettlementId;

            Name = name;
            Alias = name;

            Init();

            TryCreateDefaultInstitutions();

            Settlement.Zones.Add(this);
        }

        public Dictionary<string, string> Validate()
        {
            var d = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Name)) d.Add("Name", "Nombre es requerido.");

            return d;
        }
        public void Update(string name)
        {
            Name = name;
        }
        public void UpdateImage(string imageCode)
        {
            ImageCode = imageCode;
        }
        public void UpdatePercentage(double percentage)
        {
            Percentage += percentage;
        }
        public void SetPercentage(double percentage)
        {
            Percentage = percentage;
        }
        public void UpdateCurrentPeriodInitialPercentage()
        {
            CurrentPeriodInitialPercentage = Percentage;
        }
        public void AssignMap(Map map)
        {
            Map = map ?? throw new DomainException();
            MapId = map.MapId;
        }
        public void SetImage(string imageCode)
        {
            ImageCode = imageCode;
        }
        public double GetPercentage(int decimals = 1)
        {
            return Math.Round(Percentage, decimals);
        }
        public double GetStartingPercentage(int decimals = 1)
        {
            return Math.Round(CurrentPeriodInitialPercentage, decimals);
        }
        public void ResetData()
        {
            Percentage = 0;
            CurrentPeriodInitialPercentage = 0;

            foreach (var zonePeriod in ZonePeriods)
                zonePeriod.ResetData();
        }

        public void TryCreateDefaultInstitutions()
        {
            if (!Institutions.Any(x => x.Name == "RUV"))
                new Institution(this, "RUV", 0);

            if (!Institutions.Any(x => x.Name == "Bancario"))
                new Institution(this, "Bancario", 0);
        }

        public Output ToOutput()
        {
            return new Output
            {
                ZoneId = ZoneId,
                Name = Name,
                Alias = Alias,

                Percentage = GetPercentage(),
                StartingPercentage = GetStartingPercentage(),

                MapId = MapId,
                SettlementId = SettlementId,
            };
        }

        public void SetMaxPercentage()
        {
            if (!Lots.Any())
                return;

            var maxPercentage = Lots
                .Sum(x => x.Model.MaxPercentage);

            maxPercentage /= Lots.Count;

            if (MaxPercentage != maxPercentage)
                MaxPercentage = maxPercentage;
        }
        internal void SyncPeriod(SettlementContext.ResyncPeriodsOutput result, Period period)
        {
            if (Percentage == MaxPercentage)
                return;

            if (!Lots.Any())
                return;

            var startingPercentage = Percentage;

            Percentage = Lots.Sum(x => x.Percentage);
            Percentage /= Lots.Count;

            if (Percentage == 0)
                return;

            var zonePeriod = ZonePeriods
                .SingleOrDefault(x => x.Period == period);

            if (zonePeriod == null)
            {
                zonePeriod = new ZonePeriod(this, period, startingPercentage);
                result.ZonePeriods.Add(zonePeriod);
            }
            else
            {
                if (zonePeriod.StartingPercentage != startingPercentage)
                    zonePeriod.SetStartingPercentage(startingPercentage);

                if (zonePeriod.EndingPercentage != Percentage)
                    zonePeriod.SetEndingPercentage(Percentage);
            }
        }

        public override string ToString()
        {
            return $"{ZoneId} | {Name} | {Percentage}%";
        }

    }
}