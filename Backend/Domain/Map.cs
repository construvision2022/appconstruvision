﻿using System;
using System.Collections.Generic;


namespace Domain
{
    public class Map
    {
        public int MapId { private set; get; }
        public string Name { private set; get; }
        public DateTime CreationDate { private set; get; }
        public int Width { private set; get; }
        public int Height { private set; get; }

        public Settlement Settlement { private set; get; }
        public int? SettlementId { private set; get; }

        public List<Marker> Markers { private set; get; }
        public List<MapImage> MapImages { private set; get; }

        void Init()
        {
            Markers = new List<Marker>();
            MapImages = new List<MapImage>();
        }
        Map() { Init(); }
        public Map(Settlement settlement, string name, int width, int height)
        {
            Settlement = settlement ?? throw new DomainException();
            SettlementId = settlement.SettlementId;

            Name = name.Trim();
            Width = width;
            Height = height;
            CreationDate = TimeZoner.Now;

            Init();

            Settlement.Maps.Add(this);
        }

        public void SetId(int id)
        {
            if (id <= 0) { throw new ArgumentException("ID"); }
            MapId = id;
        }

        public Dictionary<string, string> Validate()
        {
            var validator = new Dictionary<string, string>();

            if (Name == null) validator.Add(string.Empty, "Nombre no encontrado.");
            if (Settlement == null) validator.Add(string.Empty, "Residencial no encontrado.");

            return validator;
        }

        public void Update(string name)
        {
            Name = name.Trim();
        }

        public void Delete() { throw new NotImplementedException(); }
    }
}
