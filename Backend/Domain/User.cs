﻿using System;
using System.Collections.Generic;
using Domain.SessionContext;

namespace Domain
{
    public class User
    {
        User() { }
        public User(Business business, string name, string firstLastName, string secondLastName, string email, string mobilePhone, string localPhone, string comments)
        {
            Business = business ?? throw new DomainException();
            BusinessId = business.BusinessId;

            Tennant = business.Tennant ?? throw new DomainException();
            TennantId = business.TennantId;

            if (!string.IsNullOrWhiteSpace(email)) Email = email.Trim().ToLower();

            Name = name.Trim();
            FirstLastName = firstLastName.Trim();
            SecondLastName = secondLastName.Trim();
            MobilePhone = string.IsNullOrWhiteSpace(mobilePhone) ? string.Empty : mobilePhone.Trim();
            LocalPhone = string.IsNullOrWhiteSpace(localPhone) ? string.Empty : localPhone.Trim();
            Comments = string.IsNullOrWhiteSpace(comments) ? string.Empty : comments.Trim();
            Salt = Cryptography.CreateSalt();
            Password = Cryptography.BCrypt("123456789", Salt);
            CreationDate = TimeZoner.Now;
            GUID = Guid.NewGuid().ToString();

            //Business.Users.Add(this);
        }


        public int UserId { private set; get; }

        public string Name { private set; get; }
        public string FirstLastName { private set; get; }
        public string SecondLastName { private set; get; }
        public string Email { private set; get; }
        public string Password { private set; get; }
        public string Salt { private set; get; }
        public string MobilePhone { private set; get; }
        public string LocalPhone { private set; get; }
        public string Comments { private set; get; }
        public string GUID { private set; get; }
        public string ImageCode { private set; get; }
        public bool IsBadEmail { private set; get; }
        public bool IsApproved { private set; get; }
        public bool IsDeactivated { private set; get; }
        public bool IsLockedOut { private set; get; }

        public DateTime CreationDate { private set; get; }
        public DateTime? AccountValidationDate { private set; get; }
        public DateTime? LastPasswordChange { private set; get; }
        public DateTime? LastLogin { private set; get; }

        public int BusinessId { private set; get; }
        public Business Business { private set; get; }

        public int? TennantId { private set; get; }
        public Tennant Tennant { private set; get; }

        public string FullName { get { return Name + " " + FirstLastName + " " + SecondLastName; } }

        public Dictionary<string, string> Validate()
        {
            var validator = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Name))
                validator.Add("Name", "Nombre es requerido.");

            if (string.IsNullOrWhiteSpace(FirstLastName))
                validator.Add("FirstLastName", "Apellido paterno es requerido.");

            if (string.IsNullOrWhiteSpace(SecondLastName))
                validator.Add("SecondLastName", "Apellido materno es requerido.");

            if (string.IsNullOrWhiteSpace(Email))
                validator.Add("Email", "Email es requerido.");

            try
            {
                var emailAddress = new System.Net.Mail.MailAddress(Email);
                Email = emailAddress.ToString();
            }
            catch
            {
                validator.Add("Email", "Formato de email inválido.");
            }

            return validator;
        }
        public void Update(string name, string firstLastName, string secondLastName, string email, string mobilePhone, string localPhone, string comments)
        {
            Name = name;
            FirstLastName = firstLastName;
            SecondLastName = secondLastName;
            Email = email;


            MobilePhone = string.IsNullOrWhiteSpace(mobilePhone) ? string.Empty : mobilePhone.Trim();
            LocalPhone = string.IsNullOrWhiteSpace(localPhone) ? string.Empty : localPhone.Trim();
            Comments = string.IsNullOrWhiteSpace(comments) ? string.Empty : comments.Trim();
        }
        public void Update(UserContext.PutInput dto)
        {
            Name = dto.Name;
            FirstLastName = dto.FirstLastName;
            SecondLastName = dto.SecondLastName;
            Email = dto.Email;
            MobilePhone = string.IsNullOrWhiteSpace(dto.MobilePhone) ? string.Empty : dto.MobilePhone.Trim();
            LocalPhone = string.IsNullOrWhiteSpace(dto.LocalPhone) ? string.Empty : dto.LocalPhone.Trim();
        }
        public void UpdateLastLoginDate()
        {
            LastLogin = TimeZoner.Now;
        }
        public void UpdateAccountValidationDate()
        {
            AccountValidationDate = TimeZoner.Now;
        }
        public void UpdatePassword(string password)
        {
            Password = password;
        }
        public Dictionary<string, string> ValidatePassword()
        {
            var validator = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Password))
            {
                validator.Add("Password", "Contraseña es requerida.");
                return validator;
            }

            if (Password.Length <= 5)
                validator.Add("Password", "Contraseña no puede tener menos de 6 caractéres.");

            if (IsBannedPassword(Password))
                validator.Add("Password", "Contraseña no puede ser '" + Password + "'.");

            if (Password.Contains(Name) ||
                Password.Contains(FirstLastName) ||
                Password.Contains(SecondLastName) ||
                Password.Contains(Email))
                validator.Add("Password", "Contraseña no puede contener informacion personal.");


            return validator;
        }
        public void HashPassword()
        {
            Salt = Cryptography.CreateSalt();
            Password = Cryptography.BCrypt(Password, Salt);
            LastPasswordChange = TimeZoner.Now;
        }
        public bool IsThisMyPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password)) return false;

            if (password.Length <= 5) return false;

            if (string.IsNullOrWhiteSpace(Password)) return false;
            if (string.IsNullOrWhiteSpace(Salt)) return false;

            return Password == Cryptography.BCrypt(password, Salt);
        }
        public void SetImage(string imageCode)
        {
            ImageCode = imageCode;
        }
        public string GetImage()
        {
            return string.IsNullOrWhiteSpace(ImageCode) ? "user-image.jpg" : ImageCode;
        }
        public void SetBadEmail(bool isBadEmail)
        {
            IsBadEmail = isBadEmail;
        }
        public UserContext.Output ToOutput()
        {
            return new UserContext.Output
            {
                UserId = UserId,

                Email = Email,
                Name = Name,
                FirstLastName = FirstLastName,
                SecondLastName = SecondLastName,
                MobilePhone = MobilePhone,
                LocalPhone = LocalPhone,
            };
        }


        bool IsBannedPassword(string password)
        {
            var list = new List<string>
            {
                "password",
                "password123",
                "password1234",
                "password1235",
                "pass123",
                "pass1234",
                "pass1235",
                "qwerty",
                "asdfgh",
                "zxcvbn",
                "qwerqwer",
                "asdfasdf",
                "zxcvzxcv",
                "contraseña",
                "contraseña123",
                "contraseña1234",
                "contraseña12345",
                "contrase;a",
                "harrypotter",
                "internet",
                "wikipedia",
                "constructora",
                "construccion",
            };

            return list.Contains(password.ToLower());
        }
    }
}