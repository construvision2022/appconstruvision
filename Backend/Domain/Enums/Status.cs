﻿namespace Domain.LotContext
{
    public enum Status
    {
        None = 0,
        Pending = 1,
        InConstruction = 2,
        Finished = 3
    }
}