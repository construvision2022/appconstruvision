﻿namespace Domain
{
    public enum UserRoles
    {
        None = 0,
        Resident = 1,
        Supervisor = 2,
        Admin = 3,
    }
}