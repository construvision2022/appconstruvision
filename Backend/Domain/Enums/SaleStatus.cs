﻿namespace Domain.LotContext
{
    public enum SaleStatus
    {
        None = 0,
        Available = 1,
        Purchased = 2,
        Integrated = 6,
        Signed = 3,
        Paid = 4,
        Delivered = 5,
    }
}
