﻿namespace Domain.ResponsibleContext
{
    public enum Kinds
    {
        None = 0,
        Resident = 1,
        Admin = 2,
        Supervisor = 3,
        SalesAdmin = 4,
        Manager = 5,
        Seller = 6
    }
}