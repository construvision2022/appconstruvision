﻿namespace Domain.SessionContext
{
    public enum SessionTypes
    {
        None = 0,
        User = 1,
        Admin = 2,
        //Resident = 3,
        Supervisor = 4,
        SalesAdmin = 5,
        Seller = 6,
        Manager = 7,
    }
}
