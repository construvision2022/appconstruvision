﻿namespace Domain.EmailContext
{
    public enum Types
    {
        None = 0,
        Events = 1,
        News = 2,
        Surveys = 3,
        AdminMessage = 4,
        PeriodReport = 5
    }
}
