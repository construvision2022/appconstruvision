﻿namespace Domain.NotificationContext
{
    public enum ResidentNotificationTypes
    {
        PaymentOverdue = 0,
        WarrantyExpiration = 1,
    }
}
