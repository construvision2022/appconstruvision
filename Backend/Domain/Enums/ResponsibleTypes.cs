﻿namespace Domain.ResponsibleContext
{
    public enum ResponsibleTypes
    {
        None = 0,
        SettlementContact = 1,
        WarrantiesManager = 2,
        BoardMember = 3,
        Provider = 4,
        BuildingManager = 5,
    }
}