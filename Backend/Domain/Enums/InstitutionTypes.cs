﻿namespace Domain.PackageContext
{
    public enum InstitutionTypes
    {
        None = 0,
        Ruv = 1,
        Bank = 2
    }
}