﻿namespace Domain.MapContext.Image
{
    public enum ImageTypes
    {
        Path = 0,
        Lot = 1,
        EventArea = 2,
    }
}
