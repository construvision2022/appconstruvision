﻿namespace Domain.ResidentContext
{
    public enum ResidentTypes
    {
        Resident = 0,
        Owner = 1,
    }
}
