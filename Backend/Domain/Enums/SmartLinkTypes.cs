﻿namespace Domain
{
    public enum SmartLinkTypes
    {
        AccountValidation = 0,
        PasswordReset = 1,
        PasswordChange = 2,
    }
}