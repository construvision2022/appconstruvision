﻿namespace Domain
{
    public enum LogTypes
    {
        Exception = 0,
        HttpException = 1,
        Service = 2,
        PushNotificationService = 3,
        MandrillApp = 4,
        Emailer = 10,
        ImageUpload = 50,
        CacheSendEmail = 60,
        CacheCreatePayments = 61,
        CacheCheckDebt = 62,
        CacheCheckWarranties = 63,
        CacheCheckEvents = 64,
        CacheSendResidentAlerts = 65,
        CacheSendPeriodNotifications = 66,
        CheckPeriods = 67,
        SetPeriodReports = 68,

        ChronJob = 69,
        ChronJob_Pinger = 70,
        ChronJob_Excel = 71,
        ChronJob_SendEmail = 72,
        Webhook_Email = 73,

        ScriptService = 998,
        Log = 999,
    }
}
