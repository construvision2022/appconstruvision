﻿using System;

namespace Domain.ResidentContext
{
    [Flags]
    public enum ResidentStatus
    {
        JustCreated = 1,
        WithBadEmail = 2,
        WithInvitation = 4,
        WithSessions = 8,
        WithPayments = 16,
        WithClaims = 32,
    }
}
