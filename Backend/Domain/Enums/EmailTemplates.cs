﻿namespace Domain.EmailContext
{
    public enum EmailTemplates
    {
        DownloadApp = 0,
        NewReport = 1,
        NewExcelExport = 2,
        Welcome = 3,
        PeriodExpiring = 4,
        PeriodReport = 5,
        PasswordRecovery = 6,

        SomeEmail = 69,
    }
}
