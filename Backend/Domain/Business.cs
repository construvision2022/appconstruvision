﻿using Domain.BusinessContext;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class Business
    {
        Business() { Init(); }
        public Business(
            Tennant tenant,
            string name,
            string registeredName,
            string description,
            string rfc,
            string phone,
            string webSite,
            DateTime creationDate)
        {
            Tennant = tenant ?? throw new DomainException();
            TennantId = tenant.TennantId;

            Name = name;
            RegisteredName = registeredName;
            Description = description;
            RFC = rfc;
            Phone = phone;
            Website = webSite;

            CreationDate = creationDate;

            Init();

            Tennant.Businesses.Add(this);
        }



        public int BusinessId { private set; get; }

        public string Name { private set; get; }
        public string RegisteredName { private set; get; }
        public string Description { private set; get; }
        public string RFC { private set; get; }
        public string Website { private set; get; }
        public string Phone { private set; get; }
        public string Address { private set; get; }
        public string Banner { private set; get; }
        public string InvitationBanner { private set; get; }
        public string ImageCode { private set; get; }
        public DateTime CreationDate { private set; get; }

        public Tennant Tennant { private set; get; }
        public int? TennantId { private set; get; }

        public List<Manager> Managers { private set; get; }
        public List<Settlement> Settlements { private set; get; }
        public List<ExcelExport> ExcelReports { private set; get; }

        void Init()
        {
            ExcelReports = new List<ExcelExport>();
            Managers = new List<Manager>();
            Settlements = new List<Settlement>();
        }
        public Dictionary<string, string> Validate()
        {
            var d = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Name))
                d.Add("Name", "Nombre es requerido.");

            if (string.IsNullOrWhiteSpace(RegisteredName))
                d.Add("RegisteredName", "Razón social es requerida.");

            if (string.IsNullOrWhiteSpace(RFC))
                d.Add("RFC", "RFC es requerido.");

            if (string.IsNullOrWhiteSpace(Description))
                d.Add("Description", "Descripcion es requerida.");

            if (string.IsNullOrWhiteSpace(Phone))
                d.Add("Phone", "Teléfono es requerido.");

            if (string.IsNullOrWhiteSpace(Website))
                d.Add("Website", "Sitio web es requerido.");

            return d;
        }
        public void UpdateAddress(string address)
        {
            Address = address;
        }
        public Output ToOutput()
        {
            return new Output
            {
                Address = Address,
                Banner = Banner,
                Description = Description,
                Logo = ImageCode,
                Name = Name,
                Phone = Phone,
                RegisteredName = RegisteredName,
                RFC = RFC,
                Website = Website,
            };
        }
        public CreatorContext.Output Create(CreatorContext.PostInput dto, User manager, DateTime now)
        {
            RemoveSpecialChars(dto);

            var random = new Random();

            var output = new CreatorContext.Output();

            for (int ix = 0; ix < dto.Settlements.Count; ix++)
            {
                var settlementString = dto.Settlements[ix];
                if (string.IsNullOrWhiteSpace(settlementString))
                {
                    output.Error = $"Residenciales -> línea #{ix + 1}";
                    return output;
                }

                var settlement = Settlements
                    .SingleOrDefault(x => x.Name.Equals(settlementString, StringComparison.OrdinalIgnoreCase));

                if (settlement == null)
                {
                    settlement = new Settlement(this, manager, settlementString);
                    output.Settlements++;
                }

                var zoneString = dto.Zones[ix];
                if (string.IsNullOrWhiteSpace(zoneString))
                {
                    output.Error = $"Cerradas -> línea #{ix + 1}";
                    return output;
                }

                var zone = settlement.Zones
                    .SingleOrDefault(x => x.Name.Equals(zoneString, StringComparison.OrdinalIgnoreCase));

                if (zone == null)
                {
                    zone = new Zone(settlement, zoneString);
                    output.Zones++;
                }
                else
                {
                    zone.TryCreateDefaultInstitutions();
                }

                var ruvInstitution = zone.Institutions.Single(x => x.Name == "RUV");
                var bankInstitution = zone.Institutions.Single(x => x.Name == "Bancario");

                var blockString = dto.Blocks[ix];
                if (string.IsNullOrWhiteSpace(blockString))
                {
                    output.Error = $"Manzanas -> línea #{ix + 1}";
                    return output;
                }

                var block = zone.Blocks
                    .SingleOrDefault(x => x.Name.Equals(blockString, StringComparison.OrdinalIgnoreCase));

                if (block == null)
                {
                    block = new Block(settlement, zone, blockString, GetRandomColor(random));
                    output.Blocks++;
                }

                var modelString = dto.Models[ix];
                if (string.IsNullOrWhiteSpace(modelString))
                {
                    output.Error = $"Modelos -> línea #{ix + 1}";
                    return output;
                }

                var model = zone.Models
                    .SingleOrDefault(x => x.Name.Equals(modelString, StringComparison.OrdinalIgnoreCase));

                if (model == null)
                {
                    model = new Model(zone, modelString, GetRandomColor(random), 0, 0);
                    model.UpdateDefaultData(true);
                    output.Models++;
                }

                var streetString = dto.Streets[ix];
                if (string.IsNullOrWhiteSpace(streetString))
                {
                    output.Error = $"Calles -> línea #{ix + 1}";
                    return output;
                }

                var street = zone.Streets
                    .SingleOrDefault(x => x.Name.Equals(streetString, StringComparison.OrdinalIgnoreCase));

                if (street == null)
                {
                    street = new Street(settlement, zone, streetString);
                    output.Streets++;
                }

                var lotNumberString = dto.LotNumbers[ix];
                if (string.IsNullOrWhiteSpace(lotNumberString))
                {
                    output.Error = $"Números de lote -> línea #{ix + 1}";
                    return output;
                }

                var officialNumberString = dto.OfficialNumbers[ix];
                if (string.IsNullOrWhiteSpace(officialNumberString))
                {
                    output.Error = $"Números oficiales -> línea #{ix + 1}";
                    return output;
                }

                var lot = zone.Lots
                    .SingleOrDefault(x => x.Street == street
                        && x.Block == block
                        && x.LotNumber == lotNumberString
                        && x.OfficialNumber == officialNumberString);

                if (lot == null)
                {
                    lot = new Lot(settlement,
                        street,
                        zone,
                        model,
                        block,
                        0,
                        0,
                        officialNumberString,
                        lotNumberString,
                        LotContext.Status.Pending,
                        LotContext.SaleStatus.Available);

                    output.Lots++;
                }
                else
                {
                    output.Error = $"Ya existe un lote " +
                        $"{lot.GetAddres()} " +
                        $"[L-{lot.LotNumber} M-{block.Name}] " +
                        $"-> línea #{ix + 1}";

                    return output;
                }

                var ruvString = dto.RUVPackages[ix];
                if (string.IsNullOrWhiteSpace(ruvString))
                {
                    output.Error = $"Paquetes RUV -> línea #{ix + 1}";
                    return output;
                }

                var ruvPackage = zone.Packages
                    .SingleOrDefault(x => x.Institution == ruvInstitution
                        && x.Name.Equals(ruvString, StringComparison.OrdinalIgnoreCase));

                if (ruvPackage == null)
                {
                    ruvPackage = new Package(zone, ruvInstitution, ruvString);
                    output.RUVPackages++;
                }

                new PackageMembership(lot, ruvPackage);

                var bankString = dto.BankPackages[ix];
                if (string.IsNullOrWhiteSpace(bankString))
                {
                    output.Error = $"Paquetes bancarios -> línea #{ix + 1}";
                    return output;
                }

                var bankPackage = zone.Packages
                    .SingleOrDefault(x => x.Institution == bankInstitution
                        && x.Name.Equals(bankString, StringComparison.OrdinalIgnoreCase));

                if (bankPackage == null)
                {
                    bankPackage = new Package(zone, bankInstitution, bankString);
                    output.BankPackages++;
                }

                new PackageMembership(lot, bankPackage);
            }

            return output;
        }
        string GetRandomColor(Random random)
        {
            return string.Format("#{0:X6}", random.Next(0x1000000));
        }
        void RemoveSpecialChars(CreatorContext.PostInput dto)
        {
            string cleanString(string input)
            {
                return input
                    .Replace("\t", string.Empty)
                    .Replace("\n", string.Empty);
            }

            void cleanList(List<string> data)
            {

                for (int i = 0; i < data.Count; i++)
                    data[i] = cleanString(data[i]);
            }

            cleanList(dto.Zones);
            cleanList(dto.Blocks);
            cleanList(dto.Models);
            cleanList(dto.Streets);
            cleanList(dto.LotNumbers);
            cleanList(dto.OfficialNumbers);
            cleanList(dto.RUVPackages);
            cleanList(dto.BankPackages);
        }
    }
}