﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.SettlementContext;

namespace Domain
{
    public class Institution
    {
        Institution() { Init(); }
        public Institution(Zone zone, string name, double percentage)
        {
            Zone = zone ?? throw new DomainException();
            ZoneId = zone.ZoneId;

            Name = name;
            Percentage = percentage;
            CreationDate = TimeZoner.Now;

            Init();

            Zone.Institutions.Add(this);
        }



        public int InstitutionId { private set; get; }

        public string Name { private set; get; }
        public double Percentage { private set; get; }
        public double CurrentPeriodInitialPercentage { private set; get; }
        public DateTime CreationDate { private set; get; }
        public double MaxPercentage { private set; get; }

        public int ZoneId { private set; get; }
        public Zone Zone { private set; get; }

        public List<InstitutionPeriod> InstitutionPeriods { private set; get; }
        public List<Package> Packages { private set; get; }

        void Init()
        {
            InstitutionPeriods = new List<InstitutionPeriod>();
            Packages = new List<Package>();
        }
        public void Update(string name)
        {
            Name = name;
        }
        public void UpdatePercentage(double percentage)
        {
            Percentage += percentage;
        }
        public void SetPercentage(double percentage)
        {
            Percentage = percentage;
        }
        public void UpdateCurrentPeriodInitialPercentage()
        {
            CurrentPeriodInitialPercentage = Percentage;
        }
        public double GetPercentage(int decimals = 1)
        {
            return Math.Round(Percentage, decimals);
        }
        public double GetStartingPercentage(int decimals = 1)
        {
            return Math.Round(CurrentPeriodInitialPercentage, decimals);
        }
        public void ResetData()
        {
            Percentage = 0.0;
            CurrentPeriodInitialPercentage = 0.0;

            foreach (var institutionPeriod in InstitutionPeriods)
                institutionPeriod.ResetData();
        }
        public void SetMaxPercentage()
        {
            var lots = Packages
                .SelectMany(x => x.PackageMemberships)
                .Select(x => x.Lot)
                .ToList();

            if (!lots.Any())
                return;

            var maxPercentage = lots
                .Sum(x => x.Model.MaxPercentage);

            maxPercentage /= lots.Count;

            if (MaxPercentage != maxPercentage)
                MaxPercentage = maxPercentage;
        }
        public void SyncPeriod(ResyncPeriodsOutput result, Period period)
        {
            if (Percentage == MaxPercentage)
                return;

            if (!Packages.Any())
                return;

            var lots = Packages
               .SelectMany(x => x.PackageMemberships)
               .Select(x => x.Lot)
               .ToList();

            if (!lots.Any())
                return;

            var startingPercentage = Percentage;

            Percentage = lots.Sum(x => x.Percentage);
            Percentage /= lots.Count;

            if (Percentage == 0)
                return;

            var institutionPeriod = InstitutionPeriods
                .SingleOrDefault(x => x.Period == period);

            if (institutionPeriod == null)
            {
                institutionPeriod = new InstitutionPeriod(this, period, startingPercentage);
                result.InstitutionPeriods.Add(institutionPeriod);
            }
            else
            {
                if (institutionPeriod.StartingPercentage != startingPercentage)
                    institutionPeriod.SetStartingPercentage(startingPercentage);

                if (institutionPeriod.EndingPercentage != Percentage)
                    institutionPeriod.SetEndingPercentage(Percentage);
            }
        }

        public override string ToString()
        {
            return $@"{InstitutionId} | {Percentage}%";
        }
    }
}