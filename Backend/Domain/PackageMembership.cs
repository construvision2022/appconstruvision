﻿namespace Domain
{
    public class PackageMembership
    {
        public int PackageMembershipId { private set; get; }

        public Lot Lot { private set; get; }
        public int LotId { private set; get; }

        public Package Package { private set; get; }
        public int PackageId { private set; get; }

        PackageMembership() { }
        public PackageMembership(Lot lot, Package package)
        {
            Lot = lot ?? throw new DomainException();
            LotId = lot.LotId;

            Package = package ?? throw new DomainException();
            PackageId = package.PackageId;

            Lot.PackageMemberships.Add(this);
            Package.PackageMemberships.Add(this);
        }
    }
}