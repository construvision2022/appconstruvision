﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Period
    {
        public int PeriodId { private set; get; }

        public DateTime StartDate { private set; get; }
        public DateTime EndDate { private set; get; }
        public DateTime? SendDate { private set; get; }

        public int? TennantId { private set; get; }
        public Tennant Tennant { private set; get; }

        public List<Report> Reports { private set; get; }

        void Init()
        {
            Reports = new List<Report>();
        }
        Period() { Init(); }
        public Period(Tennant tenant, DateTime start, DateTime end)
        {
            Tennant = tenant ?? throw new DomainException();
            TennantId = tenant.TennantId;

            StartDate = start;
            EndDate = end;

            Init();

            Tennant.Periods.Add(this);
        }

        public void SetSendDate(DateTime sendDate)
        {
            SendDate = sendDate;
        }
        public void ResetData()
        {
            SendDate = null;
        }
        public override string ToString()
        {
            return $"{PeriodId} | {StartDate.ToString("dd/MM/yy")} ~ {EndDate.ToString("dd/MM/yy")}";
        }
    }
}