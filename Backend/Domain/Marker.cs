﻿using System;

namespace Domain
{
    public class Marker
    {
        public int MarkerId { private set; get; }

        public double X { private set; get; }
        public double Y { private set; get; }
        public string Label { private set; get; }

        public int MapId { private set; get; }
        public Map Map { private set; get; }

        Marker() { }
        public Marker(Map map, double x, double y, string label)
        {
            Map = map ?? throw new DomainException();

            X = x;
            Y = y;
            Label = label;

            Map.Markers.Add(this);
        }
    }
}
