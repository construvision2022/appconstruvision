﻿using Domain.PeriodContext;
using System;
using System.Collections.Generic;

namespace Domain
{
    public class PackagePeriod
    {
        PackagePeriod() { }
        public PackagePeriod(Package package, Period period, double startingPerentage)
        {
            Package = package ?? throw new DomainException();
            PackageId = package.PackageId;

            Period = period ?? throw new DomainException(); ;
            PeriodId = period.PeriodId;

            StartingPercentage = startingPerentage;
            EndingPercentage = Package.Percentage;

            CreationDate = TimeZoner.Now;

            Package.PackagePeriods.Add(this);
        }



        public int PackagePeriodId { private set; get; }

        public double StartingPercentage { private set; get; }
        public double EndingPercentage { private set; get; }
        public DateTime CreationDate { private set; get; }

        public Package Package { private set; get; }
        public int PackageId { private set; get; }

        public int PeriodId { private set; get; }
        public Period Period { private set; get; }


        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (StartingPercentage > EndingPercentage)
            {
                validator.Add(string.Empty, "Porcentaje inicial no puede ser mayor a porcentaje final.");
            }
        }
        public double GetStartingPercentage(int decimals = 1)
        {
            return Math.Round(StartingPercentage, decimals);
        }
        public double GetEndingPercentage(int decimals = 1)
        {
            return Math.Round(EndingPercentage, decimals);
        }
        public void SetStartingPercentage(double percentage)
        {
            StartingPercentage = percentage;
        }
        public void SetEndingPercentage(double percentage)
        {
            EndingPercentage = percentage;
        }
        public void ResetData()
        {
            StartingPercentage = 0.0;
            EndingPercentage = 0.0;
        }

        public HistoryOutput ToHistoryOutput()
        {
            return new HistoryOutput
            {
                PeriodId = PeriodId,
                StartingPercentage = GetStartingPercentage(),
                EndingPercentage = GetEndingPercentage(),
                StartDate = Period.StartDate,
                EndDate = Period.EndDate,
            };
        }

        public override string ToString()
        {
            var output = $"{PackagePeriodId} | {StartingPercentage}% | {EndingPercentage}%";

            if (Period != null)
                output += $" | {Period}";

            if (Package != null)
                output += $" | {Package.Name}";

            return output;
        }
    }
}