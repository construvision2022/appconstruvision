﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain
{
    public class Manager : IActor
    {
        Manager() { }
        public Manager(User user, Business business)
        {
            User = user ?? throw new DomainException();
            UserId = user.UserId;

            Business = business ?? throw new DomainException();
            BusinessId = business.BusinessId;

            CreationDate = TimeZoner.Now;

            Business.Managers.Add(this);
        }


        public int ManagerId { private set; get; }

        public DateTime CreationDate { private set; get; }

        public int UserId { private set; get; }
        public User User { private set; get; }

        public int BusinessId { private set; get; }
        public Business Business { private set; get; }


        public int GetId()
        {
            return ManagerId;
        }
        public void Validate()
        {
        }

        public Expression<Func<Admin, bool>> Admins()
        {
            return admin => admin.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<Lot, bool>> Lots()
        {
            return lot => lot.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<Street, bool>> Streets()
        {
            return street => street.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<Block, bool>> Blocks()
        {
            return block => block.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<Model, bool>> Models()
        {
            return model => model.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<Zone, bool>> Zones()
        {
            return zone => zone.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<Institution, bool>> Institutions()
        {
            return institution => institution.Zone.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<Package, bool>> Packages()
        {
            return package => package.Zone.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<Map, bool>> Maps()
        {
            return map => map.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<MapImage, bool>> MapImages()
        {
            return mapImage => mapImage.Map.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<Settlement, bool>> Settlements()
        {
            return settlement => settlement.BusinessId == BusinessId;
        }
        public Expression<Func<Report, bool>> Reports()
        {
            return report => report.Lot.Settlement.BusinessId == BusinessId;
        }
        public Expression<Func<ReportMembership, bool>> ReportMemberships()
        {
            return reportMembership => reportMembership.Report.Lot.Settlement.BusinessId == BusinessId;
        }
    }
}