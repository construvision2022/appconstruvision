﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class Tennant
    {
        Tennant() { Init(); }
        public Tennant(string name, DateTime? startDate)
        {
            Name = name;
            StartDate = startDate;

            Init();
        }


        public int TennantId { private set; get; }

        public string Name { private set; get; }
        public string SenderEmail { private set; get; }
        public string EmailSenderCredential { private set; get; }
        public string EmailSenderPassword { private set; get; }
        public string PlatformLink { private set; get; }
        public string ImageFolder { private set; get; }
        public string AndroidLink { private set; get; }
        public string IOSLink { private set; get; }
        public string Logo { private set; get; }
        public string TicketLogo { private set; get; }
        public string Color { private set; get; }
        public string BrokerToken { private set; get; }
        public DateTime? StartDate { private set; get; }

        public List<Business> Businesses { set; get; }
        public List<Period> Periods { set; get; }

        void Init()
        {
            Businesses = new List<Business>();
            Periods = new List<Period>();
        }
        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Name)) validator.Add("Name", "Nombre es requerido.");
        }
        public void Update(string name)
        {
            Name = name;
        }
        public Period GetCurrentPeriod()
        {
            var now = TimeZoner.Now;

            var currentPeriod = Periods
                .SingleOrDefault(x =>
                    x.StartDate <= now
                    && x.EndDate >= now);

            return currentPeriod;
        }
        public Period GetPastPeriod()
        {
            var now = TimeZoner.Now;

            var currentPeriod = Periods
                .SingleOrDefault(x =>
                    x.StartDate <= now
                    && x.EndDate >= now);

            if (currentPeriod == null)
                return null;

            var periods = Periods
                .OrderBy(x => x.StartDate)
                .ToList();

            var ix = periods.IndexOf(currentPeriod);

            if (ix == 0)
                return null;

            return periods[ix - 1];
        }

        public void CreatePeriods(DateTime now)
        {
            if (!StartDate.HasValue)
                throw new DomainException("Tenant has no StartDate");

            var startDate = new DateTime(now.Year, 1, 1);
            var endDate = startDate.AddYears(2);

            var periods = Periods
                .OrderBy(x => x.StartDate)
                .ToList();

            var iDate = StartDate.Value;
            while (iDate < endDate)
            {
                var month = iDate.Month;

                var firstMonthPeriod = periods
                    .SingleOrDefault(x => x.StartDate == iDate);

                if (firstMonthPeriod == null)
                {
                    var periodFirstEndDate = iDate
                        .AddDays(16)
                        .AddMilliseconds(-3);

                    new Period(this, iDate, periodFirstEndDate);
                }

                var periodSecondStartDate = iDate
                    .AddDays(15);

                var periodSecondEndDate = iDate
                    .AddMonths(1)
                    .AddMilliseconds(-3);

                var secondMonthPeriod = periods
                    .SingleOrDefault(x => x.StartDate == periodSecondStartDate);

                if (secondMonthPeriod == null)
                    new Period(this, periodSecondStartDate, periodSecondEndDate);

                iDate = iDate.AddMonths(1);
            }
        }
    }
}