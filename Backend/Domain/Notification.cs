﻿using Domain.NotificationContext;
using System;
using System.Collections.Generic;

namespace Domain
{
    public class Notification
    {
        public int NotificationId { private set; get; }

        public int Id { private set; get; }
        public string Message { private set; get; }
        public NotificationTypes Type { private set; get; }
        public DateTime CreationDate { private set; get; }

        public Settlement Settlement { private set; get; }
        public int SettlementId { private set; get; }

        public List<NotificationMembership> NotificationMemberships { private set; get; }

        void Init()
        {
            NotificationMemberships = new List<NotificationMembership>();
        }
        Notification() { Init(); }
        public Notification(Settlement settlement, string message, int id, NotificationTypes type)
        {
            Settlement = settlement ?? throw new DomainException();
            SettlementId = settlement.SettlementId;

            Message = message;
            Id = id;
            Type = type;
            CreationDate = TimeZoner.Now;

            Init();
        }

        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Message)) validator.Add("Message", "Mensaje es requerido.");
        }
        public string GetTitle()
        {
            switch (Type)
            {
                default: return "Lorem Ipsum";
            }
        }
        public void AddSupervisors(List<Supervisor> supervisors)
        {
            if (supervisors == null) throw new ArgumentNullException("supervisors");
            if (supervisors.Count == 0) throw new InvalidOperationException("supervisors required");

            foreach (var supervisor in supervisors)
            {
                NotificationMemberships.Add(new NotificationMembership(
                    this,
                    supervisor.User,
                    supervisor.SupervisorId,
                    SessionContext.SessionTypes.Supervisor));
            }
        }
        public void AddAdmins(List<Admin> admins)
        {
            if (admins == null) throw new ArgumentNullException("admins");
            if (admins.Count == 0) throw new InvalidOperationException("admins required");

            foreach (var admin in admins)
            {
                NotificationMemberships.Add(new NotificationMembership(
                    this,
                    admin.User,
                    admin.AdminId,
                    SessionContext.SessionTypes.Admin));
            }
        }
    }
}