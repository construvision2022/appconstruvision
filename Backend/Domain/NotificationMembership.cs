﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class NotificationMembership
    {
        NotificationMembership() { }
        public NotificationMembership(Notification notification, User user, int id, SessionContext.SessionTypes type)
        {
            Notification = notification ?? throw new DomainException();
            NotificationId = notification.NotificationId;

            User = user ?? throw new DomainException();
            UserId = user.UserId;

            Id = id;
            SessionType = type;

            Notification.NotificationMemberships.Add(this);
        }



        public int NotificationMembershipId { private set; get; }

        public DateTime? ReadDate { private set; get; }        
        public int Id { private set; get; }
        public SessionContext.SessionTypes SessionType { private set; get; }

        public User User { private set; get; }
        public int UserId { private set; get; }

        public int NotificationId { set; get; }
        public Notification Notification { set; get; }

        public void Validate()
        {
            var validator = new Dictionary<string, string>();
            if (Id <= 0) validator.Add("Id", "Id es requerido.");
            if (SessionType == SessionContext.SessionTypes.None) validator.Add("SessionType", "Típo de sesión inválido.");
        }
        public void MarkAsRead(DateTime now)
        {
            ReadDate = now;
        }
    }
}