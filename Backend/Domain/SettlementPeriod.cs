﻿using Domain.PeriodContext;
using System;
using System.Collections.Generic;

namespace Domain
{
    public class SettlementPeriod
    {
        SettlementPeriod() { }
        public SettlementPeriod(Settlement settlement, Period period, double startingPerentage)
        {
            Settlement = settlement ?? throw new DomainException();
            SettlementId = settlement.SettlementId;

            Period = period ?? throw new DomainException();
            PeriodId = period.PeriodId;

            StartingPercentage = startingPerentage;
            EndingPercentage = settlement.Percentage;
            CreationDate = TimeZoner.Now;

            Settlement.SettlementPeriods.Add(this);
        }



        public int SettlementPeriodId { private set; get; }

        public double StartingPercentage { private set; get; }
        public double EndingPercentage { private set; get; }
        public DateTime CreationDate { private set; get; }

        public int SettlementId { private set; get; }
        public Settlement Settlement { private set; get; }

        public int PeriodId { private set; get; }
        public Period Period { private set; get; }

        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (StartingPercentage > EndingPercentage)
            {
                validator.Add(string.Empty, "Porcentaje inicial no puede ser mayor a porcentaje final.");
            }
        }
        public double GetStartingPercentage(int decimals = 1)
        {
            return Math.Round(StartingPercentage, decimals);
        }
        public double GetEndingPercentage(int decimals = 1)
        {
            return Math.Round(EndingPercentage, decimals);
        }
        public void SetEndingPercentage(double endingPercentage)
        {
            EndingPercentage = endingPercentage;
        }
        public void SetStartingPercentage(double startingPercentage)
        {
            StartingPercentage = startingPercentage;
        }
        public void ResetData()
        {
            StartingPercentage = 0.0;
            EndingPercentage = 0.0;
        }

        public HistoryOutput ToHistoryOutput()
        {
            return new HistoryOutput
            {
                PeriodId = PeriodId,

                StartingPercentage = GetStartingPercentage(),
                EndingPercentage = GetEndingPercentage(),

                StartDate = Period.StartDate,
                EndDate = Period.EndDate,
            };
        }

        public override string ToString()
        {
            var output = $"{SettlementPeriodId} | {StartingPercentage}% | {EndingPercentage}%";

            if (Period != null)
                output += $" | {Period}";

            if (Settlement != null)
                output += $" | {Settlement.Name}";

            return output;
        }
    }
}