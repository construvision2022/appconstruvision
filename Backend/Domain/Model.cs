﻿using Domain.ModelContext;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class Model
    {
        public int ModelId { private set; get; }

        public int Floors { private set; get; }
        public string Name { private set; get; }
        public string Color { private set; get; }
        public double Area { private set; get; }
        public double GroundArea { private set; get; }
        public string File { private set; get; }
        public string ImageCode { private set; get; }
        public bool DefaultData { private set; get; }
        public double MaxPercentage { private set; get; }

        public int SettlementId { private set; get; }
        public Settlement Settlement { private set; get; }

        public int ZoneId { private set; get; }
        public Zone Zone { private set; get; }

        public List<BuildingConcept> BuildingConcepts { private set; get; }

        void Init()
        {
            BuildingConcepts = new List<BuildingConcept>();
        }
        Model() { Init(); }
        public Model(Zone zone, string name, string color, double area, double groundArea, int floors = 1)
        {
            Zone = zone ?? throw new DomainException();
            ZoneId = zone.ZoneId;

            Settlement = zone.Settlement ?? throw new DomainException();
            SettlementId = zone.Settlement.SettlementId;

            Name = name;
            Floors = floors;
            Color = color;
            Area = area;
            GroundArea = groundArea;
            MaxPercentage = 100;

            Init();

            Zone.Models.Add(this);
        }

        public void Update(PutInput dto)
        {
            Name = dto.Name;
            Floors = dto.Floors;
            Area = dto.Area;
            GroundArea = dto.GroundArea;
            Color = dto.Color;
        }
        public void UpdateColor(string color)
        {
            Color = color;
        }
        public void UpdateDefaultData(bool defaultData)
        {
            DefaultData = defaultData;
        }
        public void SetImage(string imageCode)
        {
            ImageCode = imageCode;
        }
        public void SetFile(string file)
        {
            File = file;
        }
        public void SetMaxPercentage()
        {
            var maxPercentage = BuildingConcepts
                .Sum(x => x.Percentage * 100);

            if (MaxPercentage != maxPercentage)
                MaxPercentage = maxPercentage;
        }

        public Dictionary<string, string> Validate()
        {
            var validator = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Name)) validator.Add("Name", "Nombre es requerido.");

            return validator;
        }
        public Output ToOutput()
        {
            return new Output
            {
                ModelId = ModelId,

                Name = Name,
                Floors = Floors,
                Area = Area,
                GroundArea = GroundArea,
                Color = Color,
                DefaultData = DefaultData,

                ImageCode = ImageCode,
                File = File,

                ZoneId = ZoneId,
                SettlementId = SettlementId,

                ZoneName = Zone?.Name,
            };
        }

        public override string ToString()
        {
            return $"{ModelId} | {Name}";
        }
    }
}