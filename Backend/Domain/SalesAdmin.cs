﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain
{
    public class SalesAdmin : IActor
    {
        SalesAdmin()
        {
        }
        public SalesAdmin(User user, Settlement settlement)
        {
            User = user ?? throw new DomainException();
            UserId = user.UserId;

            Settlement = settlement ?? throw new DomainException(); ;
            SettlementId = settlement.SettlementId;

            CreationDate = TimeZoner.Now;

            Settlement.SalesAdmins.Add(this);
        }



        public int SalesAdminId { private set; get; }

        public DateTime CreationDate { private set; get; }
        public bool IsDeleted { private set; get; }

        public int UserId { private set; get; }
        public User User { private set; get; }

        public int SettlementId { private set; get; }
        public Settlement Settlement { private set; get; }


        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (UserId <= 0) validator.Add(string.Empty, "Usuario no encontrado.");
            if (SettlementId <= 0) validator.Add(string.Empty, "Residencial no encontrado.");
        }
        public Expression<Func<Admin, bool>> Admins()
        {
            return admin => admin.SettlementId == SettlementId;
        }
        public Expression<Func<Lot, bool>> Lots()
        {
            return lot => lot.SettlementId == SettlementId;
        }
        public Expression<Func<Street, bool>> Streets()
        {
            return street => street.SettlementId == SettlementId;
        }
        public Expression<Func<Block, bool>> Blocks()
        {
            return block => block.SettlementId == SettlementId;
        }
        public Expression<Func<Model, bool>> Models()
        {
            return model => model.SettlementId == SettlementId;
        }
        public Expression<Func<Zone, bool>> Zones()
        {
            return zone => zone.SettlementId == SettlementId;
        }
        public Expression<Func<Institution, bool>> Institutions()
        {
            return institution => institution.Zone.SettlementId == SettlementId;
        }
        public Expression<Func<Package, bool>> Packages()
        {
            return package => package.Zone.SettlementId == SettlementId;
        }
        public Expression<Func<Map, bool>> Maps()
        {
            return map => map.SettlementId == SettlementId;
        }
        public Expression<Func<MapImage, bool>> MapImages()
        {
            return mapImage => mapImage.Map.SettlementId == SettlementId;
        }
        public Expression<Func<Settlement, bool>> Settlements()
        {
            return settlement => settlement.SettlementId == SettlementId;
        }
        public Expression<Func<Report, bool>> Reports()
        {
            return report => report.Lot.SettlementId == SettlementId;
        }
        public Expression<Func<ReportMembership, bool>> ReportMemberships()
        {
            return reportMembership => reportMembership.Report.Lot.SettlementId == SettlementId;
        }
    }
}
