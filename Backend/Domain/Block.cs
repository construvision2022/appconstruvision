﻿using System;
using System.Collections.Generic;
using Domain.BlockContext;

namespace Domain
{
    public class Block
    {
        Block() { Init(); }
        public Block(Settlement settlement, Zone zone, string name, string color)
        {
            Settlement = settlement ?? throw new DomainException();
            SettlementId = settlement.SettlementId;

            Zone = zone ?? throw new DomainException();
            ZoneId = Zone.ZoneId;

            Name = name;
            Color = color;

            Init();

            Zone.Blocks.Add(this);
        }


        public int BlockId { private set; get; }

        public string Name { private set; get; }
        public string Color { private set; get; }

        public int SettlementId { private set; get; }
        public Settlement Settlement { private set; get; }

        public int ZoneId { private set; get; }
        public Zone Zone { private set; get; }

        public List<Lot> Lots { private set; get; }

        void Init()
        {
            Lots = new List<Lot>();
        }
        public Dictionary<string, string> Validate()
        {
            var validator = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Name)) validator.Add("Name", "Nombre es requerido.");

            return validator;
        }        
        public void Update(string name, string color)
        {
            Name = name;
            Color = color;
        }
        public Output ToOutput()
        {
            return new Output
            {
                BlockId = BlockId,
                Name = Name,
                Color = Color,

                ZoneId = ZoneId,
                ZoneName = Zone?.Name,

                SettlementId = SettlementId,
            };
        }
    }
}