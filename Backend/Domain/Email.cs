﻿using System;
using System.Collections.Generic;
using Domain.EmailContext;

namespace Domain
{
    public class Email
    {
        Email() { }
        public Email(Settlement settlement, string email, string guid, string data, string layoutData, EmailTemplates emailTemplate, DateTime schedule, Types type = Types.None, int id = 0)
        {
            Settlement = settlement ?? throw new ArgumentException("settlement");
            SettlementId = settlement.SettlementId;

            AuxEmail = email;
            GUID = guid;

            EmailTemplate = emailTemplate;
            Type = type;
            Id = id;

            Data = data;
            LayoutData = layoutData;
            Schedule = schedule;

            CreationDate = TimeZoner.Now;
        }


        public int EmailId { private set; get; }

        public string Data { private set; get; }
        public string LayoutData { private set; get; }
        public string AuxEmail { private set; get; }
        public string GUID { private set; get; }
        public string FileNames { private set; get; }

        public EmailTemplates EmailTemplate { set; get; }
        public Types Type { private set; get; }
        public int Id { private set; get; }

        public DateTime Schedule { private set; get; }
        public DateTime? EndDate { private set; get; }
        public DateTime CreationDate { private set; get; }

        public bool IsDeleted { private set; get; }

        public Settlement Settlement { private set; get; }
        public int? SettlementId { private set; get; }

        public User User { private set; get; }
        public int? UserId { private set; get; }

        public void Validate()
        {
            var validator = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Data)) validator.Add("Data", "Serializado de modelo no encontrado.");
            if (string.IsNullOrWhiteSpace(AuxEmail)) validator.Add("AuxEmail", "Email es requerido");
            if (string.IsNullOrWhiteSpace(GUID)) validator.Add("GUID", "GUID es requerido.");
            //throw new NotImplementedException();
        }
        public void End()
        {
            EndDate = TimeZoner.Now;
        }
        public void SetFileNames(List<string> fileNames)
        {
            if (fileNames.Count == 0) return;

            FileNames = string.Empty;

            foreach (var fileName in fileNames)
            {
                FileNames += fileName + " ";
            }

            FileNames = FileNames.Trim();
        }
    }
}