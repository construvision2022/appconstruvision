﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.SettlementContext;

namespace Domain
{
    public class Settlement
    {
        Settlement() { Init(); }
        public Settlement(Business business, User user, string name, string alias, string description, string rfc, string zipCode)
        {
            User = user ?? throw new DomainException();
            UserId = user.UserId;

            Business = business ?? throw new DomainException();
            BusinessId = business.BusinessId;

            Name = name;
            Alias = alias;
            Description = description;
            RFC = rfc;
            ZipCode = zipCode;

            CreationDate = TimeZoner.Now;

            Init();

            Business.Settlements.Add(this);
        }
        public Settlement(Business business, User user, string name)
        {
            User = user ?? throw new DomainException();
            UserId = user.UserId;

            Business = business ?? throw new DomainException();
            BusinessId = business.BusinessId;

            Name = name;
            Alias = name;
            Description = "Descripción";
            RFC = "RFC";
            ZipCode = "83100";

            CreationDate = TimeZoner.Now;

            Init();

            Business.Settlements.Add(this);
        }


        public int SettlementId { private set; get; }

        public string Name { private set; get; }
        public string Alias { private set; get; }
        public string Description { private set; get; }
        public string ZipCode { private set; get; }
        public string RFC { private set; get; }
        public double CurrentPeriodInitialPercentage { private set; get; }
        public double Percentage { private set; get; }
        public double MaxPercentage { private set; get; }

        public DateTime CreationDate { private set; get; }
        public DateTime? BuildingStartDate { private set; get; }
        public string ImageCode { private set; get; }

        public int UserId { private set; get; }
        public User User { private set; get; }

        public int BusinessId { private set; get; }
        public Business Business { private set; get; }

        public List<Admin> Admins { private set; get; }
        public List<Lot> Lots { private set; get; }
        public List<Map> Maps { private set; get; }
        public List<SalesAdmin> SalesAdmins { private set; get; }
        public List<SettlementPeriod> SettlementPeriods { private set; get; }
        public List<Zone> Zones { private set; get; }

        public Dictionary<string, string> Validate()
        {
            var d = new Dictionary<string, string>();

            if (UserId <= 0) d.Add(string.Empty, "Administrador no encontrado.");

            if (string.IsNullOrWhiteSpace(Name))
                d.Add("Name", "Nombre es requerido.");

            if (string.IsNullOrWhiteSpace(RFC))
                d.Add("RFC", "RFC es requerido.");

            if (string.IsNullOrWhiteSpace(ZipCode))
                d.Add("ZipCode", "Apartado postal es requerido.");

            if (Percentage < 0) d.Add("Percentage", "Porcentaje no puede ser menor a 0.");

            return d;
        }
        public void Update(string name)
        {
            Name = name;
            Alias = name;
        }
        public void UpdateImage(string imageCode)
        {
            ImageCode = imageCode;
        }
        public void UpdateCurrentPeriodInitialPercentage()
        {
            CurrentPeriodInitialPercentage = Percentage;
        }
        public double GetPercentage(int decimals = 1)
        {
            return Math.Round(Percentage, decimals);
        }
        public double GetStartingPercentage(int decimals = 1)
        {
            return Math.Round(CurrentPeriodInitialPercentage, decimals);
        }
        public void UpdatePercentage(double percentage)
        {
            //if (percentage < 0) throw new InvalidOperationException("Cannot update the settlement with a lower percentage.");
            Percentage += percentage;
            //if (Percentage >= 99.9999) Percentage = 100;

            if (!BuildingStartDate.HasValue) BuildingStartDate = TimeZoner.Now.Date;
        }

        public ResyncPeriodsOutput Resync(DateTime now)
        {
            var result = new ResyncPeriodsOutput();

            if (Percentage == MaxPercentage)
                return result;

            if (!Lots.Any())
                return result;

            ResetData();

            var periods = Business.Tennant.Periods
                .OrderBy(x => x.StartDate)
                .ToList();

            SetMaxPercentage();

            foreach (var period in periods)
            {
                if (period.EndDate > now)
                    continue;

                foreach (var lot in Lots)
                {
                    if (lot.Percentage == lot.Model.MaxPercentage)
                        continue;

                    var lotReports = period.Reports
                        .Where(x => x.Lot == lot)
                        .ToList();

                    if (!lotReports.Any())
                        continue;

                    foreach (var lotReport in lotReports)
                        lot.UpdatePercentageWithReport(lotReport);
                }

                foreach (var zone in Zones)
                {
                    if (zone.Percentage == zone.MaxPercentage)
                        continue;

                    if (!zone.Lots.Any())
                        continue;

                    zone.SyncPeriod(result, period);

                    if (zone.Percentage == 0)
                        continue;

                    foreach (var package in zone.Packages)
                        package.SyncPeriod(result, period);

                    foreach (var institution in zone.Institutions)
                        institution.SyncPeriod(result, period);
                }

                SyncPeriod(result, period);
            }

            var currentPeriod = Business.Tennant.Periods
                .SingleOrDefault(x =>
                    x.StartDate <= now
                    && x.EndDate >= now);

            if (currentPeriod == null)
                throw new DomainException("current period not found");

            foreach (var zone in Zones)
            {
                zone.UpdateCurrentPeriodInitialPercentage();

                foreach (var lot in zone.Lots)
                {
                    var hasCurrentPeriodReport = lot.Reports
                        .Any(x => x.Period == currentPeriod);

                    lot.UpdatePeriodReportFlag(hasCurrentPeriodReport);

                    lot.UpdateCurrentPeriodInitialPercentage();
                }

                foreach (var package in zone.Packages)
                    package.UpdateCurrentPeriodInitialPercentage();

                foreach (var institutions in zone.Institutions)
                    institutions.UpdateCurrentPeriodInitialPercentage();
            }

            UpdateCurrentPeriodInitialPercentage();

            return result;
        }
        public Output ToOutput()
        {
            return new Output
            {
                SettlementId = SettlementId,

                Description = Description,
                Name = Name,
                Alias = Alias,
                RFC = RFC,
                ZipCode = ZipCode,
                ImageCode = ImageCode,

                Percentage = GetPercentage(),
                StartingPercentage = GetStartingPercentage(),
            };
        }

        internal void SetPercentage(double percentage)
        {
            Percentage = percentage;
        }
        internal void ResetData()
        {
            Percentage = 0.0;
            CurrentPeriodInitialPercentage = 0.0;
            BuildingStartDate = null;

            foreach (var zone in Zones)
            {
                zone.ResetData();

                zone.Lots.ForEach(x => x.SoftReset());

                zone.Packages.ForEach(x => x.ResetData());

                zone.Institutions.ForEach(x => x.ResetData());
            }

            SettlementPeriods.ForEach(x => x.ResetData());
        }
        internal void SetMaxPercentage()
        {
            if (!Lots.Any())
                return;

            var maxPercentage = Lots
                .Sum(x => x.Model.MaxPercentage);

            maxPercentage /= Lots.Count;

            if (MaxPercentage != maxPercentage)
                MaxPercentage = maxPercentage;

            foreach (var zone in Zones)
            {
                if (!zone.Lots.Any())
                    continue;

                zone.SetMaxPercentage();

                foreach (var institution in zone.Institutions)
                {
                    institution.SetMaxPercentage();

                    foreach (var package in institution.Packages)
                    {
                        package.SetMaxPercentage();
                    }
                }
            }
        }
        internal void CreateSummaryPeriods()
        {
            var periods = Business.Tennant.Periods
                .OrderBy(x => x.StartDate)
                .ToList();

            foreach (var period in periods)
            {
                var settlementPeriod = SettlementPeriods
                    .SingleOrDefault(x => x.Period == period);

                if (settlementPeriod == null)
                    new SettlementPeriod(this, period, 0.0);

                foreach (var zone in Zones)
                {
                    var zonePeriod = zone.ZonePeriods
                        .SingleOrDefault(x => x.Period == period);

                    if (zonePeriod == null)
                        new ZonePeriod(zone, period, 0.0);

                    foreach (var package in zone.Packages)
                    {
                        var packagePeriod = package.PackagePeriods
                            .SingleOrDefault(x => x.Period == period);

                        if (packagePeriod == null)
                            new PackagePeriod(package, period, 0.0);
                    }

                    foreach (var institution in zone.Institutions)
                    {
                        var institutionPeriod = institution.InstitutionPeriods
                            .SingleOrDefault(x => x.Period == period);

                        if (institutionPeriod == null)
                            new InstitutionPeriod(institution, period, 0.0);
                    }
                }
            }
        }
        internal void SyncPeriod(ResyncPeriodsOutput result, Period period)
        {
            if (Percentage == MaxPercentage)
                return;

            if (!Lots.Any())
                return;

            var startingPercentage = Percentage;

            Percentage = Lots.Sum(x => x.Percentage);
            Percentage /= Lots.Count;

            if (Percentage == 0)
                return;

            var settlementPeriod = SettlementPeriods
                .SingleOrDefault(x => x.Period == period);

            if (settlementPeriod == null)
            {
                settlementPeriod = new SettlementPeriod(this, period, startingPercentage);
                result.SettlementPeriods.Add(settlementPeriod);
            }
            else
            {
                if (settlementPeriod.StartingPercentage != startingPercentage)
                    settlementPeriod.SetStartingPercentage(startingPercentage);

                if (settlementPeriod.EndingPercentage != Percentage)
                    settlementPeriod.SetEndingPercentage(Percentage);
            }
        }

        void Init()
        {
            Admins = new List<Admin>();
            Lots = new List<Lot>();
            Maps = new List<Map>();
            SalesAdmins = new List<SalesAdmin>();
            SettlementPeriods = new List<SettlementPeriod>();
            Zones = new List<Zone>();
        }

        public override string ToString()
        {
            return $@"{SettlementId} | {Name} | {Percentage}%";
        }
    }
}
